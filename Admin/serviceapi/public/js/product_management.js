var productSelect;

$(document).ready(function(){

	loadAllProductCategory();
	var filterOption = $('#filterOption').val();
	var keyword = $('#input-search-appointment').val();
	
	loadAllProduct(0,filterOption,keyword);

	$(document).on('click', '.box-content .pagination a',function(event)
      {
        event.preventDefault();
        var filterOption = $('#filterOption').val();
		var keyword = $('#input-search-appointment').val();
        $('.box-content .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];

        loadAllProduct(page,filterOption,keyword);
      });
	
	$("#product-thumb").change(function(){
   		 readURL(this);
	});
	$(document).on('click','#box-content-data .item-table',function(){
		var url = $(this).data('href');
		console.log(url);
		// window.location = url;
	});

	$("#filterOption").change(function(){
		var keyword = $('#input-search-appointment').val();
		loadAllProduct(0,this.value,keyword);
	});
	$('#btn-search-appoinment').click(function(){
		var keyword = $('#input-search-appointment').val();
		var filterOption = $('#filterOption').val();
		loadAllProduct(0,filterOption,keyword);
	});

	// $(document).on('click','.box-content .item-table',function(event){
 //          event.preventDefault();
 //          var url = $(this).data('href');
 //          window.location = $(this).data('href');
 //      });

	$(document).on('click','.item-table .btn-edit',function(event){
			 event.preventDefault();
			 event.stopPropagation();
			 var row = $(this).closest("tr");
			 $tds = row.find("td");
			 console.log(row.data('catid'));
			 if($tds.length >4){
			 	var title = $tds.eq(1).text();
			 	var currentPrice = $tds.eq(4).text();
			 	var originPrice = $tds.eq(3).text();
			 	$('#img-thumb').attr('src',row.data('thumb'));
			 	$('#img-thumb').data('href',row.data('thumb'));
			 	$('#product-name').val(title);
			 	$('#current-price').val(currentPrice);
			 	$('#original-price').val(originPrice);
			 	$('#btnSave').data('idproduct', $(this).data('id'));
			 	$('#modalProduct').modal('show');
			 	$('#category-product').val(row.data('catid'));
			 	$('#product-thumb').val('');
				$('#btnDelete').hide();

			 }
			 
	});

	$(document).on('click','.item-table .btn-remove',function(event){
			 event.preventDefault();
			 event.stopPropagation();
			 var row = $(this).closest("tr");
			showDeleteDialog(row.data('id'))
			 
	});
	$('#btnSave').click(function(){
		var nameProduct = $('#product-name').val();
		var currentPrice = $('#current-price').val();
		var categoryId = $('#category-product').val();
		var originPrice = $('#original-price').val();
		if(nameProduct.trim()==''||currentPrice.trim()==''){
				Swal.fire({
                  title: 'Thông báo',
                  text: 'Bạn chưa điền đầy đủ thông tin!',
                  icon: 'error',
                  confirmButtonText: 'OK'
             });
			
		}else if(Number(currentPrice)=='NaN'||Number(originPrice)=='NaN'||(currentPrice % 1000)!=0||(originPrice % 1000)!=0){
			Swal.fire({
                  title: 'Thông báo',
                  text: 'Giá bạn nhập không hợp lệ!',
                  icon: 'error',
                  confirmButtonText: 'OK'
             });
		}else if(!categoryId){
			showError('Thông báo','Bạn chưa chọn loại sản phẩm');
		}else{
			$('#modalProduct').modal('hide');
			showUpdateDialog($(this).data('idproduct'),categoryId,nameProduct,originPrice,currentPrice);
		}
	});

	$('#btnDelete').click(function(){
		$("#product-thumb").val('');
		var href = $('#img-thumb').data('href');
    	$('#img-thumb').attr('src',href);
    	$('#btnDelete').hide();

	});

	$('#btnImport').click(function(){
			var url = $(this).data('href');
           window.location = $(this).data('href');
	});

	$('#btnAddProduct').click(function(){
			var url = $(this).data('href');
           window.location = $(this).data('href');
	});
	$('#btnCreateOrder').click(function(){
			var url = $(this).data('href');
           window.location = $(this).data('href');
	});
	


});

function readURL(input){
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	console.log("load");
            $('#img-thumb').attr('src', e.target.result);
            $('#btnDelete').show();
        }
        reader.readAsDataURL(input.files[0]);
    	
	}else{
    	var href = $('#img-thumb').data('href');
    	$('#img-thumb').attr('src',href);
    	$('#btnDelete').hide();
    }
}

function showUpdateDialog(product_id,categoryId,title,originPrice,current_price){
	Swal.fire({
			title:'Cập nhật sản phẩm',
			text:'Cập nhật thành công',
			onBeforeOpen:()=>{
				Swal.showLoading()
			},
			onOpen :()=>{

				var form_data = new FormData();
				form_data.append('image', $("#product-thumb")[0].files[0]);
				form_data.append('product_id', product_id);
				form_data.append('cat_id',categoryId);
				form_data.append('title', title);
				form_data.append('originPrice', originPrice);
				form_data.append('current_price', current_price);
				$.ajax({
					headers: {
			            'Accept':'application/json',
			            'Authorization':'Bearer ' + token,
			        },
					type:'POST',
					url:BASE_URL+'api/v1/salon/updateProduct',
					processData: false,
		    		contentType: false,
			        cache : false,
					data:form_data,
					success:function(data){
						Swal.hideLoading();

						if(data.success){
							$('.nav-tabs').find('li').removeClass('active');
							$('.nav-tabs').find('li').eq(0).addClass('active');
							$('#browseTab').removeClass('active');
							$('#uploadTab').addClass('active');
							$('#product-thumb').val('');
							$('#btnDelete').hide();
							updateDataTable(product_id,categoryId,title,originPrice,current_price,data.thumbUrl);
							Swal.fire({
			                  title: 'Thông báo',
			                  text: 'Chỉnh sửa sản phẩm thành công',
			                  icon: 'success',
			                  confirmButtonText: 'OK'
				             });
			         }else{
			         	Swal.fire({
		                  title: 'Thông báo',
		                  text: data.message,
		                  icon: 'error',
		                  confirmButtonText: 'OK'
			             });
			         }

					},
					 error:function(jqXHR, ajaxOptions, thrownError){
					 	Swal.hideLoading();
			             handleUnAuthenticated(jqXHR);
			       }

		 	});
			}
		});
}

function showDeleteDialog(product_id){
	Swal.fire({
			title:'Xóa sản phẩm',
			text:'Xóa sản phẩm thành công',
			onBeforeOpen:()=>{
				Swal.showLoading()
			},
			onOpen :()=>{

				$.ajax({
					headers: {
			            'Accept':'application/json',
			            'Authorization':'Bearer ' + token,
			        },
					type:'PATCH',
					url:BASE_URL+'api/v1/salon/deleteProduct',

					data:{
						product_id:product_id,
					},
					success:function(data){
						Swal.hideLoading();

						if(data.success){
							$('.nav-tabs').find('li').removeClass('active');
							$('.nav-tabs').find('li').eq(0).addClass('active');
							$('#browseTab').removeClass('active');
							$('#uploadTab').addClass('active');
							$('#product-thumb').val('');
							$('#btnDelete').hide();
							deleteRowTable(product_id);
			         }else{
			         	Swal.fire({
		                  title: 'Thông báo',
		                  text: data.message,
		                  icon: 'error',
		                  confirmButtonText: 'OK'
			             });
			         }

					},
					 error:function(jqXHR, ajaxOptions, thrownError){
					 	Swal.hideLoading();
			             handleUnAuthenticated(jqXHR);
			       }

		 	});
			}
		});
}

function updateDataTable(product_id,categoryId,title,originPrice,current_price,thumbUrl){
	var rows = $('tbody').find('tr');
	var categoryTitle = $(`#category-product option[value=${categoryId}]`).text();

	$.each(rows, function() {               
    console.log($(this).data('id'));
    if($(this).data('id')==product_id){
    	$(this).find("td").eq(1).text(title);
    	$(this).find("td").eq(2).text(categoryTitle);
    	$(this).find("td").eq(3).text(originPrice);
    	$(this).find("td").eq(4).text(current_price);
    	if(thumbUrl){
    		$(this).data('thumb',thumbUrl);
    	}
    }
});

}
function deleteRowTable(product_id){
	var filterOption = $('#filterOption').val();
	var keyword = $('#input-search-appointment').val();
	
	loadAllProduct(0,filterOption,keyword);
}

function loadAllProduct(page,filterOption,keyword){
	$('#progress-loading').show();
	$.ajax({

			headers: {
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/salon/getAllProductSalon',
			data:{
				salon_id:salon_id,
				cat_id:filterOption,
				keyword:keyword,
				page:page
			},
			success:function(data){
				$('#progress-loading').hide();
				$('#box-content-data').empty().html(data);
			},
			 error:function(jqXHR, ajaxOptions, thrownError){
			 	$('#progress-loading').hide();
	             handleUnAuthenticated(jqXHR);
	       }

 	});

}


function loadAllProductCategory(){
	$.ajax({
			headers: {
	            
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/salon/getAllProductCategorySalon',
			datatype:'html',
			data:{
				salon_id:salon_id,
			},
			success:function(data){
				var allOption = '<option value=\"-1\">Tất cả</option>\n';
				$('#filterOption').empty();
				$('#filterOption').append(allOption);
				$('#filterOption').append(data);
				
				$('#category-product').empty().html(data);


			},
			 error:function(jqXHR, ajaxOptions, thrownError){
	             handleUnAuthenticated(jqXHR);
	       }

 	});

}



