
var user;

$(document).ready(function(){

      var showPass = 0;
        $(document).on('click', '#show-pass',function(event)
        {
          event.preventDefault();
          if(showPass == 0) {
            $(this).next('input').attr('type','text');
            $(this).find('i').removeClass('fa-eye');
            $(this).find('i').addClass('fa-eye-slash');
            showPass = 1;
        }
        else {
            $(this).next('input').attr('type','password');
            $(this).find('i').addClass('fa-eye');
            $(this).find('i').removeClass('fa-eye-slash');
            showPass = 0;
        }
        
    });
    var user_id = $('#content-grid').data('id');
    if(user_id && user_id!=0){
        getStaffDetail(user_id)
    }else{
        $('#view-no-data').show();
        $('#layout-btn').hide();
        $('#content-grid').hide();
    }


   

    $("#user-avatar-input").change(function(){
         readURL(this);
    });

    $('#btnSave').click(function(){
            if(!checkErrorInput()){

                showDialogCreateStaff();
            }
    });
    $('#btnCancel').click(function(){
        var url = $(this).data('href');
        window.location = $(this).data('href');
    });
    $('#user-name').focus(function(){
        $(this).addClass('input-active');
        $('#error-user-name').hide();
    });
    $('#user-email').focus(function(){
        $(this).addClass('input-active');
        $('#error-user-email').hide();
    });
    
    $('#user-phone-number').focus(function(){
        $(this).addClass('input-active');
        $('#error-user-phone-number').hide();
    });

    $('#user-password').focus(function(){
        $(this).addClass('input-active');
        $('#error-user-password').hide();
    });




    $('#user-email').blur(function(){
        $(this).removeClass('input-active');
    });

    $('#user-name').blur(function(){
        $(this).removeClass('input-active');
    });
        
    $('#user-phone-number').blur(function(){
        $(this).removeClass('input-active');
    });
    $('#user-password').blur(function(){
        $(this).removeClass('input-active');    
    });

});

function readURL(input) {
    console.log("read");
    if (input.files && input.files[0]) {
        $('#error-avatar-user').hide();
        var reader = new FileReader();
        console.log("reader");
        reader.onload = function (e) {
            console.log("load");
            $('#avatar-user').attr('src', e.target.result);
            $('#avatar-user').show();
        }

        reader.readAsDataURL(input.files[0]);
    }else{
        $('#avatar-user').attr('src','#');
        $('#avatar-user').hide();
    }
}

function checkErrorInput(){
    var user_name = $('#user-name').val();
    var email = $('#user-email').val();
    var phone_number = $('#user-phone-number').val();
    var password = $('#user-password').val();

    var error = false;
    if(user_name.trim()==''){
        error = true;
        $('#error-user-name').show();
    }else{
        $('#error-user-name').hide();
    }
    if(email.trim()==''){
        error = true;
        $('#error-user-email').show();
    }else{
        $('#error-user-email').hide();
    }
     if(password.trim().length > 0 && password.trim().length <6){
        error = true;
        $('#error-user-password').text('Mật khẩu tối thiểu là 6 kí tự');
        $('#error-user-password').show();
    }else{
         $('#error-user-password').hide();
    }
    if(email.trim()==''){
        error = true;
        $('#error-user-email').text('Bạn chưa nhập email');
        $('#error-user-email').show();
    }else if(!checkEmailValid(email)){
          error=true;
        $('#error-user-email').text('Email không hợp lệ');
        $('#error-user-email').show();

    }else{
        $('#error-user-email').hide();
    }
    if(phone_number.trim()==''){
        error = true;
        $('#error-user-phone-number').text('Bạn chưa nhập số điện thoại');
        $('#error-user-phone-number').show();
    }else if(!checkPhoneValid(phone_number.trim())){
         error=true;
        $('#error-user-phone-number').text('Số điện thoại không hợp lệ');
        $('#error-user-phone-number').show();

    }else{
        $('#error-user-phone-number').hide();
    }

 
    return error;
}


function checkEmailValid(email){
    if(email.trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
    }else{
        return true;
    }

}
function checkPhoneValid(phone){
    var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
        if (vnf_regex.test(phone) == false) {
            return false;
        }else{
                return true;
        }

}



function showDialogCreateStaff(){
    Swal.fire({
            title:'Thêm nhân viên',
            onBeforeOpen:()=>{
                Swal.showLoading();
            },
            onOpen :()=>{
                createStaff();
            },
        });
}
function createStaff(){
    var user_name = $('#user-name').val();
    var email = $('#user-email').val();
    var phone_number = $('#user-phone-number').val();
    var password = $('#user-password').val();
    var form_data = new FormData();
    form_data.append('image', $("#user-avatar-input")[0].files[0]);
    form_data.append('user_id', $('#content-grid').data('id'));
    form_data.append('user_name',user_name);

    form_data.append('email', email);
    form_data.append('phone_number', phone_number);

    if(password.trim()!=''){
        form_data.append('password', password);
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
            headers: {
                'Accept':'application/json',
                'Authorization':'Bearer ' + token,
            },
            type:'POST',
            processData: false,
            contentType: false,
            cache : false,
            url: BASE_URL+'api/v1/user/updateUser',
            data:form_data,
            success:function(data){
                Swal.hideLoading();
                if(data.success){
                    user.email = email;
                    user.phone_number = phone_number;
                    user.name = user_name;
                    Swal.fire({
                          title: 'Thông báo',
                          text: data.message,
                          icon: 'success',
                          confirmButtonText: 'OK'
                    });

                }else{
                    Swal.fire({
                          title: 'Thông báo',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'OK'
                    });
                }

            },
             error:function(jqXHR, ajaxOptions, thrownError){
                Swal.hideLoading();
                handleUnAuthenticated(jqXHR);
                Swal.fire({
                          title: 'Thông báo',
                          text: 'Có lỗi xảy ra, vui lòng thử lại sau ít phút!',
                          icon: 'error',
                          confirmButtonText: 'OK'
                    });
           }

    });
}

function deleteStaff(){

}

function getStaffDetail(user_id){
    $('#view-no-data').hide();
    $('#progress-loading').show();
    $('#content-grid').hide();
    $('#layout-btn').hide();

    $.ajax({
            headers: {
                'Accept':'application/json',
                'Authorization':'Bearer ' + token,
            },
            type:'GET',
            url: BASE_URL+'api/v1/staff/getStaffDetail',
            data:{
                salon_id:salon_id,
                staff_id:user_id
            },
            success:function(data){
                $('#progress-loading').hide();
                if(data.success){
                     user = data.data;
                    if(data){
                        $('#user-name').val(user.name);
                        $('#user-email').val(user.email);
                        if(user.phone_number){
                           $('#user-phone-number').val(user.phone_number);  
                        }
                        if(user.avatar){
                           $('#avatar-user').show();
                            $('#avatar-user').attr('src',user.avatar);
                        }else{
                            $('#avatar-user').hide();
                             $('#avatar-user').attr('src','#');
                        }
                        $('#view-no-data').hide();
                        $('#layout-btn').show();
                        $('#content-grid').show();
                        
                    }else{
                         $('#view-no-data').show();
                        $('#layout-btn').hide();
                        $('#content-grid').hide();
                    }

                }else{
                    $('#view-no-data').show();
                    $('#layout-btn').hide();
                    $('#content-grid').hide();
                }

            },
             error:function(jqXHR, ajaxOptions, thrownError){
                Swal.hideLoading();
                $('#progress-loading').hide();
                handleUnAuthenticated(jqXHR);
                $('#view-no-data').show();
                $('#content-grid').hide();
                $('#layout-btn').hide();
           }

    });
   

}

function resetForm(){
    $('#user-name').val('');
    $('#user-email').val('');
    $('#user-phone-number').val('');
    $("#user-password").val('');
    $('#avatar-user').hide();
    $('#avatar-user').attr('src','#');
}
