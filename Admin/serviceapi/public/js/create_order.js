var rowTable;


$(document).ready(function(){
	
	loadAllProduct();
	loadAllProductCategory();
	var currentProducts = [];
	$('#product-box-select #product-select').selectpicker();
	$("#product-thumb").change(function(){
   		 readURL(this);
	});

	$(document).on('change','#table-order-info .selectpicker',function(){
			var product = $(this).find(':selected').data('product');
			if(product){
				var  leftQuantity = getLeftQuantity(product.id);
				var price = product.current_price;
				// var quantity = product.quantity;
				var cells = $(this).parents('tr').find('td');
				cells.eq(2).text(price);
				cells.eq(3).find('input').val(Math.min(1,leftQuantity));
				cells.eq(4).text(Math.min(1,leftQuantity)*price);
				updateTotalPrice();
			}else{
				var cells = $(this).parent().parent().parent().parent().find('td');
				cells.eq(2).text(0);
				cells.eq(3).find('input').val(0);
				cells.eq(4).text(0);
				updateTotalPrice();
			}
	});
	$('#btnSave').click(function(){
			var productSelected = getOrderProduct();
			if(productSelected.size==0){
				showError('Lỗi','Bạn chưa chọn sản phẩm nào cho đơn hàng');
			}else{
				if(!checkErrorInput()){
					var errorMsg = getErrorSelectedProduct(productSelected);
					if(errorMsg==''){
						var customerName = $('#order-name-customer').val();
						var customerPhoneNumber = $('#order-phone-customer').val();
						var customerAddress =  $('#order-address').val();

						createOrder(productSelected,customerName,customerPhoneNumber,customerAddress);
					}else{
						showError('Lỗi',errorMsg);
					}
				}
			}

	});
	$(document).on('click','#table-order-info .btn-remove',function(){
				$(this).parent().parent().remove();
				updateIndexRowTable();
				updateTotalPrice();
		});
	$(document).on('click','#table-order-info .btn-increase',function(){
				var product = $(this).parents('tr').find('select').find(':selected').data('product');
				var previous = $(this).parent().find('input').val();
				if(product){
					var leftQuantity = getLeftQuantity(product.id);
					if(parseInt(previous)&& leftQuantity>=1){
						var newQuantity = parseInt(previous)+1;
						$(this).parent().find('input').val(newQuantity);
						var newTotalPrice = newQuantity * product.current_price;
						$(this).parents('tr').find('.total-price-cell').text(newTotalPrice);
						updateTotalPrice();
					}else{
						showError('Lỗi','Số lượng còn lại của sản phẩm này là '+leftQuantity)
					}
				}else{
					showError('Lỗi','Số lượng tối đa cho sản phẩm này là '+previous)
				}

	});
	$(document).on('click','#table-order-info .btn-decrease',function(){
				var product = $(this).parents('tr').find('select').find(':selected').data('product');
				var previous = $(this).parent().find('input').val();
				if(parseInt(previous)>1){
					var newQuantity = parseInt(previous)-1;
					$(this).parent().find('input').val(newQuantity);
					var newTotalPrice = newQuantity * product.current_price;
					$(this).parents('tr').find('.total-price-cell').text(newTotalPrice);
					updateTotalPrice();
				}
				
				
	});

	$('#btnCancel').click(function(){
		var url = $(this).data('href');
        window.location = $(this).data('href');
	});

	$('#btn-add').click(function(){
		if(typeof rowTable!== 'undefined'){
			var rows = $('#table-order-info > tbody > tr');
				$('#table-order-info > tbody > tr').eq(rows.length-2).before(rowTable);
				$('#table-order-info .selectpicker').selectpicker();
				updateIndexRowTable();
		}
	});

	$('#order-name-customer').focus(function(){
		$(this).addClass('input-active');
		$('#error-name-customer').hide();
	});
	
	$('#order-phone-customer').focus(function(){
		$(this).addClass('input-active');
		$('#error-phone-number').hide();
	});
	$('#order-address').focus(function(){
		$(this).addClass('input-active');
		$('#error-address-customer').hide();
	});

	$('#order-name-customer').blur(function(){
		$(this).removeClass('input-active');
	});

	$('#order-phone-customer').blur(function(){
		$(this).removeClass('input-active');
	});
		
	$('#order-address').blur(function(){
		$(this).removeClass('input-active');
		
	});


});

function readURL(input) {
	console.log("read");
    if (input.files && input.files[0]) {
    	$('#error-thumb-product').hide();
        var reader = new FileReader();
		console.log("reader");
        reader.onload = function (e) {
        	console.log("load");
            $('#thumb-product').attr('src', e.target.result);
            $('#thumb-product').show();
        }

        reader.readAsDataURL(input.files[0]);
    }else{
    	$('#thumb-product').attr('src','#');
    	$('#thumb-product').hide();
    }
}

function checkErrorInput(){
	// var title = $('#product-title').val();
	// var originPrice = $('#origin-price').val();
	var customerName = $('#order-name-customer').val();
	var customerPhoneNumber = $('#order-phone-customer').val();
	var customerAddress =  $('#order-address').val();

	// var description = $('#description-product').val();
	// var catId =  $('#category-product').val();
	// var isFileSelected = $("#product-thumb").val();
	var error = false;

	if(customerName.trim()==''){
		error = true;
		$('#error-name-customer').show();
	}else{
		$('#error-name-customer').hide();
	}


	var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
		if(customerPhoneNumber !==''){
			if (vnf_regex.test(customerPhoneNumber) == false) 
			{
				error = true;
				$('#error-phone-number').show();
				$('#error-phone-number').text('Số điện thoại không hợp lệ');

			}else{
				$('#error-phone-number').hide();
			}
		}else{
			error = true;
			$('#error-phone-number').show();
			$('#error-phone-number').text('Bạn chưa nhập số điện thoại');
	}

	if(customerAddress.trim()==''){
		error = true;
		$('#error-address-customer').show();
	}else{
		$('#error-address-customer').hide();
	}
	log(error);

	return error;
}

function loadAllProduct(){

	$.ajax({
			headers: {
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/salon/getAllSalonProduct',
			data:{
				salon_id:salon_id,
			},
			success:function(data){

				rowTable = data;
				var rows = $('#table-order-info > tbody > tr');
				$('#table-order-info > tbody > tr').eq(rows.length-2).before(data);
				$('#table-order-info .selectpicker').selectpicker();
				updateIndexRowTable();

			},
			 error:function(jqXHR, ajaxOptions, thrownError){
	             handleUnAuthenticated(jqXHR);
	       }

 	});

}

function loadAllProductCategory(){

	$.ajax({
			headers: {
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/salon/getAllProductCategorySalon',
			data:{
				salon_id:salon_id,
			},
			success:function(data){

				$('#category-product').empty().html(data);

			},
			 error:function(jqXHR, ajaxOptions, thrownError){
	             handleUnAuthenticated(jqXHR);
	       }

 	});

}

function showDialogCreateProduct(){
	Swal.fire({
			title:'Tạo hóa đơn',
			onBeforeOpen:()=>{
				Swal.showLoading();
			},
			onOpen :()=>{
				createProduct();
			},
		});
}
function createProduct(){
	var title = $('#product-title').val();
	var originPrice = $('#origin-price').val();
	var currentPrice = $('#current-price').val();
	var description = $('#description-product').val();
	var catId =  $('#category-product').val();
	var form_data = new FormData();
	form_data.append('image', $("#product-thumb")[0].files[0]);
	form_data.append('salon_id', salon_id);
	form_data.append('title',title);
	form_data.append('origin_price', originPrice);
	form_data.append('current_price', currentPrice);
	form_data.append('description', description);
	form_data.append('category_id', catId);
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	$.ajax({
			headers: {
	            'Accept':'application/json',
	            'Authorization':'Bearer ' + token,
	        },
			type:'POST',
			processData: false,
    		contentType: false,
	        cache : false,
			url:BASE_URL+'api/v1/salon/importProduct',
			data:form_data,
			success:function(data){
				Swal.hideLoading();
				if(data.success){
					resetForm();
					Swal.fire({
		                  title: 'Thông báo',
		                  text: data.message,
		                  icon: 'success',
		                  confirmButtonText: 'OK'
			        });

				}else{
					Swal.fire({
		                  title: 'Thông báo',
		                  text: data.message,
		                  icon: 'error',
		                  confirmButtonText: 'OK'
			        });
				}

			},
			 error:function(jqXHR, ajaxOptions, thrownError){
			 	Swal.hideLoading();
	            handleUnAuthenticated(jqXHR);
	       }

 	});
}

function addProductToOrderList(currentProducts,product){
	var found = false;
	var totalPrice = 0;
	console.log(currentProducts);
	currentProducts.forEach(function(item){
	  if(product.id==item.id){
	  	found = true;
	  	item.quantity += product.quantity;
	  }
	  totalPrice += item.price * item.quantity;
	});
	if(!found){
		currentProducts.push(product);
		totalPrice += product.quantity * product.price;
		console.log(product);
	}
	console.log(totalPrice);
	$('#table-order .total-price-row').find('td').eq(1).text(totalPrice);

}

function updateIndexRowTable(){
	var index = 1;
	rows = 	$('#table-order-info > tbody > tr > .index-cell');
	$.each(rows, function() {
		$(this).text(index);
		index++;
	});
}

function updateTotalPrice(){
	rows = 	$('#table-order-info > tbody > tr > .total-price-cell');
	var totalPrice = 0;
	$.each(rows, function() {
		var total = parseInt($(this).text());
		totalPrice += total;
	});
	$('#table-order-info > tbody >tr > .total-price').text(totalPrice);
	
	
}
function resetForm(){
	$('#product-title').val('');
	$('#origin-price').val('');
	$('#current-price').val('');
	$("#product-thumb").val('');
	$('#description-product').val('');
}
function getLeftQuantity(idProduct){
	var quantity = 0;
	var quantityProduct = 0;
	var totalQuantitySelect = 0;
	var select = $('#table-order-info').find('select').find(':selected');

	$.each(select,function(){
		var product = $(this).data('product');
		var selectQuantity =  $(this).parents('tr').find('.quantity-input').val();
		if(product && product.id==idProduct){
			quantityProduct = product.quantity
			if(parseInt(selectQuantity)){
				totalQuantitySelect += parseInt(selectQuantity);
			}
			
			
		}
	});
	quantity = Math.max(0,quantityProduct - totalQuantitySelect); 
	return quantity;
}
function getOrderProduct(){
	var mapProduct = new Map();
	var rows = $('#table-order-info > tbody > .item-order');
		$.each(rows,function(){
			var product = $(this).find('select').find(':selected').data('product');
			var quantity = $(this).find('.quantity-input').val();
			if(product){
				quantity = parseInt(quantity);
				if(mapProduct.has(product.id)){
					var productItem = mapProduct.get(product.id);
					productItem.selectQuantity += quantity;
				}else{
					mapProduct.set(product.id,new Product(product,quantity));
				}
			}
		});
	return mapProduct;
	
}

function getErrorSelectedProduct(mapProduct){
	var errorMsg ='';
	mapProduct.forEach(function(product,key){
		if(!product.isValid()){
			errorMsg = errorMsg.concat(`Sản phẩm : ${product.title} số lượng còn ${product.quantity} nhưng đang chọn ${product.selectQuantity}\n`);	
		}
	});
	return errorMsg;
}

function createOrder(productSelected,cus_name,phone_number,address){

	var dataRequest = {
		salon_id:salon_id,
		cus_name:cus_name,
		phone_number:phone_number,
		address:address,
		dataProduct:Object.fromEntries(productSelected)
	};
	var json = JSON.stringify(dataRequest);
	Swal.fire({
		title:'Nhập hàng',
		onBeforeOpen:()=>{
				Swal.showLoading()
			},
		onOpen:()=>{
					$.ajax({
						headers: {
				            'Authorization':'Bearer ' + token,
				        },
						type:'POST',
						url:BASE_URL+'api/v1/salon/createOrder',
						data:json,
						success:function(data){
							Swal.hideLoading();
							if(data.success){
								
								var rowItems = $('#table-order-info > tbody').find('.item-order');
								$.each(rowItems,function(){
									$(this).remove();
								});
								showSuccess('Thành công',data.message);
								$('#order-name-customer').val('');
								$('#order-phone-customer').val('');
								$('#order-address').val('');
								var rows = $('#table-order-info > tbody > tr');
								$('#table-order-info > tbody > tr').eq(rows.length-2).before(rowTable);
								$('#table-order-info .selectpicker').selectpicker();
								
								updateIndexRowTable();
								
							}else{
								showError('Thất bại',data.message);
							}
						},
						 error:function(jqXHR, ajaxOptions, thrownError){
						 	Swal.hideLoading();
				            handleUnAuthenticated(jqXHR);
				            showError('Thất bại','Có lỗi trong quá trình gửi yêu cầu, vui lòng thử lại');
				       }

			 	});
				
			},
	});

}

class Product {
  constructor(product,selectQuantity) {
    this.id = product.id;
    this.title = product.title;
    this.price = product.current_price;
    this.quantity = product.quantity;
    this.selectQuantity = selectQuantity;
  }
  isValid(){
  	return this.quantity >= this.selectQuantity;
  }
}