
$(document).ready(function(){

    var id_booking = $('#box-content-container').data('id');
     $.ajax(
      {
        type:'GET',
        headers: {
          'Authorization':'Bearer ' + token,
          'X-Requested-With':'XMLHttpRequest'
        },
        url:BASE_URL+'api/v1/salon/getAppointmentDetail',
        datatype: "html",
        data:{

          'booking_id':id_booking,
          'salon_id':salon_id
        }

      }).done(function(data){
       $("#content-detail-appointment").empty().html(data);

     }).fail(function(jqXHR, ajaxOptions, thrownError){
      handleUnAuthenticated(jqXHR);
    });

     $('#content-detail-appointment').on('click','#btnCheckIn',function(){
        var id_booking = $(this).data('booking');
        var data_expired = $(this).data('expired');
        if(id_booking){
          if(data_expired==1){
            Swal.fire({
              title: 'Lịch quá hẹn',
              text: "Lịch đã quá hẹn, bạn có chắc muốn check in?",
              icon: 'warning',
              confirmButtonText:'tiếp tục',
              cancelButtonText:'hủy bỏ',
              showCancelButton: true,

            }).then((result)=>{
                if(result.value){
                  handleAppointment(id_booking,'checkInAppointment','Check in');
                }
            });
          }else{
            handleAppointment(id_booking,'checkInAppointment','Check in');
          }
          
        }
     });
     $('#content-detail-appointment').on('click','#btnCancel',function(){
        var id_booking = $(this).data('booking');
        if(id_booking){
          handleAppointment(id_booking,'cancelAppointment','Đang hủy lịch');
        }

     });
     $('#content-detail-appointment').on('click','#btnBack',function(event){
        event.preventDefault();
        var redirect = $('#box-content-container').data('href');
        window.location = redirect;
     });
      $('#content-detail-appointment').on('click','#btn-show-detail-receipt',function(event){
        event.preventDefault();
        var redirect = $(this).data('href');
        window.location = redirect;
     });
  

});

function handleAppointment(id_booking,method,title){
   Swal.fire({
      title:title,
            onBeforeOpen:()=>{
                Swal.showLoading()
            },
            onOpen:()=>{
              $.ajax({

                type:'PATCH',
                headers: {
                  'Accept':'application/json',
                  'Authorization':'Bearer ' + token,
                },
                url:BASE_URL+'api/v1/admin/'+method,
                data:{

                  'booking_id':id_booking
                },
                success:function(data){
                  Swal.hideLoading();
                  console.log(data);
                  if(data.success){
                    Swal.fire({
                      title: 'Thành công',
                      text: data.message,
                      icon: 'success',
                      confirmButtonText: 'OK'
                   }).then((result)=>{
                    var redirect = $('#box-content-container').data('href');
                    window.location = redirect;
                   });
                  }else{
                    Swal.fire({
                      title: 'Thất bại',
                      text: data.message,
                      icon: 'error',
                      confirmButtonText: 'OK'
                   });
                  }
                },
                error:function(jqXHR, ajaxOptions, thrownError){
                  Swal.hideLoading();
                  handleUnAuthenticated(jqXHR);
                }

              });
            }
    });
}