var productSelect;

$(document).ready(function () {

    var filterOption = $('#filterOption').val();
    var keyword = $('#input-search-order').val();

    loadAllOrder(0, filterOption, keyword);

    $(document).on('click', '.box-content .pagination a', function (event) {
        event.preventDefault();
        var filterOption = $('#filterOption').val();
        var keyword = $('#input-search-order').val();
        $('.box-content .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        loadAllOrder(page, filterOption, keyword);
    });

    $("#product-thumb").change(function () {
        readURL(this);
    });
    $(document).on('click', '#box-content-data .item-table', function () {
        var url = $(this).data('href');
        console.log(url);
        // window.location = url;
    });

    $("#filterOption").change(function () {
        var keyword = $('#input-search-order').val();
        loadAllOrder(0, this.value, keyword);
    });
    $('#btn-search-order').click(function () {
        var keyword = $('#input-search-order').val();
        var filterOption = $('#filterOption').val();
        loadAllOrder(0, filterOption, keyword);
    });

    $(document).on('click', '.box-content .item-table', function (event) {
        event.preventDefault();
        var url = $(this).data('href');
        window.location = $(this).data('href');
    });

    $(document).on('click', '.item-table .btn-edit', function (event) {
        event.preventDefault();
        event.stopPropagation();
        var id = $(this).data('id');
        var status = $(this).data('status');
        if(status==4){
             Swal.fire({
                title: 'Thông báo',
                icon: 'info',
                text:'Đơn hàng này đã được thanh toán!'
                });
             return;
        }

        Swal.fire({
            title: '<strong>Trạng thái đơn hàng</strong>',
            icon: 'info',
            html:
                '<select class="form-control" id="statusOption">\n' +
                '\t\t\t\t\t\t<option value="0"">Đã đặt</option>\n' +
                '\t\t\t\t\t\t<option value="1">Đang giao</option>\n' +
                '\t\t\t\t\t\t<option value="2">Đã giao</option>\n' +
                '\t\t\t\t\t\t<option value="3">Đã hủy</option>\n' +
                '\n' +
                '\t\t\t\t  </select>',
            onOpen: () => {
                $('#statusOption').val(status);
            },
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText:
                'Thay đổi',
            cancelButtonText:
                'Hủy bỏ'
        }).then((result) => {
            if (result.value) {
                var status = $('#statusOption').val();
                showUpdateDialog(id,status)
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        });

    });

    $(document).on('click', '.item-table .btn-remove', function (event) {
        event.preventDefault();
        event.stopPropagation();
        var row = $(this).closest("tr");
        showDeleteDialog(row.data('id'))

    });
    $('#btnSave').click(function () {
        var nameProduct = $('#product-name').val();
        var currentPrice = $('#current-price').val();
        var categoryId = $('#category-product').val();
        var originPrice = $('#original-price').val();
        if (nameProduct.trim() == '' || currentPrice.trim() == '') {
            Swal.fire({
                title: 'Thông báo',
                text: 'Bạn chưa điền đầy đủ thông tin!',
                icon: 'error',
                confirmButtonText: 'OK'
            });

        } else if (Number(currentPrice) == 'NaN' || Number(originPrice) == 'NaN' || (currentPrice % 1000) != 0 || (originPrice % 1000) != 0) {
            Swal.fire({
                title: 'Thông báo',
                text: 'Giá bạn nhập không hợp lệ!',
                icon: 'error',
                confirmButtonText: 'OK'
            });
        } else if (!categoryId) {
            showError('Thông báo', 'Bạn chưa chọn loại sản phẩm');
        } else {
            $('#modalProduct').modal('hide');
            showUpdateDialog($(this).data('idproduct'), categoryId, nameProduct, originPrice, currentPrice);
        }
    });

    $('#btnDelete').click(function () {
        $("#product-thumb").val('');
        var href = $('#img-thumb').data('href');
        $('#img-thumb').attr('src', href);
        $('#btnDelete').hide();

    });

    $('#btnImport').click(function () {
        var url = $(this).data('href');
        window.location = $(this).data('href');
    });

    $('#btnAddProduct').click(function () {
        var url = $(this).data('href');
        window.location = $(this).data('href');
    });
    $('#btnCreateOrder').click(function () {
        var url = $(this).data('href');
        window.location = $(this).data('href');
    });


});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log("load");
            $('#img-thumb').attr('src', e.target.result);
            $('#btnDelete').show();
        }
        reader.readAsDataURL(input.files[0]);

    } else {
        var href = $('#img-thumb').data('href');
        $('#img-thumb').attr('src', href);
        $('#btnDelete').hide();
    }
}

function showUpdateDialog(order_id, status) {
    Swal.fire({
        title: 'Cập nhật đơn hàng',
        onBeforeOpen: () => {
            Swal.showLoading()
        },
        onOpen: () => {
            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token,
                },
                type: 'POST',
                url: BASE_URL + 'api/v1/salon/updateOrderStatus',

                data: {
                    order_id: order_id,
                    status: status
                },
                success: function (data) {
                    Swal.hideLoading();

                    if (data.success) {

                        Swal.fire({
                            title: 'Thông báo',
                            text: 'Cập nhật trạng thái đơn hàng thành công',
                            icon: 'success',
                            confirmButtonText: 'OK'
                        });
                        var filterOption = $('#filterOption').val();
                        var keyword = $('#input-search-order').val();

                        loadAllOrder(0, filterOption, keyword);
                    } else {
                        Swal.fire({
                            title: 'Thông báo',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        });
                    }

                },
                error: function (jqXHR, ajaxOptions, thrownError) {
                    Swal.hideLoading();
                    handleUnAuthenticated(jqXHR);
                }

            });
        }
    });
}

function showDeleteDialog(product_id) {
    Swal.fire({
        title: 'Xóa sản phẩm',
        text: 'Xóa sản phẩm thành công',
        onBeforeOpen: () => {
            Swal.showLoading()
        },
        onOpen: () => {

            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token,
                },
                type: 'PATCH',
                url: BASE_URL + 'api/v1/salon/deleteProduct',

                data: {
                    product_id: product_id,
                },
                success: function (data) {
                    Swal.hideLoading();

                    if (data.success) {
                        $('.nav-tabs').find('li').removeClass('active');
                        $('.nav-tabs').find('li').eq(0).addClass('active');
                        $('#browseTab').removeClass('active');
                        $('#uploadTab').addClass('active');
                        $('#product-thumb').val('');
                        $('#btnDelete').hide();
                        deleteRowTable(product_id);
                    } else {
                        Swal.fire({
                            title: 'Thông báo',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        });
                    }

                },
                error: function (jqXHR, ajaxOptions, thrownError) {
                    Swal.hideLoading();
                    handleUnAuthenticated(jqXHR);
                }

            });
        }
    });
}

function updateDataTable(product_id, categoryId, title, originPrice, current_price, thumbUrl) {
    var rows = $('tbody').find('tr');
    var categoryTitle = $(`#category-product option[value=${categoryId}]`).text();

    $.each(rows, function () {
        console.log($(this).data('id'));
        if ($(this).data('id') == product_id) {
            $(this).find("td").eq(1).text(title);
            $(this).find("td").eq(2).text(categoryTitle);
            $(this).find("td").eq(3).text(originPrice);
            $(this).find("td").eq(4).text(current_price);
            if (thumbUrl) {
                $(this).data('thumb', thumbUrl);
            }
        }
    });

}

function deleteRowTable(product_id) {
    var filterOption = $('#filterOption').val();
    var keyword = $('#input-search-order').val();

    loadAllOrder(0, filterOption, keyword);
}

function loadAllOrder(page, filterOption, keyword) {
    $('#progress-loading').show();
    $.ajax({

        headers: {
            'Authorization': 'Bearer ' + token,
            'X-Requested-With': 'XMLHttpRequest'
        },
        type: 'GET',
        url: BASE_URL + 'api/v1/salon/getOrderSalon',
        data: {
            salon_id: salon_id,
            status: filterOption,
            keyword: keyword,
            page: page
        },
        success: function (data) {
            $('#progress-loading').hide();
            $('#box-content-data').empty().html(data);
        },
        error: function (jqXHR, ajaxOptions, thrownError) {
            $('#progress-loading').hide();
            handleUnAuthenticated(jqXHR);
        }

    });

}



