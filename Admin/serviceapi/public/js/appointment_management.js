


$(document).ready(function(){

	var filterOption = $('#filterOption').val();
	var keyword = $('#input-search-appointment').val();
	
	getAppointmentInfo();
	loadAllAppointment(0,filterOption,keyword);

	$(document).on('click', '.box-content .pagination a',function(event)
      {
        event.preventDefault();
        var filterOption = $('#filterOption').val();
		var keyword = $('#input-search-appointment').val();
        $('.box-content .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var page=$(this).attr('href').split('page=')[1];

        loadAllAppointment(page,filterOption,keyword);
      });

	$("#filterOption").change(function(){
		var keyword = $('#input-search-appointment').val();
		loadAllAppointment(0,this.value,keyword);
	});
	$('#btn-search-appoinment').click(function(){
		var keyword = $('#input-search-appointment').val();
		var filterOption = $('#filterOption').val();
		loadAllAppointment(0,filterOption,keyword);
	});

    $('#btnAddAppointment').click(function(){
         var url = $(this).data('href');
          window.location = $(this).data('href');
    });

	$(document).on('click','.box-content .item-table',function(event){
          event.preventDefault();
          var url = $(this).data('href');
          window.location = $(this).data('href');
      });


});

function getAppointmentInfo(){
	 $.ajax(
  {
    type:'GET',
    headers: {
      'Authorization':'Bearer ' + token,
    },
    url:BASE_URL+'api/v1/salon/getAppointmentInfo',
    data:{
      'salon_id':salon_id,
    },
   success:function(data){

       console.log(data);
       if(data.success){
          $('#total-sale-value').text(data.totalSale +"VNĐ");
          
          var appointment = data.appointment;
          
          var totalNeedNotify = 0;
          var totalAppointment = 0;
          var totalCancelAppointment = 0;
          for(index in appointment){
            var appointmentItem = appointment[index];
            if(appointmentItem.status ==0){
            	totalAppointment +=1;
                var bookingTime  = appointmentItem.booking_time;
                var dateTimeBooking = new Date(bookingTime);
                var compareDataNeedNotify = new Date();
                var currentDate = new Date();
                compareDataNeedNotify.setMinutes( compareDataNeedNotify.getMinutes() + 15 );
                if(currentDate.getTime() <dateTimeBooking.getTime() && dateTimeBooking.getTime()<= compareDataNeedNotify.getTime()){
                    totalNeedNotify += 1;
                }
            }else{
            	if(appointmentItem.status ==2){
            		totalCancelAppointment +=1;
            	}else{
            		totalAppointment +=1;
            	}

            }
           
          }
          $('#appointment-value').text(totalAppointment);
          $('#cancel-appointment-value').text(totalCancelAppointment);
          $('#notify-customer-value').text(totalNeedNotify);
          
       }

      },
       error:function(jqXHR, ajaxOptions, thrownError){
               handleUnAuthenticated(jqXHR);
         }

  });
}

function loadAllAppointment(page,filterOption,keyword){
	$('#progress-loading').show();
	$.ajax({
			headers: {
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/salon/getAllAppointmentOfSalonByFilter',
			datatype: "html",
			data:{
				salon_id:salon_id,
				status:filterOption,
				keyword:keyword,
				page:page
			},
			success:function(data){
				console.log(data);
				$('#progress-loading').hide();
				$('#box-content-data').empty().html(data);
			},
			 error:function(jqXHR, ajaxOptions, thrownError){
			 	$('#progress-loading').hide();
			 	console.log(thrownError);
			 	console.log(jqXHR);
	             handleUnAuthenticated(jqXHR);
	       }

 	});

}

