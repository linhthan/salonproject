var currentBlob;

$(document).ready(function(){

var image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:512,
      height:512,
      type:'square' //circle
    },
    boundary:{
      width:600,
      height:600
    }
  });

$('#hair-title').focus(function(){
		$(this).addClass('input-active');
		$('#error-hair-title').hide();
	});
$('#hair-title').blur(function(){
		$(this).removeClass('input-active');
	});	
$("#input-image-source").change(function(){
		$('#img-hair-style').hide();
		$('#form-input-title').hide();
		$('#img-hair-style').attr('src','#');
   		 readURL(this,image_crop);
   		 $('#uploadimageModal').modal('show');
   		 console.log('change');
	});
 $('.crop_image').click(function(event){
 	image_crop.croppie('result', {
 	 format: 'png',
      type: 'blob',
      size: 'viewport'
    }).then(function(response){
    	var objectURL = URL.createObjectURL(response);
    	// var file = new File([resp], $("#input-image-source").originalFiles[0]['name'].split(".")[0]+".jpeg");
    	 $('#img-source').attr('src', objectURL);
    	 $('#img-source').data('currentimage', response);
         $('#img-source').show();
         $('#uploadimageModal').modal('hide');
    });
 });

 $('#btnExtract').click(function(){
 	$('#progress-loading').show();
 	extractHairStyle();
 });
 $('#btnSave').click(function(){
 	var title = $('#hair-title').val();
 	if(title.trim()==''){
 		$('#error-hair-title').show();
 	}else{
 		saveHairStyle(title);
 	}
 });
 $('#btnCancel').click(function(){
 	var url = $(this).data('href');
    window.location = $(this).data('href');
 });


});


function readURL(input,image_crop) {
    if (input.files && input.files[0]) {
    	$('#error-image-source').hide();
        var reader = new FileReader();
        reader.onload = function (e) {
        	image_crop.croppie('bind', {
		        url: e.target.result
		      }).then(function(){
		        console.log('jQuery bind complete');
		      });
            // $('#img-source').attr('src', e.target.result);
            // $('#img-source').show();
        }

        reader.readAsDataURL(input.files[0]);
    }else{
    	$('#img-source').attr('src','#');
    	$('#img-source').hide();
    }
}

function saveHairStyle(title){
	var urlhairSytle  = $('#img-hair-style').attr('src');
	if(urlhairSytle!='#'){
		
		Swal.fire({
			title:'Lưu mẫu tóc',
			onBeforeOpen:()=>{
			    Swal.showLoading()
			},
			onOpen :()=>{
				$.ajaxSetup({
               	 headers: {
                    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    	'Authorization':'Bearer ' + token,
                	}
           		 });
				$.ajax({
					type:'POST',
					url:BASE_URL+'api/v1/hairstyle/saveHairStyle',
			        data:{
			        	salon_id:salon_id,
			        	title:title,
			        	url:urlhairSytle
			        },
			        success:function(data){
			        	Swal.hideLoading();
			        	if(data.success){
			        		$('#input-image-source').val('');
			        		$('#img-hair-style').hide();
			        		$('#img-hair-style').attr('src','#');
			        		$('#img-source').hide();
			        		$('#img-source').attr('src','#');
							$('#form-input-title').hide();
			        		showSuccess('Thành công','Lưu mẫu tóc thành công!');
			        	}else{
			        		showError('Thất bại','Có lỗi trong quá trình lưu mẫu tóc, vui lòng thử lại sau!');
			        	}
			        },
			        error:function(jqXHR, ajaxOptions, thrownError){
	              		handleUnAuthenticated(jqXHR);
	              		Swal.hideLoading();
	              		showError('Thất bại','Có lỗi trong quá trình lưu mẫu tóc, vui lòng thử lại sau!');
	              	}
				});

			},
		});
	}else{
		showError('Lỗi','Chưa có kiểu tóc để lưu, hãy chọn ảnh để trích xuất kiểu tóc!');
	}

}
function extractHairStyle(){
	var blobImg = $('#img-source').data('currentimage');
	if(blobImg){
		$('#img-hair-style').hide();
		$('#form-input-title').hide();
		var form_data = new FormData();
		form_data.append('file',blobImg);
		$.ajax({
				headers: {
		            'Accept':'application/json',
		        },
				type:'POST',
				url:'http://127.0.0.1:5000/predict',
				processData: false,
	    		contentType: false,
		        cache : false,
		        data:form_data,
				success:function(data){
					console.log(data);
					$('#progress-loading').hide();
					$('#img-hair-style').attr('src',data.url);
					$('#img-hair-style').show();
					$('#form-input-title').show();
					

				},
				 error:function(jqXHR, ajaxOptions, thrownError){
				 	$('#progress-loading').hide();
				 	log(jqXHR);
				 	log(ajaxOptions);
				 	log(thrownError);
		            handleUnAuthenticated(jqXHR);
		       }

	 	});
	}else{
		$('#progress-loading').hide();
		showError('Lỗi','Bạn chưa chọn ảnh để trích xuất mẫu tóc!');
	}
	
}
