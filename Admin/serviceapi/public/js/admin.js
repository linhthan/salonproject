
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
  console.log(document.cookie);
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
  var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/;"
    }

}

function showError(title,message){
  Swal.fire({
            title:title,
            text: message,
            icon: 'error',
            confirmButtonText: 'OK'
  });
}
function showSuccess(title,message){
  Swal.fire({
            title:title,
            text: message,
            icon: 'success',
            confirmButtonText: 'OK'
  });
}

function log(message){
  if(DEBUG){
    console.log(message);
  }
}

function handleUnAuthenticated(jqXHR){
      if(jqXHR.status==401){
       eraseCookie('token');
       eraseCookie('user_type');
       eraseCookie('salon_id');
       window.location = '/login';
     }
}
var token = getCookie('token');
var salon_id = getCookie('salon_id');
var name = getCookie('name');
var avatar = getCookie('avatar');
if(!token || !salon_id ){
  window.location ='/login'
}

$(document).ready(function(){


  var avatarDefault = $('#img-avatar').data('default');
  $('#img-avatar').attr("src",avatar?avatar:avatarDefault);
  $('#img-avatar-header').attr("src",avatar?avatar:avatarDefault);
  $('#img-avatar-header-right').attr("src",avatar?avatar:avatarDefault);
  $('#img-avatar-header-right-large').attr("src",avatar?avatar:avatarDefault);

  $('#username').text(name?name:'Anonymous');
  $('#username-menu').text(name?name:'Anonymous');
  $('#user-name-sidebar').text(name?name:'Anonymous');

$('#btn-sign-out').click(function(){
        $.ajax(
        {
          type:'POST',
          headers: {
            'Accept':'application/json',
            'Authorization':'Bearer ' + token,
          },
          url:BASE_URL+'api/v1/admin/logout',

        }).done(function(data){
       // location.hash = page;
       eraseCookie('token');
       eraseCookie('user_type');
       eraseCookie('salon_id');
       window.location = '/login';

     }).fail(function(jqXHR, ajaxOptions, thrownError){
      handleUnAuthenticated(jqXHR);
    });

   });

$('.sidebar-menu li').removeClass('active');
var activeurl = window.location;
console.log(activeurl);
if(activeurl.pathname=='/admin'){
  console.log("oke");
  $('a[href="'+activeurl.href+'"]').parent('li').addClass('active');
 }else{
  var menu = $('.sidebar-menu').find('a');
  $.each(menu, function() { 
    if(String(activeurl).includes($(this).attr('href'))){
      if((activeurl.origin+"/admin")!=$(this).attr('href')){
      $(this).parent('li').addClass('active');
      }
    }
  });
 }


});
