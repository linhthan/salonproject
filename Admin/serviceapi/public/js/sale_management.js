var productSelect;
var currentMode = 0;
$(document).ready(function(){


	var filterOption = $('#filterOption').val();
	var keyword = $('#input-search-sale').val();
	
	loadAllSale(0,filterOption,keyword);

	$(document).on('click', '.box-content .pagination a',function(event)
      {
        event.preventDefault();
        var filterOption = $('#filterOption').val();
		var keyword = $('#input-search-sale').val();
        $('.box-content .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];

        loadAllSale(page,filterOption,keyword);
      });
	
	$("#sale-thumb").change(function(){
   		 readURL(this);
	});
	$(document).on('click','#box-content-data .item-table',function(){
		var url = $(this).data('href');
		console.log(url);
		// window.location = url;
	});

	$("#filterOption").change(function(){
		var keyword = $('#input-search-sale').val();
		loadAllSale(0,this.value,keyword);
	});
	$('#btn-search-appoinment').click(function(){
		var keyword = $('#input-search-sale').val();
		var filterOption = $('#filterOption').val();
		loadAllSale(0,filterOption,keyword);
	});

	// $(document).on('click','.box-content .item-table',function(event){
 //          event.preventDefault();
 //          var url = $(this).data('href');
 //          window.location = $(this).data('href');
 //      });

	$(document).on('click','.item-table .btn-edit',function(event){
			 $('#btnSave').text('Lưu lại');
			 currentMode = 1;
			 event.preventDefault();
			 event.stopPropagation();
			 var row = $(this).closest("tr");
			 $tds = row.find("td");
			 console.log(row.data('id'));
			 if($tds.length >4){
			 	var title = $tds.eq(1).text();
			 	var currentPrice = $tds.eq(4).text();
			 	var urlSale = $tds.eq(3).text();
			 	$('#sale-status-box').show();
			 	$('#titleModal').text('Chỉnh sửa khuyến mại');
			 	$('#img-thumb').attr('src',row.data('thumb'));
			 	$('#img-thumb').data('href',row.data('thumb'));
			 	$('#status-sale').val(row.data('status'));
			 	$('#title-sale').val(title);
			 	$('#url-sale').val(urlSale);
			 	$('#btnSave').data('idSale', $(this).data('id'));
			 	$('#modalSale').modal('show');
			 	$('#sale-thumb').val('');
				$('#btnDelete').hide();
			 }
			 
	});

	$(document).on('click','.item-table .btn-remove',function(event){
			 event.preventDefault();
			 event.stopPropagation();

			 Swal.fire({
			  title: 'Xóa khuyến mại?',
			  text: "Khuyến mại sẽ bị xóa!",
			  icon: 'warning',
			  showCancelButton: true,
			  cancelButtonColor: '#d33',
			  confirmButtonColor: '#3085d6',
			  confirmButtonText: 'Xóa!'
			}).then((result) => {
			  if (result.value) {
			   showDeleteDialog($(this).data('id'));
			  }
			});
			 
	});
	$('#btnSave').click(function(){
		var titleSale = $('#title-sale').val();
		var urlSale = $('#url-sale').val();
		var file = $("#sale-thumb")[0].files[0];
		var status = $('#status-sale').val();
		var id_sale = $(this).data('idSale');

		if(titleSale.trim()==''||urlSale.trim()==''){
			Swal.fire({
                  title: 'Thông báo',
                  text: 'Bạn chưa điền đầy đủ thông tin!',
                  icon: 'error',
                  confirmButtonText: 'OK'
             });
			
		}else if(currentMode==0 && typeof file=='undefined' ){
				Swal.fire({
	                  title: 'Thông báo',
	                  text: 'Bạn chưa chọn banner!',
	                  icon: 'error',
	                  confirmButtonText: 'OK'
	             });
		}else{
			$('#modalSale').modal('hide');
			if(currentMode==0){
				showCreateDialog(titleSale,urlSale,file);
			}else{
				showUpdateDialog(titleSale,urlSale,status,id_sale,file)
			}
			
		}
	});

	$('#btnDelete').click(function(){
		$("#sale-thumb").val('');
		var href = $('#img-thumb').data('href');
    	$('#img-thumb').attr('src',href);
    	$('#btnDelete').hide();

	});


	$('#btnAddProduct').click(function(){
		currentMode = 0;
		$('#sale-status-box').hide();
		$('#btnSave').text('Thêm');
		$('#titleModal').text('Tạo khuyến mại')
		$('#img-thumb').attr('src','');
		$('#img-thumb').hide();
		$('#status-sale').val('1');
		$('#title-sale').val('');
		$('#url-sale').val('');
		$('#btnSave').data('idSale', '');
		$('#modalSale').modal('show');
		$('#sale-thumb').val('');
		$('#btnDelete').hide();
		$('#modalSale').modal('show');
			
	});

	


});

function readURL(input){
	if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        	console.log("load");
            $('#img-thumb').attr('src', e.target.result);
            $('#img-thumb').show();
            if(currentMode==1){
 				$('#btnDelete').show();
            }
           
        }
        reader.readAsDataURL(input.files[0]);
    	
	}else{
    	var href = $('#img-thumb').data('href');
    	$('#img-thumb').attr('src',href);
    	$('#btnDelete').hide();
    }
}

function showCreateDialog(titleSale, urlSale,thumbSale){
	Swal.fire({
			title:'Tạo khuyến mại',
			onBeforeOpen:()=>{
				Swal.showLoading()
			},
			onOpen :()=>{

				var form_data = new FormData();
				form_data.append('image', thumbSale);
				form_data.append('titleSale', titleSale);
				form_data.append('urlSale',urlSale);
				form_data.append('salon_id',salon_id);
		
				$.ajax({
					headers: {
			            'Authorization':'Bearer ' + token,
			        },
					type:'POST',
					url:BASE_URL+'api/v1/sale/createSale',
					processData: false,
		    		contentType: false,
			        cache : false,
					data:form_data,
					success:function(data){
						Swal.hideLoading();
						if(data.success){
							$('.nav-tabs').find('li').removeClass('active');
							$('.nav-tabs').find('li').eq(0).addClass('active');
							$('#browseTab').removeClass('active');
							$('#uploadTab').addClass('active');
							$('#sale-thumb').val('');
							$('#btnDelete').hide();
							var filterOption = $('#filterOption').val();
							var keyword = $('#input-search-sale').val();
							
							loadAllSale(0,filterOption,keyword);
							Swal.fire({
			                  title: 'Thông báo',
			                  text: data.message,
			                  icon: 'success',
			                  confirmButtonText: 'OK'
				             });
			         }else{
			         	Swal.fire({
		                  title: 'Thông báo',
		                  text: data.message,
		                  icon: 'error',
		                  confirmButtonText: 'OK'
			             });
			         }

					},
					 error:function(jqXHR, ajaxOptions, thrownError){
					 	Swal.hideLoading();
			             handleUnAuthenticated(jqXHR);
			             Swal.fire({
		                  title: 'Thông báo',
		                  text: 'Có lỗi trong quá trình tạo khuyến mại, vui lòng thử lại sau!',
		                  icon: 'error',
		                  confirmButtonText: 'OK'
			             });
			       }

		 	});
			}
		});
}
function showUpdateDialog(titleSale, urlSale,status,id_sale,thumbSale){
	Swal.fire({
			title:'Cập nhật khuyến mại',
			onBeforeOpen:()=>{
				Swal.showLoading()
			},
			onOpen :()=>{

				var form_data = new FormData();
				form_data.append('image', thumbSale);
				form_data.append('titleSale', titleSale);
				form_data.append('urlSale',urlSale);
				form_data.append('status',status);
				form_data.append('id_sale',id_sale);
		
				$.ajax({
					headers: {
			            'Authorization':'Bearer ' + token,
			        },
					type:'POST',
					url:BASE_URL+'api/v1/sale/updateSale',
					processData: false,
		    		contentType: false,
			        cache : false,
					data:form_data,
					success:function(data){
						Swal.hideLoading();
						if(data.success){
							$('.nav-tabs').find('li').removeClass('active');
							$('.nav-tabs').find('li').eq(0).addClass('active');
							$('#browseTab').removeClass('active');
							$('#uploadTab').addClass('active');
							$('#sale-thumb').val('');
							$('#btnDelete').hide();
							var filterOption = $('#filterOption').val();
							var keyword = $('#input-search-sale').val();
							
							loadAllSale(0,filterOption,keyword);
							Swal.fire({
			                  title: 'Thông báo',
			                  text: data.message,
			                  icon: 'success',
			                  confirmButtonText: 'OK'
				             });
			         }else{
			         	Swal.fire({
		                  title: 'Thông báo',
		                  text: data.message,
		                  icon: 'error',
		                  confirmButtonText: 'OK'
			             });
			         }

					},
					 error:function(jqXHR, ajaxOptions, thrownError){
					 	Swal.hideLoading();
			             handleUnAuthenticated(jqXHR);
			             Swal.fire({
		                  title: 'Thông báo',
		                  text: 'Có lỗi trong quá trình chỉnh sửa khuyến mại, vui lòng thử lại sau!',
		                  icon: 'error',
		                  confirmButtonText: 'OK'
			             });
			       }

		 	});
			}
		});
}
function showDeleteDialog(id_sale){
	Swal.fire({
			title:'Xóa khuyến mại',
			onBeforeOpen:()=>{
				Swal.showLoading()
			},
			onOpen :()=>{

				$.ajax({
					headers: {
			            'Accept':'application/json',
			            'Authorization':'Bearer ' + token,
			        },
					type:'delete',
					url:BASE_URL+'api/v1/sale/deleteSale',

					data:{
						id_sale:id_sale,
					},
					success:function(data){
						Swal.hideLoading();

						if(data.success){
							deleteRowTable(id_sale);
			         }else{
			         	Swal.fire({
		                  title: 'Thông báo',
		                  text: data.message,
		                  icon: 'error',
		                  confirmButtonText: 'OK'
			             });
			         }

					},
					 error:function(jqXHR, ajaxOptions, thrownError){
					 	Swal.hideLoading();
			             handleUnAuthenticated(jqXHR);
			       }

		 	});
			}
		});
}

function updateDataTable(product_id,categoryId,title,originPrice,current_price,thumbUrl){
	var rows = $('tbody').find('tr');
	var categoryTitle = $(`#category-product option[value=${categoryId}]`).text();

	$.each(rows, function() {               
    console.log($(this).data('id'));
    if($(this).data('id')==product_id){
    	$(this).find("td").eq(1).text(title);
    	$(this).find("td").eq(2).text(categoryTitle);
    	$(this).find("td").eq(3).text(originPrice);
    	$(this).find("td").eq(4).text(current_price);
    	if(thumbUrl){
    		$(this).data('thumb',thumbUrl);
    	}
    }
});

}
function deleteRowTable(product_id){
	var filterOption = $('#filterOption').val();
	var keyword = $('#input-search-sale').val();
	loadAllSale(0,filterOption,keyword);
}

function loadAllSale(page,filterOption,keyword){
	$('#progress-loading').show();
	$.ajax({

			headers: {
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/salon/getSaleSalon',
			data:{
				salon_id:salon_id,
				status:filterOption,
				keyword:keyword,
				page:page
			},
			success:function(data){
				$('#progress-loading').hide();
				$('#box-content-data').empty().html(data);
			},
			 error:function(jqXHR, ajaxOptions, thrownError){
			 	$('#progress-loading').hide();
	             handleUnAuthenticated(jqXHR);
	       }

 	});

}










