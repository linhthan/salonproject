


function setCookie(name,value,days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days*24*60*60*1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function eraseCookie(name) {   
	document.cookie = name+'=; Max-Age=-99999999;';  
}

if(getCookie('token')&&getCookie('salon_id')){
	window.location ='/admin'
}


var vnData =""

$(document).ready(function(){
	readTextFile("json/vn.json", function(text){
		vnData = JSON.parse(text);
		var data_option_city = "";
		for(var item in vnData){
			var dataCity = vnData[item];
			data_option_city+="<option value=\""+item +"\">";
			data_option_city+=dataCity.name;
			data_option_city+="<option>\n";
			if(item==0){
				changeCitySelected(item);
			}

		}
		if(data_option_city==""){
			data_option_city ="<option disabled=\"disabled\" selected=\"selected\">Chọn thành phố</option>\n";
		}
		$("#select-city-option").empty().html(data_option_city);
	});



	$("#select-city-option").change(function(){
		changeCitySelected(this.value);
		
	});

	$("#select-district-option").change(function(){
		var value = this.value;
		var dataCity = vnData[$("#select-city-option").val()];
		var dataDistricts =  dataCity.districts;
		var districtSelected = dataDistricts[value];
		changeDistrictSelected(districtSelected);
	});

	$('.inputData').each(function(){
		$(this).on('blur', function(){
			if($(this).val().trim() != "") {
				$(this).addClass('has-val');
			}
			else {
				$(this).removeClass('has-val');
			}
		});
	});

	$('.input-group .inputData').each(function(){
		$(this).focus(function(){
			hideValidate(this);
		});
	});

	var input = $('.input-group .inputData');

	$("#form-sign-up").on('submit',function(event){
		event.preventDefault();
		for(var i=0; i<input.length; i++) {
			if(validate(input[i]) == false){
				showValidate(input[i]);
				return
			}
		}

		submitSignUp();
		
		



	});




	try {
		var selectSimple = $('.js-select-simple');

		selectSimple.each(function () {
			var that = $(this);
			var selectBox = that.find('select');
			var selectDropdown = that.find('.select-dropdown');
			selectBox.select2({
				dropdownParent: selectDropdown
			});
		});

	} catch (err) {
		console.log(err);
	}








//usage:

});

function readTextFile(file, callback) {
	var rawFile = new XMLHttpRequest();
	rawFile.overrideMimeType("application/json");
	rawFile.open("GET", file, true);
	rawFile.onreadystatechange = function() {
		if (rawFile.readyState === 4 && rawFile.status == "200") {
			callback(rawFile.responseText);
		}
	}
	rawFile.send(null);
}

function changeCitySelected(idCity){
	var data_option_district ="";

	var dataCity = vnData[idCity];
	var dataDistricts =  dataCity.districts;
	for(var item in dataDistricts){
		var itemDistrict = dataDistricts[item];
		if(item==0){
			changeDistrictSelected(itemDistrict);
		}
		data_option_district+="<option value=\""+item +"\">";
		data_option_district+=itemDistrict.name;
		data_option_district+="<option>\n";
	}


	$("#select-district-option").empty().html(data_option_district);
}
function changeDistrictSelected(itemDistrict){
	var data_option_ward ="";
	for(var idWard in itemDistrict.wards){
		var wardsName = itemDistrict.wards[idWard].name;
		data_option_ward+="<option value=\""+idWard +"\">";
		data_option_ward+=wardsName;
		data_option_ward+="<option>\n";
	}
	$("#select-ward-option").empty().html(data_option_ward);
}


function validate (input) {
	if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
		if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
			return false;
		}
	}else if($(input).attr('name') == 'phone'){
		var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
		var phone = $(input).val().trim();
		if(phone !==''){
			if (vnf_regex.test(phone) == false) 
			{
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}

	}
	else {
		if($(input).val().trim() == ''){
			return false;
		}
	}
}


function showValidate(input) {
	var thisAlert = $(input).parent();

	$(thisAlert).addClass('alert-validate');
}

function hideValidate(input) {
	var thisAlert = $(input).parent();

	$(thisAlert).removeClass('alert-validate');
}
function submitSignUp(){
	var _URL = window.URL || window.webkitURL;
	var file, img;
	if ((file = $("#thumbUrl").prop('files')[0])) {
		console.log("oke");
		img = new Image();
		img.onload = function () {
			if(this.width < 600 || this.height < 300 ){

				Swal.fire({
					icon: 'error',
					title: 'Lỗi',
					text: 'Ảnh bìa phải có kích thước tối thiểu là 600x300!',
				});
				return false;
			}else{
				signUp();
				return true;
			}
		};
		img.src = _URL.createObjectURL(file);
	}else{
		$.sweetModal({
			content: 'Chưa chọn ảnh bìa',
			title: 'Lỗi',
			icon: $.sweetModal.ICON_ERROR,

			buttons: [
			{
				label: 'OK',
				classes: 'redB'
			}
			]
		});
	}
}

function signUp(){
	var city = $("#select-city-option option:selected").text();
	var district = $("#select-district-option option:selected").text();
	var ward =  $("#select-ward-option option:selected").text();
	if(!city || !district || !ward){
		Swal.fire({
			icon: 'error',
			title: 'Lỗi',
			text: 'Bạn cần chọn địa chỉ!',
			confirmButtonText: 'OK'
		});
	}else{
	var form_data = new FormData();
	form_data.append('image', $("#thumbUrl")[0].files[0]);
	form_data.append('name', $('#fullname').val().trim());
	form_data.append('email',$('#email').val().trim());
	form_data.append('phone_number', $('#phone-number').val().trim());
	form_data.append('password', $('#password').val().trim());
	form_data.append('user_type', 2);
	form_data.append('salonName', $('#salonName').val().trim());
	form_data.append('detailLocation', $('#detail-location').val().trim());
	form_data.append('city', city);
	form_data.append('district', district);
	form_data.append('ward', ward);

		Swal.fire({
			title:'Đăng ký',
			onBeforeOpen:()=>{
				Swal.showLoading()
			},
			onOpen :()=>{


				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax(
				{
					type:'POST',
					processData: false,
		    		contentType: false,
			        cache : false,
					url:'/api/v1/register',
					data:form_data,

				}).done(function(data){
					Swal.hideLoading();
					if(data.success){
						setCookie('token',data.data.token,1000);
						setCookie('user_type',2,1000);
						setCookie('salon_id',data.data.salon_id,1000);
						setCookie('name',data.data.user.name,1000);
						setCookie('avatar',data.data.user.avatar,1000);
						window.location.href ='/admin';
					}else{
						Swal.fire({
							title: 'Thất bại',
							text: data.message,
							icon: 'error',
							confirmButtonText: 'OK'
						});
					}  

       // location.hash = page;
   }).fail(function(jqXHR, ajaxOptions, thrownError){
   	Swal.hideLoading();
   	Swal.fire({
   		title: 'Thất bại',
   		text: 'Có lỗi trong quá trình đăng ký, vui lòng thử lại sau!',
   		icon: 'error',
   		confirmButtonText: 'OK'
   	});

   });

}


});




	}
}