
$(document).ready(function(){
	var keyword = $('#input-search-hair-style').val();
	getAllHairStyle(0,keyword,true);
	$(document).on('click', '#box-all-hair-style .pagination a',function(event)
      {
        event.preventDefault();
        var keyword = $('#input-search-hair-style').val();
        $('#box-all-hair-style .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];
        getAllHairStyle(page,keyword,true);
    });

     $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $("#nav-image-infor").hasClass("visible");
        var isContain = clickover.closest("#nav-image-infor").length;
        if (_opened === true && !clickover.hasClass("navbar-toogleable") && !isContain && !clickover.hasClass("img-hair-style")) {
            $('#nav-image-infor').removeClass('visible');
    		$('#nav-image-infor').animate({'width':'toggle'});
        }
    });


    $(document).on('click','.box-hair-style .img-hair-style',function(){
    		var hairstyle = $(this).data('hair');

    		if(typeof hairstyle != 'undefined'){
				if($('#nav-image-infor').hasClass('visible')){
	    			$('#image-preview-hair-style').attr('src',$(this).attr('src'));
	    			$('#title-hair').text(hairstyle.title);
	    			$('#view-hair').text(hairstyle.view_count);
	    			$('#download-hair').text(hairstyle.download_count);
	    			$('#date-create-hair').text($(this).data('date'));
	    			
	    		}else{
	    			$('#image-preview-hair-style').attr('src',$(this).attr('src'));
	    			$('#title-hair').text(hairstyle.title);
	    			$('#view-hair').text(hairstyle.view_count);
	    			$('#download-hair').text(hairstyle.download_count);
	    			$('#date-create-hair').text($(this).data('date'));
	    			$('#nav-image-infor').addClass('visible');
	    			$('#nav-image-infor').animate({'width':'toggle'});
	    		}
    		}
    		
    		
    		
    });
    $('#img-close').click(function(){
    	$('#nav-image-infor').removeClass('visible');
    	$('#nav-image-infor').animate({'width':'toggle'});
    });
    $('#btn-search-hair-style').click(function(){
    	var keyword = $('#input-search-hair-style').val();
		getAllHairStyle(0,keyword,true);
    });
	$(document).on('click','.box-hair-style .btn-edit',function(){
		var hairstyle = $(this).data('hair');
		if(typeof hairstyle!='undefined'){
			Swal.fire({
						title:'Chỉnh sửa mẫu tóc',
						html:
						    `<input id="swal-input1" class="swal2-input"></input>`,
						onBeforeOpen:()=>{
						    $('#swal-input1').val(hairstyle.title);
						},
						showCancelButton: true,
			  			confirmButtonText: 'Lưu lại',
			  			cancelButtonText:'Hủy bỏ',
			  			preConfirm:function(){
			                var in1= $('#swal-input1').val(); // use user input value freely 
			                if(in1.trim()==''){
			                	Swal.showValidationMessage(
					                'Bạn chưa nhập tên mẫu tóc!'
					             );
			                	return false;
			                }else{
			                	return true;
			                }
			         		},
			}).then((result) => {
				if (result.value) {
					var in1= $('#swal-input1').val();
					updateHairStyle(in1.trim(),hairstyle.title,hairstyle.id);    
				}
			});
		}
		
	});
	$('#btnAddHairStyle').click(function(){
		var url = $(this).data('href');
        window.location = $(this).data('href');
	});

});
function updateHairStyle(newTitle, oldTitle,hair_id){
	if(newTitle==oldTitle){
		return;
	}
	Swal.fire({
			title:'Cập nhật mẫu tóc',
			onBeforeOpen:()=>{
			    Swal.showLoading()
			},
			onOpen :()=>{
				$.ajaxSetup({
               	 headers: {
                    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    	'Authorization':'Bearer ' + token,
                	}
           		 });
				$.ajax({
					type:'PATCH',
					url:BASE_URL+'api/v1/hairstyle/updateHairStyle',
			        data:{
			        	title:newTitle,
			        	hair_id:hair_id
			        },
			        success:function(data){
			        	Swal.hideLoading();
			        	if(data.success){
			        		showSuccess('Thành công','Lưu mẫu tóc thành công!');
							getAllHairStyle(0,'',true);
			        	}else{
			        		showError('Thất bại','Có lỗi trong quá trình lưu mẫu tóc, vui lòng thử lại sau!');
			        	}
			        },
			        error:function(jqXHR, ajaxOptions, thrownError){
	              		handleUnAuthenticated(jqXHR);
	              		Swal.hideLoading();
	              		showError('Thất bại','Có lỗi trong quá trình lưu mẫu tóc, vui lòng thử lại sau!');
	              	}
				});

			},
		});

}
function getAllHairStyle(page,keyword,withLoading){
	if(withLoading){
		$('#progress-loading').show();
	}
	$.ajax({
			headers: {
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/hairstyle/getAllHairStyle',
			datatype: "html",
			data:{
				salon_id:salon_id,
				keyword:keyword,
				page:page
			},
			success:function(data){
				$('#progress-loading').hide();
				$('#box-all-hair-style').empty().html(data);
			},
			 error:function(jqXHR, ajaxOptions, thrownError){
			 	$('#progress-loading').hide();
	             handleUnAuthenticated(jqXHR);
	       }

 	});
}