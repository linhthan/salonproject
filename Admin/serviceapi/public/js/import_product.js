var rowTable;


$(document).ready(function(){
	
	loadAllProduct();
	// loadAllProductCategory();
	var currentProducts = [];
	$('#product-box-select #product-select').selectpicker();
	$("#product-thumb").change(function(){
   		 readURL(this);
	});

	$(document).on('keyup','.form-import #currency-field',function(){
		formatCurrency($(this));
		var price = $(this).val();
		updatePriceProductCell($(this));
		updateTotalPrice();

	});
	$(document).on('blur','.form-import #currency-field',function(){
		formatCurrency($(this), "blur");
		updatePriceProductCell($(this));
		updateTotalPrice();
	});



	$(document).on('change','#table-order-info .selectpicker',function(){
			var product = $(this).find(':selected').data('product');
			if(product){
				var price = product.current_price;
				// var quantity = product.quantity;
				var cells = $(this).parents('tr').find('td');
				var inputPrice = cells.eq(2).find('#currency-field');
				inputPrice.val(price);
				formatCurrency(inputPrice);
				cells.eq(3).find('input').val(1);
				cells.eq(4).text(price);
				updateTotalPrice();
			}else{
				var cells = $(this).parent().parent().parent().parent().find('td');
				var inputPrice = cells.eq(2).find('#currency-field');
				inputPrice.val(0);
				cells.eq(3).find('input').val(0);
				cells.eq(4).text(0);
				updateTotalPrice();
			}
	});
	$('#btnSave').click(function(){
			var productSelected = getOrderProduct();
			if(productSelected.size==0){
				showError('Lỗi','Bạn chưa chọn sản phẩm nào cho đơn hàng');
			}else{
					var errorMsg = getErrorSelectedProduct(productSelected);
					if(errorMsg==''){
						log('yes')
						createOrder(productSelected);
					}else{
						showError('Lỗi',errorMsg);
					}
			}

	});
	$(document).on('click','#table-order-info .btn-remove',function(){
				$(this).parent().parent().remove();
				updateIndexRowTable();
				updateTotalPrice();
		});
	$(document).on('click','#table-order-info .btn-increase',function(){
				var product = $(this).parents('tr').find('select').find(':selected').data('product');
				var previous = $(this).parent().find('input').val();
					if(parseInt(previous)){
						var newQuantity = parseInt(previous)+1;
						$(this).parent().find('input').val(newQuantity);
						if(product){
							updatePriceProductCell($(this).parents('tr').find('td').eq(2).find('#currency-field'));
						}else{
							$(this).parents('tr').find('.total-price-cell').text(0);
						}
						updateTotalPrice();
					}

	});
	$(document).on('click','#table-order-info .btn-decrease',function(){
				var product = $(this).parents('tr').find('select').find(':selected').data('product');
				var previous = $(this).parent().find('input').val();
				if(parseInt(previous)>1){
					var newQuantity = parseInt(previous)-1;
					$(this).parent().find('input').val(newQuantity);
					if(product){
						var newTotalPrice = newQuantity * product.current_price;
						$(this).parents('tr').find('.total-price-cell').text(newTotalPrice);
						updatePriceProductCell( $(this).parents('tr').find('td').eq(2).find('#currency-field'));
					}else{
						
						$(this).parents('tr').find('.total-price-cell').text(0);
					}
					
					updateTotalPrice();
				}
				
				
	});

	$('#btnCancel').click(function(){
		var url = $(this).data('href');
        window.location = $(this).data('href');
	});

	$('#btn-add').click(function(){
		if(typeof rowTable!== 'undefined'){
			var rows = $('#table-order-info > tbody > tr');
				$('#table-order-info > tbody > tr').eq(rows.length-2).before(rowTable);
				$('#table-order-info .selectpicker').selectpicker();
				updateIndexRowTable();
		}
	});

	$('#order-name-customer').focus(function(){
		$(this).addClass('input-active');
		$('#error-name-customer').hide();
	});
	
	$('#order-phone-customer').focus(function(){
		$(this).addClass('input-active');
		$('#error-phone-number').hide();
	});
	$('#order-address').focus(function(){
		$(this).addClass('input-active');
		$('#error-address-customer').hide();
	});

	$('#order-name-customer').blur(function(){
		$(this).removeClass('input-active');
	});

	$('#order-phone-customer').blur(function(){
		$(this).removeClass('input-active');
	});
		
	$('#order-address').blur(function(){
		$(this).removeClass('input-active');
		
	});


});

function readURL(input) {
    if (input.files && input.files[0]) {
    	$('#error-thumb-product').hide();
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#thumb-product').attr('src', e.target.result);
            $('#thumb-product').show();
        }

        reader.readAsDataURL(input.files[0]);
    }else{
    	$('#thumb-product').attr('src','#');
    	$('#thumb-product').hide();
    }
}

function loadAllProduct(){

	$.ajax({
			headers: {
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/salon/getAllSalonProductForImport',
			data:{
				salon_id:salon_id,
			},
			success:function(data){

				rowTable = data;
				var rows = $('#table-order-info > tbody > tr');
				$('#table-order-info > tbody > tr').eq(rows.length-2).before(data);
				$('#table-order-info .selectpicker').selectpicker();
				updateIndexRowTable();

			},
			 error:function(jqXHR, ajaxOptions, thrownError){
	             handleUnAuthenticated(jqXHR);
	       }

 	});

}

// function loadAllProductCategory(){

// 	$.ajax({
// 			headers: {
// 	            'Authorization':'Bearer ' + token,
// 	            'X-Requested-With':'XMLHttpRequest'
// 	        },
// 			type:'GET',
// 			url:BASE_URL+'api/v1/salon/getAllProductCategorySalon',
// 			data:{
// 				salon_id:salon_id,
// 			},
// 			success:function(data){

// 				$('#category-product').empty().html(data);

// 			},
// 			 error:function(jqXHR, ajaxOptions, thrownError){
// 	             handleUnAuthenticated(jqXHR);
// 	       }

//  	});

// }


function addProductToOrderList(currentProducts,product){
	var found = false;
	var totalPrice = 0;
	currentProducts.forEach(function(item){
	  if(product.id==item.id){
	  	found = true;
	  	item.quantity += product.quantity;
	  }
	  totalPrice += item.price * item.quantity;
	});
	if(!found){
		currentProducts.push(product);
		totalPrice += product.quantity * product.price;
	}
	$('#table-order .total-price-row').find('td').eq(1).text(totalPrice);

}

function updateIndexRowTable(){
	var index = 1;
	rows = 	$('#table-order-info > tbody > tr > .index-cell');
	$.each(rows, function() {
		$(this).text(index);
		index++;
	});
}

function updateTotalPrice(){
	rows = 	$('#table-order-info > tbody > tr > .total-price-cell');
	var totalPrice = 0;
	$.each(rows, function() {
		var total = parseInt($(this).text());
		totalPrice += total;
	});
	$('#table-order-info > tbody >tr > .total-price').text(totalPrice);
	
	
}
function resetForm(){
	$('#product-title').val('');
	$('#origin-price').val('');
	$('#current-price').val('');
	$("#product-thumb").val('');
	$('#description-product').val('');
}

function getOrderProduct(){
	var mapProduct = new Map();
	var rows = $('#table-order-info > tbody > .item-order');
		$.each(rows,function(){
			var product = $(this).find('select').find(':selected').data('product');
			var quantity = $(this).find('.quantity-input').val();
			var selectedPrice = $(this).find('#currency-field').val();
			if(product){
				quantity = parseInt(quantity);
				if(mapProduct.has(product.id)){
					var productItem = mapProduct.get(product.id);
					productItem.selectQuantity += quantity;
				}else{
					mapProduct.set(product.id,new Product(product,quantity,selectedPrice));
				}
			}
		});
	return mapProduct;
	
}

function createOrder(productSelected){

	var dataRequest = {
		salon_id:salon_id,
		dataProduct:Object.fromEntries(productSelected)
	};
	var json = JSON.stringify(dataRequest);
	Swal.fire({
		title:'Nhập hàng',
		text:'Đang nhập hàng, vui lòng chờ',
		onBeforeOpen:()=>{
				Swal.showLoading()
			},
		onOpen:()=>{
					$.ajax({
						headers: {
				            'Authorization':'Bearer ' + token,
				        },
						type:'POST',
						url:BASE_URL+'api/v1/salon/importListProduct',
						data:json,
						success:function(data){
							Swal.hideLoading();
							if(data.success){
								
								var rowItems = $('#table-order-info > tbody').find('.item-order');
								$.each(rowItems,function(){
									$(this).remove();
								});
								var rows = $('#table-order-info > tbody > tr');
								$('#table-order-info > tbody > tr').eq(rows.length-2).before(rowTable);
								$('#table-order-info .selectpicker').selectpicker();
								updateIndexRowTable();
								showExportDialog(data.data);
							}else{
								showError('Thất bại',data.message);
							}
						},
						 error:function(jqXHR, ajaxOptions, thrownError){
						 	Swal.hideLoading();
				            handleUnAuthenticated(jqXHR);
				            showError('Thất bại','Có lỗi trong quá trình gửi yêu cầu, vui lòng thử lại');
				       }

			 	});
				
			},
	});

}

function getErrorSelectedProduct(mapProduct){
	var errorMsg ='';
	var productTitle='';
	mapProduct.forEach(function(product,key){
		if(!product.isValid()){
			if(productTitle==''){
				productTitle = productTitle.concat(`${product.title}`);
			}else{
				productTitle = productTitle.concat(`,${product.title}`);
			}
			
				
		}
	});
	if(productTitle!=''){
		errorMsg = `Thông tin nhập cho các sản phẩm ${productTitle} chưa hợp lệ \n`;
	}
	return errorMsg;
}
function showExportDialog(id_import){
	Swal.fire({
	  title: '<strong>Nhập hàng thành công</strong>',
	  icon: 'success',
	  html:
	    'Bạn có muốn in hóa đơn không?',
	  confirmButtonText:
	    '<i class="fa fa-file-word-o"></i> Xuất hóa đơn!',
	  confirmButtonAriaLabel: 'Thumbs up, great!',
	  cancelButtonText:
	    '<span>Hủy bỏ</span>',
	  cancelButtonAriaLabel: 'Thumbs down'
	}).then((result) => {
  if (result.value) {
 			 $.ajax({
						headers: {
				            'Authorization':'Bearer ' + token,
				        },
						type:'GET',
						url:BASE_URL+'api/v1/salon/exportImportListProduct',
						data:{
							import_id:id_import,
							salon_id:salon_id
						},

						success:function(data){
							// window.location = data.url;
							window.open(data.url,'_blank');
			                
						},
						 error:function(jqXHR, ajaxOptions, thrownError){
				            handleUnAuthenticated(jqXHR);
				            showError('Thất bại','Có lỗi trong quá trình gửi yêu cầu, vui lòng thử lại');
				  }

		});  	
  }
});
}


class Product {
  constructor(product,selectQuantity,selectedPrice) {
    this.id = product.id;
    this.title = product.title;
    this.price = product.current_price;
    this.quantity = product.quantity;
    this.selectQuantity = selectQuantity;
    this.selectedPrice = selectedPrice.split(",").join("");
  }

  isValid(){
  	return this.selectedPrice && parseInt(this.selectedPrice) > 0 && parseInt(this.selectedPrice)%1000==0;
  }
}

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // join number by .
    input_val =  left_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = input_val;
    
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

function updatePriceProductCell(input){
	var price = input.val();
	var row =  input.parents('tr').find('td');
	var quantity = row.eq(3).find('input').val();
	var totalPriceCell = row.eq(4);
		if(price){
			price = price.split(",").join("");
			if(quantity){
				totalPriceCell.text(parseInt(price) * quantity);
			}else{
				totalPriceCell.text(0);
			}
			
		}else{
			totalPriceCell.text(0);
		}
}