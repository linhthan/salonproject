


$(document).ready(function(){
	var id = $('#content-product-detail').data('id');
	loadProductById(id);
	$(document).on('change','.form-input #category-product',function(){
		if(this.value==-2){
			$('#form-input-category').val('');
			$('#form-input-category').show();
		}else{
			$('#form-input-category').val('');
			$('#form-input-category').hide();
		}
	});
	$("#product-thumb").change(function(){
   		 readURL(this);
	});
	$('#btnSave').click(function(){
		var url = $(this).data('href');
        window.location = $(this).data('href');
	});


	$('#product-title').focus(function(){
		$(this).addClass('input-active');
		$('#error-title-product').hide();
	});
	$('#input-category-product').focus(function(){
		$(this).addClass('input-active');
		$('#error-category-product').hide();
	});
	
	$('#origin-price').focus(function(){
		$(this).addClass('input-active');
		$('#origin-price-head').addClass('input-active');
		$('#error-origin-price').hide();
	});
	$('#current-price').focus(function(){
		$(this).addClass('input-active');
		$('#current-price-head').addClass('input-active');
		$('#error-current-price').hide();
	});
	$('#description-product').focus(function(){
		$(this).addClass('input-active');
		$('#error-description-product').hide();
		
	});

	$('#description-product').blur(function(){
		$(this).removeClass('input-active');
	});

	$('#input-category-product').blur(function(){
		$(this).removeClass('input-active');
	});

	$('#product-title').blur(function(){
		$(this).removeClass('input-active');
	});
		
	$('#origin-price').blur(function(){
		$(this).removeClass('input-active');
		$('#origin-price-head').removeClass('input-active');
	});
	$('#current-price').blur(function(){
		$(this).removeClass('input-active');	
		$('#current-price-head').removeClass('input-active');	
	});

});

function readURL(input) {
	console.log("read");
    if (input.files && input.files[0]) {
    	$('#error-thumb-product').hide();
        var reader = new FileReader();
		console.log("reader");
        reader.onload = function (e) {
        	console.log("load");
            $('#thumb-product').attr('src', e.target.result);
            $('#thumb-product').show();
        }

        reader.readAsDataURL(input.files[0]);
    }else{
    	$('#thumb-product').attr('src','#');
    	$('#thumb-product').hide();
    }
}

function checkErrorInput(){
	var title = $('#product-title').val();
	var originPrice = $('#origin-price').val();
	var currentPrice = $('#current-price').val();
	var description = $('#description-product').val();
	log($('#category-product').is(':visible'));
	if($('#category-product').is(':visible')){
		var catId =  $('#category-product').val();
		if(!catId){
			error = true;
		}
	}else{
		var categoryTitle =  $('#input-category-product').val();
		if(categoryTitle.trim()==''){
			error = true;
			$('#error-category-product').show();
		}else{
			$('#error-category-product').hide();
		}
	}
	
	var isFileSelected = $("#product-thumb").val();
	var error = false;
	if(title.trim()==''){
		error = true;
		$('#error-title-product').show();
	}else{
		$('#error-title-product').hide();
	}
	if(description.trim()==''){
		error = true;
		$('#error-description-product').show();
	}else{
		$('#error-description-product').hide();
	}
	// if(quantity.trim()==''){
	// 	error = true;
	// 	$('#error-quantity-product').show();
	// 	$('#error-quantity-product').text("Bạn chưa nhập số lượng sản phẩm");
	// }else if(Number(quantity)=='NaN' || Number(quantity)<=0){
	// 	$('#error-quantity-product').show();
	// 	$('#error-quantity-product').text("Số lượng sản phẩm không hợp lệ");
	// }else{
	// 	$('#error-quantity-product').hide();
	// }
	if(originPrice.trim()==''){
		error = true;
		$('#error-origin-price').show();
		$('#error-origin-price').text("Bạn chưa nhập giá gốc");
	}else if(Number(originPrice)=='NaN'||(originPrice % 1000)!=0){
		error = true;
		$('#error-origin-price').show();
		$('#error-origin-price').text("Giá gốc không hợp lệ");
	}else{
		$('#error-origin-price').hide();
	}

	if(originPrice.trim()==''){
		error = true;
		$('#error-current-price').show();
		$('#error-current-price').text("Bạn chưa nhập giá hiện tại");
	}else if(Number(originPrice)=='NaN'||(originPrice % 1000)!=0){
		error = true;
		$('#error-current-price').show();
		$('#error-current-price').text("Giá hiện tại không hợp lệ");
	}else{
		$('#error-current-price').hide();
	}
	if(isFileSelected){
		$('#error-thumb-product').hide();
	}else{
		error = true;
		$('#error-thumb-product').show();
	}
	return error;
}

function loadProductById(id){
	$('#progress-loading').show();
	$('#form-input-category').hide();
	$.ajax({
			headers: {
	            
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/salon/getProductById',
			datatype:'html',
			data:{
				id:id,
			},
			success:function(data){
				$('#progress-loading').hide();
				$('#grid-infor').empty().html(data);
				// if(data){
				// 	$('#category-product').empty().html(data);
				// 	var addNewOption = `<option value=\"-2\">Thêm mới</option>`;
				// 	$('#category-product').append(addNewOption);
				// 	$('#form-input-category').hide();
				// }else{
				// 	$('#form-input-category').show();

				// }
				
				
			},
			 error:function(jqXHR, ajaxOptions, thrownError){
			 	$('#progress-loading').hide();
	             handleUnAuthenticated(jqXHR);
	             $('#category-product').hide();
	             $('#form-input-category').show();
	       }

 	});

}

function showDialogCreateProduct(){
	Swal.fire({
			title:'Cập nhật sản phẩm',
			onBeforeOpen:()=>{
				Swal.showLoading();
			},
			onOpen :()=>{
				createProduct();
			},
		});
}
function createProduct(){
	var title = $('#product-title').val();
	var originPrice = $('#origin-price').val();
	var currentPrice = $('#current-price').val();
	var description = $('#description-product').val();
	var form_data = new FormData();
	form_data.append('image', $("#product-thumb")[0].files[0]);
	form_data.append('salon_id', salon_id);
	form_data.append('title',title);
	form_data.append('origin_price', originPrice);
	form_data.append('current_price', currentPrice);
	form_data.append('description', description);
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	var url='';
	if($('#form-input-category').is(':visible')){
		var categoryTitle =  $('#input-category-product').val();
		form_data.append('category_title', categoryTitle);
		url = BASE_URL+'api/v1/product/importProductWithNewCategory';
	}else{
		var catId =  $('#category-product').val();
		form_data.append('category_id', catId);
		url = BASE_URL+'api/v1/product/importProduct';
	}
	$.ajax({
			headers: {
	            'Accept':'application/json',
	            'Authorization':'Bearer ' + token,
	        },
			type:'POST',
			processData: false,
    		contentType: false,
	        cache : false,
			url:url,
			data:form_data,
			success:function(data){
				Swal.hideLoading();
				if(data.success){
					resetForm();
					Swal.fire({
		                  title: 'Thông báo',
		                  text: data.message,
		                  icon: 'success',
		                  confirmButtonText: 'OK'
			        });

				}else{
					Swal.fire({
		                  title: 'Thông báo',
		                  text: data.message,
		                  icon: 'error',
		                  confirmButtonText: 'OK'
			        });
				}

			},
			 error:function(jqXHR, ajaxOptions, thrownError){
			 	Swal.hideLoading();
	            handleUnAuthenticated(jqXHR);
	       }

 	});
}

function resetForm(){
	$('#product-title').val('');
	$('#origin-price').val('');
	$('#current-price').val('');
	$("#product-thumb").val('');
	$('#description-product').val('');
	$('#thumb-product').hide();
	$('#thumb-product').attr('src','#');

}