function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

var user_type = getCookie('user_type');

if(getCookie('token') && getCookie('salon_id') && user_type){
  if(user_type==2){
    window.location ='/admin';
  }else if(user_type==3){
    window.location ='/waiter';
  }
  
}
$(document).ready(function(){

    var showPass = 0;
    $(document).on('click', '#show-pass',function(event)
    {
      event.preventDefault();
      if(showPass == 0) {
        $(this).next('input').attr('type','text');
        $(this).find('i').removeClass('fa-eye');
        $(this).find('i').addClass('fa-eye-slash');
        showPass = 1;
    }
    else {
        $(this).next('input').attr('type','password');
        $(this).find('i').addClass('fa-eye');
        $(this).find('i').removeClass('fa-eye-slash');
        showPass = 0;
    }
    
});
 /*==================================================================
 [ Focus input ]*/
 $('.input100').each(function(){
    $(this).on('blur', function(){
        if($(this).val().trim() != "") {
            $(this).addClass('has-val');
        }
        else {
            $(this).removeClass('has-val');
        }
    })    
})


    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(event){
        event.preventDefault(); 


        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                return
            }
        }


        Swal.fire({
            title:'Đăng nhập',
            onBeforeOpen:()=>{
                Swal.showLoading()
            },
            onOpen :()=>{
               $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
               $.ajax(
               {
                type:'POST',
                url:'api/v1/loginEmail',
                data:{
                  'email':$('#email').val().trim(),
                  'password':$('#password').val().trim(),
              }

          }).done(function(data){
            Swal.hideLoading();
            if(data.success && data.data.salon_id){
              var salon_id = data.data.salon_id;
              var token  = data.data.token;
              var user_type = data.data.user.user_type;
              if(salon_id && token && user_type){
                if(user_type==2){
                  setCookie('token',token,1000);
                  setCookie('salon_id',salon_id,1000);
                  setCookie('user_type',user_type,1000);
                  setCookie('name',data.data.user.name,1000);
                  setCookie('avatar',data.data.user.avatar,1000);
                  window.location.href ='/admin';
                }else if(user_type==3){
                  setCookie('token',token,1000);
                  setCookie('salon_id',salon_id,1000);
                  setCookie('user_type',user_type,1000);
                  setCookie('name',data.data.user.name,1000);
                  setCookie('avatar',data.data.user.avatar,1000);
                  window.location.href ='/waiter';
                }else{
                    Swal.fire({
                      title: 'Thất bại',
                      text: 'Chỉ chủ salon và nhân viên có thể sử dụng tính năng này, vui lòng thử lại!',
                      icon: 'error',
                      confirmButtonText: 'OK'
               });
                }
                
              }else{
                Swal.fire({
                    title: 'Thất bại',
                    text: 'Có lỗi trong quá trình đăng nhập, vui lòng thử lại sau!',
                    icon: 'error',
                    confirmButtonText: 'OK'
               });
              }
             }else{
               Swal.fire({
                  title: 'Thất bại',
                  text: data.message,
                  icon: 'error',
                  confirmButtonText: 'OK'
              });
       }


       // location.hash = page;
   }).fail(function(jqXHR, ajaxOptions, thrownError){
      Swal.hideLoading();
       Swal.fire({
              title: 'Thất bại',
              text: 'Có lỗi trong quá trình đăng nhập, vui lòng thử lại sau!',
              icon: 'error',
              confirmButtonText: 'OK'
         });
  });
}


});




    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
         hideValidate(this);
     });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }

    /*==================================================================
    [ Show pass ]*/

});
