
$(document).ready(function(){

    var receipt_id = $('#box-content-container').data('id');

    if(receipt_id==0){
      $('#progress-loading').hide();
      $('#view-no-data').show();
    }else{
      $('#view-no-data').hide();

      $('#progress-loading').show();
      $.ajax(
      {
        type:'GET',
        headers: {
          'Authorization':'Bearer ' + token,
          'X-Requested-With':'XMLHttpRequest'
        },
        url:BASE_URL+'api/v1/salon/getOrderDetail',
        datatype: "html",
        data:{

          'order_id':receipt_id
        }

        }).done(function(data){
          $('#progress-loading').hide();
          $("#content-detail-receipt").empty().html(data);
       }).fail(function(jqXHR, ajaxOptions, thrownError){
         $('#progress-loading').hide();
         $('#view-no-data').show();
        handleUnAuthenticated(jqXHR);
      });

    }


      // $(document).on('click','#content-detail-receipt #btnProcess',function(){
      //    var discount = Number($('#receipt-discount-input').val().split(",").join(""));
      //   var discountPercent =Number($('#receipt-discount-percent-input').val());
      //   var customerPaid = Number($('#customer-paid-input').val().split(",").join(""));
      //   var totalPrice = Number($('#receipt-total-price-input').val());
      //   if(!checkInputValid(discount,discountPercent,customerPaid,totalPrice)){
      //       processReceipt(receipt_id,type,discount,discountPercent,totalPrice);
      //   }
      // });
     $(document).on('click','#content-detail-receipt #btnCancel',function(){
         var url = $(this).data('href');
           window.location = url;
      });

});

function checkInputValid(discount,discountPercent,customerPaid,totalPrice){
  var error = false;
  if(discount=='undefined' || discount%500!=0){
    var error = true;
    $('#receipt-discount-input-error').show();
  }

  if(discountPercent=='undefined'|| discountPercent<0 || discountPercent>100){
    var error = true;
    $('#receipt-discount-percent-input-error').show();
  }

  if(customerPaid=='undefined' || customerPaid%500!=0){
    var error = true;
    $('#customer-paid-input-error').show();
  }
  if(totalPrice=='undefined'||totalPrice > customerPaid){
    var error = true;
    $('#customer-paid-input-error').show();
  }

  return error;
}

function processReceipt(receipt_id,type,discount,discount_percent,totalPrice){
    var id_booking = type==1 ? receipt_id : null;
    var id_order = type==2 ? receipt_id : null;
   Swal.fire({
            title:'Xử lý hóa đơn',
            onBeforeOpen:()=>{
                Swal.showLoading()
            },
            onOpen:()=>{
              $.ajax({

                type:'POST',
                headers: {
                  'Accept':'application/json',
                  'Authorization':'Bearer ' + token,
                },
                url:BASE_URL+'api/v1/admin/createBill',
                data:{
                  'salon_id':salon_id,
                  'id_booking':id_booking,
                  'id_order':id_order,
                  'discount':discount,
                  'discount_percent':discount_percent,
                  'totalPrice':totalPrice
                },
                success:function(data){
                  Swal.hideLoading();
                  if(data.success){
                    showSuccessDialog(data.data);
                   // showExportDialog(data.id);
                  }else{
                    Swal.fire({
                      title: 'Thất bại',
                      text: data.message,
                      icon: 'error',
                      confirmButtonText: 'OK'
                   });
                  }
                },
                error:function(jqXHR, ajaxOptions, thrownError){
                  Swal.hideLoading();
                  handleUnAuthenticated(jqXHR);
                  Swal.fire({
                      title: 'Thất bại',
                      text: 'Có lỗi trong quá trình xử lý hóa đơn, vui lòng thử lại sau!',
                      icon: 'error',
                      confirmButtonText: 'OK'
                   });
                  
                }

              });
            }
    });
}

function showSuccessDialog(data){
    Swal.fire({
       title: 'Thành công',
        text: data.message,
        icon: 'success',
        confirmButtonText: 'OK',
        onClose:()=>{
          var url = $('#btnProcess').data('href') +`/${data.id}`;
          window.location = url;
        }
    });
}

function showExportDialog(billId){
  Swal.fire({
    title: '<strong>Nhập hàng thành công</strong>',
    icon: 'success',
    html:
      'Bạn có muốn in hóa đơn không?',
    showCloseButton: true,
    showCancelButton: true,
    focusConfirm: false,
    cancelButtonText:
      '<span>Hủy bỏ</span>',
    cancelButtonAriaLabel: 'Thumbs down',
    confirmButtonText:
      `<a href=\"{{route(\'export-receipts\', [\'id\' => ${billId}])}}\"><i class="fa fa-file-word-o"></i> Xuất hóa đơn!</a>`,
    confirmButtonAriaLabel: 'Thumbs up, great!',
    
  }).then((result)=>{
      log(result.value);
  });
}

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // join number by .
    input_val =  left_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = input_val;
    
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}
