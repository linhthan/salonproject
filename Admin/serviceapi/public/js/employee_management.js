

$(document).ready(function(){
	var filterOption = $('#filterOption').val();
	var keyword = $('#input-search-user').val();
	loadAllStaff(0,filterOption,keyword);

	

	$('#btnAddStaff').click(function(){
		var url = $(this).data('href');
		window.location = url;
	});
	$(document).on('click','.box-content .btn-remove',function(event){
			event.preventDefault();
			event.stopPropagation();
			Swal.fire({
			  title: 'Xóa nhân viên?',
			  text: "Nhân viên sẽ bị xóa!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Xóa!'
			}).then((result) => {
			  if (result.value) {
			   deleteStaff($(this).data('id'));
			  }
			});

	});

	$(document).on('click', '.box-content .pagination a',function(event)
      {
        event.preventDefault();
        var filterOption = $('#filterOption').val();
		var keyword = $('#input-search-user').val();
        $('.box-content .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var page=$(this).attr('href').split('page=')[1];
        loadAllStaff(page,filterOption,keyword);
      });

	$('#btn-search-user').click(function(){
		$('#box-content-data').hide();
	 	var filterOption = $('#filterOption').val();
		var keyword = $('#input-search-user').val();
	 	loadAllStaff(0,this.value,keyword);
	});
 	$(document).on('click','.box-content .item-table',function(event){
          event.preventDefault();
          var url = $(this).data('href');
          window.location = $(this).data('href');
    });
	 $('#filterOption').change(function(){
	 	$('#box-content-data').hide();
	 	var keyword = $('#input-search-user').val();
	 	loadAllStaff(0,this.value,keyword);
	 });

	});


function deleteStaff(idStaff){
	Swal.fire({
      title:'Xóa nhân viên',
            onBeforeOpen:()=>{
                Swal.showLoading()
            },
            onOpen:()=>{
              $.ajax({

                type:'PATCH',
                headers: {
                  'Accept':'application/json',
                  'Authorization':'Bearer ' + token,
                },
                url:BASE_URL+'api/v1/staff/deleteStaff',
                data:{
                  'user_id':idStaff,
                  'salon_id':salon_id
                },
                success:function(data){
                  Swal.hideLoading();
                  if(data.success){
	                  var filterOption = $('#filterOption').val();
					  var keyword = $('#input-search-user').val();
					  loadAllStaff(0,filterOption,keyword);
                    Swal.fire({
                      title: 'Thành công',
                      text: data.message,
                      icon: 'success',
                      confirmButtonText: 'OK'
                   });
                  }else{
                    Swal.fire({
                      title: 'Thất bại',
                      text: data.message,
                      icon: 'error',
                      confirmButtonText: 'OK'
                   });
                  }
                },
                error:function(jqXHR, ajaxOptions, thrownError){
                  Swal.hideLoading();
                  handleUnAuthenticated(jqXHR);
                  Swal.fire({
                      title: 'Thất bại',
                      text: 'Có lỗi xảy ra, vui lòng thử lại!',
                      icon: 'error',
                      confirmButtonText: 'OK'
                   });
                }

              });
            }
    });
}

function loadAllStaff(page,filterOption,keyword){
	$('#progress-loading').show();
	$.ajax({
			headers: {
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/staff/getAllStaff',
			datatype: "html",
			data:{
				salon_id:salon_id,
				filter:filterOption,
				keyword:keyword,
				page:page
			},
			success:function(data){
				$('#box-content-data').show();
				$('#progress-loading').hide();
				$('#box-content-data').empty().html(data);
			},
			 error:function(jqXHR, ajaxOptions, thrownError){
			 	$('#progress-loading').hide();
			 	console.log(thrownError);
			 	console.log(jqXHR);
	             handleUnAuthenticated(jqXHR);
	       }

 	});
 }