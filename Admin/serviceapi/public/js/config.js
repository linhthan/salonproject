var vnData =""
var start;
var configSalon;
$(document).ready(function () {
    readTextFile("../json/vn.json", function(text){
        vnData = JSON.parse(text);
        var data_option_city = "";
        for(var item in vnData){
            var dataCity = vnData[item];
            data_option_city+="<option value=\""+item +"\">";
            data_option_city+=dataCity.name;
            data_option_city+="<option>\n";
            if(item==0){
                changeCitySelected(item);
            }

        }
        if(data_option_city==""){
            data_option_city ="<option disabled=\"disabled\" selected=\"selected\">Chọn thành phố</option>\n";
        }
        $("#select-city-option").empty().html(data_option_city);
    });


    $('#view-no-data').hide();
    $('#box-content-salon').hide();
    
    
    $("#select-city-option").change(function(){
        changeCitySelected(this.value);

    });

    $("#salon-thumb").change(function(){
   		 readURL(this);
	});

   

    $('#icon-edit-cancel').click(function(){
    	if($(this).hasClass('edit-mode')){
    		if(configSalon){
    			$(this).removeClass('edit-mode');
	    		$("#box-config-salon :input").prop("disabled", true);
	    		$("#box-button").hide();
	    		$('.input-info-product').removeClass('input-active');
	    		start.enable(false);
	    		end.enable(false);
    			updateConfigData(configSalon,start,end);
    		}
    		
    	}else{
    		$(this).addClass('edit-mode');
    		$("#box-config-salon :input").prop("disabled", false);
    		$("#box-button :button").prop("disabled", false);
    		$("#box-button").show();
    		start.enable(true);
    		end.enable(true);
    		if(configSalon){
    			$('#box-content-salon').show();
    		}

    	}
    });
    $("#select-district-option").change(function(){
        var value = this.value;
        var dataCity = vnData[$("#select-city-option").val()];
        var dataDistricts =  dataCity.districts;
        var districtSelected = dataDistricts[value];
        changeDistrictSelected(districtSelected);
    });
    function startChange() {
        var startTime = start.value();

        if (startTime) {
            // startTime = new Date(startTime);

            // end.max(startTime);

            // startTime.setMinutes(startTime.getMinutes() + this.options.interval);

            // end.min(startTime);
            // end.value(startTime);
            $('#error-open-time-input').hide();
        }
    }
    function endChange() {
        var startTime = end.value();

        if (end) {
           $('#error-close-time-input').hide();
        }
    }
    //init start timepicker
    start = $("#open-time-input").kendoTimePicker({
        change: startChange
    }).data("kendoTimePicker");

    //init end timepicker
    var end = $("#close-time-input").kendoTimePicker({
        change:endChange
    }).data("kendoTimePicker");

    //define min/max range
    start.min("6:00 AM");
    start.max("22:00 PM");

    //define min/max range
    end.min("6:30 AM");
    end.max("22:00 PM");
    $("#box-config-salon :input").prop("disabled", true);
    $("#box-button").hide();
	$('.input-info-product').removeClass('input-active');
	start.enable(false);
	end.enable(false);
    getConfigSalon(start,end);

   	$('#btnSave').click(function(){
        var salonName = $('#salon-title').val();
        var count_seat = $('#count-seat-input').val();
        var avg_time_slot = $('#duration-slot-input').val();
        var startTime = start.value();
        var endTime = end.value();
        var city = $("#select-city-option option:selected").text();
        var district = $("#select-district-option option:selected").text();
        var ward =  $("#select-ward-option option:selected").text();
        var detail_location = $('#detail-location').val();
   		if(!hasError(salonName,count_seat
            ,avg_time_slot,startTime,endTime,city
            ,district,ward,detail_location)){
            var startDate = new Date(startTime);
            var endDate = new Date(endTime);
            startMinutes  = startDate.getHours()* 60 + startDate.getMinutes();
            endMinutes =  endDate.getHours()* 60 + endDate.getMinutes();
            showDialogSaveConfig(salonName,count_seat,avg_time_slot,startMinutes,endMinutes
                                ,city,district,ward,detail_location)

        }
   	});
    $('#btnDelete').click(function(){
        $('#salon-thumb').val('');
        $('#box-delete-thumb').hide();
        $('#thumb-salon').attr('src',configSalon.thumb_url?configSalon.thumb_url:'#');
         
    });
    initInputEvent($('#salon-title'),$('#error-salon-title'));
    initInputEvent($('#count-seat-input'),$('#error-count-seat-input'));
    initInputEvent($('#duration-slot-input'),$('#error-duration-slot-input'));
    initInputEvent($('#detail-location'),$('#error-detail-location'));
    initInputEvent($('#open-time-input'),$('#error-open-time-input'));
    initInputEvent($('#close-time-input'),$('#error-close-time-input'));

    try {
        var selectSimple = $('.js-select-simple');

        selectSimple.each(function () {
            var that = $(this);
            var selectBox = that.find('select');
            var selectDropdown = that.find('.select-dropdown');
            selectBox.select2({
                dropdownParent: selectDropdown
            });
        });

    } catch (err) {
        console.log(err);
    }

});
function initInputEvent(input, errorSpan){
	input.focus(function(){
        $(this).addClass('input-active');
        errorSpan.hide();
    });
   input.blur(function(){
        $(this).removeClass('input-active');
    });
}

function hasError(salonName,count_seat,avg_time_slot,startTime,endTime
    ,city,district,ward,detail_location) {

    var error = false;
	empty_title = checkInputEmpty(salonName,$('#error-salon-title'),'Bạn chưa nhập tên salon');
	empty_count_seat = checkInputEmpty(count_seat,$('#error-count-seat-input'),'Bạn chưa nhập số chỗ');
	empty_avg_time_slot= checkInputEmpty(avg_time_slot,$('#error-duration-slot-input'),'Bạn chưa nhập thời gian');
	empty_detail_location = checkInputEmpty(detail_location,$('#error-detail_location'),'Bạn chưa nhập chi tiết địa chỉ');
	empty_city = checkInputEmpty(city,$('#error-select-city-option'),'Bạn chưa chọn tỉnh/thành phố');
	empty_district = checkInputEmpty(district,$('#error-select-district-option'),'Bạn chưa chọn quận/huyện');
	empty_ward = checkInputEmpty(ward,$('#error-select-ward-option'),'Bạn chưa chọn phường/xã');
	invalidStart = checkTimeValid(startTime,$('#error-open-time-input'),'Thời gian mở cửa không hợp lệ');
	invalidEnd = checkTimeValid(endTime,$('#error-close-time-input'),'Thời gian đóng cửa không hợp lệ');
    errorLocation = empty_city && empty_district &&empty_ward;
    if(errorLocation){
        error = true;
    }

	error = empty_title && empty_count_seat && empty_avg_time_slot && empty_detail_location&&invalidStart &&invalidEnd
	if(error){
		return true;
	}else{
        if(typeof Number(count_seat)=='undefined'){
            $('#error-count-seat-input').text('Số chỗ ngồi chưa hợp lệ!');
            error = true;
        }
        if(typeof Number(avg_time_slot)=='undefined' || Number(avg_time_slot)<10 ){
            $('#error-duration-slot-input').text('Thời gian/chỗ chưa hợp lệ!');
            error = true;
        }
		var startSelect = new Date(startTime);
		var endSelect = new Date(endTime);
		log(startSelect.getMinutes());
        if(!invalidStart && !invalidEnd && startSelect >= endSelect){
            showError('Lỗi','Thời gian mở phải nhỏ hơn thời gian đóng cửa!');
            return false;
        }
        return error;
	}


}

function showDialogSaveConfig(salonName,count_seat,avg_time_slot,startTime,endTime
    ,city,district,ward,detail_location){
     Swal.fire({
            title:'Lưu cấu hình',
            onBeforeOpen:()=>{
                Swal.showLoading();
            },
            onOpen :()=>{
                saveSalonConfig(salonName,count_seat,avg_time_slot,startTime,endTime
                ,city,district,ward,detail_location);
            },
        });
}


function saveSalonConfig(salonName,count_seat,avg_time_slot,startTime,endTime
    ,city,district,ward,detail_location){

    var form_data = new FormData();
    form_data.append('image', $("#salon-thumb")[0].files[0]);
    form_data.append('salon_id', salon_id);
    form_data.append('salonName', salonName);
    form_data.append('count_seat',count_seat);
    form_data.append('avg_time_slot', avg_time_slot);
    form_data.append('startTime', startTime);
    form_data.append('endTime', endTime);
    form_data.append('city', city);
    form_data.append('district', district);
    form_data.append('ward', ward);
    form_data.append('detail_location', detail_location);

    $.ajax({
            headers: {
                'Authorization':'Bearer ' + token,
            },
            type:'POST',
            processData: false,
            contentType: false,
            cache : false,
            url:BASE_URL+'api/v1/salon/saveSalonConfig',
            data:form_data,
            success:function(data){
                Swal.hideLoading();
                if(data.success){
                    Swal.fire({
                          title: 'Thông báo',
                          text: data.message,
                          icon: 'success',
                          confirmButtonText: 'OK'
                    });

                }else{
                    Swal.fire({
                          title: 'Thông báo',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'OK'
                    });
                }

            },
             error:function(jqXHR, ajaxOptions, thrownError){
                Swal.hideLoading();
                handleUnAuthenticated(jqXHR);
                Swal.fire({
                          title: 'Thông báo',
                          text: 'Có lỗi trong quá trình cập nhật cấu hình, vui lòng thử lại sau!',
                          icon: 'error',
                          confirmButtonText: 'OK'
                    });
           }

    });
}


function checkTimeValid(value,errorSpan,errorMsg){
	if(!value){
		errorSpan.show();
		errorSpan.text(errorMsg);
		return true;
	}else{
		errorSpan.hide();
		return false;
	}
}
function checkInputEmpty(value,errorSpan,errorMsg){
	if(value.trim()==''){
		errorSpan.show();
		errorSpan.text(errorMsg);
		return true;
	}else{
		errorSpan.hide();
		return false;
	}
}

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

function changeCitySelected(idCity){
    var data_option_district ="";

    var dataCity = vnData[idCity];
    if(typeof dataCity == 'undefined'){
    	return;
    }
    var dataDistricts =  dataCity.districts;
    for(var item in dataDistricts){
        var itemDistrict = dataDistricts[item];
        if(item==0){
            changeDistrictSelected(itemDistrict);
        }
        data_option_district+="<option value=\""+item +"\">";
        data_option_district+=itemDistrict.name;
        data_option_district+="<option>\n";
    }


    $("#select-district-option").empty().html(data_option_district);
}
function changeDistrictSelected(itemDistrict){
    var data_option_ward ="";
    for(var idWard in itemDistrict.wards){
        var wardsName = itemDistrict.wards[idWard].name;
        data_option_ward+="<option value=\""+idWard +"\">";
        data_option_ward+=wardsName;
        data_option_ward+="<option>\n";
    }
    $("#select-ward-option").empty().html(data_option_ward);
}

function getConfigSalon(start,end){
	$('#view-no-data').hide();
	$('#box-content-data').show();
	$('#box-content-salon').hide();
	
	$.ajax(
  {
    type:'GET',
    headers: {
      'Authorization':'Bearer ' + token,
    },
    url:BASE_URL+'api/v1/salon/getSalonConfig',
    data:{
      'salon_id':salon_id,
    },
   success:function(data){
	$('#box-content-data').hide();
	
       console.log(data);
       if(data.success){
         var config = data.config;
         configSalon = data.config;
         updateConfigData(configSalon,start,end);
       }else{
       	$('#view-no-data').show();
       	$('#box-content-salon').hide();
       }
      },
       error:function(jqXHR, ajaxOptions, thrownError){
               handleUnAuthenticated(jqXHR);
               $('#box-config-salon').hide();
               $('#box-button').hide();
               $('#view-no-data').show();
         }

  });

}

function updateConfigData(config,start,end){
		 $('#salon-title').val(config.name);
		 $('#salon-thumb').val('');
         $('#count-seat-input').val(config.count_seat);
         $('#duration-slot-input').val(config.avg_time_slot);
         $('#detail-location').val(config.detail_location);
         $('#thumb-salon').attr('src',configSalon.thumb_url);
         $('#box-delete-thumb').hide();
       	 if(start && end){
       	 	start.value(convertMinutesToDisplay(config.open_time));
       	 	end.value(convertMinutesToDisplay(config.close_time));
       	 }
       	 var city = config.city_province;
       	 $.each($('#select-city-option').find('option'),function(){
       	 		if($(this).text() == city){
       	 			log('found: ' + $(this).text() +" " + $(this).val());
       	 			$('#select-city-option').val($(this).val());
       	 			// $('#select-city-option').select2('data', {id:  $(this).val(), text: city});
       	 			$("#select-city-option").select2().trigger('change');
       	 			return;
       	 		}
       	 });
       	 $('#box-content-salon').show();
}


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#thumb-salon').attr('src', e.target.result);
            $('#box-delete-thumb').show();
        }

        reader.readAsDataURL(input.files[0]);
    }else{
        $('#box-delete-thumb').hide();
    	if(configSalon){
    		$('#thumb-product').attr('src',configSalon.thumb_url);
    	}
    	
    }
}

function convertMinutesToDisplay(minutes){
    console.log("convert");
    console.log(minutes);
	var hours = Math.floor(minutes / 60);          
    var minutes = minutes % 60;
    minutes = minutes<10 ?"0"+minutes: minutes;
    var surfix = hours<12? "AM":"PM";
    hours = hours > 12? (hours - 12) : hours
    console.log(hours);
    return hours + ":" + minutes +" "+ surfix;
}
 



