


$(document).ready(function(){

	var filterOption = $('#filterOption').val();
	var keyword = $('#input-search-appointment').val();
	loadInfoBill();
	loadAllReciepts(0,filterOption,keyword);

	$(document).on('click', '.box-content .pagination a',function(event)
      {
        event.preventDefault();
        var filterOption = $('#filterOption').val();
		var keyword = $('#input-search-appointment').val();
        $('.box-content .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var page=$(this).attr('href').split('page=')[1];

        loadAllReciepts(page,filterOption,keyword);
      });

	$("#filterOption").change(function(){
		var keyword = $('#input-search-appointment').val();
		loadAllReciepts(0,this.value,keyword);
	});
	$('#btn-search-appoinment').click(function(){
		var keyword = $('#input-search-appointment').val();
		var filterOption = $('#filterOption').val();
		loadAllReciepts(0,filterOption,keyword);
	});

	$(document).on('click','.box-content .item-table',function(event){
          event.preventDefault();
          var url = $(this).data('href');
          window.location = $(this).data('href');
      });


});


function loadInfoBill(){
  $.ajax(
  {
    type:'GET',
    headers: {
      'Authorization':'Bearer ' + token,
    },
    url:BASE_URL+'api/v1/salon/getInfoBill',
    data:{
      'salon_id':salon_id,
    },
   success:function(data){

       console.log(data);
       if(data.success){
          $('#total-sale').text(data.total_sale+" VNĐ");
          $('#booking-paid').text(data.total_appointment);
          $('#order-paid').text(data.total_order);
          $('#pending-receipt').text(data.total_pending);
       }

      },
       error:function(jqXHR, ajaxOptions, thrownError){
               handleUnAuthenticated(jqXHR);
         }

  });
  
}

function loadAllReciepts(page,filterOption,keyword){
	$('#progress-loading').show();
	$.ajax({
			headers: {
	            'Authorization':'Bearer ' + token,
	            'X-Requested-With':'XMLHttpRequest'
	        },
			type:'GET',
			url:BASE_URL+'api/v1/salon/getAllReceiptSalon',
			datatype: "html",
			data:{
				salon_id:salon_id,
				filter:filterOption,
				keyword:keyword,
				page:page
			},
			success:function(data){
				$('#progress-loading').hide();
				$('#box-content-data').empty().html(data);
				var table = $('#table-receipt');
			},
			 error:function(jqXHR, ajaxOptions, thrownError){
			 	$('#progress-loading').hide();
			 	console.log(thrownError);
			 	console.log(jqXHR);
	             handleUnAuthenticated(jqXHR);
	       }

 	});

}

