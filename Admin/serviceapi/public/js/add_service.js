


$(document).ready(function(){

    $("#service-thumb").change(function(){
         readURL(this);
    });
    $('#btnSave').click(function(){
            if(!checkErrorInput()){

                showDialogCreateService();
            }
    });
    $('#btnCancel').click(function(){
        var url = $(this).data('href');
        window.location = $(this).data('href');
    });
    $('#service-title').focus(function(){
        $(this).addClass('input-active');
        $('#error-title-service').hide();
    });

    $('#service-price').focus(function(){
        $(this).addClass('input-active');
        $('#origin-price-head').addClass('input-active');
        $('#error-service-price').hide();
    });
    $('#duration-input').focus(function(){
        $(this).addClass('input-active');
        $('#current-price-head').addClass('input-active');
        $('#error-duration').hide();
    });
    $('#description-service').focus(function(){
        $(this).addClass('input-active');
        $('#error-description-service').hide();
        
    });

    $('#description-service').blur(function(){
        $(this).removeClass('input-active');
    });


    $('#service-title').blur(function(){
        $(this).removeClass('input-active');
    });
        
    $('#service-price').blur(function(){
        $(this).removeClass('input-active');
        $('#origin-price-head').removeClass('input-active');
    });
    $('#duration-input').blur(function(){
        $(this).removeClass('input-active');    
        $('#current-price-head').removeClass('input-active');   
    });

});

function readURL(input) {
    console.log("read");
    if (input.files && input.files[0]) {
        $('#error-thumb-service').hide();
        var reader = new FileReader();
        console.log("reader");
        reader.onload = function (e) {
            console.log("load");
            $('#thumb-service').attr('src', e.target.result);
            $('#thumb-service').show();
        }

        reader.readAsDataURL(input.files[0]);
    }else{
        $('#thumb-service').attr('src','#');
        $('#thumb-service').hide();
    }
}

function checkErrorInput(){
    var title = $('#service-title').val();
    var servicePrice = $('#service-price').val();
    var duration = $('#duration-input').val();
    var description = $('#description-service').val();
    
    var error = false;
    if(title.trim()==''){
        error = true;
        $('#error-title-service').show();
    }else{
        $('#error-title-service').hide();
    }
    if(description.trim()==''){
        error = true;
        $('#error-description-service').show();
    }else{
        $('#error-description-service').hide();
    }
  
    if(servicePrice.trim()==''){
        error = true;
        $('#error-service-price').show();
        $('#error-service-price').text("Bạn chưa nhập giá dịch vụ");
    }else if(Number(servicePrice)=='NaN'||(servicePrice % 1000)!=0){
        error = true;
        $('#error-service-price').show();
        $('#error-service-price').text("Giá gốc không hợp lệ");
    }else{
        $('#error-service-price').hide();
    }

    if(duration.trim()==''){
        error = true;
        $('#error-duration').show();
        $('#error-duration').text("Bạn chưa nhập thời gian dịch vụ");
    }else if(Number(duration)=='NaN'||duration <0 || duration>300 ){
        error = true;
        $('#error-duration').show();
        $('#error-duration').text("Thời gian không hợp lệ");
    }else{
        $('#error-duration').hide();
    }
    return error;
}



function showDialogCreateService(){
    Swal.fire({
            title:'Tạo dịch vụ',
            onBeforeOpen:()=>{
                Swal.showLoading();
            },
            onOpen :()=>{
                createService();
            },
        });
}
function createService(){
    var title = $('#service-title').val();
    var servicePrice = $('#service-price').val();
    var duration = $('#duration-input').val();
    var description = $('#description-service').val();
    var form_data = new FormData();
    form_data.append('image', $("#service-thumb")[0].files[0]);
    form_data.append('salon_id', salon_id);
    form_data.append('title',title);
    form_data.append('servicePrice', servicePrice);
    form_data.append('duration', duration);
    form_data.append('description', description);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
            headers: {
                'Accept':'application/json',
                'Authorization':'Bearer ' + token,
            },
            type:'POST',
            processData: false,
            contentType: false,
            cache : false,
            url:BASE_URL+'api/v1/salon/createService',
            data:form_data,
            success:function(data){
                Swal.hideLoading();
                if(data.success){
                    resetForm();
                    Swal.fire({
                          title: 'Thông báo',
                          text: data.message,
                          icon: 'success',
                          confirmButtonText: 'OK'
                    });

                }else{
                    Swal.fire({
                          title: 'Thông báo',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'OK'
                    });
                }

            },
             error:function(jqXHR, ajaxOptions, thrownError){
                Swal.hideLoading();
                handleUnAuthenticated(jqXHR);
                Swal.fire({
                          title: 'Thông báo',
                          text: 'Có lỗi xảy ra, vui lòng thử lại sau',
                          icon: 'error',
                          confirmButtonText: 'OK'
                    });
           }

    });
}

function resetForm(){
    $('#service-title').val('');
    $('#service-price').val('');
    $('#duration-input').val('');
    $("#service-thumb").val('');
    $('#description-service').val('');
    $('#thumb-service').hide();
    $('#thumb-service').attr('src','#');
    
}