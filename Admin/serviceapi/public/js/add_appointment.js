var listServices = [];
var daysMin = ["CN", "T2", "T3", "T4", "T5", "T6", "T7"];
var dayOfWeeks = ["8", "2", "3", "4", "5", "6", "7"];
var currentStep = 1;
$(document).ready(function(){
	var salon_id = getCookie('salon_id');
	var token  =  getCookie('token');
	var dateNow = new Date();
	var weekAfter =  new Date();
	weekAfter.setDate(weekAfter.getDate()+6);
	$('[data-toggle="datepicker"]').datepicker({
		autoHide: true,
		zIndex: 2048,
		days: ["Chủ Nhật", "Thứ Hai", "Thứ Ba", "Thứ tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy"],
		daysShort: ["CN", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"],
		daysMin: daysMin,
		months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
		monthsShort: ["Th1", "Th2", "Th3", "Th4", "Th5", "Th6", "Th7", "Th8", "Th9", "Th10", "Th11", "Th12"],
		today: "Hôm nay",
		startDate:dateNow,
		endDate:weekAfter,
		clear: "Xóa",
		format: "dd/mm/yyyy"
	});
	$('#view-no-data').hide();
	$('#header-logo').show();
	loadStylistSalon();
	updateShowHideCart();
	$('#progress-loading').hide();
	updateDateSelected();
	$('[data-toggle="datepicker"]').on('pick.datepicker',function(e){
			updateDateSelected();
			loadStylistSalon();
			loadTimeSlotAvailable();

	});

	$("#select-stylist-option").change(function(){
		changeStylistSelected(this.value);
		
	});

	$('#btn-previous-date').click(function(){
			var date = $('[data-toggle="datepicker"]').datepicker('getDate');
			date.setDate(date.getDate() -1);
			$('[data-toggle="datepicker"]').datepicker('setDate',date);


	});

	$('#btn-next-date').click(function(){
			var date = $('[data-toggle="datepicker"]').datepicker('getDate');
			date.setDate(date.getDate() +1);
			$('[data-toggle="datepicker"]').datepicker('setDate',date);

	});
	$('#btn-search-user').click(function(){
		console.log('click');
		var keyword = $('#input-search-user').val();
		console.log(keyword);

		if(keyword==''){
			$('.bs-service-item').removeClass('service-hide');
		}else{
			$.each($('.bs-service-item'),function(){
				var titleService = $(this).find('.service-name').text();
				if(S(titleService).latinise().s.toLowerCase().includes(S(keyword).latinise().s.toLowerCase())){
					$(this).removeClass('service-hide');
				}else{
					$(this).addClass('service-hide');
				}
			});

		}
		if($('.service-hide').length==$('.bs-service-item').length){
				$('#view-no-data').show();
		}else{
				$('#view-no-data').hide();
		}
	});
	var stepper_element = document.querySelector('#stepper1');
	var stepper = new Stepper(stepper_element);

	$.ajax({
		type:'GET',
		headers: {
            'X-Requested-With':'XMLHttpRequest',
            'Authorization':'Bearer ' + token,
          },
		url:BASE_URL+'api/v1/salon/getSalonService',
		datatype: "html",
		data:{
			'salon_id':salon_id,
		},
		success:function(data){
			$("#content-services").empty().html(data);
		},
		error:function(jqXHR, ajaxOptions, thrownError){
			handleUnAuthenticated(jqXHR);
		}

	});
	$("#content-services").on("click", ".btn-add-service", function(){
    	if($(this).hasClass('added')){
    		$(this).removeClass('added');
    		var serviceId = $(this).data('service');
    		var removeIndex = listServices.map(function(item) { return item.id; })
                       .indexOf(serviceId);
            listServices.splice(removeIndex, 1);

    	}else{
    		$(this).addClass('added');
    		listServices.push({
    			'id':$(this).data('service'),
    			'duration':$(this).data('duration'),
    			'price':$(this).data('price'),
    			'name':$(this).data('name')
    		});

    	}
    
    	updateShowHideCart();
	});



	$('#btn-next-step1').click(function(){
		if(checkDoneStep1()){
			changeStylistSelected($("#select-stylist-option").val());
			showStep2();	
		}else{
			Swal.fire({
                  title: 'Thông báo',
                  text: 'Bạn chưa chọn dịch vụ nào!',
                  icon: 'error',
                  confirmButtonText: 'OK'
             });
		}
	});

	$('.calendar-date-box').on("click", ".item-time", function(){ 
			if( $(this).hasClass('expired')){
				Swal.fire({
                  title: 'Thông báo',
                  text: 'Không còn chỗ trống, vui lòng chọn thời gian khác!',
                  icon: 'error',
                  confirmButtonText: 'OK'
              });
			}else if($(this).hasClass('selected')){
				$(this).removeClass('selected');
				$('#appointment-time').text('Chưa chọn');
			}else{
				$('.calendar-date-box .item-time').removeClass('selected');
				$(this).addClass('selected');
				$('#appointment-time').text($(this).text());
			}
			
	});


	 $('#btn-sign-out').click(function(){
        $.ajax(
        {
          type:'POST',
          headers: {
            'Accept':'application/json',
            'Authorization':'Bearer ' + token,
          },
          url:BASE_URL+'api/v1/admin/logout',

        }).done(function(data){
       // location.hash = page;
       eraseCookie('token');
       eraseCookie('user_type');
       eraseCookie('salon_id');
       window.location = '/login';

     }).fail(function(jqXHR, ajaxOptions, thrownError){
     handleUnAuthenticated(jqXHR);
    });

   });


	$('#btn-back').click(function(){
		showStep1();
	});
	$('#btn-next').click(function(){
		showStep3();
	});

	$('#tab-step-1').click(function(){
		showStep1();
	});
	$('#tab-step-2').click(function(){
		if(checkDoneStep1()){
			showStep2();	
		}else{
			Swal.fire({
                  title: 'Thông báo',
                  text: 'Bạn chưa chọn dịch vụ nào!',
                  icon: 'error',
                  confirmButtonText: 'OK'
             });
		}

	});
	$('#tab-step-3').click(function(){
		if(!checkDoneStep1()){
			Swal.fire({
                  title: 'Thông báo',
                  text: 'Bạn chưa chọn dịch vụ nào!',
                  icon: 'error',
                  confirmButtonText: 'OK'
             });	
		}else if(!checkDoneStep2()){
			Swal.fire({
                  title: 'Thông báo',
                  text: 'Bạn chưa chọn thời gian!',
                  icon: 'error',
                  confirmButtonText: 'OK'
             });
		}else{
			showStep3();
		}
	});
	$('#btn-apply-booking').click(function(){
			applyBooking();
	});
	$('#btn-back-to-step-2').click(function(){
		showStep2();
	});

	try {
		var selectSimple = $('.js-select-simple');

		selectSimple.each(function () {
			var that = $(this);
			var selectBox = that.find('select');
			var selectDropdown = that.find('.select-dropdown');
			selectBox.select2({
				dropdownParent: selectDropdown
			});
		});

	} catch (err) {
		console.log(err);
	}


});

function showStep1(){
	currentStep =1;
	updateStepChange(currentStep);
	$('#content-step-1').show();
	$('#content-step-2').hide();
	$('#content-step-3').hide();
	$('#tab-step-2').removeClass('active');
	$('#tab-step-3').removeClass('active');

}

function showStep2(){
	currentStep = 2;
	updateStepChange(currentStep);
	$('#content-step-1').hide();
	$('#content-step-3').hide();
	$('#tab-step-2').addClass('active');
	$('#tab-step-3').removeClass('active');
	$('#content-step-2').show();
	
}
function showStep3(){
	if(checkDoneStep2()){
		currentStep = 3;
		updateStepChange(currentStep);
		bindDataServiceToTable();
		$('#content-step-1').hide();
		$('#content-step-2').hide();
		$('#tab-step-3').addClass('active');
		$('#content-step-3').show();
	}else{
		Swal.fire({
                  title: 'Lỗi',
                  text: 'Bạn chưa chọn thời gian!',
                  icon: 'error',
                  confirmButtonText: 'OK'
        });
	}
	
}
function updateShowHideCart(){
	if(typeof listServices !== 'undefined' && listServices.length > 0){
		$('#cart-service').show();
		var totalPrice = getTotalPriceService();

		$('#total-price').text(totalPrice/1000 +"K");
		$('#number-service').text(listServices.length);
		$('#number-service-span').text(listServices.length);
		$('#desc-cart').show();

	}else{
		$('#cart-service').hide();
		$('#number-service').text(0);
		$('#desc-cart').hide();
	}
}

function loadStylistSalon(){
	var daySelect = $('[data-toggle="datepicker"]').datepicker('getDayName', true, true);
	var dayOfWeek = dayOfWeeks[daysMin.indexOf(daySelect)];
	$.ajax({
		type:'GET',
		headers: {
            'X-Requested-With':'XMLHttpRequest',
            'Authorization':'Bearer ' + token,
          },
		url:BASE_URL+'api/v1/salon/getStylistAvailable',
		datatype: "html",
		data:{
			'salon_id':salon_id,
			'day_of_week':dayOfWeek,
		},
		success:function(data){
			if(data && data.data){
				bindStylistData(data.data);
			}
		},
		error:function(jqXHR, ajaxOptions, thrownError){
			handleUnAuthenticated(jqXHR);
		}

	});
	
}

function bindStylistData(dataStylists){
		var data_option_stylist = "<option value=\"-1\">Chọn ngẫu nhiên thợ</option>\n";
		for(var item in dataStylists){

			var dataStylist = dataStylists[item];
			console.log(dataStylist.id);
			data_option_stylist+="<option value=\""+dataStylist.id +"\">";
			data_option_stylist+=dataStylist.name;
			data_option_stylist+="</option>\n";

		}
		$("#select-stylist-option").empty().html(data_option_stylist);
}

function loadTimeSlotAvailable(){
	var currentSelected = $('[data-toggle="datepicker"]').datepicker('getDate');
	var dateSelected = formatDate(currentSelected);
	var totalDuration = listServices.reduce(function(a,b){
				return a + b.duration;
		},0);
	$('#box-time-appoinment').empty();
	$('#progress-loading').css('display','inline-block');
	$.ajax({
		type:'GET',
		headers: {
            'X-Requested-With':'XMLHttpRequest',
            'Authorization':'Bearer ' + token,
          },
		url:BASE_URL+'api/v1/salon/getSalonSlotAvailable',
		datatype: "html",
		data:{
			'salon_id':salon_id,
			'date':dateSelected,
			'duration':totalDuration
		},
		success:function(data){
			$('#progress-loading').hide();
			$('#box-time-appoinment').empty().html(data);
		},
		error:function(jqXHR, ajaxOptions, thrownError){
			$('#progress-loading').hide();
			handleUnAuthenticated(jqXHR);
		}

	});

}

function loadTimeSlotAvailableByStylist(stylistId){
	var currentSelected = $('[data-toggle="datepicker"]').datepicker('getDate');
	var dateSelected = formatDate(currentSelected);
	var totalDuration = listServices.reduce(function(a,b){
				return a + b.duration;
		},0);
	$('#box-time-appoinment').empty();
	$('#progress-loading').css('display','inline-block');
	$.ajax({
		type:'GET',
		headers: {
            'X-Requested-With':'XMLHttpRequest',
            'Authorization':'Bearer ' + token,
          },
		url:BASE_URL+'api/v1/salon/getSalonSlotByStylistAvailable',
		datatype: "html",
		data:{
			'salon_id':salon_id,
			'date':dateSelected,
			'duration':totalDuration,
			'stylist_id':stylistId
		},
		success:function(data){
			$('#progress-loading').hide();
			$('#box-time-appoinment').empty().html(data);
		},
		error:function(jqXHR, ajaxOptions, thrownError){
			$('#progress-loading').hide();
			handleUnAuthenticated(jqXHR);
		}

	});

}

function updateDateSelected(){
	var dateSelectedString = $('[data-toggle="datepicker"]').datepicker('getDate',true);
	var date = $('[data-toggle="datepicker"]').datepicker('getDate');
	$('#current-date-selected').text(dateSelectedString);
	$('#appointment-date').text(dateSelectedString);
	var dateNow = new Date();
	var dateAfterWeek = new Date(); 
	dateAfterWeek.setDate(dateAfterWeek.getDate() + 6);
	dateNow.setHours(0, 0, 0, 0);
	dateAfterWeek.setHours(0, 0, 0, 0);
	if ( date.getTime() == dateNow.getTime()) {
		$('#date-title').text('Hôm nay');
		$('#btn-previous-date').prop('disabled', true);
		$('#btn-previous-date').css('color', '#ccc');
	}else{
		$('#btn-previous-date').prop('disabled', false);
		$('#btn-previous-date').css('color', 'black');
		$('#date-title').text($('[data-toggle="datepicker"]').datepicker('getDayName'));
	}
	if(date.getTime() == dateAfterWeek.getTime()){
		$('#btn-next-date').prop('disabled', true);
		$('#btn-next-date').css('color', '#ccc');
	}else{
		$('#btn-next-date').prop('disabled', false);
		$('#btn-next-date').css('color', 'black');
	}
}

function formatDate(date){
  var monthNames = [
    "1", "2", "3",
    "4", "5", "6", "7",
    "8", "9", "10",
    "11", "12"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return year+'/'+monthNames[monthIndex]+'/'+day;
}


function changeStylistSelected(stylistId){
	if(stylistId==-1){
		loadTimeSlotAvailable();
	}else{
		loadTimeSlotAvailableByStylist(stylistId);
	}
}

function updateStepChange(step){
	if(step==1){
		$('#line-step-1').css('background-color','#CFD3D7');
		$('#line-step-2').css('background-color','#CFD3D7');
		$('#box-time-appoinment .item-time').removeClass('selected');

	}else if(step==2){
		$('#line-step-1').css('background-color','#007BFF');
		$('#line-step-2').css('background-color','#CFD3D7');
	}else{
		$('#line-step-1').css('background-color','#007BFF');
		$('#line-step-2').css('background-color','#007BFF');
	}
}

function checkDoneStep1(){
	return typeof listServices !== 'undefined' && listServices.length > 0;
}

function checkDoneStep2(){
	var result = false;
	$('#box-time-appoinment .item-time').each(function(){
 		if($(this).hasClass('selected')){
 			result = true;
 			return false;
 		}
	});
	return result;

}

function bindDataServiceToTable(){
	var datatable  = "";
	var i =0;
	var totalPrice = getTotalPriceService();
	listServices.forEach(function(item){
		i += 1;
		
		datatable=datatable.concat("<tr>");
		datatable=datatable.concat("<th scope=\"row\">");
		datatable=datatable.concat(i.toString());
		datatable=datatable.concat("</th>\n");
		datatable=datatable.concat("<td>");
		datatable=datatable.concat(item.name);
		datatable=datatable.concat("</td>\n");
		datatable=datatable.concat("<td>");
		datatable=datatable.concat((item.price/1000).toString());
		datatable=datatable.concat("K");
		datatable=datatable.concat("</td>\n");
		datatable=datatable.concat("</tr>\n");
	});
	datatable=datatable.concat("<tr class=\"total-price-row\">");
	datatable=datatable.concat("<td colspan=\"2\"> Tổng thanh toán");
	datatable=datatable.concat("</td>\n");
	datatable=datatable.concat("<td>");
	datatable=datatable.concat((totalPrice/1000).toString());
	datatable=datatable.concat("K");
	datatable=datatable.concat("</td>\n");
	datatable=datatable.concat("</tr>\n");
	$('#table-service-content').empty().html(datatable);
}

function getTotalPriceService(){
	return	listServices.reduce(function(a,b){
				return a + b.price;
		},0);
}

function applyBooking(){

	var services =[];
	var totalDuration = 0;
	listServices.forEach(function(item){
		services.push({
			id:item.id,
			price:item.price
		});
		totalDuration +=item.duration;
	});
	var currentSelected = $('[data-toggle="datepicker"]').datepicker('getDate');
	var date_booking = formatDate(currentSelected);
	var time_booking = $('#box-time-appoinment .selected').data("minute");
	var phone_number =$('#phoneNumber').val().trim();
	var cus_name = $('#fullname').val().trim();
	if(cus_name==''){
		Swal.fire({
		     title: 'Thất bại',
		     text: 'Bạn chưa nhập tên khách hàng!',
		     icon: 'error',
		     confirmButtonText: 'OK'
		});
	}else if(phone_number==''){
		Swal.fire({
		     title: 'Thất bại',
		     text: 'Bạn chưa nhập số điện thoại!',
		     icon: 'error',
		     confirmButtonText: 'OK'
		});
		
	}else if(!validatePhoneNumber(phone_number)){
		Swal.fire({
		     title: 'Thất bại',
		     text: 'Số điện thoại không hợp lệ!',
		     icon: 'error',
		     confirmButtonText: 'OK'
		});

	}else{

		Swal.fire({
            title:'Đặt lịch',
            onBeforeOpen:()=>{
                Swal.showLoading()
            },
            onOpen :()=>{
               $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
               $.ajax({
	                type:'POST',
	                url:BASE_URL+'api/v1/salon/applyBooking',
	                headers: {
			            'Accept':'application/json',
			            'Authorization':'Bearer ' + token,
			        },
			        dataType: "json",
	                data:{
	                  'salon_id':salon_id,
	                  'services':services,
	                  'time_booking':time_booking,
	                  'date_booking':date_booking,
	                  'duration':totalDuration,
	                  'phone_number':phone_number,
	                  'cus_name':cus_name,
	              	},
	              	success:function(data){
	              		 Swal.hideLoading();
	              		 if(data.success){
	              		 	Swal.fire({
			                    title: 'Thành công',
			                    text:'Đặt lịch thành công',
			                    icon: 'success',
			                    confirmButtonText: 'OK'
			               }).then((result) => {
							  if (result.value) {
							  	listServices =[];
							  	$("#content-services .btn-add-service").removeClass('added');
							  	$("#fullname").val("");
							  	$("#phoneNumber").val("");
							  	$('#table-service-content').empty();
							  	$('#cart-service').hide();
							    showStep1();
							  }
							});
	              		 }else{
	              		 	Swal.fire({
			                    title: 'Thất bại',
			                    text: data.message,
			                    icon: 'error',
			                    confirmButtonText: 'OK'
			               });
	              		 }
	              	},
	              	error:function(jqXHR, ajaxOptions, thrownError){
	              		 handleUnAuthenticated(jqXHR);
	              		 Swal.hideLoading();
	              		 Swal.fire({
		                    title: 'Thất bại',
		                    text: 'Có lỗi trong quá trình đặt lịch, vui lòng thử lại sau!',
		                    icon: 'error',
		                    confirmButtonText: 'OK'
		               });
	              	}
          		});
	}

	});

	}



	
}

function validatePhoneNumber(source){
	var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
		if(source !==''){
			if (vnf_regex.test(source) == false) 
			{
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
}
