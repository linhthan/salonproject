


$(document).ready(function(){

    $('.day-working-item').on('click',function(){
        var dataType = $(this).data('type');
        if(dataType==1){
            $('.day-working-item').removeClass('active-item');
            $(this).addClass('active-item');
        }else{
           if($(this).hasClass('active-item')){
            $(this).removeClass('active-item');
            }else{
                $(this).addClass('active-item');
            }
            var itemActive =  $('.active-item').length;
            if($('#day-item-all').hasClass('active-item')){
                itemActive -=1;
            }
            if(itemActive==0|| itemActive==7){
                $('.day-working-item').removeClass('active-item');
                $('#day-item-all').addClass('active-item');
            }else{
                if(itemActive < 7){
                   $('#day-item-all').removeClass('active-item'); 
                } 
            }
            
        }
        
    });

    $('#user-type-y').change(function(){
        log(this.value);
    });
    var showPass = 0;
    $(document).on('click', '#show-pass',function(event)
        {
          event.preventDefault();
          if(showPass == 0) {
            $(this).next('input').attr('type','text');
            $(this).find('i').removeClass('fa-eye');
            $(this).find('i').addClass('fa-eye-slash');
            showPass = 1;
        }
        else {
            $(this).next('input').attr('type','password');
            $(this).find('i').addClass('fa-eye');
            $(this).find('i').removeClass('fa-eye-slash');
            showPass = 0;
        }
        
    });


    $("#user-avatar-input").change(function(){
         readURL(this);
    });

    $('#btnSave').click(function(){
            if(!checkErrorInput()){
                 var itemActive =  $('.active-item').length;
                 if(itemActive==0){
                    showError('Lỗi','Bạn chưa chọn thời gian làm việc của nhân viên');
                 }else{
                    showDialogCreateStaff();
                 }
                
            }
    });
    $('#btnCancel').click(function(){
        var url = $(this).data('href');
        window.location = $(this).data('href');
    });
    $('#working-time-select').click(function(){
        $('#myModal').modal('show');
    });

    $('#user-name').focus(function(){
        $(this).addClass('input-active');
        $('#error-user-name').hide();
    });
    $('#user-email').focus(function(){
        $(this).addClass('input-active');
        $('#error-user-email').hide();
    });
    
    $('#user-phone-number').focus(function(){
        $(this).addClass('input-active');
        $('#error-user-phone-number').hide();
    });

    $('#user-password').focus(function(){
        $(this).addClass('input-active');
        $('#error-user-password').hide();
    });




    $('#user-email').blur(function(){
        $(this).removeClass('input-active');
    });

    $('#user-name').blur(function(){
        $(this).removeClass('input-active');
    });
        
    $('#user-phone-number').blur(function(){
        $(this).removeClass('input-active');
    });
    $('#user-password').blur(function(){
        $(this).removeClass('input-active');    
    });

});

function readURL(input) {
    console.log("read");
    if (input.files && input.files[0]) {
        $('#error-avatar-user').hide();
        var reader = new FileReader();
        console.log("reader");
        reader.onload = function (e) {
            console.log("load");
            $('#avatar-user').attr('src', e.target.result);
            $('#avatar-user').show();
        }

        reader.readAsDataURL(input.files[0]);
    }else{
        $('#avatar-user').attr('src','#');
        $('#avatar-user').hide();
    }
}

function checkErrorInput(){
    var user_name = $('#user-name').val();
    var email = $('#user-email').val();
    var phone_number = $('#user-phone-number').val();
    var password = $('#user-password').val();

    var error = false;
    if(user_name.trim()==''){
        error = true;
        $('#error-user-name').show();
    }else{
        $('#error-user-name').hide();
    }
    if(email.trim()==''){
        error = true;
        $('#error-user-email').show();
    }else{
        $('#error-user-email').hide();
    }
    if(password.trim()==''){
        error = true;
        $('#error-user-password').text('Bạn chưa nhập mật khẩu');
        $('#error-user-password').show();
    }else if(password.trim().length <6){
        error = true;
        $('#error-user-password').text('Mật khẩu tối thiểu là 6 kí tự');
        $('#error-user-password').show();
    }else{
         $('#error-user-password').hide();
    }
    if(email.trim()==''){
        error = true;
        $('#error-user-email').text('Bạn chưa nhập email');
        $('#error-user-email').show();
    }else if(!checkEmailValid(email)){
          error=true;
        $('#error-user-email').text('Email không hợp lệ');
        $('#error-user-email').show();

    }else{
        $('#error-user-email').hide();
    }
    if(phone_number.trim()==''){
        error = true;
        $('#error-user-phone-number').text('Bạn chưa nhập số điện thoại');
        $('#error-user-phone-number').show();
    }else if(!checkPhoneValid(phone_number.trim())){
         error=true;
        $('#error-user-phone-number').text('Số điện thoại không hợp lệ');
        $('#error-user-phone-number').show();

    }else{
        $('#error-user-phone-number').hide();
    }

 
    return error;
}


function checkEmailValid(email){
    if(email.trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
    }else{
        return true;
    }

}
function checkPhoneValid(phone){
    var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
        if (vnf_regex.test(phone) == false) {
            return false;
        }else{
                return true;
        }

}



function showDialogCreateStaff(){
    Swal.fire({
            title:'Thêm nhân viên',
            onBeforeOpen:()=>{
                Swal.showLoading();
            },
            onOpen :()=>{
                createStaff();
            },
        });
}
function createStaff(){
    var user_name = $('#user-name').val();
    var email = $('#user-email').val();
    var phone_number = $('#user-phone-number').val();
    var password = $('#user-password').val();
    var user_type =  $('#user-type').val();
    var form_data = new FormData();
    var working_day = "";
    $.each($('.active-item'),function(){
        if($(this).data('type')==1){
            working_day = $(this).data('day');
            return false;
        }else{
            working_day += $(this).data('day');
        }
    });

    form_data.append('working_day',working_day);
    form_data.append('image', $("#user-avatar-input")[0].files[0]);
    form_data.append('salon_id', salon_id);
    form_data.append('user_name',user_name);
    form_data.append('email', email);
    form_data.append('phone_number', phone_number);
    form_data.append('password', password);
    form_data.append('user_type', user_type);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
            headers: {
                'Accept':'application/json',
                'Authorization':'Bearer ' + token,
            },
            type:'POST',
            processData: false,
            contentType: false,
            cache : false,
            url: BASE_URL+'api/v1/staff/addStaff',
            data:form_data,
            success:function(data){
                Swal.hideLoading();
                if(data.success){
                    resetForm();
                    Swal.fire({
                          title: 'Thông báo',
                          text: data.message,
                          icon: 'success',
                          confirmButtonText: 'OK'
                    });

                }else{
                    Swal.fire({
                          title: 'Thông báo',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'OK'
                    });
                }

            },
             error:function(jqXHR, ajaxOptions, thrownError){
                Swal.hideLoading();
                handleUnAuthenticated(jqXHR);
                Swal.fire({
                          title: 'Thông báo',
                          text: 'Có lỗi xảy ra, vui lòng thử lại sau ít phút!',
                          icon: 'error',
                          confirmButtonText: 'OK'
                    });
           }

    });
}

function resetForm(){
    $('#user-name').val('');
    $('#user-email').val('');
    $('#user-phone-number').val('');
    $("#user-password").val('');
    $('#avatar-user').hide();
    $('#avatar-user').attr('src','#');
}