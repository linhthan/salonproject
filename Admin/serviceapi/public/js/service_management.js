
$(document).ready(function(){
  var keyword = $('#input-search-service').val();
  getAllService(0,keyword,true);
  $(document).on('click', '#box-all-service .pagination a',function(event)
      {
        event.preventDefault();
        var keyword = $('#input-search-service').val();
        $('#box-all-service .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');
        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];
        getAllService(page,keyword,true);
    });

    $('#btn-search-service').click(function(){
      var keyword = $('#input-search-service').val();
      getAllService(0,keyword,true);
    });


    $('#btnSave').click(function(){
        var titleService = $('#service-title').val();
        var priceService = $('#service-price').val();
        var durationService = $('#service-duration').val();
        var contentService = $('#service-content').val();
        var statusService =  $('#service-status').val();
        var thumbService = $("#service-thumb")[0].files[0];

        if(titleService.trim()==''||priceService.trim()==''
          ||durationService.trim()=='' ||contentService.trim()=='' ){
            Swal.fire({
                      title: 'Thông báo',
                      text: 'Bạn chưa điền đầy đủ thông tin!',
                      icon: 'error',
                      confirmButtonText: 'OK'
                 });
          
        }else if(Number(priceService)=='NaN'||priceService==0||(priceService % 1000)!=0){
          Swal.fire({
                      title: 'Thông báo',
                      text: 'Giá bạn nhập không hợp lệ!',
                      icon: 'error',
                      confirmButtonText: 'OK'
                 });
        }else if(Number(durationService)=='NaN'||durationService==0){
          Swal.fire({
                      title: 'Thông báo',
                      text: 'Thời gian dịch vụ không hợp lệ!',
                      icon: 'error',
                      confirmButtonText: 'OK'
                 });
        }else{
          $('#modalProduct').modal('hide');
          showUpdateDialog($(this).data('id'),titleService,priceService,durationService,contentService,statusService,thumbService);
        }
        });

  $('#btnAddService').click(function(){
    var url = $(this).data('href');
        window.location = $(this).data('href');
  });
  $(document).on('click','#box-all-service .item-table .btn-edit',function(event){
      event.preventDefault();
       event.stopPropagation();
        var row = $(this).closest("tr").find('td');
        $('#modalService').modal('show');
        $('#service-title').val(row.eq(1).text());
        $('#service-price').val(row.eq(3).text());
        $('#service-duration').val($(this).data('duration'));
        $('#service-content').val($(this).data('content'));
        $('#img-thumb').attr('src',$(this).data('thumb'));
        $('#btnSave').data('id',$(this).data('id'));
        var status = $(this).data('status');
        $('#service-status').val(status);
  });

  $(document).on('click','#box-all-service .item-table .btn-remove',function(event){
        event.preventDefault();
        event.stopPropagation();
        var id = $(this).data('id');

        Swal.fire({
          title: 'Xóa dịch vụ?',
          text: "Dịch vụ sẽ bị xóa!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Xóa!'
        }).then((result) => {
          if (result.value) {
           deleteService(id);
          }
        });
  });

});

function showUpdateDialog(service_id,titleService,priceService,durationService,contentService,statusService,thumbService){
  Swal.fire({
      title:'Cập nhật dịch vụ',
      onBeforeOpen:()=>{
        Swal.showLoading()
      },
      onOpen :()=>{

        var form_data = new FormData();
        if(thumbService){
          form_data.append('image', thumbService);
        }
        form_data.append('service_id', service_id);
        form_data.append('titleService',titleService);
        form_data.append('priceService', priceService);
        form_data.append('durationService', durationService);
        form_data.append('contentService', contentService);
        form_data.append('statusService', statusService);
        $.ajax({
          headers: {
                  'Accept':'application/json',
                  'Authorization':'Bearer ' + token,
              },
          type:'POST',
          url:BASE_URL+'api/v1/salon/updateService',
          processData: false,
          contentType: false,
          cache : false,
          data:form_data,
          success:function(data){
            Swal.hideLoading();

            if(data.success){
              $('.nav-tabs').find('li').removeClass('active');
              $('.nav-tabs').find('li').eq(0).addClass('active');
              $('#browseTab').removeClass('active');
              $('#uploadTab').addClass('active');
              $('#service-thumb').val('');
              $('#btnDelete').hide();
              var keyword = $('#input-search-service').val();
              getAllService(0,keyword,true);
              Swal.fire({
                        title: 'Thông báo',
                        text: 'Chỉnh sửa dịch vụ thành công',
                        icon: 'success',
                        confirmButtonText: 'OK'
                     });
               }else{
                Swal.fire({
                      title: 'Thông báo',
                      text: data.message,
                      icon: 'error',
                      confirmButtonText: 'OK'
                   });
               }

          },
           error:function(jqXHR, ajaxOptions, thrownError){
            Swal.hideLoading();
                   handleUnAuthenticated(jqXHR);
             }

      });
      }
    });
}


function deleteService(service_id){
  Swal.fire({
      title:'Xóa dịch vụ',
            onBeforeOpen:()=>{
                Swal.showLoading()
            },
            onOpen:()=>{
              $.ajax({
                headers: {
                        'Accept':'application/json',
                        'Authorization':'Bearer ' + token,
                    },
                type:'PATCH',
                url:BASE_URL+'api/v1/salon/deleteService',

                data:{
                  service_id:service_id,
                },
                success:function(data){
                  Swal.hideLoading();
                  if(data.success){
                    var keyword = $('#input-search-service').val();
                    getAllService(0,keyword,false);
                    Swal.fire({
                      title: 'Thành công',
                      text: data.message,
                      icon: 'success',
                      confirmButtonText: 'OK'
                   });
                  }else{
                    Swal.fire({
                      title: 'Thất bại',
                      text: data.message,
                      icon: 'error',
                      confirmButtonText: 'OK'
                   });
                  }
                },
                error:function(jqXHR, ajaxOptions, thrownError){
                  Swal.hideLoading();
                  handleUnAuthenticated(jqXHR);
                  Swal.fire({
                      title: 'Thất bại',
                      text: 'Có lỗi xảy ra, vui lòng thử lại!',
                      icon: 'error',
                      confirmButtonText: 'OK'
                   });
                }

              });
            }
    });
}


function getAllService(page,keyword,withLoading){
  if(withLoading){
    $('#progress-loading').show();
  }
  $.ajax({
      headers: {
              'Authorization':'Bearer ' + token,
              'X-Requested-With':'XMLHttpRequest'
          },
      type:'GET',
      url:BASE_URL+'api/v1/salon/getSalonServiceManagement',
      datatype: "html",
      data:{
        salon_id:salon_id,
        keyword:keyword,
        page:page
      },
      success:function(data){
        $('#progress-loading').hide();
        $('#box-all-service').empty().html(data);
      },
       error:function(jqXHR, ajaxOptions, thrownError){
        $('#progress-loading').hide();
               handleUnAuthenticated(jqXHR);
         }

  });
}