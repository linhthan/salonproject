



    $(document).ready(function(){

      var ctx =$('#lineChart').get(0).getContext('2d');
      var ctxBar = $('#barChart').get(0).getContext('2d');
      var config ={
        firstLabel:'Sản phẩm',
        secondLabel:'Dịch vụ',
        firstColor:'#0BC2B0',
        firstBorderColor:'#0BC2B0',
        secondColor: '#2B72BD',
        secondBorderColor: '#2B72BD',
        fill:false,
        callbacks:{
              label: function(tooltipItem, data) {
                var label = data.datasets[tooltipItem.datasetIndex].label || '';

                if (label) {
                  label += ': ';
                  label += tooltipItem.value+"₫";
                }

                return label;
              },


          },
          callbackBarChart:{
              label: function(tooltipItem, data) {
                var label = data.datasets[tooltipItem.datasetIndex].label || '';

                if (label) {
                  label += ': ';
                  label += tooltipItem.value;
                }

                return label;
              },


          },
          ticks: {
                beginAtZero: true,
                callback:function(value,index,values){
                  return '₫' + value;
                }
              }
          ,
          tickBarChart:{
              beginAtZero: true,
                callback:function(value,index,values){
                  if (value % 1 === 0) {
                    return value;
                  }
                }
              }
        }
       var lineChart  = new Chart(ctx, {
            type: 'line',
            data: {
              labels: [],
              datasets: [{
                label: config.firstLabel,
                data:[],
                backgroundColor:config.firstColor,
                borderColor: config.firstBorderColor,
                borderWidth: 1,
                fill:config.fill
              },{
                label: config.secondLabel,
                data:[],
                backgroundColor:  config.secondColor,
                borderColor: config.secondBorderColor,
                borderWidth: 1,
                fill:config.fill
              }]
            },
            options: {
              responsive: true,
              tooltips:{
               callbacks: config.callbacks
            },
            scales: {
              yAxes: [{
                ticks:config.ticks
              }]
            },


          }
        });
       var barChart  = new Chart(ctxBar, {
            type: 'bar',
            data: {
              labels: [],
              datasets: [{
                label: config.firstLabel,
                data:[],
                backgroundColor:config.firstColor,
                borderColor: config.firstBorderColor,
                borderWidth: 1,
                fill:config.fill
              },{
                label: config.secondLabel,
                data:[],
                backgroundColor:  config.secondColor,
                borderColor: config.secondBorderColor,
                borderWidth: 1,
                fill:config.fill
              }]
            },
            options: {
              responsive: true,
              tooltips:{
               callbacks: config.callbackBarChart
            },
            scales: {
              yAxes: [{
                ticks:config.tickBarChart
              }]
            },


          }
        });



      var filter =  $('#filter-value-appoinment').data('filter');
      loadInfoHome();
      loadAllSaleInfo(lineChart);
      loadUpcommingAppointment(filter,barChart);
      loadAllAppointment(0);
      loadAllNextAppointment(0);


      $(document).on('click', '#box-table-all-appointment .pagination a',function(event)
      {
        event.preventDefault();

        $('#box-table-all-appointment .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];

        loadAllAppointment(page);
      });

      
      $(document).on('click','#box-table-all-appointment .item-table',function(event){
          event.preventDefault();
          window.location = $(this).data('href');
      });

      $(document).on('click', '#box-table-next-appointment .pagination a',function(event)
      {
        event.preventDefault();

        $('#box-table-next-appointment .pagination li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];

        loadAllNextAppointment(page);
      });

      


    $( "#filterOption" ).click(function() {
      $("#myModal").modal();
    });

    $('#menu >li').click(function() {

      var data = $(this).data('filter');
      if(data==$('#filter-value').data('filter')){
        return;
      }else{
       $('#filter-value').text($(this).text());
       $('#filter-value').data('filter',data);
       loadAllSaleInfo(lineChart);
     }
     
   });
    $('#menu-appointment >li').click(function() {

      var data = $(this).data('filter');
      if(data==$('#filter-value-appointment').data('filter')){
        return;
      }else{
       $('#filter-value-appointment').text($(this).text());
       $('#filter-value-appointment').data('filter',data);
       loadUpcommingAppointment(data,barChart);
     }
     
   });
    

    $('#btn-search-appoinment').click(function(){
      var keyword = $('#input-search-appointment').val();
      $.ajax(
      {
        type:'GET',
        headers: {
          'Authorization':'Bearer ' + token,
        },
        url:BASE_URL+'api/v1/salon/searchAppointmentOfSalon',
        datatype: "html",
        data:{
          'keyword':keyword,
          'salon_id':salon_id
        }

      }).done(function(data){
        console.log(data);
       $("#box-table-all-appointment").empty().html(data);

     }).fail(function(jqXHR, ajaxOptions, thrownError){
      handleUnAuthenticated(jqXHR);
    });
   });

    $('#btn-search-next-appoinment').click(function(){
      var keyword = $('#input-search-next-appointment').val();
      $.ajax(
      {
        type:'GET',
        headers: {
          'Authorization':'Bearer ' + token,
        },
        url:'api/v1/salon/searchNextAppointment',
        datatype: "html",
        data:{
          'keyword':keyword,
          'salon_id':salon_id
        }

      }).done(function(data){
        $("#box-table-next-appointment").empty().html(data);

       // location.hash = page;
     }).fail(function(jqXHR, ajaxOptions, thrownError){
      handleUnAuthenticated(jqXHR);
    });


   });
  });

function loadAllSaleInfo(lineChart){
  var filter = $('#filter-value').data('filter');
  $.ajax(
  {
    type:'GET',
    headers: {
      'Accept':'application/json',
      'Authorization':'Bearer ' + token,
    },
    url:'api/v1/salon/getSaleInformation',
    data:{
      'filter':filter,
      'salon_id':salon_id
    }

  }).done(function(data){

    if(data.data && data.data && data.data.length>0){
       $('#line-char-wrapper').css('visibility','visible');
       prepareSaleProductData(filter,data.data,lineChart);
        $('#empty-sales').hide();
       
    }else{
       $('#line-char-wrapper').css('visibility','hidden');
      $('#empty-sales').show();
    }

   
    // prepareSaleBookingData(data.data.booking_sale);
  }).fail(function(jqXHR, ajaxOptions, thrownError){
    handleUnAuthenticated(jqXHR);
  });

}

function prepareSaleProductData(filter,bill,lineChart){
  var date = new Date();
  date.setDate(date.getDate() - filter);
  var dateLabel = new Date();
  dateLabel.setDate(dateLabel.getDate() -filter);
  console.log(date);
  var labels = [];
  var dataProducts =[];
  var appointmentDatas =[];
  var totalProductPrice = 0;
  var totalBookingPrice = 0;
  var mapProduct = {};
  var mapAppointment = {};
  for (var i = 0; i < (filter+1); i++) {
    var label = (dateLabel.getDate()) +"/"+ (dateLabel.getMonth()+1);
    mapProduct[label] = 0;
    mapAppointment[label] = 0;
    dateLabel.setDate(dateLabel.getDate() +1);
  }
  for(item in bill){
      var dateItem = bill[item];
      var data = new Date(dateItem.create_at);
      
      var label = (data.getDate()) +"/"+ (data.getMonth()+1);
        if(dateItem.id_booking){
          mapAppointment[label] += dateItem.total_price;
          totalBookingPrice+= dateItem.total_price;
        }else{
          totalProductPrice += dateItem.total_price;
          mapProduct[label] += dateItem.total_price;
        }
    }
    
    for(x in mapProduct){
      dataProducts.push(mapProduct[x]);
      labels.push(x);
    }
    for(y in mapAppointment){
      appointmentDatas.push(mapAppointment[y]);
    }


  var ctx =$('#lineChart').get(0).getContext('2d');
  var config ={
    firstLabel:'Sản phẩm',
    secondLabel:'Dịch vụ',
    firstColor:'#0BC2B0',
    firstBorderColor:'#0BC2B0',
    secondColor: '#2B72BD',
    secondBorderColor: '#2B72BD',
    fill:false,
    callbacks:{
          label: function(tooltipItem, data) {
            var label = data.datasets[tooltipItem.datasetIndex].label || '';

            if (label) {
              label += ': ';
              label += tooltipItem.value+"₫";
            }

            return label;
          },


      },
      ticks: {
            beginAtZero: true,
            callback:function(value,index,values){
              return 'đ' + value;
            }
          }
    }
  console.log('Total Price:'+(totalBookingPrice/1000)+"K");
  console.log('Total Price Product:'+totalProductPrice);
  $('#product-value').text((totalProductPrice/1000)+"K");
  $('#appointment-value-sale').text((totalBookingPrice/1000)+"K");
  $('#total-recent-sale').text(((totalProductPrice+totalBookingPrice)/1000)+"K");
  drawChart(ctx,lineChart,'line',labels, dataProducts,appointmentDatas,config);


}

function compareDate(date,data){
  if(date.getDate() == data.getDate()&&date.getMonth() == data.getMonth()&&date.getYear() == data.getYear()){
    return true;
  }
  return false;
}

function prepareDataUpcommingAppointment(filter,dataBooking,barChart){

  var date = new Date();
  var labels = [];
  var cancelDatas =[];
  var pendingDatas =[];
  for (var i = 1; i < (filter+1); i++) {
    
    // labels.push()
    var label = date.getDate() +"/"+ (date.getMonth()+1);
    log(label);
    labels.push(label);
    var foundCancel = false;
    var foundPending = false;
    for(item in dataBooking){
    var dateItem = dataBooking[item];
    var data = new Date(dateItem.date);
    if(date.getDate() == data.getDate()){
      if(dateItem.status==0){
        foundCancel = false;
        foundPending = true;
        pendingDatas.push(dateItem.totalBooking);
      }else{
        foundCancel = true;
        foundPending =false;
        cancelDatas.push(dateItem.totalBooking);
      }
      // break;
    }
  }

  if(!foundCancel){
    cancelDatas.push(0);
  }
  if(!foundPending){
    pendingDatas.push(0);
  }

date.setDate(date.getDate()+1);
}
log("cancelDatas");
log(cancelDatas);
 var totalCancels = cancelDatas.reduce((a, b) => a + b, 0);
  var totalPendings = pendingDatas.reduce((a, b) => a + b, 0);

  $('#value-cancel-appointment').text(totalCancels);
  $('#value-pending-appointment').text(totalPendings);
  $('#value-total-appointment').text(totalPendings + totalCancels);

var ctx = $('#barChart').get(0).getContext('2d');
var config ={
  firstLabel:'Đã đặt',
  secondLabel:'Đã hủy',
  firstColor:'#0BC2B0',
  firstBorderColor:'#0BC2B0',
  secondColor: '#D2D6DE',
  secondBorderColor: '#D2D6DE',
  fill:false,
  callbacks:{
        label: function(tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label || '';

          if (label) {
            label += ': ';
            label += tooltipItem.value;
          }

          return label;
        },


    },
    ticks: {
          beginAtZero: true,
          callback:function(value,index,values){
            return value;
          }
        }
  }
drawChart(ctx,barChart,'line',labels, pendingDatas,cancelDatas,config);

}
function loadInfoHome(){
  $.ajax(
  {
    type:'GET',
    headers: {
      'Authorization':'Bearer ' + token,
    },
    url:BASE_URL+'api/v1/salon/getInfoHome',
    data:{
      'salon_id':salon_id,
    },
   success:function(data){

       console.log(data);
       if(data.success){
          $('#order-value').text(data.order_count);
          $('#customer-value').text(data.customer_month);
          $('#appointment-value').text(data.appointment.length);
          var appointment = data.appointment;
          
          var totalNeedNotify = 0;
          for(index in appointment){
            var appointmentItem = appointment[index];
            if(appointmentItem.status !=1){
                var bookingTime  = appointmentItem.booking_time;
                var dateTimeBooking = new Date(bookingTime);
                var compareDataNeedNotify = new Date();
                var currentDate = new Date();
                compareDataNeedNotify.setMinutes( compareDataNeedNotify.getMinutes() + 15 );
                if(currentDate.getTime() <dateTimeBooking.getTime() && dateTimeBooking.getTime()<= compareDataNeedNotify.getTime()){
                    totalNeedNotify += 1;
                }
            }
           
          }
          $('#notify-customer').text(totalNeedNotify);
          
       }

      },
       error:function(jqXHR, ajaxOptions, thrownError){
               handleUnAuthenticated(jqXHR);
         }

  });
  
}

function loadAllAppointment(page){
  $.ajax(
  {
    type:'GET',
    headers: {
      'Authorization':'Bearer ' + token,
    },
    url:BASE_URL+'api/v1/salon/getAllAppointmentOfSalon?page='+page,
    datatype: "html",
    data:{
      'salon_id':salon_id,
    }

  }).done(function(data){
    $("#box-table-all-appointment").empty().html(data);

 }).fail(function(jqXHR, ajaxOptions, thrownError){

  handleUnAuthenticated(jqXHR);
});

}

function loadAllNextAppointment(page){
  $.ajax(
  {
    type:'GET',
    headers: {
      'Authorization':'Bearer ' + token,
    },
    url:BASE_URL+'api/v1/salon/getNextAppointment?page='+page,
    datatype: "html",
    data:{
      'salon_id':salon_id,
    }

  }).done(function(data){

   $("#box-table-next-appointment").empty().html(data);

 }).fail(function(jqXHR, ajaxOptions, thrownError){
  handleUnAuthenticated(jqXHR);
});
}

function loadUpcommingAppointment(filter,barChart){

 $.ajax(
 {
  type:'GET',
  headers: {
    'Authorization':'Bearer ' + token,
  },
  url:BASE_URL+'api/v1/salon/getUpcommingAppointment',
  data:{
    'filter':filter,
    'salon_id':salon_id,
  }

}).done(function(data){

  if(data && data.data && data.data.booking && data.data.booking.length>0){

    $('#bar-char-wrapper').css('visibility','visible');
    prepareDataUpcommingAppointment(filter,data.data.booking,barChart);
    $('#empty-appointment').hide();
  }else{
    //TODO
     $('#bar-char-wrapper').css('visibility','hidden');
    $('#empty-appointment').show();
  }
  

}).fail(function(jqXHR, ajaxOptions, thrownError){
  handleUnAuthenticated(jqXHR);
});
}


function drawChart(ctx,chart,chart_type,labels,firstData,secondData,config){

  var newDatasets = {
   labels: labels,
   datasets: [{
    label: config.firstLabel,
    data:firstData,
    backgroundColor: config.firstColor,
    borderColor: config.firstBorderColor,
    borderWidth: 1,
    fill:config.fill
  },{
    label: config.secondLabel,
    data:secondData,
    backgroundColor:  config.secondColor,
    borderColor:  config.secondBorderColor,
    borderWidth: 1,
    fill:config.fill
  }]
};


if(chart){

  chart.data.datasets = newDatasets.datasets;
  chart.data.labels = newDatasets.labels;
  chart.update();
}else{
  chart = new Chart(ctx, {
    type: chart_type,
    data: {
      labels: labels,
      datasets: [{
        label: config.firstLabel,
        data:firstData,
        backgroundColor:config.firstColor,
        borderColor: config.firstBorderColor,
        borderWidth: 1,
        fill:config.fill
      },{
        label: config.secondLabel,
        data:secondData,
        backgroundColor:  config.secondColor,
        borderColor: config.secondBorderColor,
        borderWidth: 1,
        fill:config.fill
      }]
    },
    options: {
      responsive: true,
      tooltips:{
       callbacks: config.callbacks
    },
    scales: {
      yAxes: [{
        ticks:config.ticks
      }]
    },


  }
});

}

}
