<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>BKSalon</title>

    <link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">

    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Main CSS-->
    <link href="{{asset('css/signup.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/icon_sign_up.min.css')}}" rel="stylesheet" media="all">
     <link href="{{asset('css/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
    
   <link href="{{asset('modalsweet/jquery.sweet-modal.min.css')}}" rel="stylesheet">


  <!-- jQuery 3 -->
  <script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
  <!-- Bootstrap 3.3.7 -->
<!--   <script src="{{asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script> -->
<script src="{{asset('js/select2.min.js')}}"></script>
<script src="{{asset('js/signupv2.js')}}"></script>
<script src="{{asset('modalsweet/jquery.sweet-modal.min.js')}}"></script>

<link href="{{asset('sweetalert/sweetalert2.min.css')}}" rel="stylesheet">
<script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>


</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">Đăng ký quản lý Salon</h2>
                </div>
                <div class="card-body">
                    <form id="form-sign-up">
                        <div class="form-row">
                            <div class="name">Họ và Tên<span>(*)</span></div>
                            <div class="value">
                                 <div class="input-group" data-validate="Bạn chưa điền họ tên">
                                    <input id="fullname" class="inputData input--style-5" type="text" name="fullname">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Tên Salon<span>(*)</span></div>
                            <div class="value">
                                <div class="input-group" data-validate="Bạn chưa điền tên Salon">
                                    <input id="salonName" class="inputData input--style-5" type="text" name="salonName">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Email<span>(*)</span></div>
                            <div class="value">
                                <div class="input-group" data-validate="Bạn cần nhập email hợp lệ">
                                    <input id="email" class="inputData input--style-5" type="email" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Số điện thoại<span>(*)</span></div>
                            <div class="value">
                                 <div class="input-group" data-validate="Bạn cần nhập số điện thoại hợp lệ">
                                    <input id="phone-number" class="inputData input--style-5" type="text" name="phone">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Mật khẩu<span>(*)</span></div>
                            <div class="value">
                                 <div class="input-group" data-validate="Bạn chưa nhập mật khẩu">
                                    <input id="password" class="inputData input--style-5" type="password" name="password">
                                </div>
                            </div>
                        </div>
                        

                        <div class="form-row">
                            <div class="name">Địa chỉ<span>(*)</span></div>
                            <div class="value">
                                <div class="input-group" data-validate="Bạn chưa chọn địa chỉ">
                                    <div class="rs-select2 js-select-simple">
                                         <select id="select-city-option" name="subject">
                                          
                                        </select>
                                     
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row m-b-55">
                            <div class="name"></div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-6">
                                        <div class="input-group" data-validate="Bạn chưa chọn địa chỉ">
                                            <div class="rs-select2 js-select-simple">

                                         <select id="select-district-option" name="subject">
                                            <option disabled="disabled" selected="selected">Chọn Quận/Huyện</option>
                                        </select>
                                     
                                        <div class="select-dropdown"></div>
                                    </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group" data-validate="Bạn chưa chọn địa chỉ">
                                            <div class="rs-select2 js-select-simple">
                                                 <select id="select-ward-option" name="subject">
                                                    <option disabled="disabled" selected="selected">Chọn Phường/Xã</option>
                                                </select>
                                     
                                                <div class="select-dropdown"></div>
                                            </div>
                                        </div>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Chi tiết địa chỉ<span>(*)</span></div>
                            <div class="value">
                                 <div class="input-group" data-validate="Bạn chưa nhập địa chỉ">
                                    <input id="detail-location" class="inputData input--style-5" type="text" name="detail-location">
                                </div>
                            </div>
                        </div>
                        <div class="form-row m-b-55">
                            <div class="name">Ảnh Salon<span>(*)</span></div>
                            <div class="value">

                                        <div class="input-group">
                                            <input id="thumbUrl" class="inputData input--style-5" type="file" name="thumb-url" accept="image/x-png,image/gif,image/jpeg">
                                        </div>
                                        <span>(tối thiểu 600x300)</span>
                                  
                            </div>
                        </div>
                       
                       
                        <div style="text-align: center;">
                            <button id="btnSignUp" class="btn btn--radius-2 btn--red" type="submit">Đăng ký</button>
                        </div>
                        <div style="text-align: center; margin-top: 20px;">
                            <span>Nếu bạn đã có tài khoản, trở về trang<a class="txt2" href="{{route('login')}}"> đăng nhập</a></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->