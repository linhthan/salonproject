<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" style="margin-left: 0px;" role="navigation">
        <!-- Sidebar toggle button-->
        <!-- Navbar Right Menu -->
        <div id="header-logo" style="display: none;">
            <a href="{{ url()->previous() }}" class="logo-header"><b>BK</b>Salon</a>
        </div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
              
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img id="img-avatar-header-right" style="width: 25px; height: 25px" src="{{ asset('adminlte/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image"/>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span id="username" class="hidden-xs">Anonymous</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img id="img-avatar-header-right-large" style="width: 90px; height: 90px" src="{{ asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
                            <p id="username-menu">
                                    Anonymous
                            </p>
                        </li>
                        
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ sơ</a>
                            </div>
                            <div id="btn-sign-out" class="pull-right">
                                <a href="#" class="btn btn-default btn-flat">Đăng xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>