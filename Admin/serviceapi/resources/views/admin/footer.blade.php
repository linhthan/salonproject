<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Liên hệ hỗ trợ 24/7: linh.tt152230@sis.hust.edu.vn
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2019 <a href="{{route('home-page')}}">BKSalon</a>.</strong> All rights reserved.
</footer>