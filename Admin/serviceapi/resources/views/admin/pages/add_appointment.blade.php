@extends('admin.waiter')

@section('title','Đặt lịch')

@section('css')
<link href="{{asset('css/icon_sign_up.min.css')}}" rel="stylesheet" media="all">
<link href="{{asset('css/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
<link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/add_appointment.css')}}">
<link rel="stylesheet" href="{{asset('datepicker/datepicker.css')}}">
<link rel="stylesheet" href="{{asset('stepper/bs-stepper.min.css')}}">

@endsection

@section('js')

<script type="text/javascript" src="{{asset('datepicker/datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('multiselect/dist/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('multiselect/dist/js/BsMultiSelect.js')}}"></script>
<script type="text/javascript" src="{{asset('stepper/bs-stepper.min.js')}}"></script>
<script src="{{asset('js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/string.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/add_appointment.js')}}"></script>
@endsection

@section('content')
<div class="row appointment-header">
    <div class="date-appointment">
        <button id="btn-previous-date" class="btn-chevron"><i class="fa fa-chevron-left"></i></button>
        <button id="date-title">Hôm nay</button>
        <button id="current-date-selected" class="date-selected" data-toggle="datepicker"></button>
        <button id="btn-next-date"><i class="fa fa-chevron-right"></i></button>
    </div>
</div>
<div id="stepper1" class="bs-stepper">
    <div class="bs-stepper-header" role="tablist">
            <div id="tab-step-1" class="step active" data-target="#test-nl-1">
              <button type="button" class="step-trigger" role="tab" id="stepper1trigger1" aria-controls="test-nl-1" aria-selected="true">
                <span class="fa fa-scissors fa-3x bs-stepper-circle">
                  
                </span>
               <span class="bs-stepper-label">Chọn dịch vụ</span>
              </button>

            </div>

            <div id="line-step-1" class="bs-stepper-line active"></div>
            <div id="tab-step-2" class="step" data-target="test-nl-2">
              <button type="button" class="step-trigger" role="tab" id="stepper1trigger2" aria-controls="test-nl-2" aria-selected="false">
                <span class="fa fa-calendar fa-3x bs-stepper-circle">
                  
                </span>
                <span class="bs-stepper-label">Chọn thời gian</span>
              </button>
            </div>
            <div id="line-step-2" class="bs-stepper-line"></div>
            <div id="tab-step-3" class="step" data-target="test-nl-3">
              <button type="button" class="step-trigger" role="tab" id="stepper1trigger3" aria-controls="test-nl-3" aria-selected="false">
                <span class="bs-stepper-circle fa fa-check-circle fa-3x">
                  
                </span>
                <span class="bs-stepper-label">Hoàn thành</span>
              </button>
            </div>
    </div>
          <div id="content-step-1" class="bs-stepper-content container">
            <!-- Step1 -->
                <div class="top-filter">
                  <div class="search-box">
          
                  <div class="input-group" >
                          <input id="input-search-user" placeholder="tìm kiếm" class="form-control width100">
                          <span class="input-group-btn">
                            <button id="btn-search-user" class="btn btn-info">
                              Tìm kiếm
                            </button>
                          </span>
                        </div>
                </div>
                </div>
                <div id="content-services" class="container">
                    
                </div>
                <div id="view-no-data" style="text-align: center;">
                    <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
                    <h3>Không tìm thấy kết quả</h3>
                    <span>Hãy thử lựa chọn tìm kiếm khác</span>
                </div>

                <div id="cart-service" class="floating-cart">
                    <div class="container">
                        <div class="row">
                            <div class="col-6 col-md-6">
                                <div class="main clearfix">
                                    <div class="count">
                                        <img src="https://isalon.vn/assets/modules/modhairworld/img/cart.png">
                                        <div id="number-service" class="number">1</div>
                                    </div>
                                    <div id="total-price" class="sum">
                                        0K
                                    </div>
                                    <div id="desc-cart" class="desc">
                                        <div class="big"><span id="number-service-span" class="number">0</span> dịch vụ được thêm
                                            vào giỏ hàng
                                        </div>
                                        <div class="small">Bạn có thể chọn thêm hoặc tiếp tục</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-6">
                                <div class="link">
                                    <button id="btn-next-step1" class="btn-select-time">Chọn thời gian</button>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>

              <!-- End Step 1 -->
            
          </div>
            <!-- Content Step 2 -->
            <div id="content-step-2" role="tabpanel" class="bs-stepper-content container" aria-labelledby="stepper2trigger2"  style="display: none;">
                <div class="bs-calendar-box container">
                        <div class="calendar-box">
                            <div class="form-row">
                                <div class="name">Chọn thợ<span>(*)</span></div>
                                <div class="value">
                                    <div class="input-group" data-validate="Bạn chưa chọn địa chỉ">
                                        <div class="rs-select2 js-select-simple">
                                             <select id="select-stylist-option" name="subject">
                                              
                                            </select>
                                         
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                                </div>
                            <div id="box-time-appoinment" class="row calendar-date-box">
                                
                            </div>
                            <div>
                                <button class="btn-step" id="btn-back">Trở lại</button>
                                <button class="btn-step" id="btn-next">Tiếp tục</button>
                            </div>
                        
                        </div>
                </div>

            </div>
            <!-- End content step 2 -->

            <!-- Content step 3 -->
              <div id="content-step-3" role="tabpanel" class="bs-stepper-pane text-center" aria-labelledby="stepper2trigger3">
                <div class="box-appoinment container">
                    <div class="row" style="text-align: center;">
                        <h3>Thông tin lịch hẹn</h3>
                    </div>
                    <div class="row box-info">

                        <div class="col-12 col-sm-6 box-appoinment-info">
                            <div>
                                <span id="appointment-date" class="fa fa-calendar fa-2x"> 20/11/2019</span>
                            </div>
                            <div>
                                <span id="appointment-time" class="fa fa-clock-o fa-2x"> 10:20</span>
                            </div>
                            <div class="box-service-added">
                                <table id="table-service" class="table table-bordered"> 
                                    <thead>
                                        <th>#</th>
                                        <th>Dịch vụ</th>
                                        <th>Giá</th>
                                    </thead>
                                    <tbody id="table-service-content">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 box-customer-info">

                             <div class="row item-customer-info">
                                <div class="col-25">
                                  <label for="fname">Họ tên</label>
                                </div>
                                <div class="col-75">
                                  <input id="fullname" class="input-customer-info" type="text" id="fullname" name="fullname" placeholder="Nhập tên khách hàng">
                                </div>
                              </div>
                              <div class="row item-customer-info">
                                <div class="col-25">
                                  <label for="lname">Số điện thoại</label>
                                </div>
                                <div class="col-75">
                                  <input id="phoneNumber" class="input-customer-info" type="text" id="phone-number" name="phone-number" placeholder="Số điện thoại...">
                                </div>
                              </div>

                        </div>
                    </div>
                    <button id="btn-back-to-step-2" type="button" class="btn-step">
                        Trở lại
                    </button>
                    <button id="btn-apply-booking" type="button" class="btn-step">
                        Đặt lịch
                    </button>

                </div>

              </div>
              <!-- End content step 3 -->
      </div>

    <!--    <div class="row service-box">
            Lựa chọn dịch vụ
            <div class="service-list">
                <select name="states" id="example" class="form-control" multiple="multiple" style="display: none;">
                @for($i=0; $i<15; $i++)
                     <option value="{{$i}}">option {{$i}}</option>
                @endfor
            </select>
            </div>
            
        </div> -->
        <!-- Modal -->


@endsection
