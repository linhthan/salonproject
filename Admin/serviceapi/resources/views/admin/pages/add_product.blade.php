@extends('admin.admin_template')
@section('title','Thêm sản phẩm')
@section('css')
<link rel="stylesheet" href="{{asset('css/add_product.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/add_product.js')}}"></script>
@endsection
@section('content')
	<div class="row">
		<div class="top-filter-header">
			<h2 class="title-top">
				Thêm sản phẩm
			</h2>
			<div class="tool-box">
				<a href="{{route('products')}}">
					<img src="{{asset('adminlte/cancel.svg')}}">
				</a>
			</div>
		</div>
		<div class="row form-import">
			<h3> Thông tin sản phẩm</h3>
		</div>
		<div class="grid form-import">
			
			<div class="col-sm-6 col-xs-12 container-info-product">
				<div class="form-input">
					<label>Tên sản phẩm</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="product-title" class="input-info-product input-active" type="text" name="product-title">
						</div>
					</div>
					<span id="error-title-product" class="error-msg">Bạn chưa nhập tên sản phẩm</span>
				</div>
				<div class="form-input-price">
					<div class="form-input-price-product">
						<label>Giá gốc</label>
						<div class="box-input-info">
							<div id="origin-price-head" class="price-unit">₫</div>
							<div class="box-container-input price-box">
								<input id="origin-price" class="input-info-product half" type="text" name="origin-price">

							</div>
						</div>
						<span id="error-origin-price" class="error-msg"></span>
					</div>
					<div class="form-input-price-product">
						<label>Giá hiện tại</label>
						<div class="box-input-info">
							<div id="current-price-head" class="price-unit">₫</div>
							<div class="box-container-input price-box">
								<input id="current-price" class="input-info-product half" type="text" name="current-price">
							</div>
						</div>
						<span id="error-current-price" class="error-msg"></span>
					</div>

				</div>
				<div class="form-input">
					<label>Loại sản phẩm</label>

					
					<span class="product-category-box">
						<select id="category-product">
						</select>
						<span class="icon-select-option">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 6"><path d="M5.5 4.793L10.146.146a.5.5 0 1 1 .708.708l-5 5a.5.5 0 0 1-.708 0l-5-5A.5.5 0 1 1 .854.146L5.5 4.793z" fill-rule="nonzero"></path></svg>
						</span>
					</span>

					<div id="form-input-category" class="box-input-info" style="margin-top: 5px;">
						<div class="box-container-input">
							<input id="input-category-product" class="input-info-product" type="text" name="input-category-product" placeholder="Tạo mới loại sản phẩm">
						</div>
					</div>
					<span id="error-category-product" class="error-msg">Bạn chưa nhập loại sản phẩm</span>
				</div>
<!-- 				<div class="form-input">
					<label>Số lượng</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="product-quantity" class="input-info-product input-count input-active" type="text" name="product-quantity">
						</div>
					</div>
					<span id="error-quantity-product" class="error-msg"></span>
				</div> -->
				<div class="form-input">
					<label>Mô tả sản phẩm</label>
					<textarea id="description-product" placeholder="Nhập mô tả sản phẩm" style="flex-grow: 1;"></textarea>
					<span id="error-description-product" class="error-msg">Bạn chưa nhập mô tả sản phẩm</span>
				</div>
				<div class="form-input">
					<label>Ảnh sản phẩm</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="product-thumb" class="input-info-product input-active" accept="image/*"
							 type="file" name="product-title">
						</div>
					</div>
					<span id="error-thumb-product" class="error-msg">Bạn chưa chọn ảnh</span>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<img id="thumb-product" style="display: none;" class="thumb-product">
			</div>
		</div>
		
	</div>
	<div class="bottom-box">
		<button id="btnCancel" data-href="{{route('products')}}" class="btn-import-product btn-cancel">
			Hủy bỏ
		</button>
		<button id="btnSave"  class="btn-import-product btn-save">
			Lưu lại
		</button>
	</div>
@endsection