@extends('admin.admin_template')
@section('title','Quản lý nhân viên')
@section('css')
<link rel="stylesheet" href="{{asset('css/employee_management.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/employee_management.js')}}"></script>
@endsection
@section('content')
	<div class="row">
			<div class="top-filter">
				<div class="search-box" > 
					<select class="form-control" id="filterOption">
						<option value="0">Tất cả</option>
					    <option value="1">Nhân viên dịch vụ</option>
					    <option value="2">Nhân viên lễ tân</option>
					    <option value="3">Nhân viên thu thập</option>
				  </select>
				</div>
				<div class="search-box">
					
					<div class="input-group" >
		              <input id="input-search-user" placeholder="tìm kiếm" class="form-control width100">
		              <span class="input-group-btn">
		                <button id="btn-search-user" class="btn btn-info">
		                  Tìm kiếm
		                </button>
		              </span>
	            	</div>
				</div>
                <div class="tool-box" style="position: absolute; right:10px;">
                    <button id="btnAddStaff" data-href="{{route('add-staff')}}" class="btn-manager"><i class="fa fa-plus" style="margin-right: 5px;"></i>Thêm nhân viên</button>
                </div>
		</div>
		<div class="box-content">
			<div id="box-content-data" class="box-content-data table-responsive">
				
                
			</div>
			<div class="view-loading">
				<div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                </div>
			</div>
			
		</div>
		
	</div>
@endsection