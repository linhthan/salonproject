@extends('admin.admin_template')
@section('title','Hóa đơn')
@section('css')
<link rel="stylesheet" href="{{asset('css/receipt_view_detail.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/receipt_view_detail.js')}}"></script>

@endsection
@section('content')
    <div id="box-content-container" class="row" data-href="{{url()->previous()}}" data-id="{{$id ?? 0}}" data-type="{{$type ?? -1}}">
           <div class="header-title">
               <span class="header-receipt-title">Chi tiết hóa đơn</span>
            
           </div>
           <div id="content-detail-receipt">
               
           </div>
           <div id="box-content-data" class="table-responsive">
				<div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
        </div>
          <div id="view-no-data" style="text-align: center; display: none;">
            <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
            <h3>Không tìm thấy kết quả</h3>
            <span style="display: block;">Vui lòng thử lại sau</span>
            <button id="btnTryAgain" class="btn-receipt btn-dark">
              Thử lại
          </button>
        </div>
                
			</div>

        
    </div>
@endsection