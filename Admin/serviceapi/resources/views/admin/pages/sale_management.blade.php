@extends('admin.admin_template')
@section('title','Quản lý doanh thu')
@section('css')
<link rel="stylesheet" href="{{asset('css/sale_management.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/sale_management.js')}}"></script>
@endsection
@section('content')
	<div id="box-info-top" class="row">
	    
	
	</div>
	<div class="row">
		<div class="top-filter">
				<div class="search-box" > 
					<select class="form-control" id="filterOption">
						<option value="-1">Tất cả</option>
					   	<option value="1">Đang hoạt động</option>
					   	<option value="0">Đã kết thúc</option>
				  </select>
				</div>
				<div class="search-box">
					
					<div class="input-group" >
		              <input id="input-search-sale" placeholder="tìm kiếm" class="form-control width100">
		              <span class="input-group-btn">
		                <button id="btn-search-appoinment" class="btn btn-info">
		                  Tìm kiếm
		                </button>
		              </span>
	            	</div>
				</div>
				<div class="tool-box" style="position: absolute; right:10px;">
					<button id="btnAddProduct" data-href="{{route('add-sale')}}" class="btn-manager"><i class="fa fa-plus" style="margin-right: 5px;"></i>Thêm khuyến mại</button>
				</div>
	
		</div>
		<div class="box-content">
			<div id="box-content-data" class="table-responsive">
				
                
			</div>
			<div style="text-align: center;">
				<div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                </div>
			</div>
		</div>
		

		

		<div class="modal fade" id="modalSale" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="titleModal">Tạo khuyến mại</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      	<div role="tabpanel">
		      	 <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#uploadTab" aria-controls="uploadTab" role="tab" data-toggle="tab">Thông tin khuyến mại</a>

                        </li>
                        <li role="presentation"><a href="#browseTab" aria-controls="browseTab" role="tab" data-toggle="tab">Banner khuyến mại</a>

                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="uploadTab">
                        	<form style="margin-top: 10px;">
						          <div class="form-group">
						            <label for="recipient-name" class="col-form-label">Tiêu đề:</label>
						            <input type="text" class="form-control"  id="title-sale">
						          </div>
						          <div class="form-group">
						            <label for="recipient-name" class="col-form-label">Liên kết:</label>
						            <input type="text" class="form-control" id="url-sale">
						          </div>
						           <div id="sale-status-box" class="form-input">
									<label>Trạng thái</label>
									<span class="product-category-box">
										<select id="status-sale">
											<option value="1">Đang hoạt động</option>
											<option value="0">Đã kết thúc</option>
										</select>
										<span class="icon-select-option">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 6"><path d="M5.5 4.793L10.146.146a.5.5 0 1 1 .708.708l-5 5a.5.5 0 0 1-.708 0l-5-5A.5.5 0 1 1 .854.146L5.5 4.793z" fill-rule="nonzero"></path></svg>
										</span>
									</span>
								  </div>
					        </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="browseTab">
                        	<div style="text-align: center;">
                        		<div class="form-input">
									<label>Ảnh banner</label>
									<div class="box-input-info">
										<div class="box-container-input">
											<input id="sale-thumb" class="input-info-product input-active" type="file" accept="image/*" name="product-title">
										</div>
									</div>
								</div>
								<img id="img-thumb" style="width:300px;margin-top: 10px; margin-bottom: 10px; ">
								<button type="button" id="btnDelete" class="btn btn-primary" style="display: none;">Xóa ảnh</button>
                        	</div>
                        </div>
                </div>
		        
		      </div>
		      <div class="modal-footer">
		        <button type="button" id="btnCancel" class="btn btn-secondary" data-dismiss="modal">Hủy bỏ</button>
		        <button type="button" id="btnSave" class="btn btn-primary">Thêm</button>
		      </div>
		    </div>
		  </div>
		</div>

	</div>
@endsection