@extends('admin.admin_template')
@section('title','Nhập hàng')
@section('css')
<link rel="stylesheet" href="{{asset('css/import_product.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/import_product.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
@endsection

@section('content')
	<div class="row">
		<div class="top-filter-header">
			<h2 class="title-top">
				Nhập hàng
			</h2>
			<div class="tool-box">
				<a href="{{route('products')}}">
					<img src="{{asset('adminlte/cancel.svg')}}">
				</a>
			</div>
		</div>
		<div class="grid form-import">
			
			<div class="col-sm-12 col-xs-12 container-info-product">
				<div class="row">
					<h3> Thông tin nhập hàng</h3>
				</div>
				<table id="table-order-info" class="table table-bordered table-hover">
						<thead>
						    <th>#</th>
						    <th>Tên sản phẩm</th>
						    <th>Giá</th>
						    <th>Số lượng</th>
						    <th>Tổng</th>
						    <th>Hành động</th>
						 </thead>
					    <tbody>
					    	  <tr>
                               	<td colspan="6" style="text-align: center;"> <button id="btn-add"><i class="fa fa-plus"></i>Thêm</button></td>
                               </tr>
					    	  <tr class="total-price-row"><td colspan="4"> Tổng thanh toán</td>
                                          <td  colspan="2" class="total-price">0</td>
                               </tr>
					    </tbody>
				</table>
			</div>

		</div>
		
	</div>
	<div class="bottom-box">
		<button id="btnCancel" data-href="{{route('products')}}" class="btn-import-product btn-cancel">
			Hủy bỏ
		</button>
		<button id="btnSave"  class="btn-import-product btn-save">
			Nhập hàng
		</button>
	</div>
@endsection