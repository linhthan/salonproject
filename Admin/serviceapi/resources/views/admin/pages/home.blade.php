@extends('admin.admin_template')
@section('title','Home')
@section('css')
<link rel="stylesheet" href="{{asset('css/home_page.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/home_page.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('adminlte/bower_components/chart.js/Chart.js')}}"></script> -->
<script type="text/javascript" src="{{asset('js/chart/Chart.min.js')}}"></script>
@endsection
@section('content')
<div class="row">
  <div class="col-sm-3 col-xs-12">
    <div class="info-box">
      <!-- Apply any bg-* class to to the icon to color it -->
      <span class="info-box-icon box-item"><i class="fa fa-shopping-cart"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Đơn hàng hôm nay</span>
        <span id="order-value" class="info-box-number">0</span>
      </div>
      <!-- /.info-box-content -->
    </div>
  </div>
  <div class="col-sm-3 col-xs-12">
    <div class="info-box">
      <!-- Apply any bg-* class to to the icon to color it -->
      <span class="info-box-icon box-item"><i class="fa fa-calendar-o"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Lịch hẹn hôm nay</span>
        <span id="appointment-value" class="info-box-number">0</span>
      </div>
      <!-- /.info-box-content -->
    </div>
  </div>
  <div class="col-sm-3 col-xs-12">
    <div class="info-box">
      <!-- Apply any bg-* class to to the icon to color it -->
      <span class="info-box-icon box-item"><i class="fa fa-user"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Khách hàng trong tháng</span>
        <span id="customer-value" class="info-box-number">0</span>
      </div>
      <!-- /.info-box-content -->
    </div>
  </div>
  <div class="col-sm-3 col-xs-12">
    <div class="info-box">
      <!-- Apply any bg-* class to to the icon to color it -->
      <span class="info-box-icon box-item"><i class="fa fa-bell"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">Khách hàng cần thông báo</span>
        <span id="notify-customer" class="info-box-number">0</span>
      </div>
      <!-- /.info-box-content -->
    </div>
  </div>
</div>
<div class="row data-dashboard">
  <!--Box Sales -->
  <div class="col-sm-6 col-xs-12 box-chart">
    <div class="box-chart-container">
      <div class="box-chart-label">
        <div class="box-chart-label-title">
          <div class="box-chart-title">
            Thu nhập gần đây
          </div>
          <div id="filter-value" data-filter ="7" class="box-chart-filter">
            7 ngày trước
          </div>
        </div>
        <div class="box-chart-icon-filter">
          <span id="filterOption" class="fa fa-ellipsis-h" type="button" data-toggle="dropdown"></span>
          <ul class="dropdown-menu" id="menu" role="menu" aria-labelledby="menu1">
            <li role="presentation" data-filter="7"><a role="menuitem" tabindex="-1" href="#">7 ngày trước</a></li>
            <li role="presentation" data-filter="30"><a role="menuitem" tabindex="-1" href="#">1 tháng trước</a></li> 
          </ul>
        </div>

      </div>
      <div class="box-chart-info">
        <span id="total-recent-sale">₫0</span>
      </div>
      <div class="box-chart-info-appointment">
        <span  class="info-appointment-label">Thu nhập từ Dịch vụ:</span>
        <span id="appointment-value-sale" class="info-appointment-value">0</span>
      </div>
      <div class="box-chart-info-appointment">
        <span  class="info-appointment-label">Thu nhập từ sản phẩm:</span>
        <span id="product-value" class="info-appointment-value">0K</span>
      </div>
      <div id="line-char-wrapper" class="chart-sales-wrapper">
        <!-- LINE CHART -->
        <div class="box box-info chart-sales">
          <div class="box-body chart-responsive">
            <div id="line-chart-box" class="chart">
              <canvas id="lineChart" style="height:250px"></canvas>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <ul class="legend-menu">
          <li>
            <span class="fa fa-circle" style="width: 8px; height: 8px; color:#0BC2B0 "></span>
            Sản phẩm
          </li>
          <li>
            <span class="fa fa-circle" style="width: 8px; height: 8px;color:#2B72BD"></span>
            Dịch vụ
          </li>

        </ul>
      </div>
      <div id="empty-sales" class="chart-empty-view" style="display: none;">
            <div>
              <span class="fa fa-bar-chart fa-3x"></span>
            </div>
            <h3>Chưa có dữ liệu</h3>
      </div>
    </div>
  </div>
<!--End Box Sales -->
<!-- Box Upcoming Appointment -->
  <div class="col-sm-6 col-xs-12 box-chart">
    <div class="box-chart-container">
      <div class="box-chart-label">
        <div class="box-chart-label-title">
          <div class="box-chart-title">
            Các lịch hẹn sắp tới
          </div>
          <div id="filter-value-appoinment" data-filter="7" class="box-chart-filter">
            7 ngày tới
          </div>
        </div>
        <div class="box-chart-icon-filter">
          <span id="filterOption-appointment" class="fa fa-ellipsis-h" type="button" data-toggle="dropdown"></span>
          <ul class="dropdown-menu" id="menu-appointment" role="menu" aria-labelledby="menu1">
            <li role="presentation" data-filter="7"><a role="menuitem" tabindex="-1" href="#">7 ngày tới</a></li>
            <li role="presentation" data-filter="30"><a role="menuitem" tabindex="-1" href="#">1 tháng tới</a></li> 
          </ul>
        </div>
      </div>
      <div class="box-chart-info">
        <span id="value-total-appointment">0</span>
      </div>
      <div class="box-chart-info-appointment">
        <span  class="info-appointment-label">Lịch đã đặt:</span>
        <span id="value-pending-appointment" class="info-appointment-value">0</span>
      </div>
      <div class="box-chart-info-appointment">
        <span  class="info-appointment-label">Lịch đã hủy:</span>
        <span id="value-cancel-appointment" class="info-appointment-value">0</span>
      </div>
      <div id="bar-char-wrapper" class="chart-sales-wrapper">
        <!-- Bar CHART -->
        <div class="box box-info chart-sales">
          <div class="box-body chart-responsive">
            <div id="bar-chart-box" class="chart">
              <canvas id="barChart" style="height:250px"></canvas>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <ul class="legend-menu">
          <li>
            <span class="fa fa-circle" style="width: 8px; height: 8px; color:#0BC2B0 "></span>
            Đã đặt
          </li>
          <li>
            <span class="fa fa-circle" style="width: 8px; height: 8px;color:#D2D6DE"></span>
            Đã hủy
          </li>

        </ul>
      </div>
      <div id="empty-appointment" class="chart-empty-view" style="display: none;">
            <div>
              <span class="fa fa-bar-chart fa-3x"></span>
            </div>
            <h3>Chưa có dữ liệu</h3>
      </div>
    </div>
  </div>
<!--End Box Upcoming Appointment -->

<!-- Box All Appointment -->
  <div class="col-sm-6 col-xs-12 box-chart">
   <div class="box-chart-container">
    <div class="box-chart-label">
      <div class="box-chart-label-title">
        <div class="box-chart-title">
            Lịch hẹn hôm nay
        </div>
      </div>
      <div class="box-chart-icon-filter">
        <div class="input-bar">
          <div class="input-bar-item width100">
            <div class="input-group">
              <input id="input-search-appointment" placeholder="tìm kiếm" class="form-control width100">
              <span class="input-group-btn">
                <button id="btn-search-appoinment" class="btn btn-info">
                  Tìm kiếm
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Table All Appointment -->
    <div id="box-table-all-appointment" class="chart-sales-wrapper table-responsive">
    

    </div>
    <!--End table All Appointment -->
    </div>
  </div>
<!--End Box All Appointment -->

<!-- Box Next Appointment -->
<div class="col-sm-6 col-xs-12 box-chart">
 <div class="box-chart-container">
    <div class="box-chart-label">
      <div class="box-chart-label-title">
        <div class="box-chart-title">
          Các lịch hẹn sắp diễn ra 
        </div>
      </div>
      <div class="box-chart-icon-filter">
        <div class="input-bar">
          <div class="input-bar-item width100">
            <div class="input-group">
              <input id="input-search-next-appointment" placeholder="tìm kiếm" class="form-control width100">
              <span class="input-group-btn">
                <button id="btn-search-next-appoinment" class="btn btn-info">
                  Tìm kiếm
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Table next Appointment -->
    <div id="box-table-next-appointment" class="chart-sales-wrapper table-responsive">
    

    </div>
    <!--End table next  Appointment -->
    </div>
</div>
<!--End Box next Appointment -->
</div>
@endsection