@extends('admin.admin_template')
@section('title','Thêm sản phẩm')
@section('css')
<link rel="stylesheet" href="{{asset('css/add_product.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/add_product.js')}}"></script>
@endsection
@section('content')
	<div class="row">
		<div class="top-filter-header">
			<h2 class="title-top">
				Thêm chương trình khuyến mại
			</h2>
			<div class="tool-box">
				<a href="{{route('products')}}">
					<img src="{{asset('adminlte/cancel.svg')}}">
				</a>
			</div>
		</div>
		<div class="row form-import">
			<h3> Thông tin khuyến mại</h3>
		</div>
		<div class="grid form-import">
			
			<div class="col-sm-6 col-xs-12 container-info-product">
				<div class="form-input">
					<label>Tiêu đề</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="product-title" class="input-info-product input-active" type="text" name="product-title">
						</div>
					</div>
					<span id="error-title-product" class="error-msg">Bạn chưa nhập tiêu đề</span>
				</div>
				<div class="form-input">
					<label>Liên kết</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="product-title" class="input-info-product input-active" type="text" name="product-title">
						</div>
					</div>
					<span id="error-title-product" class="error-msg">Bạn chưa nhập tên sản phẩm</span>
				</div>

				<div class="form-input">
					<label>Ảnh sản phẩm</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="product-thumb" class="input-info-product input-active" accept="image/*"
							 type="file" name="product-title">
						</div>
					</div>
					<span id="error-thumb-product" class="error-msg">Bạn chưa chọn ảnh</span>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<img id="thumb-product" style="display: none;" class="thumb-product">
			</div>
		</div>
		
	</div>
	<div class="bottom-box">
		<button id="btnCancel" data-href="{{route('products')}}" class="btn-import-product btn-cancel">
			Hủy bỏ
		</button>
		<button id="btnSave"  class="btn-import-product btn-save">
			Lưu lại
		</button>
	</div>
@endsection