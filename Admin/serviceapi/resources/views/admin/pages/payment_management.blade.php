@extends('admin.admin_template')
@section('title','Quản lý thu ngân')
@section('css')
<link rel="stylesheet" href="{{asset('css/payment_management.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/payment_management.js')}}"></script>
@endsection
@section('content')
	<div id="box-info-top" class="row">
		<div class="col-sm-3 col-xs-12">
		    <div class="info-box">
		      <!-- Apply any bg-* class to to the icon to color it -->
		      	<span class="info-box-icon box-item"><i class="fa fa-usd"></i></span>
			    <div class="info-box-content">
			       <span class="info-box-text">Tổng thu nhập</span>
			       <span id="total-sale" class="info-box-number">0 VNĐ</span>
			    </div>
		      <!-- /.info-box-content -->
		    </div>
		</div>  
		<div class="col-sm-3 col-xs-12">
		    <div class="info-box">
		      <!-- Apply any bg-* class to to the icon to color it -->
		      	<span class="info-box-icon box-item"><i class="fa fa-calendar-o"></i></span>
			    <div class="info-box-content">
			       <span class="info-box-text">Lịch hẹn đã thanh toán</span>
			       <span id="booking-paid" class="info-box-number">0</span>
			    </div>
		      <!-- /.info-box-content -->
		    </div>
		</div>    
	 
		<div class="col-sm-3 col-xs-12">
		    <div class="info-box">
		      <!-- Apply any bg-* class to to the icon to color it -->
		      	<span class="info-box-icon box-item"><i class="fa fa-shopping-cart"></i></span>
			    <div class="info-box-content">
			       <span class="info-box-text">Đơn hàng đã thanh toán</span>
			       <span id="order-paid" class="info-box-number">0</span>
			    </div>
		      <!-- /.info-box-content -->
		    </div>
		</div>   
		<div class="col-sm-3 col-xs-12">
		    <div class="info-box">
		      <!-- Apply any bg-* class to to the icon to color it -->
		      	<span class="info-box-icon box-item"><i class="fa fa-clock-o"></i></span>
			    <div class="info-box-content">
			       <span class="info-box-text">Đơn hàng chờ thanh toán</span>
			       <span id="pending-receipt" class="info-box-number">0</span>
			    </div>
		      <!-- /.info-box-content -->
		    </div>
		</div>     
	
	</div>
	<div class="row">
			<div class="top-filter">
				<div class="search-box" > 
					<select class="form-control" id=filterOption>
						<option value="-1">Tất cả</option>
					    <option value="0">Chưa thanh toán</option>
					    <option value="1">Đã thanh toán</option>
					    <option value="2">Hóa đơn dịch vụ</option>
					    <option value="3">Hóa đơn bán hàng</option>
					   
				  </select>
				</div>
				<div class="search-box">
					
					<div class="input-group" >
		              <input id="input-search-appointment" placeholder="tìm kiếm" class="form-control width100">
		              <span class="input-group-btn">
		                <button id="btn-search-appoinment" class="btn btn-info">
		                  Tìm kiếm
		                </button>
		              </span>
	            	</div>
				</div>
		</div>
		<div class="box-content">
			<div id="box-content-data" class="table-responsive">
				<div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                </div>
                
			</div>
			
		</div>
		
	</div>
@endsection