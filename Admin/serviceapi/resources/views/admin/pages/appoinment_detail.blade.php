@extends('admin.admin_template')
@section('title','Chi tiết lịch hẹn')
@section('css')
<link rel="stylesheet" href="{{asset('css/appointment_detail.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/appointment_detail.js')}}"></script>

@endsection
@section('content')
    <div id="box-content-container" class="row" data-href="{{url()->previous()}}" data-id="{{$id ?? 0}}">
        <!--    <div class="header-title">
               <span class="header-appointment-title">Chi tiết lịch hẹn</span>
	            <div class="tool-box">
					<a href="{{route('products')}}">
						<img src="{{asset('adminlte/cancel.svg')}}">
					</a>
				</div>
           </div> -->
           <div class="top-filter-header">
            <h2 class="title-top">
                Thêm dịch vụ
            </h2>
            <div class="tool-box">
                <a href="{{url()->previous()}}">
                    <img src="{{asset('adminlte/cancel.svg')}}">
                </a>
            </div>
        </div>
           <div id="content-detail-appointment">
               
           </div>
          
    </div>
@endsection