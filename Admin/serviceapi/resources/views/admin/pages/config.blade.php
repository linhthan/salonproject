@extends('admin.admin_template')
@section('title','Cấu hình Salon')
@section('css')
    <link rel="stylesheet" href="{{asset('css/kendo.min.css')}}">


    <link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
    <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/config.css')}}">
    <link href="{{asset('css/icon_sign_up.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

@endsection
@section('js')
    <script type="text/javascript" src="{{asset('js/timepicker/kendo.min.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/config.js')}}"></script>
@endsection
@section('content')
    <div class="row">
        <div class="top-filter-header">
            <h2 class="title-top">
                Cấu hình Salon
            </h2>
            <div class="tool-box">
                    <img class="img-icon" id="icon-edit-cancel" data-edit="{{asset('adminlte/edit.svg')}}" data-cancel="{{asset('adminlte/cancel.svg')}}" src="{{asset('adminlte/edit.svg')}}">
            </div>
        </div>
        <div class="row form-import">
	            <h3> Thông tin Salon</h3>
	        </div>
        <div id="box-content-salon">
        	
	        <div id="box-config-salon" class="row grid form-import">

	            <div class="col-sm-6 col-xs-12 container-info-product">
	                <div class="form-input">
	                    <label>Tên salon</label>
	                    <div class="box-input-info">
	                        <div class="box-container-input">
	                            <input id="salon-title" class="input-info-product input-active" type="text"
	                                   name="product-title">
	                        </div>
	                    </div>
	                    <span id="error-salon-title" class="error-msg">Bạn chưa nhập tên salon</span>
	                </div>
	                <div class="form-input-price">
	                    <div class="form-input-price-product">
	                        <label>Số ghế:</label>
	                        <div class="box-input-info">
	                            <div class="box-container-input">
	                                <input id="count-seat-input" class="input-info-product duration-input half" type="text"
	                                       name="current-price">
	                            </div>
	                        </div>
	                        <span id="error-count-seat-input" class="error-msg"></span>
	                    </div>
	                    <div class="form-input-price-product">
	                        <label>Thời gian/chỗ ( tối thiểu 10 phút)</label>
	                        <div class="box-input-info">
	                            <div class="box-container-input">
	                                <input id="duration-slot-input" class="input-info-product duration-input half" type="text"
	                                       name="current-price">
	                            </div>
	                        </div>
	                        <span id="error-duration-slot-input" class="error-msg"></span>
	                    </div>

	                </div>
	                <div class="form-input-price">
	                    <div class="form-input-price-product">
	                        <label>Thời gian mở cửa:</label>
	                        <div class="box-input-info">
	                            <div class="box-container-input">
	                                <input id="open-time-input" type="text" style="width: 100%;" name="current-price">
	                            </div>
	                        </div>
	                        <span id="error-open-time-input" class="error-msg"></span>
	                    </div>
	                    <div class="form-input-price-product">
	                        <label>Thời gian đóng cửa</label>
	                        <div class="box-input-info">
	                            <div class="box-container-input">
	                                <input id="close-time-input" type="text" style="width: 100%;" name="current-price">
	                            </div>
	                        </div>
	                        <span id="error-close-time-input" class="error-msg"> xxx</span>
	                    </div>

	                </div>
	                <div class="form-input">
	                    <label>Địa chỉ<span>(*)</span></label>
	                    <div class="box-input-info">
	                        <div class="box-container-input rs-select2 js-select-simple" >
	                            <select id="select-city-option" name="subject">

	                            </select>

	                            <div class="select-dropdown"></div>
	                        </div>

	                    </div>
	                    <span id="error-select-city-option" class="error-msg">Bạn chưa chọn thành phố</span>
	                </div>

	                <div class="form-input-price">
	                    <div class="form-input-price-product">
	                        <label>Quận/Huyện:</label>
	                        <div class="box-input-info">
	                            <div class="box-container-input rs-select2 js-select-simple">
	                                <select id="select-district-option" name="subject">
	                                    <option disabled="disabled" selected="selected">Chọn Quận/Huyện</option>
	                                </select>

	                                <div class="select-dropdown"></div>
	                            </div>
	                        </div>
	                        <span id="error-select-district-option" class="error-msg">Bạn chưa chọn quận/huyện</span>
	                    </div>
	                    <div class="form-input-price-product">
	                        <label>Phường/Xã</label>
	                        <div class="box-input-info">
	                            <div class="box-container-input rs-select2 js-select-simple">
	                                <select id="select-ward-option" name="subject">
	                                    <option disabled="disabled" selected="selected">Chọn Phường/Xã</option>
	                                </select>

	                                <div class="select-dropdown"></div>
	                            </div>
	                        </div>
	                        <span id="error-select-ward-option" class="error-msg">Bạn chưa chọn phường/xã</span>
	                    </div>

	                </div>



	                <div class="form-input">
	                    <label>Chi tiết địa chỉ</label>
	                    <textarea id="detail-location" class="detail-location" placeholder="Nhập chi tiết địa chỉ"
	                              style="flex-grow: 1;"></textarea>
	                    <span id="error-detail-location" class="error-msg">Bạn chưa nhập chi tiết địa chỉ</span>
	                </div>
	                <div class="form-input">
	                    <label>Ảnh salon</label>
	                    <div class="box-input-info">
	                        <div class="box-container-input">
	                            <input id="salon-thumb" class="input-info-product input-active" accept="image/*" type="file"
	                                   name="product-title">
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-sm-6 col-xs-12">
	                <img id="thumb-salon" class="thumb-product">
	                <div id="box-delete-thumb" style="text-align: center; display: none;">
	                	<button id="btnDelete"  class="btn-import-product btn-save">
				            Xóa ảnh
				        </button>
	                </div>
	            </div>
	        </div>

	        <div id="box-button" class="bottom-box">
		        <button id="btnSave"  class="btn-import-product btn-save">
		            Lưu lại
		        </button>
		    </div>
        </div>

        <div class="box-content">
			<div id="box-content-data" class="table-responsive" style="text-align: center;">
				<div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                </div>
                
			</div>
			<div id="view-no-data">
			    <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
			    <h3>Không tìm thấy kết quả</h3>
			    <span>Hãy thử lựa chọn tìm kiếm khác</span>
			</div>
			
		</div>

    </div>
@endsection
