@extends('admin.admin_template')
@section('title','Quản lý mẫu tóc')
@section('css')
<link rel="stylesheet" href="{{asset('css/gallery_management.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/gallery_management.js')}}"></script>
@endsection
@section('content')
	<div class="row">
		<div class="top-filter">

				<div class="search-box">
					
					<div class="input-group" >
		              <input id="input-search-hair-style" placeholder="tìm kiếm" class="form-control width100">
		              <span class="input-group-btn">
		                <button id="btn-search-hair-style" class="btn btn-info">
		                  Tìm kiếm
		                </button>
		              </span>
	            	</div>
				</div>
				<div class="tool-box" style="position: absolute; right:10px;">
					<button id="btnAddHairStyle" data-href="{{route('add-hair-style')}}" class="btn-manager"><i class="fa fa-plus" style="margin-right: 5px;"></i>Thêm mẫu tóc</button>
				</div>
		</div>
		<div id="box-all-hair-style" style="text-align: center;">
			<div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                </div>
		</div>
		<nav id="nav-image-infor" class="navbar-toogleable dialog-right">
			<img id="img-close" class="close top-left-fixed" src="{{asset('adminlte/cancel.svg')}}">
			
			<div class="center-item">
					<h3>Thông tin ảnh</h3>
				</div>
			<div class="center-item item-image">
					<img id="image-preview-hair-style" style="width: 200px;">
				</div>
			<div class="infor-hair">
				<div class="header-infor">
					<span>Thông tin chi tiết</span>
				</div>
				
				<div class="flex-item">
					<span class="text-align-right">Tên Kiểu tóc:</span>
					<span class="text-infor"  id="title-hair"></span>
				</div>
				<div class="flex-item">
					<span class="text-align-right">Ngày tạo:</span>
					<span class="text-infor"  id="date-create-hair"></span>
				</div>
				<div class="flex-item">
					<span class="text-align-right">Số lượt xem:</span>
					<span class="text-infor"  id="view-hair"></span>
				</div>
				<div class="flex-item">
					<span class="text-align-right">Số lượt tải xuống:</span>
					<span class="text-infor"  id="download-hair"></span>
				</div>
			</div>
		</nav>
	</div>
@endsection