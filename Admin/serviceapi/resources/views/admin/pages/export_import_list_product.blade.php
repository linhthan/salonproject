 @if(isset($import) &&isset($import_detail))
 @php
    $totalPrice = 0;
 @endphp
 @foreach($import_detail as $item)
    @php
      $totalPrice =  $totalPrice + $item->price * $item->quantity;
    @endphp                 
 @endforeach
 @endif
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/receipt_detail_export.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
  <script type="text/javascript" src="{{asset('js/receipt_detail_export.js')}}"></script>

</head>
<body>

  <div id="box-content-container" class="container-receipt" data-href="{{url()->previous()}}" data-id="{{$id ?? 0}}" data-type="{{$type ?? -1}}">
           <div class="header-title">
               <span class="header-receipt-title">Hóa đơn</span>
            
           </div>
           <div class="infor-receipt">
               <div style="float: right; width: 200px; text-align: right;">Thời gian: {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $import->import_at)->format('H:i d/m/Y')}}</div>
                <div>Hóa đơn: Nhập hàng</div>

           </div>
           <div class="content-receipt" id="content-detail-receipt">

               <table id="table-service" class="table table-bordered"> 
                                <thead>
                                    <th>#</th>
                                    <th>Dịch vụ</th>
                                    <th>Giá</th>
                                </thead>
                                <tbody id="table-service-content">
                                    <tr>
                                          <td>#</td>
                                          <td>Sản phẩm </td>
                                          <td>Số lượng</td>
                                          <td>Đơn giá</td>
                                          <td>Thành tiền</td>
                                        </tr>
                                    @if($import_detail)
                                      @foreach($import_detail as $item)
                                        <tr>
                                          <th scope="row">{{$loop->iteration}}</th>
                                          <td>{{$item->title}}</td>
                                          <td>{{$item->quantity}}</td>
                                          <td>{{$item->price}}</td>
                                          <td>{{$item->quantity * $item->price}}</td>
                                        </tr>
                                      @endforeach
                                          <tr class="total-price-row"><td colspan="4"> Tổng hóa đơn</td>
                                          <td>{{$totalPrice}}</td>
                                          </tr>
 
                                    @endif
                                </tbody>
                </table>
           
           </div>
      </div>
 
    </div>

</body>
</html>
