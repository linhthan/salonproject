@extends('admin.admin_template')
@section('title','Nhập hàng')
@section('css')
<link rel="stylesheet" href="{{asset('css/create_order.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/create_order.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
@endsection
@section('content')
	<div class="row">
		<div class="top-filter-header">
			<h2 class="title-top">
				Tạo đơn hàng
			</h2>
			<div class="tool-box">
				<a href="{{route('order')}}">
					<img src="{{asset('adminlte/cancel.svg')}}">
				</a>
			</div>
		</div>
		<div class="grid form-import">
			
			<div class="col-sm-6 col-xs-12 container-info-product">
				<div class="row">
					<h3> Thông tin đơn hàng</h3>
				</div>
				<table id="table-order-info" class="table table-bordered table-hover">
						<thead>
						    <th>#</th>
						    <th>Tên sản phẩm</th>
						    <th>Giá</th>
						    <th>Số lượng</th>
						    <th>Tổng</th>
						    <th>Hành động</th>
						 </thead>
					    <tbody>
					    	  <tr>
                               	<td colspan="6" style="text-align: center;"> <button id="btn-add"><i class="fa fa-plus"></i>Thêm</button></td>
                               </tr>
					    	  <tr class="total-price-row"><td colspan="4"> Tổng thanh toán</td>
                                          <td  colspan="2" class="total-price">0</td>
                               </tr>
					    </tbody>
				</table>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="row form-import">
					<h3> Thông tin khách hàng</h3>
				</div>
				<div class="form-input">
					<label>Tên khách hàng</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="order-name-customer" class="input-info-product input-active" type="text" name="product-title">
						</div>
					</div>
					<span id="error-name-customer" class="error-msg">Bạn chưa nhập tên khách hàng</span>
				</div>
				<div class="form-input">
					<label>Số điện thoại</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="order-phone-customer" class="input-info-product input-active" type="text" >
						</div>
					</div>
					<span id="error-phone-number" class="error-msg"></span>
				</div>
				<div class="form-input">
					<label>Địa chỉ</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="order-address" class="input-info-product input-active" type="text" name="product-title">
						</div>
					</div>
					<span id="error-address-customer" class="error-msg">Bạn chưa nhập địa chỉ</span>
				</div>
			</div>
		</div>
		
	</div>
	<div class="bottom-box">
		<button id="btnCancel" data-href="{{route('order')}}" class="btn-import-product btn-cancel">
			Hủy bỏ
		</button>
		<button id="btnSave"  class="btn-import-product btn-save">
			Thêm vào
		</button>
	</div>
@endsection
