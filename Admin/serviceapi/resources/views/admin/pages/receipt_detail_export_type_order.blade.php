 @if($bill && $order && $order_detail && $salon)
 @php
    $totalPrice = 0
 @endphp
 @foreach($order_detail as $item)
    @php
      $totalPrice =  $totalPrice + $item->price * $item->quantity
    @endphp                 
 @endforeach
 @endif
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/receipt_detail_export.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
  <script type="text/javascript" src="{{asset('js/receipt_detail_export.js')}}"></script>

</head>
<body>

  <div id="box-content-container" class="container-receipt" data-href="{{url()->previous()}}">
           <div class="header-title">
               <span class="header-receipt-title">Hóa đơn</span>
            
           </div>
           <div class="infor-receipt">
               <div style="float: right; width: 200px; text-align: right;">Thời gian: {{Carbon\Carbon::now()->format('H:i d-m-Y')}}</div>
                <div style="font-size: 20px; font-weight: bold;">{{$salon->name}}</div>
                <div>{{$salon->detail_location}}</div>
                <div>Hóa đơn: sử dụng dịch vụ</div>
                <div >Khách hàng: {{$order['cus_name']}}</div>
                <div >Số điện thoại: {{$order['phone_number']}}</div>
                <div> Địa chỉ: {{$order['address']}}</div>
                

           </div>
           <div class="content-receipt" id="content-detail-receipt">

               <table id="table-service" class="table table-bordered"> 
                                <thead>
                                    <th>#</th>
                                    <th>Sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Đơn Giá</th>
                                    <th>Tổng</th>
                                </thead>
                                <tbody id="table-service-content">
                                    <tr>
                                        <td>#</td>
	                                    <td>Sản phẩm</td>
	                                    <td>Số lượng</td>
	                                    <td>Đơn Giá</td>
	                                    <td>Tổng</td>
                                        </tr>
                                    @if($order_detail)
                                      @foreach($order_detail as $item)
                                        <tr>
                                          <th scope="row">{{$loop->iteration}}</th>
                                          <td>{{$item->title}}</td>
                                          <td>{{$item->quantity}}</td>
                                          <td>{{$item->price}}</td>
                                          <td>{{$item->quantity * $item->price}}</td>
                                        </tr>
                                      @endforeach
                                          <tr class="total-price-row"><td colspan="2"> Tổng hóa đơn</td>
                                          <td>{{$totalPrice}}</td>
                                          </tr>
 
                                    @endif
                                </tbody>
                </table>
               <div class="receipt-detail">

                  <div align="right">Giảm giá(%): {{$bill['discount_percent'] ?? 0}}%</div>
                  <div align="right">Giảm giá(VNĐ): {{$bill['discount'] ?? 0}} VNĐ</div>
                  <div align="right">Tổng thanh toán: {{$bill['total_price']}}</div>
                 
               </div>
               <div style="text-align: center; margin-top: 20px; font-size: 18px;">Cảm ơn quý khách đã sử dụng dịch vụ</div>
           </div>
      </div>
 
    </div>

</body>
</html>
