@extends('admin.admin_template')
@section('title','Chi tiết nhân viên')
@section('css')
<link rel="stylesheet" href="{{asset('css/staff_detail.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/staff_detail.js')}}"></script>
@endsection
@section('content')
    <div class="row">
        <div class="top-filter-header">
            <h2 class="title-top">
                Thông tin về nhân viên
            </h2>
            <div class="tool-box">
                <a href="{{route('employees')}}">
                    <img src="{{asset('adminlte/cancel.svg')}}">
                </a>
            </div>
        </div>
        <div style="text-align: center;">
            <div id="box-content-data" class="table-responsive" style="text-align: center;">
                <div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                </div>
                <div id="view-no-data">
                    <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
                    <h3>Không tìm thấy kết quả</h3>
                    <span>Hãy thử lựa chọn tìm kiếm khác</span>
                </div>
            </div>
            
        </div>
        <div id="content-grid" data-id="{{$id ?? 0}}" class="grid form-import">
            
            <div class="col-sm-6 col-xs-12 container-info-product">
                <div class="form-input" style="text-align: center;">
                    <h3> Thông tin nhân viên</h3>
                </div>
                <div class="form-input">
                    <label>Tên nhân viên</label>
                    <div class="box-input-info">
                        <div class="box-container-input">
                            <input id="user-name" class="input-info-product" type="text" name="product-title">
                        </div>
                    </div>
                    <span id="error-user-name" class="error-msg">Bạn chưa nhập tên nhân viên</span>
                </div>
                
                <div class="form-input">
                    <label>Địa chỉ Email</label>
                    <div class="box-input-info">
                        <div class="box-container-input">
                            <input id="user-email" class="input-info-product" type="email" name="email">
                        </div>
                    </div>
                    <span id="error-user-email" class="error-msg">Bạn chưa nhập email</span>
                </div>
                <div class="form-input">
                    <label>Số điện thoại</label>
                    <div class="box-input-info">
                        <div class="box-container-input">
                            <input id="user-phone-number" class="input-info-product input-password" type="text" name="phone" >
                        </div>
                    </div>
                    <span id="error-user-phone-number" class="error-msg">Bạn chưa nhập số điện thoại</span>
                </div>

                <div class="form-input">
                    <label>Mật khẩu</label>
                    <div class="box-input-info">
                        <div class="box-container-input">
                            <span id="show-pass" class="btn-show-pass">
                                <i class="fa fa-eye"></i>
                            </span>
                            <input id="user-password" class="input-info-product" type="password" >
                        </div>
                    </div>
                    <span id="error-user-password" class="error-msg">Bạn chưa nhập mật khẩu</span>
                </div>

                <div class="form-input">
                    <label>Vai trò</label>

                    
                    <span class="product-category-box">
                        <select disabled="true" id="user-type">
                            <option value="1" selected="true">Nhân viên dịch vụ</option>
                            <option value="3">Nhân viên lễ tân</option>
                             <option value="4">Nhân viên thu thập</option>
                        </select>
                        <span  style="display: none;" class="icon-select-option">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 6"><path d="M5.5 4.793L10.146.146a.5.5 0 1 1 .708.708l-5 5a.5.5 0 0 1-.708 0l-5-5A.5.5 0 1 1 .854.146L5.5 4.793z" fill-rule="nonzero"></path></svg>
                        </span>
                    </span>

                </div>
<!--                <div class="form-input">
                    <label>Số lượng</label>
                    <div class="box-input-info">
                        <div class="box-container-input">
                            <input id="product-quantity" class="input-info-product input-count input-active" type="text" name="product-quantity">
                        </div>
                    </div>
                    <span id="error-quantity-product" class="error-msg"></span>
                </div> -->
                <div class="form-input">
                    <label>Ảnh nhân viên</label>
                    <div class="box-input-info">
                        <div class="box-container-input">
                            <input id="user-avatar-input" class="input-info-product input-active" accept="image/*" type="file" name="product-title">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="form-input" style="text-align: center;">
                    <h3>Ảnh nhân viên</h3>
                </div>
                
                <img id="avatar-user" style="display: none;" class="thumb-product">
            </div>
        </div>
        
    </div>
    <div id="layout-btn" class="bottom-box">
        <button id="btnCancel" data-href="{{route('employees')}}" class="btn-import-product btn-cancel">
            Hủy bỏ
        </button>
        <button id="btnSave"  class="btn-import-product btn-save">
            Lưu lại
        </button>
    </div>
@endsection