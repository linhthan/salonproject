@extends('admin.admin_template')
@section('title','Home')
@section('css')
<link rel="stylesheet" href="{{asset('css/appointment_management.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/appointment_management.js')}}"></script>
@endsection
@section('content')
	<div id="box-info-top" class="row">
		<div class="col-sm-3 col-xs-12">
		    <div class="info-box">
		      <!-- Apply any bg-* class to to the icon to color it -->
		      	<span class="info-box-icon box-item"><i class="fa fa-calendar-o"></i></span>
			    <div class="info-box-content">
			       <span class="info-box-text">Lịch hẹn hôm nay</span>
			       <span id="appointment-value" class="info-box-number">0</span>
			    </div>
		      <!-- /.info-box-content -->
		    </div>
		</div>    
		 <div class="col-sm-3 col-xs-12">
		    <div class="info-box">
		      <!-- Apply any bg-* class to to the icon to color it -->
		      	<span class="info-box-icon box-item"><i class="fa fa-usd"></i></span>
			    <div class="info-box-content">
			       <span class="info-box-text">Tổng thu nhập</span>
			       <span id="total-sale-value" class="info-box-number">0 VNĐ</span>
			    </div>
		      <!-- /.info-box-content -->
		    </div>
		</div>   
		<div class="col-sm-3 col-xs-12">
		    <div class="info-box">
		      <!-- Apply any bg-* class to to the icon to color it -->
		      	<span class="info-box-icon box-item"><i class="fa fa-bell"></i></span>
			    <div class="info-box-content">
			       <span class="info-box-text">Khách hàng cần thông báo</span>
			       <span id="notify-customer-value" class="info-box-number">0</span>
			    </div>
		      <!-- /.info-box-content -->
		    </div>
		</div>   
		<div class="col-sm-3 col-xs-12">
		    <div class="info-box">
		      <!-- Apply any bg-* class to to the icon to color it -->
		      	<span class="info-box-icon box-item"><i class="fa fa-ban"></i></span>
			    <div class="info-box-content">
			       <span class="info-box-text">Lịch hẹn bị hủy hôm nay</span>
			       <span id="cancel-appointment-value" class="info-box-number">0</span>
			    </div>
		      <!-- /.info-box-content -->
		    </div>
		</div>     
	
	</div>
	<div class="row">
			<div class="top-filter">
				<div class="search-box" > 
					<select class="form-control" id="filterOption">
						<option value="-1">Tất cả</option>
					    <option value="0">Lịch hẹn mới</option>
					     <option value="1">Lịch đã check in</option>
					    <option value="2">Lịch đã hủy</option>
					    <option value="3">Đã thanh toán</option>
					   
				  </select>
				</div>
				<div class="search-box">
					
					<div class="input-group" >
		              <input id="input-search-appointment" placeholder="tìm kiếm" class="form-control width100">
		              <span class="input-group-btn">
		                <button id="btn-search-appoinment" class="btn btn-info">
		                  Tìm kiếm
		                </button>
		              </span>
	            	</div>
				</div>

				 <div class="tool-box" style="position: absolute; right:10px;">
                    <button id="btnAddAppointment" data-href="{{route('add-appointment')}}" class="btn-manager"><i class="fa fa-plus" style="margin-right: 5px;"></i>Tạo lịch hẹn</button>
                </div>
		</div>
		<div class="box-content">
			<div id="box-content-data" class="table-responsive">
				<div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                </div>
                
			</div>
			
		</div>
		
	</div>
@endsection