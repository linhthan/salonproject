@extends('admin.admin_template')
@section('title','Chi tiết sản phẩm')
@section('css')
<link rel="stylesheet" href="{{asset('css/product_detail.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/product_detail.js')}}"></script>
@endsection
@section('content')
	<div class="row" id="content-product-detail" data-href="{{url()->previous()}}" data-id="{{$id ?? 0}}">
		<div class="top-filter-header">
			<h2 class="title-top">
				Thêm sản phẩm
			</h2>
			<div class="tool-box">
				<a href="{{route('products')}}">
					<img src="{{asset('adminlte/cancel.svg')}}">
				</a>
			</div>
		</div>
		<div class="row form-import">
			<h3> Thông tin sản phẩm</h3>
		</div>
		<div id="box-content-data" class="table-responsive" style="text-align: center;"> 
				<div id="progress-loading">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                </div>
                
		</div>
		<div id="grid-infor" class="grid form-import">
			
			
		</div>
		
	</div>
	<div class="bottom-box">
		<button id="btnSave" data-href="{{route('products')}}"   class="btn-import-product btn-save">
			Trở lại
		</button>
	</div>
@endsection