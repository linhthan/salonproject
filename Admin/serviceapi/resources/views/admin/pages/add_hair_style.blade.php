@extends('admin.admin_template')
@section('title','Thêm mẫu tóc')
@section('css')
<link rel="stylesheet" href="{{asset('css/add_hair_style.css')}}">
<link rel="stylesheet" href="{{asset('croppie/croppie.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('croppie/croppie.js')}}"></script>
<script type="text/javascript" src="{{asset('js/add_hair_style.js')}}"></script>
@endsection
@section('content')
	<div class="row">
		<div class="top-filter-header">
			<h2 class="title-top">
				Tạo mẫu tóc
			</h2>
			<div class="tool-box">
				<a href="{{route('gallerys')}}">
					<img src="{{asset('adminlte/cancel.svg')}}">
				</a>
			</div>
		</div>
		<div class="row form-import">
			<h3> Trích xuất mẫu tóc</h3>
		</div>
		<div class="grid form-import">
			<div class="col-sm-6 col-xs-12">
				<div class="form-input">
					<label>Ảnh thu thập</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="input-image-source" class="input-info-product" accept="image/*" type="file" name="product-title">
						</div>
					</div>
					<span id="error-image-soure" class="error-msg">Bạn chưa chọn ảnh</span>
					<img id="img-source" style="display: none; margin-top: 10px;" class="img-select">
					
				</div>
				<div class="form-input" style="text-align: center;">
					<button id="btnExtract" class="btn-import-product btn-save">
						Lấy mẫu tóc
					</button>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-input" style="text-align: center;">
					<h3>Ảnh mẫu tóc</h3>
					<img id="img-hair-style" style="display: none;" class="hair-style-image">
				</div>
				<div id="form-input-title" class="form-input" style="display: none;">
					<label>Tên mẫu tóc</label>
					<div class="box-input-info">
						<div class="box-container-input">
							<input id="hair-title" class="input-info-product" type="text" name="hair-title">
						</div>
					</div>
					<span id="error-hair-title" class="error-msg">Bạn chưa nhập tên mẫu tóc</span>
				</div>
				<div class="form-input" style="text-align: center;">
					<div id="progress-loading" style="display: none;">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                                  <div class="bar4"></div>
                                  <div class="bar5"></div>
                                  <div class="bar6"></div>
                                  <div class="bar7"></div>
                                  <div class="bar8"></div>
                </div>
				</div>
			</div>
		</div>

		
	</div>
	<div class="bottom-box">
		<button id="btnCancel" data-href="{{route('gallerys')}}" class="btn-import-product btn-cancel">
			Hủy bỏ
		</button>
		<button id="btnSave"  class="btn-import-product btn-save">
			Lưu lại
		</button>
	</div>
	<div id="uploadimageModal" class="modal" role="dialog">
 		<div class="modal-dialog">
 			 <div class="modal-content">
        		<div class="modal-header">
          			<button type="button" class="close" data-dismiss="modal">&times;</button>
          			<h4 class="modal-title">Crop ảnh</h4>
        		</div>
        		<div class="modal-body">
          			<div class="row">
      					<div class="col-md-8 text-center">
        					<div id="image_demo" style="width:600px; margin-top:30px"></div>
       					</div>
				    
    				</div>
    				<div class="cr-slider-wrap" style="text-align: center;">
    					 <button class="btn btn-success crop_image">Crop</button>
    				</div>
        		</div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
		        </div>
     		</div>
    	</div>
	</div>
@endsection