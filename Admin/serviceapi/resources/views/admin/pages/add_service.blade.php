@extends('admin.admin_template')
@section('title','Thêm dịch vụ')
@section('css')
<link rel="stylesheet" href="{{asset('css/add_service.css')}}">
<link rel="stylesheet" href="{{asset('adminlte/bower_components/morris.js/morris.css')}}">
@endsection
@section('js')
<script type="text/javascript" src="{{asset('js/add_service.js')}}"></script>
@endsection
@section('content')
    <div class="row">
        <div class="top-filter-header">
            <h2 class="title-top">
                Thêm dịch vụ
            </h2>
            <div class="tool-box">
                <a href="{{route('service')}}">
                    <img src="{{asset('adminlte/cancel.svg')}}">
                </a>
            </div>
        </div>
        <div class="row form-import">
            <h3> Thông tin dịch vụ</h3>
        </div>
        <div class="grid form-import">
            
            <div class="col-sm-6 col-xs-12 container-info-product">
                <div class="form-input">
                    <label>Tên dịch vụ</label>
                    <div class="box-input-info">
                        <div class="box-container-input">
                            <input id="service-title" class="input-info-product input-active" type="text" name="product-title">
                        </div>
                    </div>
                    <span id="error-title-service" class="error-msg">Bạn chưa nhập tên dịch vụ</span>
                </div>
                <div class="form-input-price">
                    <div class="form-input-price-product">
                        <label>Giá dịch vụ</label>
                        <div class="box-input-info">
                            <div id="origin-price-head" class="price-unit">₫</div>
                            <div class="box-container-input price-box">
                                <input id="service-price" class="input-info-product half" type="text" name="origin-price">

                            </div>
                        </div>
                        <span id="error-service-price" class="error-msg"></span>
                    </div>
                    <div class="form-input-price-product">
                        <label>Thời gian dịch vụ(phút)</label>
                        <div class="box-input-info">
                            <div class="box-container-input">
                                <input id="duration-input" class="input-info-product duration-input half" type="text" name="current-price">
                            </div>
                        </div>
                        <span id="error-duration" class="error-msg"></span>
                    </div>

                </div>

                <div class="form-input">
                    <label>Mô tả dịch vụ</label>
                    <textarea id="description-service" placeholder="Nhập mô tả dịch vụ" style="flex-grow: 1;"></textarea>
                    <span id="error-description-service" class="error-msg">Bạn chưa nhập mô tả dịch vụ</span>
                </div>
                <div class="form-input">
                    <label>Ảnh minh họa</label>
                    <div class="box-input-info">
                        <div class="box-container-input">
                            <input id="service-thumb" class="input-info-product input-active" accept="image/*" type="file" name="product-title">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <img id="thumb-service" style="display: none;" class="thumb-product">
            </div>
        </div>
        
    </div>
    <div class="bottom-box">
        <button id="btnCancel" data-href="{{route('service')}}" class="btn-import-product btn-cancel">
            Hủy bỏ
        </button>
        <button id="btnSave"  class="btn-import-product btn-save">
            Lưu lại
        </button>
    </div>
@endsection
