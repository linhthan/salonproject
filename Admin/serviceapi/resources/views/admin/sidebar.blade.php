<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                  <img id="img-avatar"  style="width: 45px; height: 45px;" data-default="{{asset('adminlte/dist/img/user2-160x160.jpg')}}"  class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p id="user-name-sidebar">Alexander Pierce</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
<!--         <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
<span class="input-group-btn">
  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
</span>
            </div>
        </form> -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
<!--             <li class="header">HEADER</li> -->
            <!-- Optionally, you can add icons to the links -->
            <li class="active">
                <a href="{{route('home')}}"><i class="fa fa-home"></i> <span>Trang chủ</span></a>
            </li>
            <li><a href="{{route('receipts')}}"><i class="fa fa-credit-card"></i><span>Quản lý thu ngân</span></a></li>
            
            <li><a href="{{route('appointments')}}"><i class="fa fa-calendar"></i><span>Quản lý lịch hẹn</span></a></li>

            <li><a href="{{route('order')}}"><i class="fa fa-cart-arrow-down"></i><span>Quản lý đơn hàng</span></a></li>
            
            <li><a href="{{route('products')}}"><i class="fa fa-shopping-cart"></i><span>Quản lý sản phẩm</span></a></li>
            <li><a href="{{route('gallerys')}}"><i class="fa fa-file-image-o"></i><span>Quản lý mẫu tóc</span></a></li>
            <li><a href="{{route('service')}}"><i class="fa fa-scissors"></i><span>Quản lý dịch vụ</span></a></li>
            <li><a href="{{route('sales')}}"><i class="fa fa-bell"></i><span>Quản lý khuyến mại</span></a></li>
            <li><a href="{{route('employees')}}"><i class="fa fa-users"></i><span>Quản lý nhân viên</span></a></li>
             <li><a href="{{route('config')}}"><i class="fa fa-cogs"></i><span>Cài đặt</span></a></li>
<!-- 
            <li class="treeview">
                <a href="#"><span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Link in level 2</a></li>
                    <li><a href="#">Link in level 2</a></li>
                </ul>
            </li> -->
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
