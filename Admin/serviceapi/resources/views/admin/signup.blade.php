<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BKSalon</title>

	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{asset('adminlte/dist/css/AdminLTE.min.css')}}">
	  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
	        page. However, you can choose any other skin. Make sure you
	        apply the skin class to the body tag so the changes take effect. -->
	        <link rel="stylesheet" href="{{asset('adminlte/dist/css/skins/skin-blue.min.css')}}">

	        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	  <!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

	<link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/authenticate.css')}}">
	<!-- jQuery 3 -->
	<script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="{{asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	<!-- AdminLTE App -->
	<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
	<script src="{{asset('js/signup.js')}}"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}"/>

</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form id="login-form" class="login100-form validate-form">
					<span class="login100-form-title p-b-48" style="padding-bottom: 20px;">
						Đăng ký thành viên X
					</span>
					<img class="img-logo" style="width: 70px; height: 70px; margin-bottom: 20px;" src="{{asset('adminlte/ic_launcher_144.png')}}">
				
					<div class="wrap-input100 validate-input" data-validate = "Họ tên không được để trống">
						<input id="fullname" class="input100" type="text" name="fullname">
						<span class="focus-input100" data-placeholder="Họ và tên"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Email không hợp lệ">
						<input id="email" class="input100" type="text" name="email">
						<span class="focus-input100" data-placeholder="Email"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Số điện thoại không hợp lệ">
						<input id="phone-number" class="input100" type="text" name="phone">
						<span class="focus-input100" data-placeholder="Số điện thoại"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Mật khẩu không được để trống">
						<span id="show-pass" class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
						<input id="password" class="input100" type="password" name="pass">
						<span class="focus-input100" data-placeholder="Mật khẩu"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Bạn chưa điền tên Salon">
						<input id="salon-name" class="input100" type="text" name="salon-name">
						<span class="focus-input100" data-placeholder="Tên Salon"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Đăng ký
							</button>
						</div>
					</div>

					<div class="text-center p-t-115">
						<span class="txt1">
							Bạn đã có tài khoản
						</span>

						<a class="txt2" href="{{route('login')}}">
							Đăng nhập
						</a>
					</div>
				</form>
				
			</div>
		</div>
	</div>

</body>
</html>