@if(isset($receipts) && count($receipts)>0)
	<table id="table-receipt" class="table table-bordered table-hover" data-sale="{{$total_sale}}"  data-booking="{{$total_booking_paid}}" data-order="{{$total_order_paid}}"  data-receipt="{{$total_receipt_pending}}">
						<thead>
						    <th>#</th>
						    <th>Loại hóa đơn</th>
						    <th>Khách hàng</th>
						    <th>Số điện thoại</th>
						    <th>Tổng hóa đơn</th>
						    <th>Ngày tạo</th>
						    <th>Trạng thái</th>

						 </thead>
					    <tbody>
					    	@foreach ($receipts as $key => $data)
					    		@if($data->type==1)
					    				@if(isset($data->id_bill))
					    					<tr class="item-table" data-bill="{{$data->id_bill}}" data-href="{{route('bill-detail',['id'=>$data->id_bill])}}">

					    				@else
					    					<tr class="item-table" data-bill="{{$data->id_bill}}" data-href="{{route('receipt-detail',['id'=>$data->id,'type'=>$data->type])}}">

					    				@endif
						    	@else
						    			@if(isset($data->id_bill))
						    				<tr class="item-table" data-bill="{{$data->id_bill}}" data-href="{{route('bill-detail',['id'=>$data->id_bill])}}">
					    				@else
					    					<tr class="item-table" data-bill="{{$data->id_bill}}" data-href="{{route('receipt-detail',['id'=>$data->id,'type'=>$data->type])}}">

					    				@endif
						    	@endif	
						        <td>{{$key+1 + (($receipts->currentPage() -1) * $receipts->perPage())}}</td>

						        <td>
						        	@if($data->type==1)
					    				Hóa đơn dịch vụ
						    		@else
						    			Hóa đơn bán hàng
						    		@endif	
					    		</td>
						        <td>{{$data->cus_name}}</td>
						        <td>{{$data->phone_number}}</td>
						        <td>{{$data->total_price}}</td>
						        <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->date)->format('H:i d/m/Y')}}</td>
						        <td>{{$data->status}}</td>
						      </tr>
						    @endforeach
					    </tbody>
	</table>
	{!! $receipts->render() !!}
@else
	<div id="view-no-data">
	    <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
	    <h3>Không tìm thấy kết quả</h3>
	    <span>Hãy thử lựa chọn tìm kiếm khác</span>
	</div>
@endif
