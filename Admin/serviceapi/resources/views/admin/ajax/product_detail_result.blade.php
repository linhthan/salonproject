@if(isset($product))
	<div class="col-sm-6 col-xs-12 container-info-product">
					<div class="form-input">
						<label>Tên sản phẩm</label>
						<div class="box-input-info">
							<div class="box-container-input">
								<input id="product-title" class="input-info-product" value="{{$product->title}}" type="text" name="product-title" disabled="true"> 
							</div>
						</div>
						<span id="error-title-product" class="error-msg">Bạn chưa nhập tên sản phẩm</span>
					</div>
					<div class="form-input-price">
						<div class="form-input-price-product">
							<label>Giá gốc</label>
							<div class="box-input-info">
								<div id="origin-price-head" class="price-unit">₫</div>
								<div class="box-container-input price-box">
									<input id="origin-price" value="{{$product->original_price}}" class="input-info-product half" type="text" name="origin-price" disabled="true">

								</div>
							</div>
							<span id="error-origin-price" class="error-msg"></span>
						</div>
						<div class="form-input-price-product">
							<label>Giá hiện tại</label>
							<div class="box-input-info">
								<div id="current-price-head" class="price-unit">₫</div>
								<div class="box-container-input price-box">
									<input id="current-price" value="{{$product->current_price}}" class="input-info-product half" type="text" name="current-price" disabled="true">
								</div>
							</div>
							<span id="error-current-price" class="error-msg"></span>
						</div>

					</div>
					<div class="form-input">
						<label>Loại sản phẩm</label>
						<div class="box-input-info">
							<div class="box-container-input">
								<input id="product-category-title" class="input-info-product" value="{{$product->catTitle}}" type="text" name="product-category-title" disabled="true">
							</div>
						</div>
					</div>
	<!-- 				<div class="form-input">
						<label>Số lượng</label>
						<div class="box-input-info">
							<div class="box-container-input">
								<input id="product-quantity" class="input-info-product input-count input-active" type="text" name="product-quantity">
							</div>
						</div>
						<span id="error-quantity-product" class="error-msg"></span>
					</div> -->
					<div class="form-input">
						<label>Mô tả sản phẩm</label>
						<textarea id="description-product" placeholder="Nhập mô tả sản phẩm" style="flex-grow: 1;" disabled="true">{{$product->catTitle??'Chưa có mô tả cho sản phẩm'}}</textarea>
					</div>
				</div>
	<div class="col-sm-6 col-xs-12">
		<img id="thumb-product" src="{{$product->thumbnail}}" class="thumb-product">
	</div>
@else
@endif