 @if(isset($result))
 <div class="row appointment-info">
               <div class="col-12 col-sm-8  box-appointment-info">
                    <div class="info-header">
                        <span>
                        Thông tin dịch vụ
                        </span> 
                    </div>
                    <div class="col-appointment-info">
                        
                          @if($result->status==1&&$result->checkin_at)
                          <div>
                            <label >Check in lúc:</label>
                             <span>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $result->checkin_at)->format('H:i d/m/Y')}}</span>
                           </div>
                          @else
                          <div>
                            <label >Ngày hẹn:</label>
                             <span>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $result->booking_time)->format('d/m/Y')}}</span>
                          </div>
                          <div>
                            <label >Thời gian:</label>
                                <span>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $result->booking_time)->format('H:i')}}</span>
                          </div>
                          @endif
                       
                        
                        <div> <label >Trạng thái:</label>
                              @switch($result->status)

                                @case(0)
                                  @if(\Carbon\Carbon::parse($result->booking_time)->addMinutes(15)->isPast())
                                  <span>Đã quá hẹn</span>
                                  @else
                                  <span>Đã đặt</span>
                                  @endif
                                @break
                                @case(1)
                                <span>Đã check in</span>
                                @break
                                @case(2)
                                <span>Đã hủy</span>
                                @break
                                @case(3)
                                <span>Đã thanh toán</span>
                                @break
                                @default
                                <span>Đã đặt</span>

                              @endswitch
                          </div>
                          <div> <label >Nhân viên đã chọn:</label>
                                <span>{{$result->name}}</span>
                            </div>

                        <div class="box-service-added">
                            <table id="table-service" class="table table-bordered"> 
                                <thead>
                                    <th>#</th>
                                    <th>Dịch vụ</th>
                                    <th>Giá</th>
                                </thead>
                                <tbody id="table-service-content">
                                    @if(!empty($result->services))
                                      @php
                                        $totalPrice = 0
                                      @endphp
                                      @foreach($result->services as $item)
                                        @php
                                          $totalPrice =  $totalPrice + $item->price
                                        @endphp
                                        <tr>
                                          <th scope="row">{{$loop->iteration}}</th>
                                          <td>{{$item->title}}</td>
                                          <td>{{($item->price)/1000}}K</td>
                                        </tr>
                                      
                                      @endforeach
                                          <tr class="total-price-row"><td colspan="2"> Tổng thanh toán</td>
                                          <td>{{$totalPrice/1000}}K</td>
                                          </tr>
                                    @else
                                      <tr class="total-price-row"><td colspan="3"> Không tìm thấy dữ liệu dịch vụ cho lịch hẹn</td>
                                      </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

               </div>
               <div class="col-sm-4 col-xs-12 cus-info">
                   <div class="info-header">
                       <span>
                        Thông tin khách hàng
                       </span>
                   </div>
                   <div class="row col-customer-info">
                        <div>
                            <div>
                                <label for="fname">Họ tên:</label>
                                <span>{{$result->cus_name}}</span>
                            </div>
                            <div>
                                  <label for="lname">Số điện thoại:</label>
                                  <span>{{$result->phone_number}}</span>
                            </div>
                             <div>
                                  <label for="lname">Khách hàng:</label>
                                  @if($result->customer_id)
                                  <span>Đã có tài khoản</span>
                                  @else
                                  <span>Chưa có tài khoản</span>
                                  @endif
                            </div>
                            @if($result->status==3)
                              <div>
                                    <label for="lname">Đánh giá:</label>
                                    @if($result->star !=null)
                                      @for($i =0; $i<$result->star; $i++)
                                        <span class="fa fa-star checked"></span>
                                      @endfor
                                      @for($i =0; $i<5 - $result->star; $i++)
                                        <span class="fa fa-star"></span>
                                      @endfor
                                    @else
                                       <span>Chưa có</span>
                                    @endif
                                   
                              </div>
                               @if($result->star !=null)
                                <div>
                                      <label for="lname">Nội dung đánh giá:</label>
                                       <span>{{$result->content}}</span>
                                </div>
                              @endif
                            @endif
                            
                        </div>   
                   </div>
                
               </div>

  </div>
  @if($result->status==0)
   <div class="box-btn">
        <button class="btn-appointment btn-white" id="btnCancel" data-booking="{{$result->id}}">Hủy lịch</button>
        @if(\Carbon\Carbon::parse($result->booking_time)->addMinutes(15)->isPast())
        <button class="btn-appointment btn-dark" id="btnCheckIn" data-booking="{{$result->id}}" data-expired="1">Check in</button>
        @else
        <button  class="btn-appointment btn-dark" id="btnCheckIn" data-booking="{{$result->id}}" data-expired="0">Check in</button>
        @endif
    </div>    
  @else
    <div class="box-btn">
        <button class="btn-appointment btn-white" id="btnBack" href="#">Trở lại</button>
        @if($result->status==1)
          <button id="btn-show-detail-receipt" class="btn-appointment btn-dark" id="btnProcess" data-href="{{route('receipt-detail',['id'=>$result->id,'type'=>1])}}">Thanh toán</button>
       @endif
      </div>
     
  @endif
 
@else
  <div id="view-no-data" style="text-align: center; margin-top: 20px;">
      <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
      <h3>Không tìm thấy kết quả</h3>
      <span>Không tìm thấy kết quả cho lịch hẹn này</span>
  </div>
@endif