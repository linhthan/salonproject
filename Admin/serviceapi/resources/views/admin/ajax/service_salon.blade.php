@if(!empty($services))
	@foreach($services as $item)
	<div class="bs-service-item container">
		<div class="row">
			<div class="col-12 col-md-8 box-service">
				<img class="thumb-service" src="{{empty($item->thumbnail)?asset('adminlte/dist/img/avatar.png'):$item->thumbnail}} ">
				<div class="service-box-info">
					<span class="service-name">{{$item->title}}</span>
					<span class="service-duration">{{$item->duration}} phút</span>
	        					<!-- <div class="service-name">Name Service</div>
	        						<div class="service-duration">30 minutes</div> -->
	        	</div>

	        </div>
	        <div class="col-12 col-md-4 service-price">
	        					<span>{{($item->price)/1000}}K</span>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-12 col-md-8 box-art">

	        	@foreach($item->art as $key => $art)
	        		@if($key==5)
	        			@break
	        		@endif
	        		<div class="item-art">
	        			<img src="{{$art}}">
	        		</div>
	        	@endforeach
	        
	        </div>
	        <div class="col-12 col-md-4 box-add-service">
	        	<div class="container-add-service">
	        		<button data-service="{{$item->id}}" data-duration="{{$item->duration}}" data-price="{{$item->price}}" data-name="{{$item->title}}" class="btn-service btn-add-service" >Thêm dịch vụ</button>
	        	</div>
	        	
	        </div>
	    </div>

	</div>

	@endforeach

@else
	<div>không có dữ liệu</div>
@endif