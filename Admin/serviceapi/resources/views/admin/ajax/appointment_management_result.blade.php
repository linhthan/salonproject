@if(isset($booking) && count($booking)>0)

	<table class="table table-bordered table-hover">
						<thead>
						    <th>#</th>
						    <th>Khách hàng</th>
						    <th>Số điện thoại</th>
						    <th>Dịch vụ</th>
						    <th>Tổng thanh toán</th>
						    <th>Thời gian chọn</th>
						    <th>Trạng thái</th>
						 </thead>
					    <tbody>
					    	@foreach ($booking as $key => $data)
						      <tr class="item-table" data-href="{{route('appointment-detail',['id'=>$data->id])}}">
						        <td>{{$key+1 + (($booking->currentPage() -1) * $booking->perPage())}}</td>
						        <td>{{$data->cus_name}}</td>
						        <td>{{$data->phone_number}}</td>
						        <td>{{$data->titles}}</td>
						        <td>{{$data->total_price}}</td>
						        <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->booking_time)->format('H:i d/m/Y')}}</td>
						        <td>
						          @if($data->status==0 && \Carbon\Carbon::parse($data->booking_time)->addMinutes(+15)->isPast())
						                đã quá hẹn
						          @elseif($data->status==0)
						              đã đặt
						          @elseif($data->status==1)
						              đã check in
						          @elseif($data->status==2)
						              đã hủy
						          @else
						          	  đã thanh toán
						          @endif

						        </td>
						      </tr>
						    @endforeach
					    </tbody>
	</table>
	{!! $booking->render() !!}
@else
	<div id="view-no-data">
	    <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
	    <h3>Không tìm thấy kết quả</h3>
	    <span>Hãy thử lựa chọn tìm kiếm khác</span>
	</div>
@endif
