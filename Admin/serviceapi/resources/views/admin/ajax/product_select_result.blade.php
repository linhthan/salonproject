<tr class="item-order">
	<td class="index-cell">1</td>
	<td class="cell-selected">
	<div class="box-selected" id="product-box-select">
		<select class="form-control selectpicker" data-live-search="true">
			<option selected>Chọn sản phẩm</option>
			@if(isset($products))	
				@foreach($products as $product)
					<option data-product="{{$product}}" data-tokens="{{$product->title}}">{{$product->title}}</option>
				@endforeach
			@endif
		</select>
	</div>	
	</td>
	<td>0</td>
	<td>
		<div class="flex-step">
			<button class="btn-step btn-decrease">
				<i class="fa fa-minus"></i>
			</button>
			<input type="number" class="quantity-input" min="0">
			<button  class="btn-step btn-increase">
				<i class="fa fa-plus"></i>
			</button>
		</div>


	</td>
	<td class="total-price-cell">0</td>
	<td><span class="fa fa-trash btn-remove"></span></td>
</tr>
