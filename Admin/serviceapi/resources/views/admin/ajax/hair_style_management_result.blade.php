@if(isset($hairstyles) && count($hairstyles)>0)
	<div class="box-hair-style">
		@foreach($hairstyles as $hairstyle)
			<div class="item-hair-style">
				<div class="box-img-hair">
					<img data-hair="{{$hairstyle}}" data-date="{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $hairstyle->create_at)->format('H:i d/m/Y')}}" class="img-hair-style" src="{{$hairstyle->url}}">
					<div class="infor-view-download">
						<div>
							<span><i class="fa fa-thumbs-up"></i>{{$hairstyle->view_count}}</span>
						</div>
						<div>
							<span><i class="fa fa-cloud-download"></i>{{$hairstyle->download_count}}</span>
						</div>
					</div>
				</div>
				<div class="box-infor">
								
					<div class="title-hair">
						<span> {{$hairstyle->title}}</span>
					</div>
				</div>
				<div class="option-hair dropdown">
					<span type="button" class="fa fa-ellipsis-v dropdown-toggle" data-toggle="dropdown">
					</span>
					<div class="dropdown-menu dropdown-menu-right">
						<div data-hair="{{$hairstyle}}" class="btn-edit dropdown-item" ><span><i class="fa fa-pencil"></i>Sửa</span></div>
					<div data-id="{{$hairstyle->id}}" class="btn-remove dropdown-item" ><span><i class="fa fa-trash"></i>Xóa</span></div>
					</div>
				</div>
			</div>
		@endforeach
		
	</div>
	{!! $hairstyles->render() !!}
		
@else
	<div id="view-no-data">
	    <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
	    <h3>Không tìm thấy kết quả</h3>
	    <span>Bạn chưa có mẫu tóc nào!</span>
	</div>
@endif