 @if($bill && $order && $orderDetail)
 @php
    $totalPrice = 0
 @endphp
 @foreach($orderDetail as $item)
    @php
      $totalPrice =  $totalPrice + $item->price * $item->quantity;
    @endphp                 
 @endforeach
 
 <div class="row receipt-info">
               <div class="col-12 col-sm-8  box-receipt-info">
                    <div class="info-header">
                        <span>
                        Thông tin đơn hàng
                        </span> 
                    </div>
                    <div class="col-receipt-info">
                        
                        <div>
                          <label >Thời gian:</label>
                                <span>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->order_created_at)->format('H:i d-m-Y')}}</span>
                        </div>
                       

                        <div class="box-service-added">
                            <table id="table-service" class="table table-bordered"> 
                                <thead>
                                    <th>#</th>
                                    <th>Sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Đơn Giá</th>
                                    <th>Tổng</th>
                                </thead>
                                <tbody id="table-service-content">
                                    @if(!empty($orderDetail))
                                      
                                      @foreach($orderDetail as $item)
                                        <tr>
                                          <th scope="row">{{$loop->iteration}}</th>
                                          <td>{{$item->title}}</td>
                                          <td>{{$item->quantity}}</td>
                                          <td>{{$item->price}}</td>
                                          <td>{{$item->quantity*$item->price}}</td>
                                        </tr>
                                      @endforeach
                                          <tr class="total-price-row">
                                            <td colspan="4"> Tổng hóa đơn</td>
                                            <td>{{$totalPrice}}</td>
                                          </tr>
                                    @else
                                      <tr class="total-price-row"><td colspan="5"> Không tìm thấy dữ liệu cho đơn hàng</td>
                                      </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

               </div>
               <div class="col-sm-4 col-xs-12 cus-info">
                   <div class="info-header">
                       <span>
                        Thông tin khách hàng
                       </span>
                   </div>
                   <div class="row col-customer-info">
                        <div>
                            <div>
                                <label for="fname">Họ tên:</label>
                                <span>{{$order->cus_name}}</span>
                            </div>
                            <div>
                                  <label for="lname">Số điện thoại:</label>
                                  <span>{{$order->phone_number}}</span>
                            </div>
                             <div>
                                  <label for="lname">Địa chỉ:</label>
                                  <span>{{$order->address}}</span>
                            </div>
                             <div>
                                  <label for="lname">Khách hàng:</label>
                                  @if($order->customer_id)
                                  <span>Đã có tài khoản</span>
                                  @else
                                  <span>Chưa có tài khoản</span>
                                  @endif
                            </div>
                            
                        </div>   
                   </div>
                
               </div>

</div>
<div class="form-receipt" style="margin-top: 20px; text-align: center;">
    <div class="container-info-product">
          <div class="form-input">
            <label>Tổng hóa đơn:</label>
            <div class="box-input-info">
              <div class="box-container-input">
                    <input id="receipt-price-input" class="input-info-product" type="text" name="product-title" disabled="true" value="{{$totalPrice}}">
              </div>

            </div>
        </div>
        <div class="form-input">
            <label>Giảm giá(VNĐ):</label>
            <div class="box-input-info">
              <div class="box-container-input">
                    <input id="receipt-discount-input" class="input-info-product" type="text" name="product-title" disabled="true" value="{{$bill->discount??0}}VNĐ" >
                  
              </div>
            </div>
        </div>
        <div class="form-input">
            <label>Giảm giá(%):</label>
            <div class="box-input-info">
              <div class="box-container-input">
                    <input id="receipt-discount-percent-input" class="input-info-product" type="text" name="product-title" disabled="true" value="{{$bill->discount_percent??0}}%" >
                
              </div>
            </div>
        </div>
        <div class="form-input">
            <label>Tổng thanh toán:</label>
            <div class="box-input-info">
              <div class="box-container-input">
                    <input id="receipt-total-price-input" class="input-info-product" type="text" name="product-title"  disabled="true"  value="{{$totalPrice}}" >
              </div>
            </div>
        </div>
        
    </div>
   
</div>

  <div class="box-btn">
          <button id="btnCancel" data-href="{{route('receipts')}}" class="btn-receipt btn-white">
              Hủy bỏ
          </button>

          <button id="btnProcess" data-href="{{route('export-receipts-order',['order'=>$order,'order_detail'=>serialize($orderDetail),'bill'=>$bill])}}" class="btn-receipt btn-dark">
              Xuất PDF
          </button>
</div> 

@else
  <div>
    Không tìm thấy dữ liệu cho hóa đơn này
  </div>
@endif