@if(isset($services) && count($services)>0)
    <table class="table table-bordered table-hover">
                        <thead>
                            <th>#</th>
                            <th>Tên dịch vụ</th>
                            <th>Thời gian</th>
                            <th>Giá</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                         </thead>
                        <tbody>
                            @foreach ($services as $key => $data)
                              <tr class="item-table" data-href="{{route('appointment-detail',['id'=>$data->id])}}">
                                <td>{{$key+1 + (($services->currentPage() -1) * $services->perPage())}}</td>
                                <td>{{$data->title}}</td>
                                <td>{{$data->duration}} phút</td>
                                <td>{{$data->price}}</td>
                                <td>
                                  @if($data->status==0)
                                      Ngừng cung cấp
                                  @else
                                      Đang hoạt động
                                  @endif

                                </td>
                                <td>
                                  <a class="fa fa-pencil-square-o btn-edit" data-id="{{$data->id}}" data-content="{{$data->content}}" data-status="{{$data->status}}" data-duration="{{$data->duration}}" data-thumb="{{$data->thumbnail}}" href="#" style="margin-right: 10px"></a>
                                  <a class="fa fa-trash btn-remove" data-id="{{$data->id}}" href="#"></a>
                                </td>
                              </tr>
                            @endforeach
                        </tbody>
    </table>
    {!! $services->render() !!}
@else
    <div id="view-no-data">
        <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
        <h3>Không tìm thấy kết quả</h3>
        <span>Hãy thử lựa chọn tìm kiếm khác</span>
    </div>
@endif
