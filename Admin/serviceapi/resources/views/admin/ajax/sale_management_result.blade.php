@if(isset($sales) && count($sales)>0)
	<table class="table table-bordered table-hover">
						<thead>
						    <th>#</th>
						    <th>Tiêu đề</th>
						    <th>Ngày tạo</th>
						    <th>Liên kết</th>
						    <th>Trạng thái</th>
						    <th>Hành động</th>
						 </thead>
					    <tbody>
					    	@foreach ($sales as $key => $data)
						      <tr class="item-table" data-status ="{{$data->status}}" data-thumb="{{$data->banner_url}}" data-id="{{$data->id}}">
						        <td>{{$key+1 + (($sales->currentPage() -1) * $sales->perPage())}}</td>
						        <td>{{$data->title}}</td>
						        <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->sale_create_at)->format('H:i d/m/Y')}}</td>
						        <td><a href="{{$data->sale_url}}" target="_blank">{{$data->sale_url}}</a></td>
						        @if($data->status==0)
						        	<td>Đã kết thúc</td>
						        @else
						        	<td>Đang hoạt động</td>
						        @endif
						        <td>
						        	<a class="fa fa-pencil-square-o btn-edit" data-id="{{$data->id}}" href="#" style="margin-right: 10px"></a>
						        	<a class="fa fa-trash btn-remove" data-id="{{$data->id}}" href="#"></a>
						        </td>
						      </tr>
						    @endforeach
					    </tbody>
	</table>
	{!! $sales->render() !!}
@else
	<div id="view-no-data">
	    <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
	    <h3>Không tìm thấy kết quả</h3>
	    <span>Vui lòng thử lại</span>
	</div>
@endif
