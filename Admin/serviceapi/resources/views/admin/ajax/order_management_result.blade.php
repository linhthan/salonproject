@if(isset($order) && count($order)>0)
    <table class="table table-bordered table-hover">
        <thead>
        <th>#</th>
        <th>Khách hàng</th>
        <th>Số điện thoại</th>
        <th>Địa chỉ</th>
        <th>Ghi chú</th>
        <th>Ngày tạo</th>
        <th>Trạng thái</th>
        <th>Hành động</th>
        </thead>
        <tbody>
        @foreach ($order as $key => $data)
            <tr class="item-table" data-status="{{$data->status}}"
                data-id="{{$data->id}}" data-href="{{route('order-detail',['id'=>$data->id])}}">
                <td>{{$key+1 + (($order->currentPage() -1) * $order->perPage())}}</td>
                <td>{{$data->cus_name}}</td>
                <td>{{$data->phone_number}}</td>
                <td>{{$data->address}}</td>
                <td>{{$data->note ?? "Không có" }}</td>
                <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->order_created_at)->format('H:i d/m/Y')}}</td>
                <td>
                    @switch($data->status)
                        @case(0)
                        Đã đặt
                        @break
                        @case(1)
                        Đang giao
                        @break
                        @case(2)
                        Đã giao
                        @break
                        @case(3)
                        Đã hủy
                        @break
                        @case(4)
                        Đã thanh toán
                        @break
                        @default
                        Đã đặt

                    @endswitch
                </td>
                <td>
                    <a class="fa fa-pencil-square-o btn-edit" data-id="{{$data->id}}" data-status="{{$data->status}}" href="#"
                       style="margin-right: 10px"></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $order->render() !!}
@else
    <div id="view-no-data">
        <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
        <h3>Không tìm thấy kết quả</h3>
        <span>Hãy thử lựa chọn tìm kiếm khác</span>
    </div>
@endif
