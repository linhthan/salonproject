@if(isset($product) && count($product)>0)
	<table class="table table-bordered table-hover">
						<thead>
						    <th>#</th>
						    <th>Tên sản phẩm</th>
						    <th>Loại sản phẩm</th>
						    <th>Giá gốc</th>
						    <th>Giá hiện tại</th>
						    <th>Số lượng</th>
						    <th>Hành động</th>
						 </thead>
					    <tbody>
					    	@foreach ($product as $key => $data)
						      <tr class="item-table" data-catid ="{{$data->cat_id}}" data-thumb="{{$data->thumbnail}}" data-id="{{$data->id}}" data-href="{{route('product-detail',['id'=>$data->id])}}">
						        <td>{{$key+1 + (($product->currentPage() -1) * $product->perPage())}}</td>
						        <td>{{$data->title}}</td>
						        <td>{{$data->catTitle}}</td>
						        <td>{{$data->original_price}}</td>
						        <td>{{$data->current_price}}</td>
						        <td>{{$data->quantity}}</td>
						        <td>
						        	<a class="fa fa-pencil-square-o btn-edit" data-id="{{$data->id}}" href="#" style="margin-right: 10px"></a>
						        	<a class="fa fa-trash btn-remove" data-id="{{$data->id}}" href="#"></a>
						        </td>
						      </tr>
						    @endforeach
					    </tbody>
	</table>
	{!! $product->render() !!}
@else
	<div id="view-no-data">
	    <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
	    <h3>Không tìm thấy kết quả</h3>
	    <span>Hãy thử lựa chọn tìm kiếm khác</span>
	</div>
@endif
