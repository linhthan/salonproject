@if(!empty($slots))
	@foreach($slots as $item)
		@php
			$time = $item->time;
			$hour = (int)($time / 60);
			$minutes = $time % 60;
            $suffix = "";
            $hourString = "{$hour}";
            $minuteString = "{$minutes}";
            if ($hour < 12) {
                $suffix = "AM";
                if ($hour < 10) $hourString = "0{$hourString}";

            } else {
                $suffix = "PM";

            }
            if ($minutes < 10) {
                $minuteString = "0$minuteString";
            }
            $timeDisplay = "$hourString:$minuteString $suffix";

			$slotCount = $item->slotCount;

		@endphp
		@if($slotCount==0)
			 <div class="col-md-1 item-time expired" data-minute={{$time}}>
                 {{$timeDisplay}}
              </div>
		@else
			<div class="col-md-1 item-time" data-minute={{$time}}>
                {{$timeDisplay}}
            </div>
		@endif
	@endforeach
@else
	<div>Không có dữ liệu</div>
@endif
