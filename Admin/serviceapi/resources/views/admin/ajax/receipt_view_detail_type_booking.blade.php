 @if($bill && $booking && $services)
 @php
    $totalPrice = 0
 @endphp
 @foreach($services as $item)
    @php
      $totalPrice =  $totalPrice + $item->price
    @endphp                 
 @endforeach
 <div class="row receipt-info">
               <div class="col-12 col-sm-8  box-receipt-info">
                    <div class="info-header">
                        <span>
                        Thông tin dịch vụ
                        </span> 
                    </div>
                    <div class="col-receipt-info">
                        
                          @if($booking->status==1&&$booking->checkin_at)
                          <div>
                            <label >Check in lúc:</label>
                             <span>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->checkin_at)->format('H:i d/m/Y')}}</span>
                           </div>
                          @else
                          <div>
                            <label >Ngày hẹn:</label>
                             <span>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->booking_time)->format('d/m/Y')}}</span>
                          </div>
                          <div>
                            <label >Thời gian:</label>
                                <span>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking->booking_time)->format('H:i')}}</span>
                          </div>
                          @endif
                       
                        
                        <div> <label >Trạng thái:</label>
                              @switch($booking->status)

                                @case(0)
                                  @if(\Carbon\Carbon::parse($booking->booking_time)->addMinutes(-15)->isPast())
                                  <span>Đã quá hẹn</span>
                                  @else
                                  <span>Đã đặt</span>
                                  @endif
                                @break
                                @case(1)
                                <span>Đã sử dụng</span>
                                @break
                                @case(2)
                                <span>Đã hủy</span>
                                @break
                                @case(3)
                                <span>Đã thanh toán</span>
                                @break
                                @default
                                <span>Đã đặt</span>

                              @endswitch
                          </div>
                          <div> <label >Nhân viên đã chọn:</label>
                                <span>{{$booking->name}}</span>
                            </div>

                        <div class="box-service-added">
                            <table id="table-service" class="table table-bordered"> 
                                <thead>
                                    <th>#</th>
                                    <th>Dịch vụ</th>
                                    <th>Giá</th>
                                </thead>
                                <tbody id="table-service-content">
                                    @if(!empty($services))
                                      
                                      @foreach($services as $item)
                                        <tr>
                                          <th scope="row">{{$loop->iteration}}</th>
                                          <td>{{$item->title}}</td>
                                          <td>{{$item->price}}</td>
                                        </tr>
                                      @endforeach
                                          <tr class="total-price-row"><td colspan="2"> Tổng hóa đơn</td>
                                          <td style="font-weight: b">{{$totalPrice}}</td>
                                          </tr>
                                    @else
                                      <tr class="total-price-row"><td colspan="3"> Không tìm thấy dữ liệu dịch vụ cho lịch hẹn</td>
                                      </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

               </div>
               <div class="col-sm-4 col-xs-12 cus-info">
                   <div class="info-header">
                       <span>
                        Thông tin khách hàng
                       </span>
                   </div>
                   <div class="row col-customer-info">
                        <div>
                            <div>
                                <label for="fname">Họ tên:</label>
                                <span>{{$booking->cus_name}}</span>
                            </div>
                            <div>
                                  <label for="lname">Số điện thoại:</label>
                                  <span>{{$booking->phone_number}}</span>
                            </div>
                             <div>
                                  <label for="lname">Khách hàng:</label>
                                  @if($booking->customer_id)
                                  <span>Đã có tài khoản</span>
                                  @else
                                  <span>Chưa có tài khoản</span>
                                  @endif
                            </div>
                            
                        </div>   
                   </div>
                
               </div>

</div>
<div class="form-receipt" style="margin-top: 20px; text-align: center;">
   <div class="container-info-product">
          <div class="form-input">
            <label>Tổng hóa đơn:</label>
            <div class="box-input-info">
              <div class="box-container-input">
                    <input id="receipt-price-input" class="input-info-product" type="text" name="product-title" disabled="true" value="{{$totalPrice}}">
              </div>

            </div>
        </div>
        <div class="form-input">
            <label>Giảm giá(VNĐ):</label>
            <div class="box-input-info">
              <div class="box-container-input">
                    <input id="receipt-discount-input" class="input-info-product" type="text" name="product-title" disabled="true" value="{{$bill->discount??0}}VNĐ" >
                  
              </div>
            </div>
        </div>
        <div class="form-input">
            <label>Giảm giá(%):</label>
            <div class="box-input-info">
              <div class="box-container-input">
                    <input id="receipt-discount-percent-input" class="input-info-product" type="text" name="product-title" disabled="true" value="{{$bill->discount_percent??0}}%" >
                
              </div>
            </div>
        </div>
        <div class="form-input">
            <label>Tổng thanh toán:</label>
            <div class="box-input-info">
              <div class="box-container-input">
                    <input id="receipt-total-price-input" class="input-info-product" type="text" name="product-title"  disabled="true"  value="{{$bill->total_price}}" >
              </div>
            </div>
        </div>
        
    </div>
   
</div>
 <div class="box-btn">
          <button id="btnCancel" data-href="{{route('receipts')}}" class="btn-receipt btn-white">
              Hủy bỏ
          </button>

          <button id="btnProcess" data-services="{{$services}}" data-href="{{route('export-receipts',['booking'=>$booking,'services'=>serialize($services),'bill'=>$bill])}}" class="btn-receipt btn-dark">
              Xuất PDF
          </button>
</div> 
 
@else
  <div>
    Không tìm thấy dữ liệu cho hóa đơn này
  </div>
@endif