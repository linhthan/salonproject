<table class="table table-next-appointment table-hover">
  <thead>
    <th>#</th>
    <th>Khách hàng</th>
    <th>Số điện thoại</th>
    <th>Dịch vụ</th>
    <th>Tổng thanh toán</th>
    <th>Ngày đặt lịch</th>
    <th>Trạng thái</th>
  </thead>
  <tbody>
    @if(isset($booking) && count($booking)>0)
      @foreach ($booking as $key => $data)
      <tr class="item-table" data-href="{{route('appointment-detail',['id'=>$data->id])}}">
        <td>{{$key+1 + (($booking->currentPage() -1) * $booking->perPage())}}</td>
        <td>{{$data->cus_name}}</td>
        <td>{{$data->phone_number}}</td>
        <td>{{$data->titles}}</td>
        <td>{{$data->total_price}}</td>
        <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->booking_time)->format('H:i d/m/Y')}}</td>
        <td>@if($data->status==0 && \Carbon\Carbon::parse($data->booking_time)->addMinutes(15)->isPast())
                đã quá hẹn
          @elseif($data->status==0)
              đã đặt
          @elseif($data->status==1)
              đã check in
          @elseif($data->status==1)
              đã hủy
          @else
              đã thanh toán
          @endif</td>
      </tr>
      @endforeach
    @else 
    <td colspan="8" style="text-align: center;">
        Không tìm thấy dữ liệu nào!
    </td>
    @endif
  </tbody>
</table>
@if(isset($booking))
{!! $booking->render() !!}
@endif