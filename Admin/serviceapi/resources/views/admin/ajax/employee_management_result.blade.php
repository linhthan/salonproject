@if(isset($users) && count($users) >0)
	<table id="table-users" class="table table-bordered table-hover">
							<thead>
							    <th>#</th>
							    <th>Tên nhân viên</th>
							    <th>Email</th>
							    <th>Số điện thoại</th>
							    <th>Vai trò</th>
							    <th>Hành động</th>

							 </thead>
							 <tbody>
							 	
							 	@if($users)
							 		@foreach($users as $key=>$user)
							 		<tr class="item-table" data-href="{{route('staff-detail',['id'=>$user->id])}}">
							 			<td>
							 			 	{{$key+1 + (($users->currentPage() -1) * $users->perPage())}}
							 			</td>
									    <td>{{$user->name}}</td>
									    <td>{{$user->email}}</td>
									    <td>{{$user->phone_number?? `Chưa có`}}</td>
									    <td>
									        	@switch($user->user_type)
												    @case(1)
												    	Nhân viên dịch vụ
												        @break
												    @case(3)
												        Nhân viên lễ tân
												        @break
												    @case(4)
												        Nhân viên thu thập
												        @break
												    @default
												    	Nhân viên dịch vụ
												@endswitch
								    	</td>
								    	<td>
								    		<a class="fa fa-pencil-square-o btn-edit" href="{{route('staff-detail',['id'=>$user->id])}}" style="margin-right: 10px"></a>
						        			<a class="fa fa-trash btn-remove" data-id="{{$user->id}}" href="#"></a>
								    	</td>

							    	</tr>
							 		@endforeach
							 	@endif
								
						    </tbody>
	</table>
	{!! $users->render() !!}
@else
	<div id="view-no-data">
	    <img class="img-logo" src="{{asset('adminlte/ic_launcher_144.png')}}">
	    <h3>Không tìm thấy kết quả</h3>
	    <span>Hãy thử lựa chọn tìm kiếm khác</span>
	</div>
@endif