<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('adminlte/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{asset('adminlte/dist/css/skins/skin-blue.min.css')}}">
  <link href="{{asset('sweetalert/sweetalert2.min.css')}}" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <script type="text/javascript">
      // var BASE_URL = 'http://127.0.0.1:8400/';
      var BASE_URL = 'http://127.0.0.1/';
      function setCookie(name,value,days) {
          var expires = "";
          if (days) {
              var date = new Date();
              date.setTime(date.getTime() + (days*24*60*60*1000));
              expires = "; expires=" + date.toUTCString();
          }
          document.cookie = name + "=" + (value || "")  + expires + "; path=/";
      }

      function getCookie(name) {
          var nameEQ = name + "=";
          var ca = document.cookie.split(';');
          for(var i=0;i < ca.length;i++) {
              var c = ca[i];
              while (c.charAt(0)==' ') c = c.substring(1,c.length);
              if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
          }
          return null;
      }
      function eraseCookie(name) {   
          document.cookie = name+'=; Max-Age=-99999999;';  
      }
  </script>
  <style type="text/css">
          
          /*progressbar*/

      /* position the bars and balls correctly (rotate them and translate them outward)*/
      .bar1 {
          transform: rotate(0deg) translate(0,-40px);
          opacity: 0.12;
      }

      .bar2 {
          transform: rotate(45deg) translate(0,-40px);
          opacity: .25;

      }
      .bar3 {
          transform: rotate(90deg) translate(0,-40px);
          opacity:0.37;
      }
      .bar4 {
          transform: rotate(135deg) translate(0,-40px);
          opacity:0.50;
      }
      .bar5 {
          transform: rotate(180deg) translate(0,-40px);
        opacity:0.62;
      }
      .bar6 {
          transform: rotate(225deg) translate(0,-40px);
       opacity:0.75;
      }
      .bar7 {
          transform: rotate(270deg) translate(0,-40px);
          opacity:0.87;
      }
      .bar8 {
          transform: rotate(315deg) translate(0,-40px);
          opacity:1;
      }


      /* set up the three ball spinners */
      #progress-loading {
        position:relative;
        display: inline-block;
        width:100px;
        height:100px;
        transform: scale(.5);
      }
      #progress-loading div
      {
        display: inline-block;
        width:10px;
        height:30px;
        background:black;
        position:absolute;
        left:45px;
        top:35px;
        margin: 0px;
      }
      /* add a shadow to the first */
      #progress-loading div {
        -moz-box-shadow:black 0 0 4px;
        -webkit-box-shadow:black 0 0 4px;
      }
      #progress-loading {
        -webkit-animation-name: rotateThis;
        -webkit-animation-duration:2s;
        -webkit-animation-iteration-count:infinite;
        -webkit-animation-timing-function:linear;
      }
      @-webkit-keyframes rotateThis {
        from {-webkit-transform:scale(0.5) rotate(0deg);}
        to {-webkit-transform:scale(0.5) rotate(360deg);}
      }
  </style>
  
  <!-- jQuery 3 -->
  <script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
  <script src="{{asset('sweetalert/sweetalert2.min.js')}}"></script>
  <script src="{{asset('js/admin.js')}}"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}"/>
  @yield('css')
  @yield('js')

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini" style="background-color: white">
<div class="wrapper">
 <!-- Header -->
    @include('admin.waiterheader')

    <!-- Sidebar -->


        <div class="content" style="background-color: white">
            <!-- Your Page Content Here -->
            @yield('content')
        </div><!-- /.content -->


    <!-- Footer -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->



<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>