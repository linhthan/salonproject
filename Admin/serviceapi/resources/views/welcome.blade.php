<!DOCTYPE html>
<html>
<head>
    <title>BK Salon</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
      <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/welcome.css')}}">
     <script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/welcome.js')}}"></script>
  <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
</head>
<body>
    <nav id="header" class="top-nav">
        <a class="logo-link" href="/"><b>BK</b>Salon</a>
        <ul class="main-nav">
            <li class="main-nav__item">
              <a href="{{route('sign-up')}}" class="btn btn--small top-nav__btn">Trở thành chủ salon</a>
            </li>
            <li class="main-nav__item">
              <a href="{{route('login')}}" class="main-nav__link">Vào trang quản lý</a>
            </li>
        </ul>
    </nav>
    <section class="content">
        <h1>
            Phần mềm quản lý hàng đầu cho Salon
        </h1>
        <h4>Đơn giản, tiện lợi, cung cấp các tính năng mới và hoàn toàn miễn phí</h4>
        <div class="content-image">
            <img src="{{asset('adminlte/salonapp.JPG')}}" alt="Shedul the best Salon and Spa software in the world">
        </div>
    </section>
    <section class="content hairstyle-feature">
        <h2 class="title">
                        Tính năng nổi bật
                    </h2>
        <div class="row content-feature-hairstyle">
            <div class="col-md-6">
                <h2 class="title" style="text-align: left;">
                        Thu thập mẫu tóc
                    </h2>
                    <div class="desc-feature">
                        Tính năng tuyệt vời cho phép salon của bạn không chỉ thu thập ảnh khách hàng mà còn tạo riêng được một bộ sưu tập mẫu tóc.Tính năng được tích hợp nhằm nâng cao trải nghiệm và cũng như thêm tiện ích tuyệt vời cho Salon của bạn.
                    </div>
               
        
            </div>
            <div class="col-md-6">
                <img src="{{asset('adminlte/extract.jpg')}}">
            </div>
        </div>
    </section>
    <section class="content content-feature">
            <div class="container-box">
                <header class="section-header">
                  <h2 class="section-header__title">Cung cấp các tính năng quản lý nâng cao</h2>
                  <p class="section-header__subtitle">Các tính năng tuyệt vời giúp dễ dàng quản lý Salon của bạn</p>

                </header>
                <ul class="features-list">
                      <li class="features-list__item">
                        <div class="features-list__image">
                          <img src="{{asset('adminlte/bill.svg')}}" alt="Best Appointment scheduling Software App">
                        </div>
                        <div class="features-list__description">
                          <h5 class="features-list__header">
                            Quản lý thu ngân
                          </h5>
                          <p class="features-list__content">
                            Kiểm soát thu ngân, thanh toán hóa đơn cho khách hàng, xuất in hóa đơn.
                          </p>
                        </div>
                      </li>
                       <li class="features-list__item">
                        <div class="features-list__image">
                          <img src="{{asset('adminlte/appointment.svg')}}" alt="Phần mềm quản lý Sanlon hàng đầu">
                        </div>
                        <div class="features-list__description">
                          <h5 class="features-list__header">
                            Quản lý lịch hẹn
                          </h5>
                          <p class="features-list__content">
                            Phần lớn khách hàng có xu hướng đặt lịch hẹn trước khi đến Salon của bạn. BKSalon sẽ cung cấp cho bạn các tiện ích tốt nhất trong quản lý các lịch hẹn.
                          </p>
                        </div>
                      </li>
                     
                       <li class="features-list__item">
                        <div class="features-list__image">
                          <img src="{{asset('adminlte/ecommerce.svg')}}" alt="Phần mềm quản lý Sanlon hàng đầu">
                        </div>
                        <div class="features-list__description">
                          <h5 class="features-list__header">
                            Đặt lịch hẹn online
                          </h5>
                          <p class="features-list__content">
                            Phần mềm cung cấp tính năng đặt lịch online cho khách hàng trên thiết bị di động. Đây là một tính năng tiện lợi phục vụ cho khách h
                          </p>
                        </div>
                      </li>

                        <li class="features-list__item">
                        <div class="features-list__image">
                          <img src="{{asset('adminlte/choices.svg')}}" alt="Phần mềm quản lý Sanlon hàng đầu">
                        </div>
                        <div class="features-list__description">
                          <h5 class="features-list__header">
                            Quản lý sản phẩm và đơn hàng
                          </h5>
                          <p class="features-list__content">
                            Doanh thu của Salon một phần đến từ các sản phẩm. BKSalon cung cấp tính năng giúp quản lý toàn bộ các công việc liên quan đến xử lý đơn hàng, các sản phẩm.
                          </p>
                        </div>
                      </li>

                       <li class="features-list__item">
                        <div class="features-list__image">
                          <img src="{{asset('adminlte/camera.svg')}}" alt="Phần mềm quản lý Sanlon hàng đầu">
                        </div>
                        <div class="features-list__description">
                          <h5 class="features-list__header">
                            Thu thập ảnh khách hàng
                          </h5>
                          <p class="features-list__content">
                            Chất lượng dịch vụ là yếu tố quan trọng giúp bạn thu hút cũng như giữ được khách hàng. BKSalon cung cấp chức năng hữu ích giúp bạn thu thập được ảnh của khách hàng sử dụng dịch vụ.
                          </p>
                        </div>
                      </li>
                       <li class="features-list__item">
                        <div class="features-list__image">
                          <img src="{{asset('adminlte/team.svg')}}" alt="Phần mềm quản lý Sanlon hàng đầu">
                        </div>
                        <div class="features-list__description">
                          <h5 class="features-list__header">
                            Quản lý nhân viên
                          </h5>
                          <p class="features-list__content">
                            Quản lý nhân sự không thể thiếu trong hệ thống quản lý salon. BKSalon giúp bạn quản lý nhân viên của mình dễ dàng và thuận tiện.
                          </p>
                        </div>
                      </li>
                </ul>
                <div class="clearfix"></div>
            </div>
    </section>
    <footer class="main-footer">
        <div class="main-footer__left">
            <img src="{{asset('adminlte/ic_launcher_144.png')}}" class="main-footer__logo">
      </div>
      <div class="main-footer__row">
        <div class="main-footer__left">
          <div class="main-footer__top">
            <ul class="main-footer__links">
              <li>
                <a href="https://www.shedul.com/#top" class="main-footer__link main-footer__link--bold scroll-link">Home</a>
              </li>
              <li>
                <a href="https://www.shedul.com/contact.html" class="main-footer__link main-footer__link--bold">Contact Us</a>
              </li>
              <div class="clearfix"></div>
            </ul>
          </div>
          <div class="main-footer__bottom">
            <strong>Copyright © 2019 <a href="{{route('home-page')}}">BKSalon</a>.</strong> All rights reserved.
          </div>
        </div>
        <div class="main-footer__right">
          <ul class="main-footer__links social-links">
            <li>
              <a href="https://www.facebook.com/Shedul-1504479806470099/?fref=ts" class="social-links__item icon--fb" target="blank"><i class="icon-facebook social-links__icon fa fa-facebook-square fa-4x"></i>
              </a>
            </li>
            <li>
              <a href="https://twitter.com/heyshedul" class="social-links__item icon--twitter fa fa-twitter-square fa-4x" target="blank"><i class="icon-twitter social-links__icon"></i>
              </a>
            </li>
            <li>
              <a href="{{route('appointment-detail',['id'=>1])}}" class="social-links__item icon--instagram" target="blank"><i class="icon-instagram social-links__icon"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="clearfix"></div>
      </div>
    </footer>
</body>
</html>