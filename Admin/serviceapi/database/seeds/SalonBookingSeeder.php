<?php

use Illuminate\Database\Seeder;

class SalonBookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date_now = date('Y-m-d');
        DB::table('booking')->insert([
            [
                'customer_id' => 13,
                'id_salon' => 2,
                'stylist_id' => 6,
                'status' => 0,
                'phone_number' => '0964999989',
                'cus_name' => 'Customer Huy',
                'booking_time' => date("Y-m-d H:i:s", strtotime('+8 hours +15 minutes', strtotime($date_now))),
                'booking_end_time' => date("Y-m-d H:i:s", strtotime('+8 hours +90 minutes', strtotime($date_now)))


            ], [
                'customer_id' => 16,
                'id_salon' => 2,
                'stylist_id' => 9,
                'status' => 0,
                'phone_number' => '0964999971',
                'cus_name' => 'Customer Manh',
                'booking_time' => date("Y-m-d H:i:s", strtotime('+8 hours +15 minutes', strtotime($date_now))),
                'booking_end_time' => date("Y-m-d H:i:s", strtotime('+8 hours +90 minutes', strtotime($date_now)))

            ],
            [
                'customer_id' => 14,
                'id_salon' => 2,
                'stylist_id' => 7,
                'status' => 0,
                'phone_number' => '0964999989',
                'cus_name' => 'Customer Cuong',
                'booking_time' => date("Y-m-d H:i:s", strtotime('+8 hours +15 minutes', strtotime($date_now))),
                'booking_end_time' => date("Y-m-d H:i:s", strtotime('+8 hours +45 minutes', strtotime($date_now)))

            ], [
                'customer_id' => 15,
                'id_salon' => 2,
                'stylist_id' => 8,
                'status' => 0,
                'phone_number' => '0964999977',
                'cus_name' => 'Customer Linh',
                'booking_time' => date("Y-m-d H:i:s", strtotime('+14 hours +30 minutes', strtotime($date_now))),
                'booking_end_time' => date("Y-m-d H:i:s", strtotime('+14 hours +60 minutes', strtotime($date_now)))
            ], [
                'customer_id' => 17,
                'id_salon' => 2,
                'stylist_id' => 6,
                'status' => 0,
                'phone_number' => '0964999000',
                'cus_name' => 'Customer X',
                'booking_time' => date("Y-m-d H:i:s", strtotime('+14 hours +30 minutes', strtotime($date_now))),
                'booking_end_time' => date("Y-m-d H:i:s", strtotime('+14 hours +60 minutes', strtotime($date_now)))
            ], [
                'customer_id' => 18,
                'id_salon' => 2,
                'stylist_id' => 7,
                'status' => 0,
                'phone_number' => '0964999001',
                'cus_name' => 'Customer Y',
                'booking_time' => date("Y-m-d H:i:s", strtotime('+14 hours +30 minutes', strtotime($date_now))),
                'booking_end_time' => date("Y-m-d H:i:s", strtotime('+14 hours +105 minutes', strtotime($date_now)))
            ]
            , [
                'customer_id' => 19,
                'id_salon' => 2,
                'stylist_id' => 10,
                'status' => 0,
                'phone_number' => '0964999002',
                'cus_name' => 'Customer Z',
                'booking_time' => date("Y-m-d H:i:s", strtotime('+14 hours +30 minutes', strtotime($date_now))),
                'booking_end_time' => date("Y-m-d H:i:s", strtotime('+14 hours +105 minutes', strtotime($date_now)))
            ] , [
                'customer_id' => 20,
                'id_salon' => 2,
                'stylist_id' => 10,
                'status' => 0,
                'phone_number' => '0964999003',
                'cus_name' => 'Customer t',
                'booking_time' => date("Y-m-d H:i:s", strtotime('+16 hours +30 minutes', strtotime($date_now))),
                'booking_end_time' => date("Y-m-d H:i:s", strtotime('+16 hours +105 minutes', strtotime($date_now)))
            ], [
                'customer_id' => 21,
                'id_salon' => 2,
                'stylist_id' => 10,
                'status' => 0,
                'phone_number' => '0964999004',
                'cus_name' => 'Customer a',
                'booking_time' => date("Y-m-d H:i:s", strtotime('+16 hours +30 minutes', strtotime($date_now))),
                'booking_end_time' => date("Y-m-d H:i:s", strtotime('+16 hours +90 minutes', strtotime($date_now)))
            ]

        ]);

        DB::table('booking_detail')->insert([
            [
                'id_booking' => 1,
                'service_id' => 6,
                'price'=>25000,
            ], [
                'id_booking' => 1,
                'service_id' => 7,
                'price'=>150000,
            ],
            [
                'id_booking' => 2,
                'service_id' => 6
                ,
                'price'=>25000,
            ], [
                'id_booking' => 2,
                'service_id' => 7,
                'price'=>150000,
            ],
            [
                'id_booking' => 3,
                'service_id' => 6,
                'price'=>25000,
            ], [
                'id_booking' => 4,
                'service_id' => 8,
                'price'=>200000,
            ], [
                'id_booking' => 5,
                'service_id' => 6,
                'price'=>25000,
            ], [
                'id_booking' => 6,
                'service_id' => 6,
                'price'=>25000,
            ],
            [
                'id_booking' => 6,
                'service_id' => 7,
                'price'=>150000,
            ], [
                'id_booking' => 7,
                'service_id' => 6,
                'price'=>25000,
            ],
            [
                'id_booking' => 7,
                'service_id' => 7,
                'price'=>150000,
            ], [
                'id_booking' => 8,
                'service_id' => 6,
                'price'=>25000,
            ],
            [
                'id_booking' => 8,
                'service_id' => 7,
                'price'=>150000,
            ]
            , [
                'id_booking' => 9,
                'service_id' => 8,
                'price'=>200000,
            ],
            [
                'id_booking' => 9,
                'service_id' => 9,
                'price'=>200000,
            ]

        ]);
    }
}
