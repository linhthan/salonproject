<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $date_now = date('Y-m-d');
        DB::table('product_category')->insert([
        	[
        		'title'=>'Chăm sóc mặt',
                'salon_id'=>2

        	],
        	[
        		'title'=>'Chăm sóc cơ thể',
                'salon_id'=>2

        	],
        	[
        		'title'=>'Chăm sóc tóc',
                'salon_id'=>2

        	],
        	[
        		'title'=>'Chăm sóc râu',
                'salon_id'=>2

        	],
        	[
        		'title'=>'Tạo kiểu tóc',
                'salon_id'=>2

        	],

        ]);

        DB::table('product')->insert([
        	[
        		'salon_id'=>2,
        		'category_id'=>1,
        		'title'=>'Sữa Rửa Mặt Giúp Sáng Mịn Da',
        		'description'=>'Sữa Rửa Mặt Giúp Sáng Mịn Da - Nature Solution Natural Cleansing Foam The Plant Base',
        		'thumbnail'=>'https://product.hstatic.net/1000306701/product/13_0406042bcd7842799da484e3b4834f97_large.jpg',
        		'original_price'=>400000,
        		'current_price'=>399000,
        		'quantity'=>5,


        	],
        	[
        		'salon_id'=>2,
        		'category_id'=>1,
        		'title'=>'Sữa Rửa Mặt Se Khít Lỗ Chân Lông - Th',
        		'description'=>'Sữa Rửa Mặt Giúp  Se Khít Lỗ Chân Lông- Nature Solution Natural Cleansing Foam The Plant Base',
        		'thumbnail'=>'https://product.hstatic.net/1000306701/product/21_04862f266b474f9b8c9e649dd1855d94_large.jpg',
        		'original_price'=>399000,
        		'current_price'=>399000,
        		'quantity'=>2,

        	],
        	[
        		'salon_id'=>2,
        		'category_id'=>1,
        		'title'=>'Sữa Dưỡng Nhân Sâm Trắng Da',
        		'description'=>'Sữa Dưỡng Nhân Sâm Trắng Da - The Plant Base',
        		'thumbnail'=>'https://product.hstatic.net/1000306701/product/31_bb96d91f532f4c38bf307399892320b2_large.jpg',
        		'original_price'=>599000,
        		'current_price'=>599000,
        		'quantity'=>5,

        	],
        	[
        		'salon_id'=>2,
        		'category_id'=>3,
        		'title'=>'Dầu gội giữ màu Rockaholic',
        		'description'=>'Dầu gội giữ màu Rockaholic - Start Me Up',
        		'thumbnail'=>'https://product.hstatic.net/1000306701/product/12345678_79281dc2632e43caa4e024575673027b_large.jpg',
        		'original_price'=>278000,
        		'current_price'=>278000,
        		'quantity'=>5,

        	],
        	[
        		'salon_id'=>2,
        		'category_id'=>1,
        		'title'=>'Dầu Gội phục hồi hư tổn Rockaholic',
        		'description'=>'TIGI chưa bao giờ làm cho người dùng thất vọng.Dầu Gội phục hồi hư tổn Rockaholic - Heaven’s Door giúp bạn biến hoá mái tóc khô xơ, rối bù nhanh chóng. Hiệu quả thấy ngay sau 3 lần sử dụng.',
        		'thumbnail'=>'https://product.hstatic.net/1000306701/product/12345_8168897d10d84ceaa8233be09a264ee5_large.jpg',
        		'original_price'=>278000,
        		'current_price'=>278000,
        		'quantity'=>5,

        	],

        ]);
        DB::table('order_product')->insert([
        	[
        		'customer_id'=>15,
        		'salon_id'=>2,
        		'phone_number'=>'0964999977',
        		'cus_name'=>'Customer Linh',
        		'address'=>'Số nhà 14 ngõ 67 ngách 34 Lê Thanh Nghị Hai Bà Trưng Hà Nội',
        		'note'=>'',
        		'order_created_at'=>date("Y-m-d H:i:s", strtotime('-8 hours +15 minutes', strtotime($date_now))),
        		'status' =>1,
        	],
        	[
        		'customer_id'=>16,
        		'salon_id'=>2,
        		'phone_number'=>'0964999971',
        		'cus_name'=>'Customer Mạnh',
        		'address'=>'Số nhà 12 ngõ 67 ngách 34 Lê Thanh Nghị Hai Bà Trưng Hà Nội',
        		'note'=>'Chuyển đến trong buổi chiều',
        		'order_created_at'=>date("Y-m-d H:i:s", strtotime('-30 hours +15 minutes', strtotime($date_now))),
        		'status' =>1,

        	],
        	[
        		'customer_id'=>14,
        		'salon_id'=>2,
        		'phone_number'=>'0964999989',
        		'cus_name'=>'Customer Cuong',
        		'address'=>'Số nhà 15 ngõ 67 ngách 34 Lê Thanh Nghị Hai Bà Trưng Hà Nội',
        		'note'=>'Cần tư vấn chính xác',
        		'order_created_at'=>date("Y-m-d H:i:s", strtotime('+12 hours +15 minutes', strtotime($date_now))),
        		'status' =>0,

        	],
        	[
        		'customer_id'=>15,
        		'salon_id'=>2,
        		'phone_number'=>'0964999977',
        		'cus_name'=>'Customer Linh',
        		'address'=>'Số nhà 14 ngõ 67 ngách 34 Lê Thanh Nghị Hai Bà Trưng Hà Nội',
        		'note'=>'Cần tư vấn chính xác',
        		'order_created_at'=>date("Y-m-d H:i:s", strtotime('-48 hours +15 minutes', strtotime($date_now))),
        		'status' =>1,

        	],
        	
        ]);

        DB::table('order_detail')->insert([
        	[
        		'id_order'=>1,
        		'product_id'=>1,
        		'price'=>399000,
        		'quantity'=>1,


        	],
        	[
        		'id_order'=>2,
        		'product_id'=>1,
        		'price'=>399000,
        		'quantity'=>1

        	],
        	[
        		'id_order'=>2,
        		'product_id'=>2,
        		'price'=>399000,
        		'quantity'=>1

        	],
        	[
        		'id_order'=>3,
        		'product_id'=>4,
        		'price'=>278000,
        		'quantity'=>1

        	],
        	[
        		'id_order'=>3,
        		'product_id'=>5,
        		'price'=>278000,
        		'quantity'=>1
        	],
        	[
        		'id_order'=>4,
        		'product_id'=>4,
        		'price'=>278000,
        		'quantity'=>1

        	],
        	[
        		'id_order'=>4,
        		'product_id'=>5,
        		'price'=>278000,
        		'quantity'=>1
        	],

        ]);
    }
}
