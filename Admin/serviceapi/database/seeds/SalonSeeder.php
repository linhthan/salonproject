<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('location')->insert([[
            'city_province' => 'Hà Nội',
            'district' => 'Hai Bà Trưng',
            'wards' => 'Trần Đại Nghĩa',
            'detail_location' => 'Số 125 Trần Đại Nghĩa Hai Bà Trưng Hà Nội',
            'location_status' => 0
        ], [
            'city_province' => 'Hà Nội',
            'district' => 'Hai Bà Trưng',
            'wards' => 'Lê Thanh Nghị',
            'detail_location' => 'Số 76A Lê Thanh Nghị Hai Bà Trưng Hà Nội',
            'location_status' => 0
        ], [
            'city_province' => 'Hà Nội',
            'district' => 'Hai Bà Trưng',
            'wards' => 'Bạch Mai',
            'detail_location' => 'Số 18 Trần Đại Nghĩa Hai Bà Trưng Hà Nội',
            'location_status' => 0
        ], [
            'city_province' => 'Hồ Chí Minh',
            'district' => 'Quận 3',
            'wards' => 'Phường 03',
            'detail_location' => 'Số 18 Bàn Cờ, Phường 03 Quận 3, Thành Phố Hồ Chí Minh',
            'location_status' => 0
        ], [
            'city_province' => 'Hồ Chí Minh',
            'district' => 'Quận 3',
            'wards' => 'Phường 06',
            'detail_location' => 'Số 37B Phạm Ngọc thạch, Phường 06 Quận 3, Thành Phố Hồ Chí Minh',
            'location_status' => 0
        ]]);
        DB::table('salon')->insert([[
            'owner_id' => 1,
            'location_id' => 1,
            'name' => 'Ken Salon',
            'thumb_url' => 'http://i.imgur.com/hMMRpaDg.png',
            'salon_create_at' => new DateTime('2019-10-22 8:37:02'),

        ], [
            'owner_id' => 2,
            'location_id' => 2,
            'name' => '30Shine',
            'thumb_url' => 'http://i.imgur.com/h1xo2hRg.png',
            'salon_create_at' => new DateTime('2019-10-25 8:33:02'),

        ], [
            'owner_id' => 3,
            'location_id' => 3,
            'name' => 'Young Salon',
            'thumb_url' => 'http://i.imgur.com/NUkEme9.jpg',
            'salon_create_at' => new DateTime('2019-10-25 8:32:02'),

        ], [
            'owner_id' => 17,
            'location_id' => 4,
            'name' => 'HairSalon and Academy Thịnh',
            'thumb_url' => 'http://i.imgur.com/NUkEme9.jpg',
            'salon_create_at' => new DateTime('2019-10-25 8:30:02'),

        ], [
            'owner_id' => 18,
            'location_id' => 5,
            'name' => 'IDHair Việt Nam',
            'thumb_url' => 'http://i.imgur.com/NUkEme9.jpg',
            'salon_create_at' => new DateTime('2019-10-25 8:40:02'),

        ]]);
        DB::table('review')->insert([
            [
                'customer_id' => 13,
                'salon_id' => 1,
                'content' => 'Good Salon',
                'star' => 5,
                'review_at' => new DateTime('2019-10-11 15:47:02')

            ], [
                'customer_id' => 13,
                'salon_id' => 2,
                'content' => 'Good Salon',
                'star' => 5,
                'review_at' => new DateTime('2019-10-11 15:47:02')

            ]
            , [
                'customer_id' => 14,
                'salon_id' => 1,
                'content' => 'Best Salon',
                'star' => 5,
                'review_at' => new DateTime('2019-10-11 15:47:07')

            ], [
                'customer_id' => 14,
                'salon_id' => 2,
                'content' => 'Good Hair Salon',
                'star' => 4,
                'review_at' => new DateTime('2019-10-11 15:30:07')
            ]
        ]);
        DB::table('salonInfo')->insert([
            [
                'salon_id' => 1,
                'open_time' => 480,
                'close_time' => 1260,
                'count_seat' => 4,

            ], [
                'salon_id' => 2,
                'open_time' => 480,
                'close_time' => 1260,
                'count_seat' => 4,

            ],
            [
                'salon_id' => 3,
                'open_time' => 480,
                'close_time' => 1290,
                'count_seat' => 2,

            ],
            [
                'salon_id' => 4,
                'open_time' => 480,
                'close_time' => 1260,
                'count_seat' => 4,

            ],
            [
                'salon_id' => 5,
                'open_time' => 480,
                'close_time' => 1290,
                'count_seat' => 1,

            ],
        ]);

    }
}
