<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalonServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('service')->insert([
            [
                'title' => 'Cắt tóc nam',
                'id_salon' => 1,
                'content' => 'Cắt tóc nam với 7 bước cơ bản ....',
                'price' => 30000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Uốn tóc',
                'id_salon' => 1,
                'content' => 'Uốn tóc giúp tóc trở nên đẹp hơn ....',
                'price' => 200000,
                'duration' => 45,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Duỗi tóc',
                'id_salon' => 1,
                'content' => 'Duỗi tóc khiến bạn tự tin hơn',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Nhuộm tóc',
                'id_salon' => 1,
                'content' => 'Nhuộm màu bạn thích cho mái tóc của mình',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ], [
                'title' => 'Phục hồi tóc',
                'id_salon' => 1,
                'content' => 'Phục hồi cho mái tóc chắc khỏe',
                'price' => 300000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Cắt tóc nam',
                'id_salon' => 2,
                'content' => 'Cắt tóc nam với 7 bước cơ bản ....',
                'price' => 25000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Uốn tóc',
                'id_salon' => 2,
                'content' => 'Uốn tóc giúp tóc trở nên đẹp hơn ....',
                'price' => 150000,
                'duration' => 45,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Duỗi tóc',
                'id_salon' => 2,
                'content' => 'Duỗi tóc khiến bạn tự tin hơn',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Nhuộm tóc',
                'id_salon' => 2,
                'content' => 'Nhuộm màu bạn thích cho mái tóc của mình',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Phục hồi tóc',
                'id_salon' => 2,
                'content' => 'Phục hồi cho mái tóc chắc khỏe',
                'price' => 300000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Cắt tóc nam',
                'id_salon' => 3,
                'content' => 'Cắt tóc nam với 7 bước cơ bản ....',
                'price' => 25000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Uốn tóc',
                'id_salon' => 3,
                'content' => 'Uốn tóc giúp tóc trở nên đẹp hơn ....',
                'price' => 150000,
                'duration' => 45,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Duỗi tóc',
                'id_salon' => 3,
                'content' => 'Duỗi tóc khiến bạn tự tin hơn',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Nhuộm tóc',
                'id_salon' => 3,
                'content' => 'Nhuộm màu bạn thích cho mái tóc của mình',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Phục hồi tóc',
                'id_salon' => 3,
                'content' => 'Phục hồi cho mái tóc chắc khỏe',
                'price' => 300000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Cắt tóc nam',
                'id_salon' => 4,
                'content' => 'Cắt tóc nam với 7 bước cơ bản ....',
                'price' => 25000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Uốn tóc',
                'id_salon' => 4,
                'content' => 'Uốn tóc giúp tóc trở nên đẹp hơn ....',
                'price' => 150000,
                'duration' => 45,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Duỗi tóc',
                'id_salon' => 4,
                'content' => 'Duỗi tóc khiến bạn tự tin hơn',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Nhuộm tóc',
                'id_salon' => 4,
                'content' => 'Nhuộm màu bạn thích cho mái tóc của mình',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Phục hồi tóc',
                'id_salon' => 4,
                'content' => 'Phục hồi cho mái tóc chắc khỏe',
                'price' => 300000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Cắt tóc nam',
                'id_salon' => 5,
                'content' => 'Cắt tóc nam với 7 bước cơ bản ....',
                'price' => 25000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Uốn tóc',
                'id_salon' => 5,
                'content' => 'Uốn tóc giúp tóc trở nên đẹp hơn ....',
                'price' => 150000,
                'duration' => 45,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Duỗi tóc',
                'id_salon' => 5,
                'content' => 'Duỗi tóc khiến bạn tự tin hơn',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Nhuộm tóc',
                'id_salon' => 5,
                'content' => 'Nhuộm màu bạn thích cho mái tóc của mình',
                'price' => 200000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ],
            [
                'title' => 'Phục hồi tóc',
                'id_salon' => 5,
                'content' => 'Phục hồi cho mái tóc chắc khỏe',
                'price' => 300000,
                'duration' => 30,
				'thumbnail'=>'https://i.imgur.com/jBommFi.jpg',
                'status' => 1
            ]
        ]);
    }
}
