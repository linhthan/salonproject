<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sale')->insert([
                [
                    'salon_id' => 1,
                    'title' => 'Khuyến mại khủng cho sinh viên',
                    'banner_url' => 'http://i.imgur.com/RpkOsJQ.jpg',
                    'sale_url' => 'https://www.facebook.com/kensalon.vn/photos/a.837674259671221/1387323944706247/',
                    'sale_create_at' => new DateTime('2019-10-10 6:15:37')
                ], [
                    'salon_id' => 2,
                    'title' => 'Cơn bão Khai trương Tháng 10 – 30Shine bùng nổ chào đón thêm 9 Salon khắp Việt Nam!',
                    'banner_url' => 'https://blog.30shine.com/wp-content/uploads/2019/10/Webp.net-resizeimage-89-2.jpg',
                    'sale_url' => 'https://blog.30shine.com/con-bao-khai-truong-thang-10-30shine-bung-no-chao-don-them-9-salon-khap-viet-nam/',
                    'sale_create_at' => new DateTime('2019-10-11 6:15:37')
                ]
                , [
                    'salon_id' => 2,
                    'title' => 'Tưng bừng khai trương salon 30Shine mới toanh tại 19 Trần Hưng Đạo, Hạ Long, Quảng Ninh!',
                    'banner_url' => 'https://blog.30shine.com/wp-content/uploads/2019/02/Banner-Blog.jpg',
                    'sale_url' => 'https://blog.30shine.com/tung-bung-khai-truong-salon-30shine-moi-toanh-tai-19-tran-hung-dao-ha-long-quang-ninh/',
                    'sale_create_at' => new DateTime('2019-10-11 6:20:15')
                ]
                , [
                    'salon_id' => 2,
                    'title' => '30Shine liên tục nổ những CÚ NỔ LỚN KHAI TRƯƠNG cán mốc 70 salon trong Tháng 8',
                    'banner_url' => 'https://blog.30shine.com/wp-content/uploads/2019/08/Webp.net-resizeimage-66-1024x984.jpg',
                    'sale_url' => 'https://blog.30shine.com/30shine-dang-lien-tuc-no-nhung-cu-no-lon-khai-truong-can-moc-70salon/',
                    'sale_create_at' => new DateTime('2019-10-11 12:30:15')
                ], [
                    'salon_id' => 2,
                    'title' => 'Sở hữu 30Shine member',
                    'banner_url' => 'https://storage.30shine.com/banner/20190822_shinememberweb.jpg',
                    'sale_url' => 'https://landing.30shine.com/shine_member',
                    'sale_create_at' => new DateTime('2019-10-11 12:30:20')
                ]
                , [
                    'salon_id' => 2,
                    'title' => 'Combo tỏa sáng chào hè',
                    'banner_url' => 'https://storage.30shine.com/banner/20190909_remix_toasang_w.jpg',
                    'sale_url' => 'https://landing.30shine.com/combo-toa-sang-chao-he',
                    'sale_create_at' => new DateTime('2019-10-11 12:30:20')
                ]
            ]
        );
    }
}
