<?php

use Illuminate\Database\Seeder;

class StylistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('employeesalon')->insert([
            [
                'user_id' => 6,
                'salon_id' => 2,
                'view_count' => 10,
                'like_count' => 0,
                'status' => 1
            ], [
                'user_id' => 7,
                'salon_id' => 2,
                'view_count' => 8,
                'like_count' => 1,
                'status' => 1
            ], [
                'user_id' => 8,
                'salon_id' => 2,
                'view_count' => 0,
                'like_count' => 0,
                'status' => 1
            ], [
                'user_id' => 9,
                'salon_id' => 2,
                'view_count' => 0,
                'like_count' => 0,
                'status' => 1
            ],
            [
                'user_id' => 10,
                'salon_id' => 2,
                'view_count' => 0,
                'like_count' => 0,
                'status' => 1
            ]
            ,
            [
                'user_id' => 24,
                'salon_id' => 2,
                'view_count' => 0,
                'like_count' => 0,
                'status' => 1
            ]
            ,
            [
                'user_id' => 25,
                'salon_id' => 2,
                'view_count' => 0,
                'like_count' => 0,
                'status' => 1
            ]
        ]);
        DB::table('stylist_working_time')->insert([
            [
                'stylist_id' => 6,
                'start_time' => 480,
                'end_time' => 1080,
                'day_of_week' => '2,3,4,5,6,7',
            ], [
                'stylist_id' => 7,
                'start_time' => 480,
                'end_time' => 720,
                'day_of_week' => '2,3,4,5,6,7,8',
            ], [
                'stylist_id' => 8,
                'start_time' => 840,
                'end_time' => 1080,
                'day_of_week' => '2,3,4,5,6,7,8',
            ], [
                'stylist_id' => 9,
                'start_time' => 480,
                'end_time' => 1080,
                'day_of_week' => '2,3,4,5,6',
            ],
            [
                'stylist_id' => 10,
                'start_time' => 480,
                'end_time' => 1080,
                'day_of_week' => '2,3,4,5,6,7',
            ]
        ]);
    }
}
