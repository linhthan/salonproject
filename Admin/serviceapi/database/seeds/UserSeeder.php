<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
                [
                    'name' => 'Admin Ken',
                    'email' => 'ken3663@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Name',
                    'user_type' => 2,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999999'],
                [
                    'name' => 'Admin 30 Shine',
                    'email' => '30shine@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 2,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999998'],
                [
                    'name' => 'Admin Young',
                    'email' => 'yuong97@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 2,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999997']
                ,
                [
                    'name' => 'Stylist Ken Huy',
                    'email' => 'stylistken@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 1,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999991']
                ,
                [
                    'name' => 'Stylist Ken Hung',
                    'email' => 'hungken@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 1,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999992'],
                [
                    'name' => 'Stylist 30Shine Tien',
                    'email' => 'tien30shine@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 1,
                    'avatar'=>'https://i.imgur.com/hTvlRj1.png',
                    'phone_number' => '0964999993'],
                [
                    'name' => 'Stylist 30shine Dung',
                    'email' => 'dung30shine@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 1,
                    'avatar'=>'https://i.imgur.com/O1MdHN7.png',
                    'phone_number' => '0964999994'],
                [
                    'name' => 'Stylist 30Shine Tung',
                    'email' => 'tung30shine@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 1,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999995'],
                [
                    'name' => 'Stylist 30Shine Cuong',
                    'email' => 'cuong30shine@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 1,
                    'avatar'=>'https://i.imgur.com/JaPICo6.png',
                    'phone_number' => '0964999985'],
                [
                    'name' => 'Stylist 30Shine Ngoc',
                    'email' => 'ngoc30shine@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 1,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999975'],
                [
                    'name' => 'Stylist Young Nam',
                    'email' => 'namyuong@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 1,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999996'],
                [
                    'name' => 'Stylist Young Ngoc',
                    'email' => 'ngocyuong@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 1,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999989'],
                [
                    'name' => 'Customer Huy',
                    'email' => 'huycus@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 0,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999989'],
                [
                    'name' => 'Customer Cuong',
                    'email' => 'cuongcus@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 0,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999989'],
                [
                    'name' => 'Customer Linh',
                    'email' => 'linhcus@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 0,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999977']
                ,
                [
                    'name' => 'Customer Manh',
                    'email' => 'manhcus@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 0,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999971']
                ,
                [
                    'name' => 'Customer X',
                    'email' => 'cuongcusx@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 0,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999000'],
                [
                    'name' => 'Customer Y',
                    'email' => 'linhcusy@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 0,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999001']
                ,
                [
                    'name' => 'Customer Z',
                    'email' => 'manhcusz@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 0,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999002']
                ,
                [
                    'name' => 'Customer t',
                    'email' => 'manhcust@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 0,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999003']
                ,
                [
                    'name' => 'Customer a',
                    'email' => 'manhcusa@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 0,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999004']
                ,
                [
                    'name' => 'Admin Academy Thịnh',
                    'email' => 'academythinh@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 2,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999909']
                ,
                [
                    'name' => 'ID Hair Việt Nam',
                    'email' => 'idhair@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 2,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999907']
                    ,
                [
                    'name' => 'Waiter Phuong',
                    'email' => 'waiter1@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nữ',
                    'user_type' => 3,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999145']
                     ,
                [
                    'name' => 'CS Huy',
                    'email' => 'cssalon@gmail.com',
                    'password' => bcrypt('linhlol123'),
                    'sex' => 'Nam',
                    'user_type' => 4,
                    'avatar'=>'https://i.imgur.com/L9NGyTf.png',
                    'phone_number' => '0964999146']
            ]
        );
    }
}
