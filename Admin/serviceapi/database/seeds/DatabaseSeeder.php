<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cls = [
            UserSeeder::class,
            SalonSeeder::class,
            SaleSeeder::class,
            StylistSeeder::class,
            SalonServiceSeeder::class,
            SalonBookingSeeder::class,
            ProductTableSeeder::class

        ];
        foreach ($cls as $c) {
            $this->call($c);
        }
    }
}
