<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('order_detail', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('id_order');
            $table->integer('product_id');
            $table->foreign('id_order')->references('id')->on('order_product');
            $table->foreign('product_id')->references('id')->on('product');
            $table->double('price');
            $table->integer('quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('order_detail');
    }
}
