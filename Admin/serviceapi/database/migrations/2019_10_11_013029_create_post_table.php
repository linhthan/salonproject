<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('post', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('user_id');
            $table->integer('post_cat_id');
            $table->integer('salon_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('post_cat_id')->references('id')->on('post_category');
            $table->foreign('salon_id')->references('id')->on('salon');
            $table->string('title');
            $table->string('content');
            $table->string('thumbnail');
            $table->integer('view_count')->default(0);
            $table->dateTime('create_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('post');
    }
}
