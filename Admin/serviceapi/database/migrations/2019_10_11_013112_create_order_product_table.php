<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('order_product', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('customer_id')->nullable();
            $table->integer('salon_id');
            $table->string('phone_number');
            $table->string('cus_name');
            $table->foreign('customer_id')->references('id')->on('users');
            $table->foreign('salon_id')->references('id')->on('salon');
            $table->string('address');
            $table->string('note')->nullable();
            $table->tinyInteger('payment_type')->default(0);
            $table->dateTime('order_created_at');
            $table->tinyInteger('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('order_product');
    }
}
