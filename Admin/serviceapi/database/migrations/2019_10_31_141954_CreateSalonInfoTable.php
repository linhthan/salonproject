<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('salonInfo', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('salon_id');
            $table->foreign('salon_id')->references('id')->on('salon');
//            $table->integer('location_id')->references('id')->on('location');
            $table->integer('avg_time_slot')->default(15);
            $table->integer('count_seat');
            $table->integer('open_time');
            $table->integer('close_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('salonInfo');
    }
}
