<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStylistinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employeesalon', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('user_id');
            $table->integer('salon_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('salon_id')->references('id')->on('salon');
            $table->string('day_of_week');
            $table->integer('view_count')->default(0);
            $table->integer('like_count')->default(0);
            $table->tinyInteger('status')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('employeesalon');
    }
}
