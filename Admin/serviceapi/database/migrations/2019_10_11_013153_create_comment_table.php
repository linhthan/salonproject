<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('comment_post', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('customer_id');
            $table->integer('post_id');
            $table->foreign('customer_id')->references('id')->on('users');
            $table->foreign('post_id')->references('id')->on('post');
            $table->string('content');
            $table->dateTime('comment_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('comment_post');
    }
}
