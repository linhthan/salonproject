<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('reaction', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('from_uid');
            $table->integer('comment_id');
            $table->integer('post_id');
            $table->integer('react_type');

            $table->foreign('from_uid')->references('id')->on('users');
            $table->foreign('comment_id')->references('id')->on('comment_post');
            $table->foreign('post_id')->references('id')->on('post');
            $table->foreign('react_type')->references('id')->on('reaction_type');
            $table->dateTime('last_update_at');
            $table->tinyInteger('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('reaction');
    }
}
