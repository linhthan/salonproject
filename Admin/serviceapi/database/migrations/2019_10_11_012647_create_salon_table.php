<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('salon', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('owner_id');
            $table->foreign('owner_id')->references('id')->on('users');
            $table->integer('location_id')->references('id')->on('location');
            $table->string('name');
            $table->string('thumb_url');
//            $table->integer('avg_time_slot')->default(15);
            $table->dateTime('salon_create_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('salon');
    }
}
