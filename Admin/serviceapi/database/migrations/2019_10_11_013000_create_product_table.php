<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('salon_id');
            $table->integer('category_id');
            $table->foreign('salon_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('product_category');
            $table->string('title');
            $table->string('description');
            $table->string('thumbnail');
            $table->double('original_price');
            $table->double('current_price');
            $table->integer('quantity');
            $table->tinyInteger('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('product');
    }
}
