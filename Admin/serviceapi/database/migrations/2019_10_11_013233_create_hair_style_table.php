<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHairStyleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('hair_style', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('salon_id');
            $table->integer('id_booking')->nullable();
            $table->foreign('salon_id')->references('id')->on('salon');
            $table->foreign('id_booking')->references('id')->on('booking');
            $table->string('title');
            $table->string('url');
            $table->dateTime('create_at');
            $table->integer('view_count')->default(0);
            $table->integer('download_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('hair_style');
    }
}
