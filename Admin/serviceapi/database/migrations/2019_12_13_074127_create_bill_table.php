<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('id_salon');
            $table->integer('id_booking')->nullable();
            $table->integer('id_order')->nullable();
            $table->integer('discount')->nullable();
            $table->integer('discount_percent')->nullable();
            $table->double('total_price');
            $table->dateTime('create_at');
            $table->foreign('id_booking')->references('id')->on('booking');
            $table->foreign('id_order')->references('id')->on('order_product');
             
            $table->foreign('id_salon')->references('id')->on('salon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill');
    }
}
