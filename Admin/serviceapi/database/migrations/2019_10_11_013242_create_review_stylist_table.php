<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewStylistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('review', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('customer_id');
            $table->integer('stylist_id')->nullable();
            $table->integer('salon_id')->nullable();
            $table->integer('booking_id')->nullable();
            $table->foreign('customer_id')->references('id')->on('users');
            $table->foreign('stylist_id')->references('id')->on('users');
            $table->foreign('booking_id')->references('id')->on('booking');
            $table->foreign('salon_id')->references('id')->on('salon');
            $table->string('content');
            $table->tinyInteger('star');
            $table->dateTime('review_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('review');
    }
}
