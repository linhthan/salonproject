<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('sale', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('salon_id');
            $table->foreign('salon_id')->references('id')->on('salon');
            $table->string('title');
            $table->string('banner_url');
            $table->string('sale_url');
            $table->tinyInteger('status');
            $table->dateTime('sale_create_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('sale');
    }
}
