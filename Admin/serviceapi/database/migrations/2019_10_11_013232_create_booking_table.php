<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('booking', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('customer_id')->nullable();
            $table->integer('id_salon');
            $table->string('phone_number');
            $table->string('cus_name');
            $table->integer('stylist_id');
            $table->foreign('customer_id')->references('id')->on('users');
            $table->foreign('id_salon')->references('id')->on('salon');
            $table->foreign('stylist_id')->references('id')->on('users');
            $table->tinyInteger('status');
            $table->dateTime('booking_time');
            $table->dateTime('booking_end_time');
            $table->dateTime('checkin_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('booking');
    }
}
