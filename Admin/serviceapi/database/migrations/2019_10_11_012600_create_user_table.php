<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        // user type
        // 0 => Customer
        // 1 => Stylist
        // 2 => Admin of salon 
        // 3 => Waiter of Salon
        // 4 => Customer service(take photo, upload photo)
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->string('name', 40)->default('Anonymous');
            $table->string('password');
            $table->string('sex')->nullable();
            $table->string('email', 40)->unique()->nullable();
            $table->string('phone_number', 15)->nullable();
            $table->integer('job_id')->nullable();
            $table->tinyInteger('user_type');
            $table->string('avatar')->nullable();
            $table->string('remember_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('users');
    }
}
