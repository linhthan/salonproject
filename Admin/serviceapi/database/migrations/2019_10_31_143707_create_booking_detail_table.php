<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('booking_detail', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('id_booking');
            $table->integer('service_id');
            $table->double('price');
            $table->foreign('id_booking')->references('id')->on('booking');
            $table->foreign('service_id')->references('id')->on('service');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('booking_detail');
    }
}
