<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('service', function (Blueprint $table) {
            $table->integer('id', 11);
            $table->integer('id_salon');
            $table->foreign('id_salon')->references('id')->on('salon');
            $table->string('title');
            $table->string('thumbnail')->nullable();
//            $table->string('barcode');
            $table->string('content');
//            $table->integer('vat');
            $table->double('price');
            $table->integer('duration');
            $table->tinyInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('service');
    }
}
