<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $user = $request->user();
        if($user && $user->user_type && $user->user_type !=2){
            return response()->json(['message' => 'Unauthenticated.'], 401);
        }
        return $next($request);
    }
}
