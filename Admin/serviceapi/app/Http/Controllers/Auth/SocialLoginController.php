<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
class SocialLoginController extends Controller
{
   

    public function loginGoogle(Request $request)
    {
        $email = $request->email;
        $user = User::where('email',$email)->first();
        if($user){
            $user->name = $request->name;
            $user->avatar = $request->avatar;
            $user->save();
            $success['user'] = $user;
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return response()->json([
                        'success'=>true,
                        'message' => 'Đăng nhập thành công',
                        'data'=>$success
                    ], 200);
        }else{
            $name = $request->name;
            $avatar = $request->avatar;
            $user = User::create([
                    'name' => $name,
                    'email' => $email,
                    'avatar'=>$avatar,
                    'password' => bcrypt('anonymouspassword'),
                    'user_type'=>1,
                ]);
            $success['user'] = $user;
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return response()->json([
                            'success'=>true,
                            'message' => 'Đăng nhập thành công',
                            'data'=>$success
                        ], 200);
        }
        
    }
    public function loginFacebook(Request $request)
    {
        $email = $request->email;
        $user = User::where('email',$email)->first();
        if($user){
            $user->name = $request->name;
            $user->avatar = $request->avatar;
            $user->save();
            $success['user'] = $user;
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return response()->json([
                        'success'=>true,
                        'message' => 'Đăng nhập thành công',
                        'data'=>$success
                    ], 200);
        }else{
            $name = $request->name;
            $avatar = $request->avatar;
            $user = User::create([
                    'name' => $name,
                    'email' => $email,
                    'avatar'=>$avatar,
                    'password' => bcrypt('anonymouspassword'),
                    'user_type'=>1,
                ]);
            $success['user'] = $user;
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return response()->json([
                            'success'=>true,
                            'message' => 'Đăng nhập thành công',
                            'data'=>$success
                        ], 200);
        }
    }

}
