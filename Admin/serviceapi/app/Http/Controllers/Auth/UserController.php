<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use App\User;
use Image;
use Illuminate\Pagination\LengthAwarePaginator;

class UserController extends Controller
{
    //

    function getUserInfo(Request $request){
        $user = Auth::user();
        if($user){
            return response()->json([
                'success'=>true,
                'data'=>Auth::user(),
            ],200);
        }else{
            return response()->json([
                'success'=>false ,
                'data'=>null,
            ],200);
        }

    }

    function updatePhoneNumber(Request $request){
        $user= Auth::user();
        $phoneNumber = $request->phone_number;
       
        $checkUser = User::where('phone_number',$phoneNumber)->first();
        if($checkUser){
            return response()->json([
                'success'=>false,
                'message' => 'Số điện thoại đã sử dụng, vui lòng kiểm tra lại!'
            ],200);
        }else{
             $user->phone_number = $phoneNumber;
             if($user->save()){
                return response()->json([
                'success'=>true,
                'message' => 'Cập nhật số điện thoại thành công, bây giờ bạn có thể đặt lịch!'
            ],200);
             }else{
                return response()->json([
                'success'=>false,
                'message' => 'Có lỗi trong quá trình cập nhật, vui lòng kiểm tra lại!'
                ],200);
             }
        }
    }

    function updatePassword(Request $request){
        $user= Auth::user();
        $oldPass = $request->old_pass;
        $newPass = bcrypt($request->new_pass);
        $anoyPass = 'anonymouspassword';
        $current_password = Auth::user()->password;
        $checkSamePass  = Hash::check($oldPass, $current_password) || Hash::check($anoyPass, $current_password); 
        if($checkSamePass){
            $user->password = $newPass;
            $user->save();
            return response()->json([
                'success'=>true,
                'message' => 'Thay đổi mật khẩu thành công!'
            ],200);
        }else{
                return response()->json([
                'success'=>false,
                'message' => 'Mật khẩu cũ không trùng khớp, vui lòng thử lại!'
                ],200);
        }
    }

    function updateProfile(Request $request){
        $user= Auth::user();
        $email = $request->email;
        $checkEmail = User::where('id','!=',$user->id)->where('email',$email)->first();
        if($checkEmail){
                return response()->json([
                    'success'=>false,
                    'message' => 'Địa chỉ email đã sử dụng, vui lòng kiểm tra lại!'
                ],200);
        }


        $phoneNumber = $request->phone_number;

        $checkUser = User::where('id','!=',$user->id)->where('phone_number',$phoneNumber)->first();
        if($checkUser){
            return response()->json([
                'success'=>false,
                'message' => 'Số điện thoại đã sử dụng, vui lòng kiểm tra lại!'
            ],200);
        }else{
            $user->name = $request->name;
            $user->email = $email;
            $user->phone_number = $phoneNumber;
            $file = $request->file('image');
            $urlUpload ='';
             if($file){
                $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
                $filePath = 'avatar/' . $imageName;
                try {
                    $image = Image::make($file)->widen(200,function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
                    Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                    $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/avatar/' . $imageName;
                } catch (Exception $e) {
                   return response()->json([
                    'success'=>false,
                    'message'=>'Có lỗi trong quá trình tải lên ảnh, vui lòng thử lại sau!'
                    ]);
                }
            }
            if($urlUpload!=''){
                $user->avatar = $urlUpload;
            }
            $user->save();
            return response()->json([
                'success'=>true,
                'message' => 'Cập nhật thông tin thành công!',
                'data'=>$user
                 ],200);
        }
    }


    function getStaffDetail(Request $request){
         $salon_id = $request->salon_id;
         $staffId =  $request->staff_id;
         $user = User::find($staffId);
         if($user){
            return response()->json([
                'success'=>true,
                'message' => 'Thành công!',
                'data'=>$user
                ],200);
         }else{
            return response()->json([
                'success'=>false,
                'message' => 'Không tìm thấy thông tin nhân viên',
                'data'=>null
                ],200);
         }
    }

    function getAllStaff(Request $request){
        $salon_id = $request->salon_id;

        $filter = $request->filter;
        $keyword = $request->keyword;


        if($salon_id){
            $user_type = [];
            switch ($filter) {
            case 0:
                $user_type =[1,3,4];
                break;
            case 1:
                $user_type =[1];
                break;
            case 2:
                $user_type =[3];
                break;
            case 3:
                $user_type =[4];
                break;    
            default:
                $user_type =[1,3,4];
                break;
            }

            $users = DB::table('users')
            ->join('employeesalon','employeesalon.user_id','users.id')
            ->where('employeesalon.salon_id',$salon_id)
            ->where(function ($query) use ($keyword) {
                $query->where('users.phone_number','like', '%' . $keyword . '%')
                ->orWhere('users.name', 'like', '%' . $keyword . '%');
            })
            ->where('employeesalon.status',1)
            ->whereIn('users.user_type',$user_type)
            ->select('users.*')
            ->paginate(4);
            if($request->ajax()){
                return view('admin.ajax.employee_management_result',compact('users'));
            }else{
                return response()->json([
                'success'=>true,
                'message' => 'Thành công!',
                'data'=>$users
                ],200);
            }
        }else{
            if($request->ajax()){
                return view('admin.ajax.employee_management_result');
            }else{
                return response()->json([
                'success'=>false,
                'message' => 'Không tìm thấy kết quả cho salon này!'
                ],200);
            }
        }
    }

    function addStaff(Request $request){
        $email = $request->email;
        if(!is_null($email)){
            if(User::where('email',$email)->first()){
                return response()->json([
                    'success'=>false,
                    'message' => 'Email đã tồn tại, vui lòng kiểm tra lại!'
                ],200);
            }
        }
        
        $phone_number = $request->phone_number;
        if(User::where('phone_number',$phone_number)->first()){
            return response()->json([
                'success'=>false,
                'message' => 'Số điện thoại đã sử dụng, vui lòng kiểm tra lại!'
            ],200);
        }

        $salon_id = $request->salon_id;
        $user_name = $request->user_name;
        $password = bcrypt($request->password);
        $user_type = $request->user_type;
        $working_day = $request->working_day;
        $urlUpload ='';
        if ($request->file('image')){
            $file = $request->file('image');
            $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
            $filePath = 'avatar/' . $imageName;
            try {
                $image = Image::make($file)->widen(200, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/avatar/' . $imageName;
            } catch (Exception $e) {
               return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình tải lên ảnh nhân viên!'
                ]);
            }

        }
        $urlUpload= $urlUpload ==''? null : $urlUpload;
        DB::beginTransaction();
        $staffId = DB::table('users')->insertGetId([
            'name'=>$user_name,
            'email'=>$email,
            'name'=>$user_name,
            'password'=> $password,
            'phone_number'=>$phone_number,
            'user_type'=>$user_type,
            'avatar'=>$urlUpload,
        ]);
        if($staffId){
            $insert = DB::table('employeesalon')->insert([
                'user_id'=>$staffId,
                'salon_id'=>$salon_id,
                'day_of_week'=>$working_day,
                'status'=>1
            ]);
            if($insert){

                 DB::commit();
                return response()->json([
                    'success'=>true,
                    'message'=>'Thêm nhân viên thành công',
                ],200);

            }else{
                DB::rollback();
                 return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình thêm nhân viên, vui lòng thử lại sau',
                ],200);
            }
        }else{
            DB::rollback();
            return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình thêm nhân viên, vui lòng thử lại sau',
            ],200);
        }

       
    }

    public function updateUser(Request $request){

        $email = $request->email;
        if(!is_null($email)){
            if(User::where('id','!=',$request->user_id)->where('email',$email)->first()){
                return response()->json([
                    'success'=>false,
                    'message' => 'Email đã tồn tại, vui lòng kiểm tra lại!'
                ],200);
            }
        }
        
        $phone_number = $request->phone_number;

        if(User::where('id','!=',$request->user_id)->where('phone_number',$phone_number)->first()){
            return response()->json([
                'success'=>false,
                'message' => 'Số điện thoại đã sử dụng, vui lòng kiểm tra lại!'
            ],200);
        }

        $user_id = $request->user_id;
        $user_name = $request->user_name;
        $password = $request->password?bcrypt($request->password) : null;
        
        $urlUpload ='';
        if ($request->file('image')){
            $file = $request->file('image');
            $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
            $filePath = 'avatar/' . $imageName;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/avatar/' . $imageName;
        }
        $urlUpload= $urlUpload ==''? null : $urlUpload;
        $updateInfor = [ 'name'=>$user_name,
            ];
        if($password){
            $updateInfor['password'] = $password;
        }

        if($urlUpload){
            $updateInfor['avatar'] = $urlUpload;
        }
        if($email){
            $updateInfor['email'] = $email;
        }
        if($phone_number){
            $updateInfor['phone_number'] = $phone_number;
        }


        User::where('id',$user_id)->update($updateInfor);
        return response()->json([
            'success'=>true,
            'message'=>'Cập nhật thành công',
            'data'=>$updateInfor
        ]);

    }
    public function deleteStaff(Request $request){
        $user_id = $request->user_id;
        $salon_id = $request->salon_id;
        if($salon_id && $user_id){
            DB::table('employeesalon')->where('user_id',$user_id)
        ->where('salon_id',$salon_id)
        ->update(['status'=>0]);
        return response()->json([
                    'success'=>true,
                    'message' => 'Xóa nhân viên thành công!'
                ],200); 
        }else{
           return response()->json([
                    'success'=>false,
                    'message' => 'Có lỗi trong quá trình xóa nhân viên, vui lòng thử lại!'
                ],200); 
            
        }
    }

    function getBookingHistoryByUser(Request $request){
        $bookings = DB::table('booking')
            ->where('booking.status',3)
            ->where('booking.customer_id',Auth::user()->id)
            ->join('salon','salon.id','booking.id_salon')
            ->leftJoin('history_booking','booking.id','history_booking.id_booking')
            ->leftJoin('bill','bill.id_booking','booking.id')
            ->leftJoin('review','booking.id','review.booking_id')
            ->select('booking.id','booking.customer_id','phone_number','cus_name','booking_end_time','history_booking.url_art','history_booking.id as idHistory','review.star','salon.name as salonName','bill.create_at as paidAt')
            ->orderBy('booking_time','desc')
            ->get();
        $results = array();
        $currentId = -1;
        $currentIndex = -1;
        foreach ($bookings as $booking) {
            if($booking->id!=$currentId){
                $currentIndex++;
                $booking->images =[];
                $currentId = $booking->id;
                if($booking->url_art){
                    array_push($booking->images, [
                        'id'=>$booking->idHistory
                        ,'url_art'=>$booking->url_art
                     ,'id_booking'=>$booking->id
                    ]);  
                }
                unset($booking->url_art);
                array_push($results,$booking);
            }else{
                $bookingResult = $results[$currentIndex];
                if(!isset($bookingResult->images)){
                    $booking->images =[];

                }
                array_push($bookingResult->images, ['id'=>$booking->idHistory,
                    'url_art'=>$booking->url_art
                    ,'id_booking'=>$booking->id]);
            }

        }

        $page = $request->page?$request->page:0; // Get the ?page=1 from the url
        $pageSize = $request->pageSize?$request->pageSize:5; // Number of items per page
        $offset = ($page * $pageSize);
        return new LengthAwarePaginator(
            array_slice($results, $offset, $pageSize), // Only grab the items we need
            count($results), // Total items
            $pageSize, // Items per page
            $page, // Current page
            ['path' => $request->url(), 'query' => $request->query()] // We need this so we can keep all old query parameters from the url
        );
    }
}
