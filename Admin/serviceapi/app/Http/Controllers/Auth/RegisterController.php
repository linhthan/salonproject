<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function signUp(Request $request) {
        $email = $request->email;
        if(!is_null($email)){
            if(User::where('email',$email)->first()){
                return response()->json([
                    'message' => 'Email đã tồn tại, vui lòng kiểm tra lại!'
                ],200);
            }
        }
        
        $phone_number = $request->phone_number;
        if(User::where('phone_number',$phone_number)->first()){
            return response()->json([
                'message' => 'Số điện thoại đã sử dụng, vui lòng kiểm tra lại!'
            ],200);
        }

        $name = $request->name;
        $password = $request->password;
        $job_id = $request->job_id;
        $sex = $request->sex;
        $avatar = $request->avatar;
        $user_type = $request->user_type; 
        $status = $request->status;
        $user = new User();
        $user->phone_number = $phone_number;
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->job_id = $job_id;
        $user->sex = $sex;
        $user->avatar = $avatar;
        $user->user_type = $user_type;
        DB::beginTransaction();
        if($user_type ==2){
            $salonName = $request->salonName;
            $detailLocation = $request->detailLocation;
            $city = $request->city;
            $district = $request->district;
            $file = $request->file('image');
            if($file){
                $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
                $filePath = 'salon/' . $imageName;
                Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
                $urlUpload = '';

                try {
                    $image = Image::make($file)->widen(200, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
                    Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                    $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/salon/' . $imageName;
                } catch (Exception $e) {
                   return response()->json([
                    'success'=>false,
                    'message'=>'Có lỗi trong quá trình tải lên ảnh nhân viên!'
                    ]);
                }

                $ward = $request->ward;
                $info =  $salonName &&  $detailLocation && $city &&$district&& $ward;

                if($info){
                     if($user->save()){
                        $success['token'] =  $user->createToken('MyApp')->accessToken; 
                        $success['user'] =  $user;
                       $location_Id =  DB::table('location')->insertGetId([
                              'city_province'=>$city,
                               'district'=>$district,
                               'wards'=>$ward, 
                               'detail_location'=>$detailLocation, 
                        ]);
                     if($location_Id){
                          $salonId =   DB::table('salon')->insertGetId([
                               'owner_id'=>$user->id,
                               'location_id'=>$location_Id,
                               'name'=>$salonName, 
                               'thumb_url'=>$urlUpload,
                               'salon_create_at'=>date("Y-m-d H:i:s"), 
                            ]);
                              DB::table('salonInfo')->insert([
                                'salon_id'=>$salonId,
                                'avg_time_slot' =>15,
                                'count_seat' =>0,
                                'open_time'=> 480,
                                'close_time'=>960
                              ]);
                            $success['salon_id'] =  $salonId;
                            DB::commit();
                            return response()->json([
                                'success'=>true,
                                'message' => 'Đăng ký thành công',
                                'data' =>$success
                        ],201); 

                        }else{
                            DB::rollBack();
                             return response()->json([
                                'success'=>false,
                                 'message' => 'Đăng ký thất bại, vui lòng thử lại sau'
                            ],200); 
                        }

                    }else{
                        DB::rollBack();
                        return response()->json([
                            'success'=>false,
                            'message' => 'Đăng ký thất bại, vui lòng thử lại sau'
                        ],200); 
                    }
                }else{
                    DB::rollBack();
                     return response()->json([
                           'success'=>false,
                            'message' => 'Thông tin đăng ký chưa đủ, vui lòng thử lại sau'
                        ],200);  
                }
            }else{
                DB::rollBack();
                     return response()->json([
                           'success'=>false,
                            'message' => 'Thông tin đăng ký chưa đủ, vui lòng thử lại sau'
                        ],200);  
                }
            
        }else{
             if($user->save()){
                    $success['token'] =  $user->createToken('MyApp')->accessToken; 
                    $success['user'] =  $user;
                     DB::commit();
                    return response()->json([
                          'success'=>true,
                        'message' => 'Đăng ký thành công',
                        'data' =>$success
                    ],201); 
                }else{
                    DB::rollBack();
                    return response()->json([
                        'success'=>false,
                        'message' => 'Đăng ký thất bại, vui lòng thử lại sau'
                    ],200); 
                }
        }


    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'job' => $data['job'],
            'sex'=> $data['sex'],

            'password' => bcrypt($data['password']),
        ]);
    }


}
