<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginWithEmail(Request $request){
            $credentials = request(['email', 'password']);
            if(Auth::attempt($credentials)){
                $user = Auth::user();
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $success['user'] = $user;
                $user_type = $user->user_type;
                if( $user_type == 2){
                    $salon_id = DB::table('salon')->where('owner_id','=', $user->id)->select('id')->first();
                    if($salon_id){
                       $success['salon_id']  =  $salon_id->id; 
                    }
                }else if($user_type !=0){
                    $salon_id = DB::table('employeesalon')->where('user_id','=',$user->id)->select('salon_id')->first();
                      if($salon_id){
                       $success['salon_id']  =  $salon_id->salon_id; 
                    }
                }

                return response()->json([
                        'success'=>true,
                        'message' => 'Đăng nhập thành công',
                        'data'=>$success
                    ], 200);
            }else{
                return response()->json([
                        'success'=>false,
                        'message' => 'Email hoặc mật khẩu không chính xác, vui lòng thử lại!'
                    ], 200);
            }
       
    }

    public function loginAnonymous(Request $request){
        $user = new User();
        $user->name = "BKSalon member";
        $user->password = bcrypt('anonymouspassword');
        $user->user_type = 0;
        if($user->save()){
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            $success['user'] = $user;

            return response()->json([
                    'success'=>true,
                    'message' => 'Đăng nhập thành công',
                    'data' =>$success
                ],200); 

        }else{
            return response()->json([
                    'success'=>false,
                    'message' => 'Đăng nhập thất bại, vui lòng thử lại sau'
            ],200); 
        }

    }

    public function loginWithPhoneNumber(Request $request){

             $credentials = request(['phone_number', 'password']);
            if(Auth::attempt($credentials)){
                $user = Auth::user();
                $success['token'] =  $user->createToken('MyApp')-> accessToken;
                $success['user'] = $user;
                $user_type = $user->user_type;
                if( $user_type == 2){
                    $salon_id = DB::table('salon')->where('owner_id','=', $user->id)->select('id')->first();
                    if($salon_id){
                       $success['salon_id']  =  $salon_id->id; 
                    }
                }else if($user_type !=0){
                    $salon_id = DB::table('employeesalon')->where('user_id','=',$user->id)->select('salon_id')->first();
                      if($salon_id){
                       $success['salon_id']  =  $salon_id->salon_id; 
                    }
                }

                return response()->json([
                        'success'=>true,
                        'message' => 'Đăng nhập thành công',
                        'data'=>$success
                    ], 200);
            }else{
                return response()->json([
                        'success'=>false,
                        'message' => 'Email hoặc mật khẩu không chính xác, vui lòng thử lại!'
                    ], 200);
            }
    }

    public function logout(Request $request){
        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        return response()->json(['success'=>true,
            'message'=>'Đăng xuất thành công'],204);
    }
}
