<?php

namespace App\Http\Controllers\Salon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Image;


class ServiceController extends Controller
{
    //

    public function createService(Request $request){
        $urlUpload ='';
        if ($request->file('image')){
            $booking_id = $request->booking_id;
            $file = $request->file('image');
            $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
            $filePath = 'services/' . $imageName;
            try {
                $image = Image::make($file)->widen(200, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/services/' . $imageName;
            } catch (Exception $e) {
               return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình tải lên ảnh minh họa dịch vụ!'
                ]);
            }


        }
        $title = $request->title;
        $salon_id = $request->salon_id;
        $servicePrice = $request->servicePrice;
        $duration = $request->duration;
        $description = $request->description;
        $urlUpload =  $urlUpload==''?null: $urlUpload;
        $create = DB::table('service')->insert([
            'title'=>$title,
            'content'=>$description,
            'price'=>$servicePrice,
            'duration'=>$duration,
            'status'=>1,
            'id_salon'=>$salon_id,
            'thumbnail'=>$urlUpload
        ]);
        if($create){

            return response()->json([
                'success'=>true,
                'message'=>'Thêm dịch vụ thành công',
            ]);

        }else{
            return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình thêm dịch vụ!',
            ]);
        }
    }

    public function updateService(Request $request){
        $urlUpload ='';
        if ($request->file('image')){
            $file = $request->file('image');
            $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
            $filePath = 'services/' . $imageName;
            try {
                $image = Image::make($file)->widen(200, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/services/' . $imageName;
            } catch (Exception $e) {
               return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình tải lên ảnh minh họa dịch vụ!'
                ]);
            }

        }

        $title = $request->titleService;
        $service_id = $request->service_id;
        $servicePrice = $request->priceService;
        $duration = $request->durationService;
        $description = $request->contentService;
        $status = $request->statusService;
        $urlUpload =  $urlUpload==''?null: $urlUpload;
        $update = [
            'title'=>$title,
            'content'=>$description,
            'price'=>$servicePrice,
            'duration'=>$duration,
            'status'=>$status,
        ];
        if($urlUpload){
            $update['thumbnail']= $urlUpload;
        }
        DB::table('service')
        ->where('id',$service_id)
        ->update($update);
         return response()->json([
                'success'=>true,
                'message'=>'Cập nhật dịch vụ thành công',
            ]);

    }

    public function deleteService(Request $request){
         $service_id = $request->service_id;
         DB::table('service')
            ->where('id',$service_id)
            ->update([
                'status'=>0
            ]);
         return response()->json([
                'success'=>true,
                'message'=>'Xóa dịch vụ thành công!',
            ]);

    }

    public function getSalonServiceManagement(Request $request){
        $salon_id = $request->salon_id;
        $keyword = $request->keyword;

        if($salon_id){
            $services = DB::table('service')->where('id_salon', '=', $salon_id)
                ->where('title','like', '%' . $keyword . '%')
                ->where('status',1)
                ->paginate(5);
                if($request->ajax()){
                    return view('admin.ajax.service_management_result',compact('services'));
                }else{
                    return response()->json([
                        'success'=>true,
                        'message'=>'Thành công!',
                        'data'=> $services
                        ],200);
                }
                
            }else{
                if($request->ajax()){
                    return view('admin.ajax.service_management_result');
                }else{
                    return response()->json([
                        'success'=>false,
                        'message'=>'Có lỗi trong quá trình xử lý hóa đơn, vui lòng thử lại sau!',
                        'data'=> null
                        ],200);
                }
            }
 }


}
