<?php

namespace App\Http\Controllers\Salon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Image;


class ProductController extends Controller
{
    //


    public function errorResponse(){
        return response()->json([
                'success'=>false,
                'message' => 'Có lỗi trong quá trình xử lý, vui lòng thử lại sau!',
                ], 200);
    }

    public function updateProduct(Request $request){
    	$product_id= $request->product_id;
    	$title = $request->title;
    	$currentPrice = $request->current_price;
        $origin_price = $request->originPrice;
        $categoryId = $request->cat_id;
        $file = $request->file('image');
        if(!$product_id ||!$title ||!$currentPrice ||!$origin_price||!$categoryId){

             return response()->json(['success'=>false,
                    'message' => 'Có lỗi trong quá trình cập nhật, vui lòng thử lại sau',
                     ], 200);
        }
        if($file){
            $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
            $filePath = 'products/' . $imageName;
            $urlUpload = '';
            try {
                $image = Image::make($file)->widen(300, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/products/' . $imageName;
            } catch (Exception $e) {
               return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình tải lên ảnh sản phẩm!'
                ]);
            }
            DB::table('product')
            ->where('id',$product_id)
            ->update(
                [
                    'title'=>$title,
                    'category_id'=>$categoryId,
                    'original_price'=>$origin_price,
                    'current_price'=>$currentPrice,
                    'thumbnail' =>$urlUpload
                ]);
            return response()->json(['success'=>true,
                'message' => 'Chỉnh sửa sản phẩm thành công',
                'thumbUrl'=>$urlUpload
                    ], 200);
        }else{
            DB::table('product')
            ->where('id',$product_id)
            ->update(
                [
                    'title'=>$title,
                    'category_id'=>$categoryId,
                    'original_price'=>$origin_price,
                    'current_price'=>$currentPrice
                ]);
            return response()->json(['success'=>true,
                'message' => 'Chỉnh sửa sản phẩm thành công',
                ], 200);
            }
    }
    

    public function deleteProduct(Request $request){
        $product_id= $request->product_id;
        DB::table('product')
            ->where('id',$product_id)
            ->update(
                [
                    'status'=>0,
                ]);
        return response()->json(['success'=>true,
            'message' => 'Xóa sản phẩm thành công',
            ], 200);
        }    


    public function getAllSalonProductForImport(Request $request){
        $salon_id = $request->salon_id;
        $products = Product::where('salon_id',$salon_id)->where('status',1)->get();
        if($products){
            if($request->ajax()){
                  return view('admin.ajax.import_product_select_result',compact('products'));  
            }else{
                  return response()->json([
                'success'=>true,
                'data' => $products,
            ], 200);
            }
          
        }else{
             if($request->ajax()){
                return view('admin.ajax.import_product_select_result');  
            }else{
                  return response()->json([
                'success'=>false,
                'data' => null,
            ], 200);
            }
        }
    }

    public function getAllProductCategorySalon(Request $request){
     $salon_id = $request->salon_id;
     if($salon_id){
        $category = DB::table('product_category')
                            ->where('product_category.salon_id',$salon_id)
                            ->get();
        if($request->ajax()){
                    return view('admin.ajax.product_category_result',compact('category'));
                }else{
                    return response()->json([
                    'success' => true,
                    'message' => 'Thành công',
                    'data' => $category
                ]);
            }
     }else{
        if($request->ajax()){
                    return view('admin.ajax.product_category_result');
                }else{
                    return response()->json([
                    'success' => false,
                    'message' => 'Có lỗi xảy ra, vui lòng thử lại sau!',
                    'data' => null
                ]);
            }
     }
    }

    public function importProduct(Request $request){
    	$salon_id = $request->salon_id;
    	$title = $request->title;
    	$origin_price = $request->origin_price;
    	$current_price = $request->current_price;
    	$category_id = $request->category_id;
    	$description = $request->description;
    	$quantity = $request->quantity;
    	$file = $request->file('image');

        $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
        $filePath = 'products/' . $imageName;
        $urlUpload = '';
            try {
                $image = Image::make($file)->widen(300, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/products/' . $imageName;
            } catch (Exception $e) {
               return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình tải lên ảnh sản phẩm!'
                ]);
        }

        $product = new Product();
        $product->title = $title;
        $product->salon_id = $salon_id;
        $product->category_id = $category_id;
        $product->description = $description;
        $product->thumbnail = $urlUpload;
        $product->original_price = $origin_price;
        $product->current_price = $current_price;
        $product->status = 1;
        $product->quantity = 0;
        if($product->save()){
        	 return response()->json([
        	 	'success'=>true,
        	 	'message' => 'Thêm sản phẩm thành công',
            ], 200);
        }else{
        	 return response()->json([
        	 	'success'=>false,
        	 	'message' => 'Có lỗi trong quá trình thêm sản phẩm',
            ], 200);
        }

    }
    public function importProductWithNewCategory(Request $request){
        $salon_id = $request->salon_id;
        $title = $request->title;
        $origin_price = $request->origin_price;
        $current_price = $request->current_price;
        $category_title = $request->category_title;
        $description = $request->description;
        $quantity = $request->quantity;
        $file = $request->file('image');
        
        $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
        $filePath = 'products/' . $imageName;
        $urlUpload = '';
            try {
                $image = Image::make($file)->widen(300, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/products/' . $imageName;
            } catch (Exception $e) {
               return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình tải lên ảnh sản phẩm!'
                ]);
        }

        $product = new Product();
        $product->title = $title;
        $product->salon_id = $salon_id;
        $product->status = 1;
        $product->description = $description;
        $product->thumbnail = $urlUpload;
        $product->original_price = $origin_price;
        $product->current_price = $current_price;
        $product->quantity = 0;
        $category_id = DB::table('product_category')->insertGetId(
                            [
                                'title'=> $category_title,
                                'salon_id'=>$salon_id
                            ]);
        if($category_id){
            $product->category_id = $category_id;
            if($product->save()){
             return response()->json([
                'success'=>true,
                'message' => 'Thêm sản phẩm thành công',
            ], 200);
            }else{
                 return response()->json([
                    'success'=>false,
                    'message' => 'Có lỗi trong quá trình thêm sản phẩm',
                ], 200);
            }
        }else{
            return response()->json([
                    'success'=>false,
                    'message' => 'Có lỗi trong quá trình thêm sản phẩm',
                ], 200);
        }
        

    }

    public function getAllSalonProduct(Request $request){
        $salon_id = $request->salon_id;
        $products = Product::where('salon_id',$salon_id)->get();
        if($products){
            if($request->ajax()){
                  return view('admin.ajax.product_select_result',compact('products'));  
            }else{
                  return response()->json([
                'success'=>true,
                'data' => $products,
            ], 200);
            }
          
        }else{
             if($request->ajax()){
                return view('admin.ajax.product_select_result');  
            }else{
                  return response()->json([
                'success'=>false,
                'data' => null,
            ], 200);
            }
        }
    }

    public function getAllProductCategory(Request $request){
     $salon_id = $request->salon_id;
     if($salon_id){
        $category = DB::table('product_category')
                            ->where('product_category.salon_id',$salon_id)
                            ->get();
        if($request->ajax()){
                    return view('admin.ajax.product_category_result',compact('category'));
                }else{
                    return response()->json([
                    'success' => true,
                    'message' => 'Thành công',
                    'data' => $category
                ]);
            }
        }else{
            if($request->ajax()){
                        return view('admin.ajax.product_category_result');
                    }else{
                        return response()->json([
                        'success' => false,
                        'message' => 'Có lỗi xảy ra, vui lòng thử lại sau!',
                        'data' => null
                    ]);
                }
            }
    }

    public function importListProduct(Request $request){
        $data = json_decode($request->getContent(), true);

        if($data){
            if (array_key_exists("salon_id",$data) && array_key_exists("dataProduct",$data)){
                 $salon_id = $data['salon_id'];
                 DB::beginTransaction();
                 $idImport = DB::table('import')->insertGetId([
                    'id_salon'=>$salon_id,
                    'import_at'=>Carbon::now()
                 ]);
                 if($idImport){
                     $dataProduct = $data['dataProduct'];
                     $arrayProductId = [];
                     $arrayImportDetail = [];
                     $arrayProductQuantity = [];
                     foreach ($dataProduct as  $product) {
                        array_push($arrayImportDetail, [
                            'id_import'=>$idImport,
                            'id_product'=> $product['id'],
                            'quantity'=>$product['selectQuantity'],
                            'price'=>(double)$product['selectedPrice']
                        ]);
                         Product::where('id',$product['id'])->increment('quantity',$product['selectQuantity']);
                        array_push($arrayProductId, $product['id']);
                        array_push($arrayProductQuantity,[
                            $product['id']=>$product['selectQuantity']
                        ] );
                     }
                   $insertState =  DB::table('import_detail')->insert($arrayImportDetail);
                   if($insertState){
                    DB::commit();
                     return response()->json([
                        'success'=>true,
                        'message' => "Nhập hàng thành công!",
                        'data'=>$idImport
                    ], 200);
                    }else{
                        DB::rollBack();
                        return $this->errorResponse();
                    }
                    
                    
                 }else{
                    DB::rollBack();
                    return $this->errorResponse();
                 }
                
                 
             }else{
                 return $this->errorResponse();
             }
            
        }else{
            return $this->errorResponse();
        }
         
    }

    public function createOrder(Request $request){
        $data = json_decode($request->getContent(), true);

        if($data){
            if (array_key_exists("salon_id",$data) && array_key_exists("dataProduct",$data)&& array_key_exists("cus_name",$data)&& array_key_exists("phone_number",$data)&& array_key_exists("address",$data)){
                 $salon_id = $data['salon_id'];
                 $cus_name = $data['cus_name'];
                 $phone_number = $data['phone_number'];
                 $address = $data['address'];
                 $note = array_key_exists("note",$data)?$data['note']:null;
                 DB::beginTransaction();
                 $idOrder = DB::table('order_product')->insertGetId([
                    'salon_id'=>$salon_id,
                    'order_created_at'=>Carbon::now(),
                    'cus_name'=>$cus_name,
                    'phone_number'=>$phone_number,
                    'address'=>$address,
                    'note'=>$note,
                    'status'=>2
                 ]);
                 if($idOrder){
                     $dataProduct = $data['dataProduct'];
                     $arrayImportDetail = [];
                     foreach ($dataProduct as  $product) {
                        array_push($arrayImportDetail, [
                            'id_order'=>$idOrder,
                            'product_id'=> $product['id'],
                            'quantity'=>$product['selectQuantity'],
                            'price'=> $product['price']
                        ]);
                         $productDatabase = Product::where('id',$product['id'])->first();
                          if($productDatabase->quantity < $product['selectQuantity']){
                             DB::rollBack();
                            return response()->json([
                            'success'=>false,
                            'message' => "Tạo hóa đơn thất bại, sản phẩm ".$productDatabase->title." chỉ còn lại ".strval($productDatabase->quantity). " sản phẩm",
                            ], 200);
                            }else{
                                $productDatabase->decrement('quantity',$product['selectQuantity']);
                            }

                     }
                   $insertState =  DB::table('order_detail')->insert($arrayImportDetail);
                   if($insertState){
                    DB::commit();
                    return response()->json([
                        'success'=>true,
                        'message' => "Tạo hóa đơn thành công!",
                    ], 200);
                    }else{
                        DB::rollBack();
                        return $this->errorResponse();
                    }
                    
                    
                 }else{
                    DB::rollBack();
                    return $this->errorResponse();
                 }
                
                 
             }else{
                 return $this->errorResponse();
             }
            
        }else{
            return $this->errorResponse();
        }
         
    }

     public function createOrderUser(Request $request){
        $listProduct = $request->listProduct;
        $salon_id = $request->salon_id;
        $custom_id = $request->custom_id;
        $cus_name = $request->cus_name;
        $phone_number = $request->phone_number;
        $address = $request->address;
        $note = $request->note; 

        if($salon_id && $cus_name && $phone_number && $address && $custom_id){
                 DB::beginTransaction();
                 $order = new Order;
                 $order->salon_id = $salon_id;
                 $order->customer_id = $custom_id;
                 $order->order_created_at = Carbon::now();
                 $order->cus_name = $cus_name;
                 $order->phone_number = $phone_number;
                 $order->address = $address;
                 $order->note = $note;
                 $order->status = 0;
                 if($order->save()){
                    $idOrder = $order->id;
                     $arrayImportDetail = [];
                     foreach ($listProduct as  $product) {
                        array_push($arrayImportDetail, [
                            'id_order'=>$idOrder,
                            'product_id'=> $product['id'],
                            'quantity'=>$product['selectedQuantity'],
                            'price'=> $product['current_price']
                        ]);
                         $productDB = Product::where('id',$product['id'])->first();
                         if($productDB){
                                if($productDB && $productDB->quantity < $product['selectedQuantity']){

                                     DB::rollBack();
                                    return response()->json([
                                    'success'=>false,
                                    'message' => "Tạo hóa đơn thất bại, sản phẩm ".$productDB['title']." chỉ còn lại ".strval($productDB['quantity']). " sản phẩm",
                                    ], 200);
                                }else{
                                     $productDB->decrement('quantity',$product['selectedQuantity']);
                                }
                         }else{
                                 return response()->json([
                                    'success'=>false,
                                    'message' => "Không tìm thấy thông tin sản phẩm của hóa đơn, vui lòng thử lại!",
                                    ], 200);
                         }
                         
                     }
                   $insertState =  DB::table('order_detail')->insert($arrayImportDetail);
                   if($insertState){
                    DB::commit();
                    return response()->json([
                        'success'=>true,
                        'message' => "Tạo hóa đơn thành công!",
                        'data'=>$order
                    ], 200);
                    }else{
                        DB::rollBack();
                        return $this->errorResponse();
                    }
                    
                    
                 }else{
                    DB::rollBack();
                    return $this->errorResponse();
                 }
            
        }else{
            return $this->errorResponse();
        }
         
    }

    public function getProductById(Request $request){
        $product_id = $request->id;
        $product = DB::table('product')->where('product.id',$product_id)
        ->join('product_category','product.category_id','product_category.id')
        ->select('product.*','product_category.title as catTitle')
        ->first();
        if($product){
            if($request->ajax()){
                return view('admin.ajax.product_detail_result',compact('product'));
            }else{
                 return response()->json([
                'success'=>true,
                'data'=>$product,
                'message' => 'Thành công!',
                ], 200);
            }
        }else{
             if($request->ajax()){
                return view('admin.ajax.product_detail_result');
            }else{
                 errorResponse();
            }
        }
    }

    public function addProductToCart(Request $request){
        $product_id = $request->product_id;
        $leftQuantity = DB::table('product')
        ->where('id',$product_id)
        ->select('quantity')
        ->first();
        if($leftQuantity && $leftQuantity>=1){
                Product::where('id',$product_id)->decrement('quantity',1);
                return response()->json([
                'success'=>true,
                'message' => 'Thành công!',
                ], 200);
        }else{
             return response()->json([
                'success'=>false,
                'message' => 'Số lượng còn lại của sản phẩm đã hết!',
                ], 200);
        }
    }
   
}
