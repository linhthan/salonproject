<?php

namespace App\Http\Controllers\Salon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\HairStyle;
use Illuminate\Support\Facades\DB;

class HairStyleController extends Controller
{
    //

    public function getAllHairStyle(Request $request){
    	$salonId = $request->salon_id;
        $keyword = $request->keyword;
    	if($salonId){
    		$hairstyles = HairStyle::where('salon_id',$salonId)
    		->where('title','like', '%' . $keyword . '%')
    		->paginate(14);
	    	if($request->ajax()){
	    		return view('admin.ajax.hair_style_management_result',compact('hairstyles'));
	    	}else{
	    		return response()->json([
	    			'success'=>true,
	    			'message'=>'Thành công!',
	    			'data' =>$hairstyles
	    		]);
	    	}
    	}else{
    		if($request->ajax()){
	    		return view('admin.ajax.hair_style_management_result');
	    	}else{
	    		return response()->json([
	    			'success'=>false,
	    			'message'=>'Có lỗi trong quá trình xử lý, vui lòng thử lại sau!',
	    			'data' =>null
	    		]);
	    	}
    	}
    	
    }
    public function saveHairStyle(Request $request){
    	$hairStyle = new HairStyle();
	    $hairStyle->salon_id = $request->salon_id;
	    $hairStyle->create_at = date('Y-m-d H:i:s');
	    $hairStyle->title = $request->title;
	    $hairStyle->url = $request->url;
	    $hairStyle->id_booking = $request->id_booking;
	    if($hairStyle->save()){
	        return response()->json([
	                    'success' => true,
	                    'message' => 'Tạo mẫu tóc thành công!',
	                    'data' => $hairStyle
	                ]);   
	    }else{
	         return response()->json([
	                    'success' => false,
	                    'message' => 'Có lỗi xảy ra, vui lòng thử lại sau!',
	                    'data' => null
	                ]);  
	    }
    }
      public function updateHairStyle(Request $request){
    	$hair_id= $request->hair_id;
    	$newTitle = $request->title;
    	

    	if(!$hair_id ||!$newTitle){
    		 return response()->json(['success'=>false,
			     	'message' => 'Có lỗi trong quá trình cập nhật, vui lòng thử lại sau',
			         ], 200);
    	}
    	DB::table('hair_style')
    	->where('id',$hair_id)
    	->update(
    		[
    			'title'=>$newTitle,
    		]);

     return response()->json(['success'=>true,
     	'message' => 'Chỉnh sửa sản phẩm thành công',
            ], 200);
    }

    public function getHairStyleDetail(Request $request){
    	$id_hair = $request->id_hair;
    	$hairstyle = DB::table('hair_style')->where('hair_style.id',$id_hair)
    	->join('salon','salon.id','hair_style.salon_id')
    	->select('hair_style.*','salon.name as salonName')
    	->first();
    	if($hairstyle){
    		return response()->json(['success'=>true,
     			'message' => 'Thành công',
     			'data'=>$hairstyle
            ], 200);
    	}else{
    		return response()->json(['success'=>false,
     			'message' => 'Không tìm thấy thông tin mẫu tóc',
            ], 200);
    	}

    }

    public function updateDownloadHairStyle(Request $request){
    	$listIds = $request->listIds;
    	if(!empty($listIds)){
    		HairStyle::whereIn('id',$listIds)->increment('download_count',1);
    		return response()->json(['success'=>true,
     			'message' => 'Cập nhật thành công!',
            ], 200);
    	}else{
    		return response()->json(['success'=>false,
     			'message' => 'Có lỗi trong quá trình cập nhật',
            ], 200);
    	}
    }
    public function updateViewHairStyle(Request $request){
    	$id_hair = $request->id_hair;
    	if($id_hair){
    		HairStyle::where('id',$id_hair)->increment('view_count',1);
    		return response()->json(['success'=>true,
     			'message' => 'Cập nhật thành công!',
            ], 200);
    	}else{
    		return response()->json(['success'=>false,
     			'message' => 'Có lỗi trong quá trình cập nhật',
            ], 200);
    	}
    }
    
}
