<?php

namespace App\Http\Controllers\Salon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ExportPdfController extends Controller
{
    //
    public function exportReceipt(Request $request){

    	$booking = $request->booking;
    	$services = unserialize($request->services);
    	$bill = $request->bill;
    	// dd($services);
    	$salon = DB::table('salon')->where('salon.id',$bill['id_salon'])
    	->join('location','location.id','salon.location_id')
    	->select('salon.name','location.detail_location')
    	->first();
    	$pdf = PDF::loadView('admin.pages.receipt_detail_export_type_booking', compact(['booking','services','bill','salon']));
    // Finally, you can download the file using download function
    	return $pdf->download('invoice.pdf');
    	// return view('admin.pages.receipt_detail_export',compact(['booking','services','bill','salon']));
    }
    public function exportReceiptTypeOrder(Request $request){
    	$order = $request->order;
    	$order_detail = unserialize($request->order_detail);
    	$bill = $request->bill;
    	// dd($services);
    	$salon = DB::table('salon')->where('salon.id',$bill['id_salon'])
    	->join('location','location.id','salon.location_id')
    	->select('salon.name','location.detail_location')
    	->first();
    	$pdf = PDF::loadView('admin.pages.receipt_detail_export_type_order', compact(['order','order_detail','bill','salon']));
    // Finally, you can download the file using download function
    	return $pdf->download('invoice_'.md5(rand(0,9999)).'.pdf');
    }

    public function exportImportListProduct(Request $request){
        $import_id = $request->import_id;
        $salon_id = $request->salon_id;
        $import = DB::table('import')
        ->where('import.id',$import_id)
        ->where('import.id_salon',$salon_id)
        ->select('import.*')
        ->first();
        $import_detail = DB::table('import_detail')
        ->where('import_detail.id_import',$import_id)
        ->join('product','product.id','import_detail.id_product')
        ->select('import_detail.quantity','import_detail.price','product.title')
        ->get();
        $pdf = PDF::loadView('admin.pages.export_import_list_product', compact(['import','import_detail']));
        $fileUrl= 'pdf/import_'.md5(rand(0,9999)) .'.pdf';
        $path =  public_path($fileUrl) ;
        
        $pdf->save($path);
       return response()->json([
        'success'=>true,
        'url'=> url($fileUrl)
       ]);

    // Finally, you can download the file using download function
        // return $pdf->download('import_'.md5(rand(0,9999)) .'.pdf');
        
    }
}
