<?php

namespace App\Http\Controllers\Salon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    //
    public function getOrderByUser(Request $request)
    {
        $customerId = Auth::user()->id;
        $order = DB::table('order_product')
            ->where('customer_id', $customerId)
            ->join('order_detail', 'order_detail.id_order', 'order_product.id')
            ->join('salon', 'salon.id', 'order_product.salon_id')
            ->join('product', 'order_detail.product_id', 'product.id')
            ->select('order_product.*', 'salon.name as salonName', DB::raw('group_concat(product.title) as titles'))
            ->groupBy('order_product.id')
            ->paginate(10);
        return response()->json([
            'success' => true,
            'data' => $order
        ]);

    }

    public function getDetailOrder(Request $request)
    {
        $orderId = $request->order_id;

        $order_detail = DB::table('order_detail')
            ->where('order_detail.id_order', $orderId)
            ->join('product', 'product.id', 'order_detail.product_id')
            ->join('product_category', 'product_category.id', 'product.category_id')
            ->select('order_detail.price', 'order_detail.quantity', 'product.title', 'product.thumbnail', 'product_category.title as catTitle')
            ->get();

        if ($order_detail) {
            return response()->json([
                'success' => true,
                'data' => $order_detail,
                'message' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'Không tìm thấy thông tin cho đơn hàng này!'
            ]);
        }
    }

    public function cancelOrder(Request $request)
    {
        $orderId = $request->order_id;

        $update = DB::table('order_product')
            ->where('order_product.id', $orderId)
            ->where('status', 0)
            ->update(['status' => 3]);

        if ($update) {
            return response()->json([
                'success' => true,
                'message' => 'Hủy đơn hàng thành công!'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Có lỗi trong quá trình hủy đơn hàng, vui lòng kiểm tra lại trạng thái đơn hàng!'
            ]);
        }
    }

    public function getOrderSalon(Request $request)
    {
        $salonId = $request->salon_id;
        $filterOption = $request->status;
        $keyword = $request->keyword;
        if ($filterOption == -1) {
            $filterOption = [0, 1, 2, 3,4];
        } else {
            $filterOption = [$filterOption];
        }
        $order = DB::table('order_product')
            ->where('salon_id', $salonId)
            ->whereIn('status', $filterOption)
            ->where(function ($query) use ($keyword) {
                $query->where('order_product.phone_number', 'like', '%' . $keyword . '%')
                    ->orWhere('order_product.cus_name', 'like', '%' . $keyword . '%');
            })
            ->paginate(5);
        if ($request->ajax()) {
            return view('admin.ajax.order_management_result', compact('order'));
        } else {
            return response()->json([
                'success' => true,
                'data' => $order
            ]);
        }

    }

    public function getOrderDetail(Request $request)
    {
        $orderId = $request->order_id;
        if ($orderId) {
            $order = DB::table('order_product')
                ->where('id', $orderId)
                ->first();
            $orderDetail = DB::table('order_detail')
                ->where('id_order', $orderId)
                ->join('product', 'order_detail.product_id', 'product.id')
                ->select('order_detail.price', 'order_detail.quantity', 'product.title')
                ->get();
            if ($order && $orderDetail) {
                if ($request->ajax()) {
                    return view('admin.ajax.order_detail_result', compact(['order', 'orderDetail']));
                } else {
                    return response()->json([
                        'success' => true,
                        'message' => 'Không tìm thấy dữ liệu về đơn hàng này!',
                        'data' => [
                            'order' => $order,
                            'order_detail' => $orderDetail
                        ]
                    ], 200);
                }

            } else {
                if ($request->ajax()) {
                    return view('admin.ajax.order_detail_result');
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Không tìm thấy dữ liệu về đơn hàng này!',
                        'data' => null
                    ], 200);
                }
            }

        } else {

            if ($request->ajax()) {
                return view('admin.ajax.order_detail_result');
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Không tìm thấy dữ liệu về đơn hàng này!',
                    'data' => null
                ], 200);
            }

        }
    }

    public function updateOrderStatus(Request $request)
    {
        $order_id = $request->order_id;
        $status = $request->status;
        if ($order_id && $status != null) {
            DB::table('order_product')->where('id', $order_id)
                ->update([
                    'status' => $status
                ]);
            return response()->json([
                'success' => true,
                'message' => 'Cập nhật đơn hàng thành công!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Không tìm thấy thông tin đơn hàng!',
            ], 200);
        }
    }

}
