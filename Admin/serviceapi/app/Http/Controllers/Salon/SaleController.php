<?php

namespace App\Http\Controllers\Salon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use http\Exception;
class SaleController extends Controller
{
    //
    public function createSale(Request $request) 
    {

    	$salon_id = $request->salon_id;
        $titleSale = $request->titleSale;
        $urlSale = $request->urlSale;
        $salon_id = $request->salon_id;
    	if($salon_id){
            $file = $request->file('image');
            $urlUpload = '';
            if($file){
                $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
                $filePath = 'sale/' . $imageName;
                try {
                    $image = Image::make($file)->widen(600,function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
                    Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                    $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/sale/' . $imageName;
                } catch (Exception $e) {
                   return response()->json([
                    'success'=>false,
                    'message'=>'Có lỗi trong quá trình tải banner!'
                    ]);
                }
                DB::table('sale')->insert([
                    'salon_id'=>$salon_id,
                    'title'=>$titleSale,
                    'sale_url'=>$urlSale,
                    'banner_url'=>$urlUpload,
                    'status'=>1,
                    'sale_create_at'=>Carbon::now()
                ]);
                return response()->json([
                    'success'=>true,
                    'message'=>'Thêm khuyến mại thành công!'
                    ]);
            }else{
                return response()->json([
                    'success'=>false,
                    'message'=>'Không tìm thấy banner khuyến mại!'
                    ]);
            }
    		
    	}else{
            return response()->json([
                'success'=>false,
                'message'=>'Có lỗi trong quá trình xử lý, vui lòng thử lại sau!'
                ]);
    		
    	}
    }

    public function updateSale(Request $request){
        $titleSale = $request->titleSale;
        $urlSale = $request->urlSale;
        $status = $request->status;
        $saleId = $request->id_sale;
        if($saleId){
            $file = $request->file('image');
            $urlUpload = '';
            if($file){
                $imageName = time() . '_' . md5(rand(0,9999999)) . '.' . $file->getClientOriginalName();
                $filePath = 'sale/' . $imageName;
                try {
                    $image = Image::make($file)->widen(600,function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
                    Storage::disk('s3')->put($filePath, $image->stream(), 'public');
                    $urlUpload = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/sale/' . $imageName;
                } catch (Exception $e) {
                   return response()->json([
                    'success'=>false,
                    'message'=>'Có lỗi trong quá trình tải banner!'
                    ]);
                }

                
            }

            $update = [
                'title'=>$titleSale,
                'sale_url'=>$urlSale,
                'status'=>$status,
            ];
            if($urlUpload!=''){
                $update['banner_url'] = $urlUpload;
            }

            DB::table('sale')->where('id',$saleId)
            ->update($update);
                return response()->json([
                    'success'=>true,
                    'message'=>'Cập nhật khuyến mại thành công!'
                    ]);
            
            }else{
                return response()->json([
                    'success'=>false,
                    'message'=>'Không tìm thấy thông tin khuyến mại!'
                    ]);
                
            }
    }
    public function deleteSale(Request $request){
        $saleId = $request->id_sale;
        if($saleId){
            DB::table('sale')->where('id',$saleId)->delete();

             return response()->json([
                    'success'=>true,
                    'message'=>'Xóa khuyến mại thành công!'
                 ],200);
        }else{
            return response()->json([
                    'success'=>false,
                    'message'=>'Không tìm thấy thông tin khuyến mại!'
                    ],200);
        }
    }
}
