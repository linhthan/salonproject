<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salon extends Model
{
    //
     protected $table = 'salon';
    public $timestamps = false;
}
