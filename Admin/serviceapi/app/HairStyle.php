<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HairStyle extends Model
{
    //
    protected $table = 'hair_style';
    public $timestamps = false;
}
