<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('test', function () {
    return 'test api';
});
// Route::get('salonSlot','Salon\SalonController@getSalonSlotAvailableRemake');

Route::prefix('v1')->group(function () {
    Route::post('register', 'Auth\RegisterController@signUp');

    Route::post('loginEmail', 'Auth\LoginController@loginWithEmail');

    Route::post('loginPhoneNumber', 'Auth\LoginController@loginWithEmail');

    Route::post('loginFacebook','Auth\SocialLoginController@loginFacebook');

    Route::post('loginGoogle','Auth\SocialLoginController@loginGoogle');

    Route::post('loginAnonymous','Auth\LoginController@loginAnonymous');

    Route::group(['prefix'=>'hairstyle','middleware'=>'auth:api'],function(){
        Route::get('getAllHairStyle','Salon\HairStyleController@getAllHairStyle');
        Route::post('saveHairStyle','Salon\HairStyleController@saveHairStyle');
        Route::patch('updateHairStyle','Salon\HairStyleController@updateHairStyle');
        
    });
    Route::group(['prefix'=>'product','middleware'=>'auth:api'],function(){

        Route::post('importProductWithNewCategory','Salon\ProductController@importProductWithNewCategory');
        Route::post('importProduct','Salon\ProductController@importProduct');
           
    });

    Route::group(['prefix' => 'salon', 'middleware' => 'auth:api'],function(){
       
        Route::post('applyBooking','Salon\SalonController@applyBooking');

        Route::get('getAllAppointmentOfSalon','Salon\SalonController@getAllAppointmentOfSalon');
        Route::get('searchAppointmentOfSalon','Salon\SalonController@searchAppointmentOfSalon');
        
        Route::get('getNextAppointment','Salon\SalonController@getNextAppointment');
        Route::get('searchNextAppointment','Salon\SalonController@searchNextAppointment');

        Route::get('getSalonInfoByAdmin','Salon\SalonController@getSalonInfoByAdmin');
        
        Route::get('getSaleInformation','Salon\SalonController@getSaleInformation');

        Route::get('getUpcommingAppointment','Salon\SalonController@getUpcommingAppointment');

        Route::get('getAllSalon','Salon\SalonController@getAllSalon');
        Route::get('getPopularSalon','Salon\SalonController@getPopularSalon');
        Route::get('getNewSalon','Salon\SalonController@getNewSalon');
        Route::get('getAllSale','Salon\SalonController@getAllSale');
        Route::get('getSalonBookingSlot','Salon\SalonController@getSalonBookingSlot');
        Route::get('getSalonSlotAvailable','Salon\SalonController@getSalonSlotAvailable');
        Route::get('getSalonSlotByStylistAvailable','Salon\SalonController@getSalonSlotByStylistAvailable');
        Route::get('getStylistAvailable','Salon\SalonController@getStylistAvailable');
        Route::get('getSalonDetail','Salon\SalonController@getSalonDetail');
        Route::get('getSalonInfo','Salon\SalonController@getSalonInfo');
        Route::get('getSalonStylists','Salon\SalonController@getSalonStylists');
        Route::get('getSalonService','Salon\SalonController@getSalonService');
        Route::get('getSaleSalon','Salon\SalonController@getSaleSalon');

        Route::get('getAppointmentDetail','Salon\SalonController@getAppointmentDetail');
        
        Route::get('getDoneAppointment','Salon\SalonController@getDoneAppointment');

        Route::get('getDetailAppointmentForCs','Salon\SalonController@getDetailAppointmentForCs');

        Route::get('getAllAppointmentOfSalonByFilter','Salon\SalonController@getAllAppointmentOfSalonByFilter');

        Route::get('getAllTodayAppointmentOfSalonByFilter','Salon\SalonController@getAllAppointmentOfSalonByFilter');
        
        Route::post('removeArt','Salon\SalonController@removeArt');
        
        Route::post('collectPhoto','Salon\SalonController@collectPhoto');

        Route::post('saveHairStyle','Salon\SalonController@saveHairStyle');

        Route::get('getNewHairStyle','Salon\SalonController@getNewHairStyle');

        Route::get('getAllHairStyle','Salon\SalonController@getAllHairStyle');

        Route::get('getAllProductSalon','Salon\SalonController@getAllProductSalon');

        Route::get('getAllProductCategorySalon','Salon\SalonController@getAllProductCategorySalon');

        Route::get('getTopArtOfSalon','Salon\SalonController@getTopArtOfSalon');

        Route::get('getAllReceiptSalon','Salon\SalonController@getAllReceiptSalon');

        Route::get('getReceiptDetailTypeOrder','Salon\SalonController@getReceiptDetailTypeOrder');

        Route::get('getReceiptDetailTypeBooking','Salon\SalonController@getReceiptDetailTypeBooking');

        Route::get('getBillDetail','Salon\SalonController@getBillDetail');
        
        Route::get('getSalonServiceManagement','Salon\ServiceController@getSalonServiceManagement');

        Route::get('getAllSalonProduct','Salon\ProductController@getAllSalonProduct');

        Route::get('getAllSalonProductForImport','Salon\ProductController@getAllSalonProductForImport');
        
        
        Route::get('getInfoHome','Salon\SalonController@getInfoHome');

        Route::get('getInfoBill','Salon\SalonController@getInfoBill');

        Route::get('getAppointmentInfo','Salon\SalonController@getAppointmentInfo');

        Route::get('getSalonConfig','Salon\SalonController@getSalonConfig');

        
        Route::post('saveSalonConfig','Salon\SalonController@saveSalonConfig');


        Route::post('updateProduct','Salon\ProductController@updateProduct');

        Route::post('importProduct','Salon\ProductController@importProduct');

        Route::post('createOrder','Salon\ProductController@createOrder');

        Route::post('createOrderUser','Salon\ProductController@createOrderUser');
        
        Route::post('importListProduct','Salon\ProductController@importListProduct');

        Route::patch('addProductToCart','Salon\ProductController@addProductToCart');

        Route::get('getOrderSalon','Salon\OrderController@getOrderSalon');

        Route::get('getOrderDetail','Salon\OrderController@getOrderDetail');

        Route::post('updateOrderStatus','Salon\OrderController@updateOrderStatus');

        

        Route::get('getProductById','Salon\ProductController@getProductById');

        Route::patch('deleteProduct','Salon\ProductController@deleteProduct');

        Route::post('createService','Salon\ServiceController@createService');

        Route::post('updateService','Salon\ServiceController@updateService');

        Route::patch('deleteService','Salon\ServiceController@deleteService');

        Route::post('rateBooking','Salon\SalonController@rateBooking');

        Route::post('updateDownloadHairStyle','Salon\HairStyleController@updateDownloadHairStyle');

        Route::post('updateViewHairStyle','Salon\HairStyleController@updateViewHairStyle');
        
        Route::get('getHairStyleDetail','Salon\HairStyleController@getHairStyleDetail');
        

        Route::get('getSalonByKeyword','Salon\SalonController@getSalonByKeyword');

        Route::get('getSalonWithGallery','Salon\SalonController@getSalonWithGallery');
        
        Route::get('getAllSalonReview','Salon\SalonController@getAllSalonReview');
        
        Route::get('exportImportListProduct','Salon\ExportPdfController@exportImportListProduct');

    });

    Route::group(['prefix' => 'staff', 'middleware' => ['auth:api']],function(){

        Route::get('getAllStaff','Auth\UserController@getAllStaff');

        Route::post('addStaff','Auth\UserController@addStaff');

        Route::get('getStaffDetail','Auth\UserController@getStaffDetail');

        Route::patch('deleteStaff','Auth\UserController@deleteStaff')->middleware('admin');

    });

     Route::group(['prefix' => 'sale', 'middleware' => ['auth:api']],function(){

        Route::post('createSale','Salon\SaleController@createSale');

        Route::post('updateSale','Salon\SaleController@updateSale');

        Route::delete('deleteSale','Salon\SaleController@deleteSale');
        
    });

    Route::group(['prefix' => 'admin', 'middleware' => ['auth:api']],function(){
         Route::post('logout','Auth\LoginController@logout');
        
         Route::patch('cancelAppointment','Salon\SalonController@cancelAppointment');
         Route::patch('checkInAppointment','Salon\SalonController@checkInAppointment');
         Route::post('createBill','Salon\SalonController@createBill');
         
     });
    
    

     Route::group(['prefix' => 'user', 'middleware' => 'auth:api'],function(){
         Route::get('getUserInfo','Auth\UserController@getUserInfo');
         Route::get('getPendingAppointmentByUser','Salon\SalonController@getPendingAppointmentByUser');

        Route::get('getAppointmentByUser','Salon\SalonController@getAppointmentByUser');
         
        Route::patch('cancelAppointment','Salon\SalonController@cancelAppointment');
        Route::get('getDetailAppointment','Salon\SalonController@getDetailAppointment');

        Route::post('logout','Auth\LoginController@logout');

        Route::patch('updatePhoneNumber','Auth\UserController@updatePhoneNumber');

        Route::get('getAllHairStyle','Salon\SalonController@getAllHairStyle');

        Route::post('updateUser','Auth\UserController@updateUser')->middleware('admin');

        Route::get('getOrderByUser','Salon\OrderController@getOrderByUser');

        Route::get('getDetailOrder','Salon\OrderController@getDetailOrder');

        Route::patch('cancelOrder','Salon\OrderController@cancelOrder');  
        
        Route::get('getBookingHistoryByUser','Auth\UserController@getBookingHistoryByUser');

        Route::post('updateProfile','Auth\UserController@updateProfile');

        Route::post('updatePassword','Auth\UserController@updatePassword');

        
         
        
     });


});
