<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
      return view('welcome');
})->name('home-page');
Route::prefix('admin')->group(function () {
   Route::get('/', function () {
   	 	return view('admin.pages.home');
    })->name('home');

    Route::prefix('appointment')->group(function(){
      Route::get('/', function () {
        return view('admin.pages.appointment_management');
     })->name('appointments');

      Route::get('/{id}',function($id){
        return view('admin.pages.appoinment_detail',compact('id'));
      })->where(['id' => '[0-9]+'])->name('appointment-detail');

      Route::get('/add-appointment',function(){
        return view('admin.pages.add_appointment');
      })->name('add-appointment');


    });

    Route::get('profile',function (){
        return view('admin.pages.profile_page');
    })->name('admin-profile');


    Route::prefix('order')->group(function(){
        Route::get('/', function () {
            return view('admin.pages.order_management');
        })->name('order');
        Route::get('/create-order', function () {
            return view('admin.pages.create_order');
        })->name('create-order');

        Route::get('/{id}',function($id){
            return view('admin.pages.order_detail',compact('id'));
        })->where(['id' => '[0-9]+'])->name('order-detail');

    });
  


   Route::prefix('gallery')->group(function(){
    Route::get('/', function () {
      return view('admin.pages.hair_style_management');
    })->name('gallerys');  
    Route::get('add-hair',function(){
      return view('admin.pages.add_hair_style');
    })->name('add-hair-style');
   });

   Route::get('setting',function(){
      return view('admin.pages.config');
   })->name('config');

   Route::prefix('products')->group(function () {
      Route::get('/', function () {
            return view('admin.pages.product_management');
          })->name('products');
      Route::get('/import', function () {
            return view('admin.pages.import_product');
          })->name('import-product');
      Route::get('/add', function () {
            return view('admin.pages.add_product');
          })->name('add-product');

      Route::get('/{id}',function($id){
        return view('admin.pages.product_detail',compact('id'));
      })->name('product-detail');
   });
   
    Route::prefix('service')->group(function(){
       Route::get('/', function () {
            return view('admin.pages.service_management');
          })->name('service');
       Route::get('/add', function () {
            return view('admin.pages.add_service');
          })->name('add-service');
     });
    
    
     Route::prefix('sale')->group(function(){
          Route::get('/', function () {
            return view('admin.pages.sale_management');
          })->name('sales'); 
          Route::get('/add', function () {
            return view('admin.pages.add_sale');
          })->name('add-sale');
     });
    Route::prefix('staff')->group(function(){
        Route::get('/', function () {
          return view('admin.pages.employee_management');
      })->name('employees');   
       Route::get('/add-staff', function () {
            return view('admin.pages.add_staff');
          })->name('add-staff');
       Route::get('/staff-detail/{id}', function ($id) {
            return view('admin.pages.staff_detail',compact('id'));
          })->name('staff-detail');
     });

   
    
    Route::prefix('receipts')->group(function () {
        Route::get('/', function () {
        return view('admin.pages.payment_management');
      })->name('receipts');  

        Route::get('/id/{id}/type/{type}',function($id,$type){
           return view('admin.pages.receipt_detail',compact(['id','type']));
        })->name('receipt-detail');

         Route::get('/{id}',function($id){
            return view('admin.pages.receipt_view_detail',compact('id'));
         })->where(['id' => '[0-9]+'])->name('bill-detail');
        Route::get('/exportpdf/bill',['uses' =>'Salon\ExportPdfController@exportReceipt', 'as'=>'export-receipts']);
        Route::get('/exportpdf/bill-order',['uses' =>'Salon\ExportPdfController@exportReceiptTypeOrder', 'as'=>'export-receipts-order']);
    });
    

   




});
Route::prefix('waiter')->group(function () {
   Route::get('/', function () {
         return view('admin.pages.appointment');
    })->name('waiter');

});

Route::get('login', function () {
      return view('admin.login');
})->name('login');

Route::get('sign-up', function () {
      return view('admin.signupv2');
})->name('sign-up');
Route::get('error', function () {
      return view('admin.error.401page');
})->name('401page');


