import torchvision.transforms as tvt
from dataloader import image_transform as imgt
from dataloader import lfw as datasetlfw
from torch.utils.data import DataLoader
import torch
from network import pspnet as network
import matplotlib.pyplot as plt
from utils import utils
from evaluate import MultiThresholdMeasures

def train():
    
    learning_rate = 0.002
    momentum = 0.9
    batch_size = 16
    num_workers = 4
    image_size_data = 250
    epochs = 100
    data_dir = 'dataloader/Lfw'
    save_path ='cpkt/'

    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    dataloader = {}

    #Data argumentation
    train_joint_transforms = imgt.Compose([
        imgt.RandomCrop(image_size_data),
        imgt.RandomRotate(10),
        imgt.RandomHorizontallyFlip()
    ])

    train_image_transforms = tvt.Compose([
        tvt.ColorJitter(0.05, 0.05, 0.05, 0.05),
        tvt.ToTensor(),
        tvt.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])
        
    test_joint_transforms = imgt.Compose([
        imgt.Safe32Padding()
    ])

    test_image_transforms = tvt.Compose([
        tvt.ToTensor(),
        tvt.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    mask_transforms = tvt.Compose([
        tvt.ToTensor()
    ])

    model = network.PSPNet().to(device)
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, momentum=momentum)
    loss_fucntion = torch.nn.BCEWithLogitsLoss()
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, verbose=True)
    dataset_train = datasetlfw.LfwDataset('train',data_dir,train_joint_transforms,train_image_transforms,mask_transforms)
    dataset_validation = datasetlfw.LfwDataset('validation',data_dir,train_joint_transforms,train_image_transforms,mask_transforms)
    dataloader['train'] = DataLoader(dataset_train, batch_size=batch_size, shuffle=True, num_workers=num_workers)
    dataloader['validation'] = DataLoader(dataset_validation, batch_size=batch_size, shuffle=False, num_workers=num_workers)
    for epoch in range(epochs):
        evaluator = {}
        for phase in ['train', 'validation']:
            if phase == 'train':
                model.train(True)
                print('Trainning on epoch:{}'.format(epoch + 1))
            else:
                print('Testing on epoch:{}'.format(epoch + 1))
                prev_grad_state = torch.is_grad_enabled()
                torch.set_grad_enabled(False)
                model.train(False)
            
            running_loss = 0.0
        # running_loss = 0.0
            evaluator['train'] = MultiThresholdMeasures()
            evaluator['validation'] = MultiThresholdMeasures()
            # model.train(True)
            for i,data in enumerate(dataloader[phase],0):
                if i==100:
                    print('Training at iterator {}'.format(i))
                img,mask = [item.to(device) for item in data]
                model.zero_grad()
                pred_mask = model(img)
                loss = loss_fucntion(pred_mask,mask)
                if phase == 'train': 
                    loss.backward()
                    optimizer.step()
                running_loss += loss.item()
                # if epoch==5:
                #     print('final:{}'.format(loss))
                #     plt.title('DL prediction')
                #     plt.subplot(1, 4, 3)
                #     plt.imshow(pred_mask[0].detach().cpu().squeeze())
                #     plt.show()
                #     return
                with torch.no_grad():
                    evaluator[phase].update(pred_mask,mask)
            epoch_loss = running_loss / len(dataloader[phase])
            with torch.no_grad():
                acc = evaluator[phase].compute_accuracy()
                iou = evaluator[phase].compute_iou()
                f1_score = evaluator[phase].compute_f1()
            print("[Log - {0} - epoch {1}]:".format(phase, epoch + 1))
            print('Accuracy at epoch {}:{}'.format(epoch + 1,acc))
            print('IOU at epoch {}:{}'.format(epoch + 1,iou))
            print('F1_score at epoch {}:{}'.format(epoch + 1,f1_score))

            
            
            #lr_scheduler.step(running_loss)
            if (epoch+1)%10==0:
                if phase == "train":
                    torch.save(model.state_dict(), 'cpkt/pspnet_resnet152_sgd_lr_0.002_epoch_{}_loss_{}'.format(epoch,epoch_loss))
            with open("trainlog_2.txt", "a") as myfile:
                myfile.write('Phase {} - Epoch{} with loss :{}\n'.format(phase, epoch + 1,epoch_loss))
            if phase == 'validation':
                    # logger.info(f"Test Results - Epoch: {epoch} Avg-loss: {epoch_loss:.3f}")
                    
                    lr_scheduler.step(epoch_loss)
                
                    torch.set_grad_enabled(prev_grad_state)


   


if __name__ == '__main__':
    train()
