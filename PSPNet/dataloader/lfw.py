import os
import numpy as np
from PIL import Image
from torch.utils.data import Dataset
import torchvision.transforms as tvt
# import image_transform as imgt
# import matplotlib.pyplot as plt


class LfwDataset(Dataset):
    """docstring for LfwDataset"""
    def __init__(self, state='train',data_dir='Lfw',joint_transform=None, transform=None, target_transform=None):
        super(LfwDataset, self).__init__()
        list_file_name ='parts_{}.txt'.format(state)
        list_file_name_path = os.path.join(data_dir,list_file_name)
        file_name_list = self.get_name_list(list_file_name_path)
        img_dir = os.path.join(data_dir, 'lfw_funneled')
        mask_dir = os.path.join(data_dir, 'parts_lfw_funneled_gt_images')
        #read images file
        self.img_path = [os.path.join(img_dir, file_name[0], file_name[1]+'.jpg') for file_name in file_name_list]

        #read mask file
        self.mask_path= [os.path.join(mask_dir, file_name[1]+'.ppm') for file_name in file_name_list]

        #data argumentation 
        self.joint_transform = joint_transform
        self.transform =transform
        self.target_transform = target_transform

    def __getitem__(self,index):
        image_path = self.img_path[index]
        input_image = Image.open(image_path)
        mask_for_image = self.mask_path[index]
        mask = Image.open(mask_for_image)
        mask = self.convertMaskToHairMask(mask)
        if self.joint_transform is not None:
            input_image, mask = self.joint_transform(input_image, mask)

        if self.transform is not None:
            input_image = self.transform(input_image)

        if self.target_transform is not None:
            mask = self.target_transform(mask)

        return input_image,mask

    def __len__(self):
        return len(self.mask_path)

    def get_name_list(self,file_path):
        with open(file_path, 'r') as fin:
            lines = fin.readlines()
            parsed = list()
            for line in lines:
                name, num = line.strip().split(' ')
                num = format(num, '0>4')
                filename = '{}_{}'.format(name, num)
                parsed.append((name, filename))
        return parsed

    def convertMaskToHairMask(self,mask):
        #AND with Red color
        mask_map = np.array(mask)== np.array([255, 0, 0])
        #convert to float numpy array
        mask_map = np.all(mask_map, axis=2).astype(np.float32)
        return Image.fromarray(mask_map)


# if __name__ == '__main__':
#     train_joint_transforms = imgt.Compose([
#         imgt.RandomCrop(250),
#         imgt.RandomRotate(10),
#         imgt.RandomHorizontallyFlip()
#     ])

#     train_image_transforms = tvt.Compose([
#         tvt.ColorJitter(0.05, 0.05, 0.05, 0.05),
#         tvt.ToTensor(),
#         tvt.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
#     ])
        
#     test_joint_transforms = imgt.Compose([
#         imgt.Safe32Padding()
#     ])

#     test_image_transforms = tvt.Compose([
#         tvt.ToTensor(),
#         tvt.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
#     ])

#     # transforms only on mask
#     mask_transforms = tvt.Compose([
#         tvt.ToTensor()
#     ])
#     dataset = LfwDataset('train',train_joint_transforms,train_image_transforms,mask_transforms)
#     print(len(dataset))
#     img,mask = dataset[1]
#     print(np.where( mask[0] ==1 ))
#     print(img.shape)
#     print(mask.shape)
#     # img = img.view(img.shape[1], img.shape[2], img.shape[0])
#     # plt.imshow(mask,cmap='gray')
   
#     plt.title('DL prediction')
#     plt.subplot(1, 4, 3)
#     plt.imshow(mask.detach().cpu().squeeze())
#     # plt.show()
#     plt.title('DL prediction')
#     # plt.subplot(1, 4, 3)
#     # plt.imshow(img.detach().cpu().squeeze())

#     plt.subplot(1, 4, 1)
#     #print(outputs.shape) # torch.Size([1, 2, 240, 320])
#     plt.imshow(img[0,:,:].detach().cpu())
#     plt.show()
