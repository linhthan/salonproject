import torch
from torch.nn.init import xavier_normal_


def initWeight(layer):
    if isinstance(layer, torch.nn.Conv2d):
        xavier_normal_(layer.weight.data)

    elif isinstance(layer, torch.nn.BatchNorm2d):
        layer.weight.data.normal_(1.0, 0.02)
        layer.bias.data.fill_(0)

	