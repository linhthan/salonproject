import torch
from torch import nn
import torch.nn.functional as f
from torchvision.models import resnet152
from torch.nn.init import xavier_normal_
from utils import utils


class FeatureExtractor(nn.Module):
    def __init__(self):
        super(FeatureExtractor,self).__init__()
        model = resnet152(pretrained=True)
        self.features = nn.Sequential(*list(model.children())[:7])

    def forward(self,x):
        return self.features(x)

class Conv2dBNReLU(nn.Sequential):
    """docstring for Conv2dBNReLU"""
    def __init__(self, in_channels, out_channels, kernel_size=3, padding=1, bias=False):
        super(Conv2dBNReLU, self).__init__(
            nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, padding=padding, bias=bias),
            nn.BatchNorm2d(out_channels),
            nn.ReLU()
            )
class UpsampleLayer(nn.Module):
    def __init__(self, in_channels, out_channels, upsample_size=None):
        super().__init__()
        self.upsample_size = upsample_size

        self.conv = Conv2dBNReLU(in_channels, out_channels, kernel_size=3, padding=1, bias=False)

    def forward(self, x):
        size = 2 * x.size(2), 2 * x.size(3)
        upsampled = f.upsample(x, size=size, mode='bilinear')
        return self.conv(upsampled)

        
class PyramidModule(nn.Module):
    """docstring for PyramidModule"""
    # features: input chanel
    def __init__(self, features =1024, out_features=1024, output_sizes=(1, 2, 3, 6)):
        super(PyramidModule, self).__init__()
        pooling_layers = nn.ModuleList()
        for size in output_sizes:
            layers = [nn.AdaptiveAvgPool2d(size), nn.Conv2d(features, features//4, kernel_size=1)]
            pyramid_layer = nn.Sequential(*layers)
            pooling_layers.append(pyramid_layer)
        # self.pooling_layers = self.outputPooling(features,output_sizes)
        self.pooling_layers = pooling_layers
    def forward(self,x):
        height, width = x.size()[2:]
        #origin feature map 
        results=[x]
        for pooling_layer in self.pooling_layers:
            # upsample to concat into origin feature map
            pool_result = pooling_layer(x)
            upsampled = f.upsample(pool_result, size=(height, width), mode='bilinear')
            results.append(upsampled)
        return torch.cat(results, dim=1)



    def outputPooling(self, features_in, output_sizes): 
        middel_layers =nn.ModuleList()
        for output_size in output_sizes:
            middel_layers.append(self.subRegionFeature(features_in,output_size))
        return middel_layers

    def subRegionFeature(self, features_in,size):
        avgPooling = nn.AdaptiveAvgPool2d(output_size=(size,size))
        conv = nn.Conv2d(features_in,features_in,kernel_size=1,bias=False)
        return nn.Sequential(avgPooling,conv)

class PSPNet(torch.nn.Module):
    """docstring for PSPNet"""
    def __init__(self):
        super(PSPNet, self).__init__()
        self.backbondBase = FeatureExtractor()
        self.pyramidModule = PyramidModule(features=1024,out_features=1024)
        self.dropout_1 = nn.Dropout2d(p=0.3)

        self.upsampled_1 = UpsampleLayer(2048,256)
        self.upsampled_2 = UpsampleLayer(256,64)
        self.final_upsampled = UpsampleLayer(64,64)
        self.dropout_2 = nn.Dropout2d(p=0.15)
        self.preidict_mask_conv = nn.Sequential(
            nn.Conv2d(64,1,kernel_size=1)
        )
        self.initWeightLayer()

    def initWeightLayer(self):
        utils.initWeight(self.dropout_1)
        utils.initWeight(self.upsampled_1)
        utils.initWeight(self.upsampled_2)
        utils.initWeight(self.final_upsampled)
        utils.initWeight(self.dropout_2)
        utils.initWeight(self.preidict_mask_conv)

    def forward(self,x):
        height, width = x.size()[2:]
        # origin features has 1/8 size compare to input image
        origin_features = self.backbondBase(x)
        # feed through pyramid module
        pyramid_concat_features = self.pyramidModule(origin_features)
        # upsample to get origin size, decrease channels 
        pyramid_concat_features = self.dropout_1(pyramid_concat_features)
        pyramid_concat_features = self.upsampled_1(pyramid_concat_features)
        pyramid_concat_features = self.dropout_2(pyramid_concat_features)
        pyramid_concat_features = self.upsampled_2(pyramid_concat_features)
        pyramid_concat_features = self.dropout_2(pyramid_concat_features)
        pyramid_concat_features = self.final_upsampled(pyramid_concat_features)

        if (pyramid_concat_features.size(2) != height) or (pyramid_concat_features.size(3) != width):
            pyramid_concat_features = f.interpolate(pyramid_concat_features, size=(height, width), mode='bilinear')

        pyramid_concat_features = self.dropout_2(pyramid_concat_features)
        return self.preidict_mask_conv(pyramid_concat_features)
