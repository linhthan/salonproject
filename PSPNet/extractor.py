from network import pspnet
import torchvision.transforms as std_trnsf
from dataloader import image_transform

from time import process_time
import cv2
import io
import numpy as np
import torch
import time
import os
import sys
import uuid
from PIL import Image

class HairStyleExtractor(object):
    """docstring for HairStyleExtractor"""
    def __init__(self, ckpt_dir):
        super(HairStyleExtractor, self).__init__()
        self.device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
        self.network = pspnet.PSPNet().to(self.device)
        state = torch.load(ckpt_dir, map_location=torch.device('cpu'))
        self.network.load_state_dict(state)
        self.test_image_transforms = std_trnsf.Compose([
            std_trnsf.ToPILImage(),
            image_transform.Safe32Padding(),
            std_trnsf.ToTensor(),
            std_trnsf.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
        self.network.eval()

    def getMask(self,img):
        data = self.test_image_transforms(img)

        #data = test_joint_transforms(data, mask=None)
        data = torch.unsqueeze(data, dim=0)
                
        data = data.to(self.device)
        logit = self.network(data)
        pred = torch.sigmoid(logit.cpu())[0][0].data.numpy()
        mh, mw = data.size(2), data.size(3)
        mask = pred >= 0.5
        image_n = np.array(img)
        image_n = cv2.cvtColor(image_n, cv2.COLOR_RGB2BGR)
        return self.processMask(data,mask,image_n)
        
            
        
    def processMask(self,data,mask,image_n):
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        mh, mw = data.size(2), data.size(3)
        mask_n = np.zeros((mh, mw, 3))
        mask_n[:,:,:] = 1
          
        mask_n[:,:,0] *= mask
        mask_n[:,:,1] *= mask
        mask_n[:,:,2] *= mask
        ih, iw, _ = image_n.shape

        delta_h = mh - ih
        delta_w = mw - iw

        top = delta_h // 2
        bottom = mh - (delta_h - top)
        left = delta_w // 2
        right = mw - (delta_w - left)
                
        mask_n = mask_n[top:bottom, left:right, :]
        mask_n = mask_n.astype('uint8')
        mask_final = np.zeros_like(mask_n[:,:,0]).astype('uint8')

        res = cv2.bitwise_and(image_n,image_n,mask = mask_n[:,:,0])
        
        res2 = cv2.cvtColor(res,cv2.COLOR_BGR2GRAY)

        ret,thresh1 = cv2.threshold(res2,0,255,cv2.THRESH_BINARY)
        dilation = cv2.dilate(thresh1,kernel,iterations = 1)

        closing = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, kernel)
        contours, _ = cv2.findContours(closing, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        areas = [cv2.contourArea(c) for c in contours]

        max_index = np.argmax(areas)
        cnt=contours[max_index]
        x,y,w,h = cv2.boundingRect(cnt)
        mask_final[y:y+h,x:x+w] = mask_n[y:y+h,x:x+w ,0]
        res = cv2.bitwise_and(image_n,image_n,mask = mask_final)
        res = self.getTransparentImage(res)
        return res[y:y+h,x:x+w, :]

    def getTransparentImage(self,image):
        tmp = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        _,alpha = cv2.threshold(tmp,0,255,cv2.THRESH_BINARY)
        b, g, r = cv2.split(image)
        rgba = [b,g,r, alpha]
        dst = cv2.merge(rgba,4)
        return dst
        