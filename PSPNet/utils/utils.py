import torch
from torch.nn.init import xavier_normal_
import numpy as np

def initWeight(layer):
    if isinstance(layer, torch.nn.Conv2d):
        xavier_normal_(layer.weight.data)

    elif isinstance(layer, torch.nn.BatchNorm2d):
        layer.weight.data.normal_(1.0, 0.02)
        layer.bias.data.fill_(0)
def pixel_accuracy(logit, y):
    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    n = y.size(0)

    y_pred = torch.sigmoid(logit)
    y_pred = y_pred.view(n, -1, 1) >= 0.5
    y = y.byte().view(n, -1, 1)

    tp = y_pred * y == 1
    tn = y_pred + y == 0
    fp = y * ~y_pred == 1
    fn = y_pred * ~y == 1
    print(tp.shape)
    print(len(tp[tp==1]))
    print(y_pred.shape)
    print(y.shape)
    pixelTP = len(tp[tp==1])
    pixelTN = len(tn[tn==1])
    pixelFP = len(fp[fp==1])
    pixelFN = len(fn[fn==1])
    acc = (pixelTP + pixelTN )/ (pixelTP+pixelTN + pixelFP +pixelFN )
    print(acc)

	
