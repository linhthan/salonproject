from flask import Flask,jsonify,request,url_for
import cv2
import io
import json
import numpy as np
import torch
import os
import sys
import uuid
import boto3
from botocore.exceptions import ClientError
from config import S3_BUCKET, S3_KEY, S3_SECRET,S3_REGION
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from flask import current_app
from network import pspnet
import torchvision.transforms as std_trnsf
from dataloader import image_transform
from flask_cors import CORS
import extractor as ext_module

app = Flask(__name__)
CORS(app)

# 
# pspnet_resnet152_sgd_lr_0.002_epoch_100.pth
ckpt_dir = './cpkt/pspnet_resnet152_sgd_lr_0.002_epoch_99_loss_0.05091719931744514'
network = 'pspnet_resnet152'.lower()
save_dir = './static/images'
useQ = False
# device = 'cuda:0' if useGPU else 'cpu'
device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
print(device)
assert os.path.exists(ckpt_dir)

os.makedirs(save_dir, exist_ok=True)

extractor  = ext_module.HairStyleExtractor(ckpt_dir)


def str2bool(s):
    return s.lower() in ('t', 'true', 1)

def has_img_ext(fname):
    ext = os.path.splitext(fname)[1]
    return ext in ('.jpg', '.jpeg', '.png','.PNG','.JPEG','.JPG')

def get_segment_crop(img,tol=0, mask=None):
    if mask is None:
        mask = img > tol
    return img[np.ix_(mask.any(0), mask.any(1))]
def getTransparentImage(image):
    tmp = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _,alpha = cv2.threshold(tmp,0,255,cv2.THRESH_BINARY)
    b, g, r = cv2.split(image)
    rgba = [b,g,r, alpha]
    dst = cv2.merge(rgba,4)
    return dst


@app.route('/')
def hello():
    return 'Hello World!'
@app.route('/predict', methods=['POST'])
def predict():
     if request.method == 'POST':
        with torch.no_grad():
            mask_file_name = uuid.uuid4().hex
            mask_path = os.path.join(save_dir, os.path.basename(mask_file_name)+'mask_.png')
            resize_path = os.path.join(save_dir, os.path.basename(mask_file_name)+'resize_.png')
            if 'file' not in request.files:
                return jsonify({'message':"post error"})
            file = request.files['file']
            img_bytes = file.read()
            #  # return jsonify({'class_id': class_id, 'class_name': crop_img})
            nparr = np.fromstring(img_bytes, np.uint8)
            img = cv2.imdecode(nparr, cv2.IMREAD_UNCHANGED)
            # img = cv2.resize(img,(256,256))

            

            shape = img.shape
            if img.shape[2] > 3:
                B, G, R, A = cv2.split(img)
                alpha = A / 255

                R = (255 * (1 - alpha) + R * alpha).astype(np.uint8)
                G = (255 * (1 - alpha) + G * alpha).astype(np.uint8)
                B = (255 * (1 - alpha) + B * alpha).astype(np.uint8)

                img = cv2.merge((R, G, B))
            else:
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            mask  = extractor.getMask(img)
            cv2.imwrite(mask_path, mask)
            imagePath =  os.path.basename(mask_path)
            maskFolder = "overlay"
            result_url = "{}{}/{}/{}".format(request.url_root,'static','images',imagePath)
            # f = file(result_url,"rb")
            
            try:
                res_aws = boto3.client(
                's3',
                aws_access_key_id=S3_KEY,
                aws_secret_access_key=S3_SECRET,region_name=S3_REGION
                )
                with current_app.open_resource('static/images/{}'.format(imagePath)) as f:
                    filePathUpload =  'hairstyles/{}.jpeg'.format(mask_file_name)
                    response = res_aws.upload_file(os.path.abspath(f.name), S3_BUCKET, filePathUpload, ExtraArgs={'ACL': 'public-read'})
                    url = 'https://bksalon.s3-ap-southeast-1.amazonaws.com/hairstyles/{}.jpeg'.format(mask_file_name)
                    return jsonify({'success ':True, 'url':url})
            except ClientError as e:
                return jsonify({'success ':False})

# if __name__ == '__main__':
#     app.run(host='0.0.0.0',port=5000)
#run production mode flask run --host=0.0.0.0