package linh.dev.base.ext.view

import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView


fun <T : RecyclerView.ViewHolder> T.onClick(event: (view: View, position: Int, type: Int) -> Unit): T {
    itemView.setOnClickListener {
        event.invoke(it, adapterPosition, itemViewType)
    }
    return this
}

fun <T : RecyclerView.ViewHolder> T.onLongClick(event: (view: View, position: Int, type: Int) -> Boolean): T {
    itemView.setOnLongClickListener {
        event.invoke(it, adapterPosition, itemViewType)
    }
    return this
}
fun <T : RecyclerView.ViewHolder> T.onTouch(event: (view: View, motionEvent: MotionEvent, position: Int, type: Int) -> Boolean): T {
    itemView.setOnTouchListener { v, e ->
        event.invoke(v, e, adapterPosition, itemViewType)
    }
    return this
}