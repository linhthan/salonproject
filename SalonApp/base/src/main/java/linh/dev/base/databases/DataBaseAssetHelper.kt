package linh.dev.base.databases

import android.content.Context
import java.io.FileOutputStream
import java.io.IOException


class DataBaseAssetHelper(val context: Context, val databaseName: String) {
    fun copyDatabase() {
        val dbPath = context.getDatabasePath(databaseName)
        if (dbPath.exists()) return

        dbPath.parentFile?.mkdirs()
        try {
            val inputStream = context.assets.open("databases/" + databaseName)
            val output = FileOutputStream(dbPath)
            inputStream.copyTo(output)
            output.flush()
            output.close()
            inputStream.close()

        } catch (io: IOException) {
            io.printStackTrace()
        }
    }
}