package linh.dev.base

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class StringUtil {
    companion object {
        fun <T> convertJsonToObject(source: String?): T {
            return Gson().fromJson<T>(source, object : TypeToken<T>() {}.type)
        }

        fun <T>convertObjectToJson(source: Any): String {
            val fooType = object : TypeToken<T>() {

            }.type
            return Gson().toJson(source, fooType)
        }
    }
}