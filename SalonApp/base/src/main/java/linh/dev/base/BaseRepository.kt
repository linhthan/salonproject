package linh.dev.base

import android.util.Log
import retrofit2.Response
import java.io.IOException
import linh.dev.base.ext.retrofit.Result

open class BaseRepository {

    suspend fun <T : Any> handleApiCall(call: suspend () -> Response<T>, message: String): T? {
        val result: Result<T> = handleApiResult(call, message)
        var data: T? = null

        when (result) {
            is Result.Success ->
                data = result.data
            is Result.Error -> {
                Log.d("1.DataRepository", "$message & Exception - ${result.exception}")
            }
        }


        return data
    }

    private suspend fun <T : Any> handleApiResult(
        call: suspend () -> Response<T>,
        errorMessage: String
    ): Result<T> {
        val response = call.invoke()
        if (response.isSuccessful) return Result.Success(response.body()!!)

        return Result.Error(IOException("Error Occurred during getting safe Api result, Custom ERROR - $errorMessage"))
    }

}