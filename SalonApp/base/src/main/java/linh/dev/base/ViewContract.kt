package com.example.core

import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

interface ViewContract<DB> {
    fun getLayoutResourceId(): Int
    fun onBindData(binding: DB)
}