package linh.dev.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.example.core.ViewContract

abstract class BaseFragment<DB : ViewDataBinding> : Fragment(), ViewContract<DB>, BaseConstant {

    protected lateinit var mBinding: DB
    private var isRegisted = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (getLayoutResourceId() != 0) {
            mBinding = DataBindingUtil.inflate(inflater, getLayoutResourceId(), container, false)
            mBinding.lifecycleOwner = viewLifecycleOwner
            return mBinding.root
        } else {
            throw IllegalArgumentException("layout resource cannot be null")
        }
    }

    override fun onResume() {
        super.onResume()
        if (!isRegisted) {
            bus.register(this)
            isRegisted = true
        }


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    open fun initView() {

    }

    override fun onDetach() {
        super.onDetach()
        if (isRegisted) {
            bus.unregister(this)
            isRegisted = false
        }
    }


    override open fun onBindData(binding: DB) {

    }

}