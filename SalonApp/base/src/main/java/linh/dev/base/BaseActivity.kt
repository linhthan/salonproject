package linh.dev.base

import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.core.ViewContract
import io.reactivex.subjects.PublishSubject
import linh.dev.base.permission.Permission
import java.lang.IllegalArgumentException

abstract class BaseActivity<DB : ViewDataBinding> : AppCompatActivity(), ViewContract<DB>, BaseConstant {
    private lateinit var mBinding: DB
    val PERMISSIONS_REQUEST_CODE = 42
    private var mSubjects: MutableMap<String, PublishSubject<Permission>> = HashMap()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getLayoutResourceId() != 0) {
            mBinding = DataBindingUtil.setContentView(this, getLayoutResourceId())
            mBinding.lifecycleOwner = this
            onBindData(mBinding)
        } else {
            throw IllegalArgumentException("layout resource id cannot be null")
        }
        initView()
        bus.register(this)
    }

    open fun initView() {

    }

    override fun getLayoutResourceId(): Int {
        return 0
    }

    override fun onBindData(binding: DB) {

    }

    override fun onDestroy() {
        super.onDestroy()
        bus.unregister(this)
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissions(permissions: Array<String>) {
        requestPermissions(permissions, PERMISSIONS_REQUEST_CODE)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode != PERMISSIONS_REQUEST_CODE) return

        val shouldShowRequestPermissionRationale = BooleanArray(permissions.size)

        for (i in 0 until permissions.size) {
            shouldShowRequestPermissionRationale[i] = shouldShowRequestPermissionRationale(permissions[i])
        }

        onRequestPermissionsResult(permissions, grantResults, shouldShowRequestPermissionRationale)
    }

    fun onRequestPermissionsResult(
        permissions: Array<String>,
        grantResults: IntArray,
        shouldShowRequestPermissionRationale: BooleanArray
    ) {
        var i = 0
        val size = permissions.size
        while (i < size) {
            // Find the corresponding subject
            val subject = mSubjects[permissions[i]]
            if (subject == null) {
                // No subject found
                Log.e(
                    "BaseFragment",
                    "RxPermissions.onRequestPermissionsResult invoked but didn't find the corresponding permission request."
                )
                return
            }
            mSubjects.remove(permissions[i])
            val granted = (grantResults[i] == PackageManager.PERMISSION_GRANTED)
            Log.e(
                "RequestResult",
                granted.toString()
            )
            subject.onNext(Permission(permissions[i], granted, shouldShowRequestPermissionRationale[i]))
            subject.onComplete()
            i++
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun isGranted(permission: String): Boolean {

        return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun isRevoked(permission: String): Boolean {
        return packageManager.isPermissionRevokedByPolicy(permission, packageName)
    }

    fun getSubjectByPermission(permission: String): PublishSubject<Permission>? {
        return mSubjects[permission]
    }

    fun containsByPermission(permission: String): Boolean {
        return mSubjects.containsKey(permission)
    }

    fun setSubjectForPermission(permission: String, subject: PublishSubject<Permission>) {
        mSubjects[permission] = subject
    }

}