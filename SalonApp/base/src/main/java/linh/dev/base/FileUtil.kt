package linh.dev.base

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import java.io.File


class FileUtil {
    companion object {
        fun createImageFile(
            context: Context,
            dirName: String,
            imageName: String,
            folderName: String
        ): File? {
            val photoDirectory = File(context.getExternalFilesDir(dirName), folderName)
            if (!photoDirectory.exists() && !photoDirectory.mkdirs()) {
                return null
            }
            return File(photoDirectory, imageName)
        }

        fun getUriForFile(context: Context, authority: String, file: File): Uri {
            return FileProvider.getUriForFile(
                context,
                authority,
                file
            )
        }
    }
}