package linh.dev.base

import android.util.Log
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecycleViewAdapter<T> :
    RecyclerView.Adapter<BaseRecycleViewAdapter<T>.BaseViewHolder>() {
    private var mItemClickListener: ItemClickListener? = null
    private var mLoadMoreListener: LoadMoreListener? = null
    private var mViewClickListener: ViewClickListener? = null
    protected val dataSources = ArrayList<T>()

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bindView(position)
        if (position == itemCount - 1 && mLoadMoreListener != null) {
            mLoadMoreListener?.onLoadMore()
        }
    }

    fun clearData() {
        dataSources.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return dataSources.size
    }

    open fun getItem(position: Int): T {
        return dataSources[position]
    }

    open fun setItems(items: Collection<T>) {
        if (items.isNotEmpty()) {
            dataSources.clear()
            dataSources.addAll(items)
            notifyDataSetChanged()
        }
    }

    open fun addItem(item: T) {
        item?.let {
            dataSources.add(item)
            notifyItemInserted(dataSources.size - 1)
        }
    }

    fun addItems(items: Collection<T>) {
        if (items.isNotEmpty()) {
            dataSources.addAll(items)
            notifyItemInserted(dataSources.size - 1)
        }
    }

    fun setOnItemClickListener(itemClickListener: ItemClickListener) {
        mItemClickListener = itemClickListener
    }

    fun setOnLoadMoreListener(loadMoreListener: LoadMoreListener) {
        mLoadMoreListener = loadMoreListener
    }

    fun setOnViewClickListener(viewClickListener: ViewClickListener) {
        Log.e("Start", "Start set listener ${viewClickListener.hashCode()}")
        mViewClickListener = viewClickListener
    }


    abstract inner class BaseViewHolder(bind: ViewDataBinding) :
        RecyclerView.ViewHolder(bind.root) {
        var index: Int = 0

        init {
            itemView.setOnClickListener {
                onViewHolderClick(it)
                mItemClickListener?.onItemClick(itemView, index)

            }
        }


        protected fun onViewHolderClick(v: View) {

        }

        fun bindView(position: Int) {
            index = position
            onBindingData(position)
        }

        protected abstract fun onBindingData(position: Int)
        protected fun onViewClick(resId: Int) {
            mViewClickListener?.let {
                Log.e("onViewClick:", " ${Thread.currentThread()}")
                it.onViewItemClock(resId, index)
            }

        }
    }

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

    interface LoadMoreListener {
        fun onLoadMore()
    }

    interface ViewClickListener {
        fun onViewItemClock(resId: Int, position: Int)
    }
}
