package linh.dev.base.ext.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import linh.dev.base.BaseActivity
import linh.dev.base.BaseFragment

fun ImageView.loadImage(context: Context, url: String, placeHolder: Int): RequestBuilder<Drawable> {
    return Glide.with(context).load(url).placeholder(placeHolder)
}

fun BaseActivity<*>.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}


fun Fragment.showToast(msg: String) {
    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
}


fun BaseActivity<*>.showMsg(msg: String?) {
    msg?.let {
        Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
    }

}


fun Fragment.showMsg(msg: String?) {
    msg?.let {
        Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
    }
}
fun Fragment.showLongMsg(msg: String?) {
    msg?.let {
        Toast.makeText(context, it, Toast.LENGTH_LONG).show()
    }
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hideKeepHeight() {
    this.visibility = View.INVISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}



inline val EditText.currentText: String
    get() = text.toString()

inline val TextView.currentText: String
    get() = text.toString()
