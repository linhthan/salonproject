package linh.dev.salonapp

enum class State {
    DONE, LOADING, ERROR
}