package linh.dev.salonapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.HairSalonRepository

class GalleryViewModel(val repository: HairSalonRepository) : BaseViewModel() {
    val salonWithHairStyleResult = repository.salonWithHairStyleResult
    val salonIdList: LiveData<List<Int>> by lazy {
        repository.getAllSalonId()
    }

    fun getSalonWithHairStyleResult(listSalonId:List<Int>) {
        user()?.let {
            viewModelScope.launch {
                repository.getSalonWithGallery(it.getBearToken(),listSalonId)
            }
        }
    }
}