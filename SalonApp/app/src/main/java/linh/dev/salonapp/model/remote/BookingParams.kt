package linh.dev.salonapp.model.remote

import com.google.gson.annotations.SerializedName
import linh.dev.salonapp.model.local.Service

class BookingParams(
    @SerializedName("salon_id") val salonId: Int,
    @SerializedName("time_booking") val timeBooking: Int,
    @SerializedName("date_booking") val dateBooking: String,
    @SerializedName("duration") val duration: Int,
    @SerializedName("phone_number") val phoneNumber: String,
    @SerializedName("cus_name") val customerName: String,
    @SerializedName("customer_id") val customer_id: Int?,
    val services: List<Service.ServiceJson>
) {
}