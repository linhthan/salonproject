package linh.dev.salonapp.ui.fragment.collector

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.core.permission.RxRequestPermission
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.dialog_take_photo.*
import kotlinx.android.synthetic.main.fragment_create_hair_style.*
import linh.dev.base.BaseActivity
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.currentText
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showToast
import linh.dev.base.permission.Permission
import linh.dev.salonapp.*
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.CreateHairStyleViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.io.IOException

class CreateHairStyleFragment :
    BaseFragment<linh.dev.salonapp.databinding.FragmentCreateHairStyleBinding>() {

    private val viewModel: CreateHairStyleViewModel by viewModel()
    var imagePath: String = ""

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_create_hair_style
    }

    override fun initView() {
        super.initView()
        initAction()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initAction() {
        imvAddImage.setOnClickListener {
            requestPermission()
        }
        imvCrop.setOnClickListener {
            requestPermission()
        }

        btnExtract.setOnClickListener {
            val filePath = viewModel.imagePathCrop.value
            if (!filePath.isNullOrEmpty()) {
                val imageFile = File(filePath)
                if (imageFile.exists()) {
                    viewModel.createHairStyleImage(imageFile)
                } else {
                    showToast(getString(R.string.image_file_does_not_exist))
                }

            } else {
                showToast(getString(R.string.msg_image_file_empty))
            }

        }
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }
        btnSave.setOnClickListener {
            val title = edtTitleHairStyle.currentText
            if (title.isNotEmpty()) {
                val resultUrl = viewModel.hairStyleResult.value
                if (resultUrl?.status == Result.Status.SUCCESS) {
                    val url = resultUrl.data?.url
                    if (url != null) {
                        viewModel.saveHairStyle(title, url)
                    } else {
                        showToast("Có lỗi xảy ra, vui lòng thử lại sau")
                    }
                } else {
                    showToast("Bạn chưa có kiểu tóc nào!")
                }
            } else {
                showToast("Bạn chưa nhập tên mẫu tóc")
            }

        }
    }

    private fun initObservable() {
        viewModel.hairStyleResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    btnExtract.showProgress {
                        buttonText = "Đang lấy mẫu tóc"
                        progressColor = Color.WHITE
                    }
                }
                Result.Status.SUCCESS -> {
                    btnExtract.hideProgress(getString(R.string.lbl_extract_hair_style))
                    val url = it.data?.url
                    if (url != null) {
                        displayHairStyle(url)
                    }

                }
                Result.Status.ERROR -> {
                    btnExtract.hideProgress(getString(R.string.lbl_extract_hair_style))
                    layoutHairInfo.hide()
                    it.message?.let { msg ->
                        showToast(msg)
                    }
                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }

            }
        })

        viewModel.saveHairStyleResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    btnSave.showProgress {
                        buttonText = "Đang lưu"
                        progressColor = Color.WHITE
                    }
                }
                Result.Status.SUCCESS -> {
                    btnSave.hideProgress(R.string.save_lbl_common)


                    if (it.data != null) {
                        if (it.data.success) {
                            logE("Success")
                            imvCrop.setImageResource(0)
                            layoutHairStyle.hide()
                            layoutHairInfo.hide()
                            layoutBtn.hide()
                            imvAddImage.show()
                            viewModel.imagePathCrop.value = EMPTY
                            viewModel.imagePathSelected.value = EMPTY
                            val hairStyle = it.data.data
                            bus.post(
                                AppAction(
                                    AppEvent.EVENT_SAVE_HAIR_STYLE_SUCCESS.setData(
                                        hairStyle
                                    )
                                )
                            )

                        }
                        showToast(it.data.message)

                    }

                }
                Result.Status.ERROR -> {
                    btnSave.hideProgress(R.string.save_lbl_common)
                    it.message?.let { msg ->
                        showToast(msg)
                    }
                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }

            }
        })
        viewModel.imagePathCrop.observe(viewLifecycleOwner, Observer { imgPath ->
            //            Glide.with(requireContext()).load(it).into(imvCrop)
            if (imgPath.isNullOrEmpty()) return@Observer
            context?.let {
                Glide.with(it).load(imgPath)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {

                            imvAddImage.show()
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {


                            imvAddImage.hide()
                            return false
                        }

                    })
                    .into(imvCrop)
            }
        })
    }

    private fun displayHairStyle(url: String) {
        context?.let {
            Glide.with(it).load(url)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {

                        layoutBtn.hide()
                        layoutHairInfo.hide()
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {


                        layoutBtn.show()
                        layoutHairInfo.show()
                        return false
                    }

                })
                .into(imvHairStyle)
        }

    }

    private fun requestPermission() {
        viewModel.compositeDisposable.add(
            RxRequestPermission(activity as BaseActivity<*>).request(
                arrayListOf(
                    Permission(Manifest.permission.CAMERA, false),
                    Permission(Manifest.permission.READ_EXTERNAL_STORAGE, false),
                    Permission(Manifest.permission.WRITE_EXTERNAL_STORAGE, false)
                )
            ).subscribe { granted ->
                if (granted) {
                    showDialogAddPhoto()

                } else {
                    showToast(getString(R.string.msg_granted_all_permission))
                }

            }
        )
    }

    private fun showDialogAddPhoto() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_take_photo)
        dialog.layoutTakePhoto.setOnClickListener {
            dispatchTakePictureIntent()
//            Navigation.findNavController(requireActivity(), R.id.nav_main)
//                .navigate(
//                    R.id.action_createHairStyleFragment_to_cameraFragment,
//                    null
//                )

            dialog.dismiss()
        }
        dialog.layoutPickPhoto.setOnClickListener {
            dispatchPickPictureIntent()
            dialog.dismiss()
        }
        dialog.setCanceledOnTouchOutside(true)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.CENTER)
        dialog.show()

    }

    private fun createImageFile(

    ): File? {
        val timeStamp: String = System.currentTimeMillis().toString()
        val storageDir: File? = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            viewModel.imagePathSelected.value = absolutePath
            imagePath = absolutePath
            PrefUtil.savePreference(
                SalonApplication.applicationContext(),
                PREF_FILE_PATH,
                absolutePath
            )
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val mPhotoUri = FileProvider.getUriForFile(
                        requireContext(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri)
                    startActivityForResult(takePictureIntent, CAMERA_PIC_REQUEST)
                }
            }
        }
    }

    private fun dispatchPickPictureIntent() {
        Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        ).also { takePictureIntent ->
            takePictureIntent.type = "image/*"

            startActivityForResult(takePictureIntent, GALLERY_REQUEST_CODE)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_PIC_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                if (imagePath.isEmpty()) {

                    val pathSave = PrefUtil.getSharePreference(
                        SalonApplication.applicationContext(),
                        PREF_FILE_PATH,
                        EMPTY
                    )
                    pathSave?.let {
                        imagePath = it
                    }
                }
                if (imagePath.isNotEmpty()) {
                    try {
                        val photo = File(imagePath)
                        if (photo.exists()) {
                            val bundle = Bundle()
                            bundle.putString(BUNDLE_FILE_PATH, imagePath)
                            Navigation.findNavController(requireActivity(), R.id.nav_main)
                                .navigate(
                                    R.id.action_createHairStyleFragment_to_cropFragment2,
                                    bundle
                                )
                        } else {
                            showToast(getString(R.string.image_file_does_not_exist))
                        }
                    } catch (ex: Exception) {
                        showToast(getString(R.string.image_file_does_not_exist))
                    }
                } else {
                    showToast(getString(R.string.save_image_error_msg))
                }
            }
        } else if (requestCode == GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            uri?.let {
                val proj = arrayOf(MediaStore.Images.Media.DATA)
                val cursor = requireContext().contentResolver.query(uri, proj, null, null, null);
                val columnIndex = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                cursor?.moveToFirst()
                columnIndex?.let {
                    val path = cursor.getString(it)
                    cursor.close()
                    try {
                        val photo = File(path)
                        if (photo.exists()) {
                            val bundle = Bundle()
                            bundle.putString(BUNDLE_FILE_PATH, path)
                            Navigation.findNavController(requireActivity(), R.id.nav_main)
                                .navigate(
                                    R.id.action_createHairStyleFragment_to_cropFragment2,
                                    bundle
                                )
                        } else {
                            showToast(getString(R.string.image_file_does_not_exist))
                        }

                    } catch (ex: Exception) {
                        showToast(getString(R.string.image_file_does_not_exist))
                    }

                }

            }

        }
    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_CROP_SUCCESS) {
            val filePath = appAction.event.getData(String::class.java)
            viewModel.imagePathCrop.value = filePath

        }
    }
}