package linh.dev.salonapp.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.layout_no_data.*
import kotlinx.android.synthetic.main.search_salon_fragment.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.SalonSearchAdapter
import linh.dev.salonapp.databinding.SearchSalonFragmentBinding
import linh.dev.salonapp.ext.isValidPhoneNumber
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.SearchSalonViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class SearchSalonFragment : BaseFragment<SearchSalonFragmentBinding>() {

    private val viewModel: SearchSalonViewModel by viewModel()
    val adapter: SalonSearchAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.search_salon_fragment
    }

    override fun initView() {
        super.initView()
        initRecycleView()
    }

    private fun initRecycleView() {
        rcvSalon.layoutManager = object : GridLayoutManager(context, 1) {
            override fun supportsPredictiveItemAnimations(): Boolean {
                return false
            }
        }
        rcvSalon.adapter = adapter
        (rcvSalon.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        rcvSalon.itemAnimator = null
        rcvSalon.addItemDecoration(SpaceItemDecoration(10, 10, 10, 10, 10))
        adapter.setItemClick(object : SalonSearchAdapter.OnItemClick {
            override fun onItemClick(position: Int) {
                if (checkPhoneNumber()) {
                    val itemSelect = adapter.getItemByPosition(position)
                    itemSelect?.let { item ->
                        val bundle = Bundle()
                        bundle.putInt(BUNDLE_ID_SALON, item.id)
                        bundle.putString(BUNDLE_NAME_SALON, item.name)
                        bundle.putInt(BUNDLE_OPEN_TIME_SALON, item.open_time)
                        bundle.putInt(BUNDLE_CLOSE_TIME_SALON, item.close_time)
                        bundle.putInt(BUNDLE_AVG_TIME_SALON, item.avg_time_slot)
                        bundle.putString(BUNDLE_THUMBNAIL_SALON, item.thumb_url)
                        bundle.putString(BUNDLE_DETAIL_LOCATION_SALON, item.detail_location)
                        item.total_rate_service?.let {
                            bundle.putFloat(BUNDLE_TOTAL_RATE_SALON, it.toFloat())
                        }
                        Navigation.findNavController(requireActivity(), R.id.nav_main)
                            .navigate(
                                R.id.action_searchSalonFragment_to_salonDetailFragment,
                                bundle
                            )
                    }

                } else {
                    showDialogUpdatePhone()
                }
            }

            override fun onViewClick(resId: Int, position: Int) {

                if (resId == R.id.buttonBook) {
                    if (checkPhoneNumber()) {
                        val itemSelect = adapter.getItemByPosition(position)
                        itemSelect?.let { item ->
                            val bundle = Bundle()
                            bundle.putInt(BUNDLE_ID_SALON, item.id)
                            bundle.putString(BUNDLE_NAME_SALON, item.name)
                            bundle.putInt(BUNDLE_OPEN_TIME_SALON, item.open_time)
                            bundle.putInt(BUNDLE_CLOSE_TIME_SALON, item.close_time)
                            bundle.putInt(BUNDLE_AVG_TIME_SALON, item.avg_time_slot)
                            bundle.putString(BUNDLE_THUMBNAIL_SALON, item.thumb_url)
                            bundle.putString(BUNDLE_DETAIL_LOCATION_SALON, item.detail_location)
                            item.total_rate_service?.let {
                                bundle.putFloat(BUNDLE_TOTAL_RATE_SALON, it.toFloat())
                            }
                            Navigation.findNavController(requireActivity(), R.id.nav_main)
                                .navigate(
                                    R.id.action_searchSalonFragment_to_salonDetailFragment,
                                    bundle
                                )
                        }

                    } else {
                        showDialogUpdatePhone()
                    }
                }
            }

        })

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                val keySearch = query ?: EMPTY
                if (viewModel.keyword.value != null && !viewModel.keyword.value.equals(keySearch)) {
                    viewModel.keyword.value = keySearch
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

        })

        btnTryAgain.setOnClickListener {
            viewModel.retryFailed()
        }
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }

    }

    private fun showDialogUpdatePhone() {
        context?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(getString(R.string.title_dialog_update_phone_number))
            builder.setMessage(getString(R.string.msg_dialog_update_phone_number))

            val input = EditText(it)
            input.hint = getString(R.string.phone_number_hint)
            input.inputType = InputType.TYPE_CLASS_PHONE
            builder.setView(input)

            builder.setPositiveButton(getString(R.string.lbl_update_common)) { p0, p1 ->
                val phone = input.text.toString()
                if (!phone.isValidPhoneNumber()) {
                    showToast(getString(R.string.phone_number_invalid_error_msg))
                } else {
                    viewModel.updatePhoneNumber(phone)
                    p0.dismiss()
                }
            }
            builder.setNegativeButton(
                getString(R.string.cancel_dialog_common)
            ) { p0, p1 -> p0.dismiss() }

            builder.show()
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun startObservable() {
        viewModel.salonList.observe(viewLifecycleOwner, Observer {
            logE(it.size)
            adapter.submitList(it)
        })
    }

    private fun checkPhoneNumber(): Boolean {
        val user = user()
        user?.let {
            return it.phoneNumber != null

        }
        return false
    }

    private fun initObservable() {
        viewModel.keyword.observe(viewLifecycleOwner, Observer {
            viewModel.replaceSubscription()
            startObservable()

        })

        viewModel.getState().observe(viewLifecycleOwner, Observer { state ->
            logE("ViewModel log")
            logE(state)
            if (viewModel.listIsEmpty() && state == State.LOADING) {
                progressLoadingData.show()
            } else {
                progressLoadingData.hide()
            }
            if (viewModel.listIsEmpty() && state == State.ERROR) {
                progressLoadingData.hide()
                viewNoData.show()
            } else {
                viewNoData.hide()
            }
            logE(viewModel.listIsEmpty())
            logE(adapter.itemCount)
            if (viewModel.listIsEmpty() && adapter.itemCount == 1) {
                viewNoData.show()
            } else {
                viewNoData.hide()
            }
            adapter.setState(state ?: State.DONE)
        })

        viewModel.updateResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoadingData.show()
                }
                Result.Status.SUCCESS -> {
                    progressLoadingData.hide()
                    it.data?.let { msg ->
                        showToast(msg)
                    }

                }
                Result.Status.ERROR -> {
                    progressLoadingData.hide()
                    it.message?.let { msg ->
                        showToast(msg)
                    }
                }
                Result.Status.UNAUTHORIZED -> {
                    progressLoadingData.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
    }


}