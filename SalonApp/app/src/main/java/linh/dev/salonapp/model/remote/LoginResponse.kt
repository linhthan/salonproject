package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.User

data class LoginResponse(val message: String, val data: UserResponse?)

data class UserResponse(val user: User, val token: String,val salon_id: Int?)