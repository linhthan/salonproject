package linh.dev.salonapp.ui.activitys

import android.content.Intent
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_customer_home.*
import linh.dev.base.BaseActivity
import linh.dev.salonapp.*
import linh.dev.salonapp.databinding.ActivityCustomerHomeBinding
import linh.dev.salonapp.viewmodel.SalonViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class CustomerHomeActivity : BaseActivity<ActivityCustomerHomeBinding>() {

    private val salonViewModel: SalonViewModel by viewModel()

    override fun getLayoutResourceId(): Int {
        return R.layout.activity_customer_home
    }

    override fun initView() {
        super.initView()
        supportActionBar?.hide()
        val currentTab = intent.getIntExtra(BUNDLE_HOME_TAB,0)
        salonViewModel.currentTab = currentTab


    }
}