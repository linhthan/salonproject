package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databases.model.HairStyle
import linh.dev.salonapp.databinding.ViewItemHairStyleListBinding

class HairStyleItemAdapter : BaseRecycleViewAdapter<HairStyle>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
            val binding: ViewItemHairStyleListBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_hair_style_list,
                parent,
                false
            )
            return ViewHolder(binding)
    }


    inner class ViewHolder(val binding: ViewItemHairStyleListBinding) :
        BaseRecycleViewAdapter<HairStyle>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            val item = getItem(position)
            binding.viewModel = item
            binding.executePendingBindings()
        }
    }

}