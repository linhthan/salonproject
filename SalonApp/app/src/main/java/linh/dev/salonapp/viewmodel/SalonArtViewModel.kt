package linh.dev.salonapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.PER_PAGE_HAIR_STYLE
import linh.dev.salonapp.State
import linh.dev.salonapp.data.HairStyleDataSource
import linh.dev.salonapp.data.HairStyleDataSourceFactory
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.HairStyle
import linh.dev.salonapp.repository.SalonRepository

class SalonArtViewModel(val salonId: Int?, val salonRepository: SalonRepository) : BaseViewModel() {
    var hairStyleList: LiveData<PagedList<HairStyle>>
    private val appointmentDataSourceFactory: HairStyleDataSourceFactory =
        HairStyleDataSourceFactory(salonId, salonRepository.api, viewModelScope)

    val topArtOfSalonResult = salonRepository.topArtOfSalonResult



    init {
        val config = PagedList.Config.Builder()
            .setPageSize(PER_PAGE_HAIR_STYLE)
            .setInitialLoadSizeHint(PER_PAGE_HAIR_STYLE)
            .setEnablePlaceholders(false)
            .build()
        hairStyleList = LivePagedListBuilder(appointmentDataSourceFactory, config).build()

        getTopArtSalon()
    }

    fun getTopArtSalon() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    salonId?.let {salonId ->
                        salonRepository.getTopArtOfSalon(it.getBearToken(), salonId)
                    }

                }
            }
        }
    }

    fun getState(): LiveData<State> = Transformations.switchMap<HairStyleDataSource,
            State>(
        appointmentDataSourceFactory.hairStyleDataSourceLiveData,
        HairStyleDataSource::state
    )

    fun retryFailed() {
        appointmentDataSourceFactory.hairStyleDataSourceLiveData.value?.retryFailedQuery()
    }


    fun listIsEmpty(): Boolean {
        return hairStyleList.value?.isEmpty() ?: true
    }

    fun refreshData() {
        appointmentDataSourceFactory.hairStyleDataSourceLiveData.value?.refresh()
    }
}