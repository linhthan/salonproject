package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Stylist

data class StylistAvailableResponse(val message:String, val data:List<Stylist>)