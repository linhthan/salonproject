package linh.dev.salonapp.repository

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.withContext
import linh.dev.base.BaseRepository
import linh.dev.base.StringUtil
import linh.dev.salonapp.CUSTOMER_TYPE
import linh.dev.salonapp.PREF_USER
import linh.dev.salonapp.SERVICE_EMPLOYEE
import linh.dev.salonapp.SalonApplication
import linh.dev.salonapp.ext.mainThread
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.model.local.User
import linh.dev.salonapp.model.remote.LoginResponse
import linh.dev.salonapp.model.remote.SimpleResponse
import linh.dev.salonapp.network.AppApi
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SingleLiveEvent
import linh.dev.salonapp.util.PrefUtil
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.HttpException
import retrofit2.Response
import java.io.File

class UserRepository(val api: AppApi) : BaseRepository() {
    val logoutResult: SingleLiveEvent<Result<Unit>> by lazy {
        SingleLiveEvent<Result<Unit>>()
    }
    val appointment: MutableLiveData<Result<Appointment>> by lazy {
        MutableLiveData<Result<Appointment>>()
    }
    val loginResult: SingleLiveEvent<Result<User>> by lazy {
        SingleLiveEvent<Result<User>>()
    }
    val listAppointment: SingleLiveEvent<Result<List<Appointment>>> by lazy {
        SingleLiveEvent<Result<List<Appointment>>>()
    }

    val updateResult: SingleLiveEvent<Result<String>> by lazy {
        SingleLiveEvent<Result<String>>()
    }
    val cancelAppointmentResult: SingleLiveEvent<Result<SimpleResponse<Any>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Any>>>()
    }

    val userProfileResult: SingleLiveEvent<Result<SimpleResponse<User>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<User>>>()
    }
    val updatePassResult: SingleLiveEvent<Result<SimpleResponse<Any>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Any>>>()
    }

    suspend fun doLogout(user: User) {

        try {
            logoutResult.postValue(Result.loading())
            logout(user.getBearToken())
            logoutResult.postValue(Result.success())
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException && ex.code() == 401) {
                    logoutResult.value =
                        Result.unauthorized()
                } else {
                    logoutResult.value =
                        Result.errorNetwork(null)
                }
            }
        }
    }

    suspend fun updatePhoneNumber(token: String, phoneNumber: String) {
        try {
            updateResult.postValue(Result.loading())
            val response = api.updatePhoneNumber(token, phoneNumber)
            if (response.success) {
                withContext(mainThread()) {
                    val user = user()
                    user?.let {
                        it.phoneNumber = phoneNumber
                        val userPref = StringUtil.convertObjectToJson<User>(it)
                        val context = SalonApplication.applicationContext()
                        PrefUtil.savePreferenceWithResult(
                            context,
                            PREF_USER, userPref
                        )
                    }
                }
                updateResult.postValue(Result.success(response.message))
            } else {
                updateResult.postValue(Result.error(response.message, null))
            }
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException && ex.code() == 401) {
                    updateResult.value = Result.unauthorized()
                } else {
                    updateResult.value =
                        Result.errorNetwork(null)
                }
            }
        }
    }

    suspend fun updatePassword(token: String, oldPass: String, newPass: String) {
        try {
            updatePassResult.postValue(Result.loading())
            val result = api.updatePassword(token, oldPass, newPass)
            updatePassResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    updatePassResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    updatePassResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun loginAnonymous() {
        loginResult.postValue(Result.loading())
        val response = api.loginAnonymous()
        val userInfo = response.data
        if (userInfo != null) {
            val userResponse = userInfo.user
            userResponse.token = userInfo.token
            withContext(mainThread()) {
                userResponse.isNonePassUser = true
                val userPref = StringUtil.convertObjectToJson<User>(userResponse)
                val context = SalonApplication.applicationContext()
                val success = PrefUtil.savePreferenceWithResult(
                    context,
                    PREF_USER, userPref
                )

                if (success) {
                    when (userResponse.user_type) {
                        CUSTOMER_TYPE,
                        SERVICE_EMPLOYEE -> {
                            loginResult.value = Result.success(userResponse)
                        }
                        else -> {
                            loginResult.value =
                                Result.error("Tính năng đăng nhập của bạn chưa hỗ trợ trên nền tảng Android")

                        }
                    }

                } else {
                    loginResult.value = Result.error(response.message)
                }

            }

        } else {
            withContext(mainThread()) {
                loginResult.value = Result.error(response.message)
            }

        }

    }

    suspend fun loginWithFacebook(email: String, name: String, avatar: String) {
        loginResult.postValue(Result.loading())
        val response = api.loginWithFacebook(email, name, avatar)
        val userInfo = response.data
        if (userInfo != null) {
            val userResponse = userInfo.user
            userResponse.token = userInfo.token
            withContext(mainThread()) {
                userResponse.isNonePassUser = true
                val userPref = StringUtil.convertObjectToJson<User>(userResponse)
                val context = SalonApplication.applicationContext()
                val success = PrefUtil.savePreferenceWithResult(
                    context,
                    PREF_USER, userPref
                )

                if (success) {
                    when (userResponse.user_type) {
                        CUSTOMER_TYPE,
                        SERVICE_EMPLOYEE -> {
                            loginResult.value = Result.success(userResponse)
                        }
                        else -> {
                            loginResult.value =
                                Result.error("Tính năng đăng nhập của bạn chưa hỗ trợ trên nền tảng Android")

                        }
                    }

                } else {
                    loginResult.value = Result.error(response.message)
                }

            }

        } else {
            withContext(mainThread()) {
                loginResult.value = Result.error(response.message)
            }

        }
    }

    suspend fun loginWithGoogle(email: String, name: String, avatar: String?) {
        loginResult.postValue(Result.loading())
        val response = api.loginWithGoogle(email, name, avatar)
        val userInfo = response.data
        if (userInfo != null) {
            val userResponse = userInfo.user
            userResponse.token = userInfo.token
            userResponse.isNonePassUser = true
            withContext(mainThread()) {
                val userPref = StringUtil.convertObjectToJson<User>(userResponse)
                val context = SalonApplication.applicationContext()
                val success = PrefUtil.savePreferenceWithResult(
                    context,
                    PREF_USER, userPref
                )

                if (success) {
                    when (userResponse.user_type) {
                        CUSTOMER_TYPE,
                        SERVICE_EMPLOYEE -> {
                            loginResult.value = Result.success(userResponse)
                        }
                        else -> {
                            loginResult.value =
                                Result.error("Tính năng đăng nhập của bạn chưa hỗ trợ trên nền tảng Android")

                        }
                    }

                } else {
                    loginResult.value = Result.error(response.message)
                }

            }

        } else {
            withContext(mainThread()) {
                loginResult.value = Result.error(response.message)
            }

        }
    }

    suspend fun loginAsync(email: String, password: String) {

        try {
            withContext(mainThread()) {
                loginResult.value = Result.loading()
            }
            val response = api.loginWithEmail(email, password)

            val userInfo = response.data

            if (userInfo != null) {
                val userResponse = userInfo.user
                userResponse.token = userInfo.token
                userResponse.isNonePassUser = false
                userInfo.salon_id?.let {
                    userResponse.salonId = it
                }
                withContext(mainThread()) {
                    val userPref = StringUtil.convertObjectToJson<User>(userResponse)
                    val context = SalonApplication.applicationContext()
                    val success = PrefUtil.savePreferenceWithResult(
                        context,
                        PREF_USER, userPref
                    )

                    if (success) {
                        when (userResponse.user_type) {
                            CUSTOMER_TYPE,
                            SERVICE_EMPLOYEE -> {
                                loginResult.value = Result.success(userResponse)
                            }
                            else -> {
                                loginResult.value =
                                    Result.error("Tính năng đăng nhập của bạn chưa hỗ trợ trên nền tảng Android")

                            }
                        }

                    } else {
                        loginResult.value = Result.error(response.message)
                    }

                }

            } else {
                withContext(mainThread()) {
                    loginResult.value = Result.error(response.message)
                }

            }

        } catch (ex: Exception) {
            withContext(mainThread()) {
                loginResult.value =
                    Result.errorNetwork(null)
            }
        }

    }

    suspend fun registerAsync(
        email: String,
        name: String,
        phoneNumber: String,
        password: String,
        userType: Int
    ): LoginResponse {
        return api.signUp(
            email = email,
            name = name,
            phoneNumber = phoneNumber,
            password = password,
            user_type = userType
        )
    }

    private suspend fun logout(authHeader: String): Response<Unit> {
        return api.logout(authHeader)
    }

    suspend fun getPendingAppointmentByUser(authHeader: String) {
        try {
            withContext(mainThread()) {
                listAppointment.value = Result.loading()
            }
            val response = api.getAppointmentByUser(authHeader)
            withContext(mainThread()) {
                listAppointment.value = Result.success(response.data)
            }
        } catch (ex: Exception) {
            withContext(mainThread()) {
                listAppointment.value =
                    Result.errorNetwork(null)
            }
        }

//        return api.getAppointmentByUser(authHeader)
    }

    //    suspend fun cancelAppointment(appointmentId: Int, authHeader: String): SimpleResponse<Any> {
//        return api.cancelAppointment(appointmentId, authHeader)
//    }
    suspend fun cancelAppointment(appointmentId: Int, authHeader: String) {
        try {
            cancelAppointmentResult.postValue(Result.loading())
            val result = api.cancelAppointment(appointmentId, authHeader)
            cancelAppointmentResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    cancelAppointmentResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    cancelAppointmentResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun getDetailAppointment(appointmentId: Int, authHeader: String) {
        try {
            withContext(mainThread()) {
                appointment.value = Result.loading()
                val result = api.getAppointmentDetail(appointmentId, authHeader)
                if (result.success) {
                    appointment.value = Result.success(result.data)

                } else {
                    appointment.value = Result.error(result.message)
                }
            }
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException && ex.code() == 401) {
                    appointment.value = Result.error(ex.message(), null, ex.code())
                } else {
                    appointment.value = Result.errorNetwork(null)
                }
            }
        }

    }

    suspend fun updateProfile(
        token: String,
        name: String,
        email: String,
        phone: String,
        file: File?
    ) {
        val userName = RequestBody.create(
            MediaType.parse("text/plain"), name
        )
        val emailUser = RequestBody.create(
            MediaType.parse("text/plain"), email
        )
        val phoneNumber = RequestBody.create(
            MediaType.parse("text/plain"), phone
        )


        try {

            var body: MultipartBody.Part? = null
            if (file != null) {
                val image = RequestBody.create(
                    MediaType.parse("multipart/form-data"), file
                )
                body = MultipartBody.Part.createFormData("image", file.name, image)
            }
            userProfileResult.postValue(Result.loading())
            val result = api.updateProfile(token, userName, emailUser, phoneNumber, body)
            userProfileResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    userProfileResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    userProfileResult.value = Result.errorNetwork(null)
                }
            }

        }
    }
}