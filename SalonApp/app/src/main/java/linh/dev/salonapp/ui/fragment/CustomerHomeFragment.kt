package linh.dev.salonapp.ui.fragment

import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.viewpager.widget.ViewPager
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_customer_home.*
import linh.dev.base.BaseFragment
import linh.dev.salonapp.AppAction
import linh.dev.salonapp.AppEvent
import linh.dev.salonapp.R
import linh.dev.salonapp.adapter.HomeCustomerPagerAdapter
import linh.dev.salonapp.databinding.FragmentCustomerHomeBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.viewmodel.CustomerHomeViewModel
import linh.dev.salonapp.viewmodel.SalonViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CustomerHomeFragment : BaseFragment<FragmentCustomerHomeBinding>() {
    private val args: CustomerHomeFragmentArgs by navArgs()
    private val viewModel: CustomerHomeViewModel by viewModel()
    private val salonViewModel: SalonViewModel by sharedViewModel()
    val pagerHomeCustomerPagerAdapter: HomeCustomerPagerAdapter by inject {
        parametersOf(
            childFragmentManager
        )
    }
    val indexToPage = mapOf(
        0 to R.id.homeTabFragment,
        1 to R.id.appointmentFragment,
        2 to R.id.shopCartTabFragment,
        3 to R.id.profileTabFragment
    )

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_customer_home
    }

    override fun initView() {
        super.initView()
        viewPagerHome.adapter = pagerHomeCustomerPagerAdapter
        viewPagerHome.offscreenPageLimit = 4
        viewPagerHome.currentItem = args.currentTab
        var itemId = indexToPage[args.currentTab] ?: R.id.homeTabFragment
        if (salonViewModel.currentTab != 0) {
            itemId = indexToPage[salonViewModel.currentTab] ?: R.id.homeTabFragment
        }
        if (bottomNavigationCustomerHome.selectedItemId != itemId) {
            bottomNavigationCustomerHome.selectedItemId = itemId
        }
        viewPagerHome.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                val itemIdSelect = indexToPage[position] ?: R.id.homeTabFragment
                if (bottomNavigationCustomerHome.selectedItemId != itemIdSelect) {
                    bottomNavigationCustomerHome.selectedItemId = itemIdSelect
                }
            }

        })
        viewModel.currentTab.observe(viewLifecycleOwner, Observer {
            val itemIdSelect = indexToPage[it] ?: R.id.homeTabFragment
            if (bottomNavigationCustomerHome.selectedItemId != itemIdSelect) {
                bottomNavigationCustomerHome.selectedItemId = itemIdSelect
            }
        })
        bottomNavigationCustomerHome.setOnNavigationItemSelectedListener {
            val position = indexToPage.values.indexOf(it.itemId)
            logE(position)
            if (viewPagerHome.currentItem != position) {
                viewPagerHome.currentItem = position
            }
            true
        }

//        findNavController().navigate(R.id.bottom_navigation_customer_home)
//        bottomNavigationCustomerHome.setupWithNavController(
//            Navigation.findNavController(
//                requireActivity(),
//                R.id.customer_home_nav_fragment
//            )
//        )
    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_BOOKING_SUCCESS) {
            viewModel.currentTab.postValue(1)

        } else if (appAction.event == AppEvent.EVENT_GO_TO_HOME) {
            viewPagerHome.currentItem = 0
            val itemId = indexToPage[0] ?: R.id.homeTabFragment
            if (bottomNavigationCustomerHome.selectedItemId != itemId) {
                bottomNavigationCustomerHome.selectedItemId = itemId
            }
        }
    }

}
