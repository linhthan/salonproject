package linh.dev.salonapp.network

import linh.dev.salonapp.ERROR_NETWORK_REQUEST

data class Result<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    val statusCode: Int?
) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING,
        UNAUTHORIZED
    }


    companion object {
        fun <T> success(data: T): Result<T> {
            return Result(Status.SUCCESS, data, null, null)
        }
        fun <T> success(): Result<T> {
            return Result(Status.SUCCESS, null, null, null)
        }

        fun <T> error(message: String, data: T? = null): Result<T> {
            return Result(Status.ERROR, data, message, null)
        }
        fun <T> errorNetwork( data: T? = null): Result<T> {
            return Result(Status.ERROR, data, ERROR_NETWORK_REQUEST, null)
        }

        fun <T> error(message: String, data: T? = null, statusCode: Int): Result<T> {
            return Result(Status.ERROR, data, message, statusCode)
        }

        fun <T> loading(data: T? = null): Result<T> {
            return Result(Status.LOADING, data, null, null)
        }

        fun <T> unauthorized(): Result<T>{
            return Result(Status.ERROR, null, null, 401)
        }
    }
}