package linh.dev.salonapp.listenters

interface OnActionSignInCallback {
    fun login(email: String, password: String)
    fun signUpClick()
}