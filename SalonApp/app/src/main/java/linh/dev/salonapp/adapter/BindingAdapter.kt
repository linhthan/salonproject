package linh.dev.salonapp.adapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.wang.avi.AVLoadingIndicatorView
import linh.dev.salonapp.*
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.model.local.Art
import linh.dev.salonapp.model.local.Ratio
import linh.dev.salonapp.model.local.Salon
import linh.dev.salonapp.util.DateTimeUtils
import me.zhanghai.android.materialratingbar.MaterialRatingBar


@BindingAdapter("thumbUrl")
fun setThumbImageUrl(imv: ImageView, imgUrl: String?) {
    Glide.with(imv.context).load(imgUrl).thumbnail(0.5f).into(imv)
}

@BindingAdapter("imgUrl")
fun setImageUrl(imv: ImageView, imgUrl: String) {
    Glide.with(imv.context).load(imgUrl).into(imv)
}

@BindingAdapter("avatarUrl")
fun setAvatar(imv: ImageView, avatarUrl: String?) {
    Glide.with(imv.context).load(avatarUrl).placeholder(R.drawable.ic_user_default)
        .transform(CircleCrop()).into(imv)
}

@BindingAdapter("avatarStylistUrl")
fun setAvatarStylist(imv: ImageView, avatarUrl: String?) {
    if (avatarUrl.isNullOrEmpty()) {
        Glide.with(imv.context).load(R.drawable.ic_stylist_avatar_default).dontAnimate()
            .into(imv)
    } else {
        Glide.with(imv.context).load(avatarUrl).dontAnimate().into(imv)
    }

}

@BindingAdapter("total_rate")
fun setRate(tv: TextView, rate: Float?) {
    if (rate == null) {
        tv.visibility = View.INVISIBLE
    } else {
        tv.text = (Math.round(rate * 10) / 10.0).toString()
        tv.visibility = View.VISIBLE
    }
}

@BindingAdapter("total_rate_service")
fun setRateSalon(rateBar: MaterialRatingBar, rate: Float?) {
    if (rate == null) {
        rateBar.rating = 0f
    } else {
        rateBar.rating = rate
    }
}


@BindingAdapter("minuteSlot")
fun setMinuteSlot(tv: TextView, minute: Int) {
    val displayMinute = DateTimeUtils.convertMinuteToSlotDisplayTime(minute)
    tv.text = displayMinute
}

@BindingAdapter("ratioItem")
fun setIconRes(imv: ImageView, ratio: Ratio) {
    if (ratio.isSelected) imv.setImageResource(ratio.activeIcon)
    else imv.setImageResource(ratio.icon)
}

@BindingAdapter("ratioItem")
fun setIconRes(tvTitle: TextView, ratio: Ratio) {
    tvTitle.setText(ratio.title)
    if (ratio.isSelected) tvTitle.setTextColor(
        ContextCompat.getColor(
            tvTitle.context,
            R.color.active_blue
        )
    )
    else tvTitle.setTextColor(Color.WHITE)
}

@BindingAdapter("price")
fun setPriceService(tvPrice: TextView, price: Double) {
    val priceK = (price / 1000).toInt()
    tvPrice.text = tvPrice.context.getString(R.string.price_service, priceK.toString())
}

@BindingAdapter("status")
fun setAppointmentStatus(tvStatus: TextView, status: Int) {
    val statusToLabel = mapOf(
        APPOINTMENT_STATUS_PENDING to "Đang chờ",
        APPOINTMENT_STATUS_DONE to "Đã check in",
        APPOINTMENT_STATUS_CANCEL to "Đã hủy",
        APPOINTMENT_STATUS_PAID to "Đã thanh toán"
    )
    tvStatus.text = statusToLabel[status] ?: "Đang chờ"
}

@BindingAdapter("serviceDuration")
fun setDurationServiceText(tvDuration: TextView, duration: Int) {
    tvDuration.text =
        tvDuration.context.resources.getString(R.string.duration_service_appointment, duration)
}

@BindingAdapter("art")
fun setThumbArtUrl(imv: ImageView, art: Art?) {
    Glide.with(imv.context).load(art?.url_art).into(imv)
}

@BindingAdapter("bookingTime")
fun setBookingTime(tv: TextView, appointment: Appointment?) {
    appointment?.let {
        if (it.checkin_at != null) {
            val timeDisplay = DateTimeUtils.formatDateString(
                it.checkin_at, SERVER_TIME_FORMAT,
                APPOINTMENT_USED_FORMAT
            )
            tv.text = timeDisplay
        } else {
            val bookingTime = DateTimeUtils.formatDateString(
                it.booking_time, SERVER_TIME_FORMAT,
                APPOINTMENT_HOUR_START_FORMAT
            )
            val bookingEndTime = DateTimeUtils.formatDateString(
                it.booking_end_time, SERVER_TIME_FORMAT,
                APPOINTMENT_USED_FORMAT
            )
            tv.text = tv.context.resources.getString(
                R.string.format_time_booking_detail,
                bookingTime,
                bookingEndTime
            )
        }


    }

}


@BindingAdapter("bookingHour")
fun setBookingHour(tv: TextView, appointment: Appointment) {

    if (appointment.checkin_at != null) {
        val timeDisplay = DateTimeUtils.formatDateString(
            appointment.checkin_at, SERVER_TIME_FORMAT,
            APPOINTMENT_HOUR_START_FORMAT
        )
        tv.text = timeDisplay
    } else {
        val bookingTime = DateTimeUtils.formatDateString(
            appointment.booking_time, SERVER_TIME_FORMAT,
            APPOINTMENT_HOUR_START_FORMAT
        )
        val bookingEndTime = DateTimeUtils.formatDateString(
            appointment.booking_end_time, SERVER_TIME_FORMAT,
            APPOINTMENT_HOUR_START_FORMAT
        )
        tv.text = tv.context.resources.getString(
            R.string.format_time_booking_detail,
            bookingTime,
            bookingEndTime
        )
    }


}

@BindingAdapter("bookingDate")
fun setBookingDate(tv: TextView, appointment: Appointment) {
    when {
        appointment.paidAt != null -> {
            val timeDisplay = DateTimeUtils.formatDateString(
                appointment.paidAt, SERVER_TIME_FORMAT,
                APPOINTMENT_DATE_FORMAT
            )
            tv.text = timeDisplay
        }
        appointment.checkin_at != null -> {
            val timeDisplay = DateTimeUtils.formatDateString(
                appointment.checkin_at, SERVER_TIME_FORMAT,
                APPOINTMENT_DATE_FORMAT
            )
            tv.text = timeDisplay
        }
        else -> {
            val bookingTime = DateTimeUtils.formatDateString(
                appointment.booking_time, SERVER_TIME_FORMAT,
                APPOINTMENT_DATE_FORMAT
            )
            tv.text = bookingTime
        }
    }


}


@BindingAdapter("labelBookingTime")
fun setLabelBookingTime(tvLabel: TextView, appointment: Appointment?) {
    appointment?.let {
        if (it.checkin_at == null) {
            tvLabel.text = tvLabel.context.resources.getString(R.string.date_time_label_common)
        } else {
            if (it.paidAt == null) {
                tvLabel.text =
                    tvLabel.context.resources.getString(R.string.date_time_label_paid_common)
            } else {
                tvLabel.text =
                    tvLabel.context.resources.getString(R.string.date_time_label_check_in_common)
            }

        }
    }

}


@BindingAdapter("salonNameAppointment")
fun setSalonNameAppointment(tvNameSalon: TextView, appointment: Appointment?) {
    appointment?.let {
        tvNameSalon.text = it.salonName
    }
}

@BindingAdapter("salonLocationAppointment")
fun setSalonLocationAppointment(tvSalonLocation: TextView, appointment: Appointment?) {
    appointment?.let {
        tvSalonLocation.text = it.detailLocation
    }
}


@SuppressLint("ResourceAsColor")
@BindingAdapter("statusAppointment")
fun setStatusAppointment(tvStatus: TextView, appointment: Appointment?) {
    appointment?.let {
        when (it.status) {
            0 -> {
                //Pending
                tvStatus.text = tvStatus.context.resources.getString(R.string.lbl_pending_common)
                tvStatus.setBackgroundResource(R.drawable.bg_tv_status_pending_rounded)
                tvStatus.setTextColor(R.color.black)

            }
            1 -> {
                tvStatus.text = tvStatus.context.resources.getString(R.string.lbl_check_in_common)
                tvStatus.setBackgroundResource(R.drawable.bg_tv_status_check_in_rounded)
                tvStatus.setTextColor(R.color.white)
            }
            2 -> {
                tvStatus.text = tvStatus.context.resources.getString(R.string.lbl_cancel_common)
                tvStatus.setBackgroundResource(R.drawable.bg_tv_status_cancel_rounded)
                tvStatus.setTextColor(R.color.black)
            }
            3 -> {
                tvStatus.text = tvStatus.context.resources.getString(R.string.lbl_paid_common)
                tvStatus.setBackgroundResource(R.drawable.bg_tv_status_check_in_rounded)
                tvStatus.setTextColor(R.color.white)
            }

        }
    }
}

@BindingAdapter("servicePrice")
fun setTotalPriceService(tvPrice: TextView, appointment: Appointment?) {
    appointment?.let {
        val services = it.services
        val price = services.fold(0.0) { sum, item ->
            sum + item.price

        }
        val priceK = (price / 1000).toInt()
        tvPrice.text = tvPrice.context.getString(R.string.price_service, priceK.toString())
    }
}

@BindingAdapter("createAt")
fun setCreateAtTime(tvCreateAt: TextView, createAt: String) {
    val timeDisplay = DateTimeUtils.formatDateString(
        createAt, SERVER_TIME_FORMAT,
        CREATE_AT_TIME_FORMAT
    )
    tvCreateAt.text = timeDisplay
}

@BindingAdapter(value = ["hairStyleUrl", "progressBar"])
fun setHairStyleUrl(imv: ImageView, imgUrl: String, progressBar: AVLoadingIndicatorView) {
    Glide.with(imv.context).load(imgUrl)
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                progressBar.hide()
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                progressBar.hide()
                return false
            }

        })
        .into(imv)
}

@BindingAdapter("orderStatus")
fun setOrderStatus(imv: ImageView, status: Int) {
    when (status) {
        ORDER_STATUS_PENDING, ORDER_STATUS_TRANS -> {
            imv.setImageResource(R.drawable.ic_progress_order)
        }
        ORDER_STATUS_RECEIVED, ORDER_STATUS_PAID -> {
            imv.setImageResource(R.drawable.ic_success)
        }
        ORDER_STATUS_CANCEL -> {
            imv.setImageResource(R.drawable.ic_cancel_order)
        }

    }
}

@BindingAdapter("orderStatusText")
fun setOrderStatusDisplay(tv: TextView, status: Int) {
    when (status) {
        ORDER_STATUS_PENDING -> {
            tv.text = tv.context.getString(
                R.string.order_status_display,
                tv.context.getString(R.string.order_pending_status)
            )
        }
        ORDER_STATUS_TRANS -> {
            tv.text = tv.context.getString(
                R.string.order_status_display,
                tv.context.getString(R.string.order_trans_status)
            )
        }
        ORDER_STATUS_RECEIVED -> {
            tv.text = tv.context.getString(
                R.string.order_status_display,
                tv.context.getString(R.string.order_received_status)
            )
        }
        ORDER_STATUS_PAID -> {
            tv.text = tv.context.getString(
                R.string.order_status_display,
                tv.context.getString(R.string.order_paid_status)
            )
        }
        ORDER_STATUS_CANCEL -> {
            tv.text = tv.context.getString(
                R.string.order_status_display,
                tv.context.getString(R.string.order_cancel_status)
            )
        }
    }
}

@BindingAdapter("orderSalonName")
fun setOrderSalonName(tv: TextView, salonName: String) {
    tv.text = tv.context.getString(R.string.order_salon_name, salonName)
}

@BindingAdapter("orderDate")
fun setOrderDate(tv: TextView, orderDate: String) {
    tv.text = tv.context.getString(
        R.string.order_date_create, DateTimeUtils.formatDateString(
            orderDate,
            SERVER_TIME_FORMAT, ORDER_DATE_FORMAT
        )
    )
}

@BindingAdapter("quantityProduct")
fun setCreateAtTime(tvQuantity: TextView, quantity: Int) {

    if (quantity == 0) {
        tvQuantity.text = tvQuantity.context.getString(R.string.quantity_zero_display)

    } else {
        tvQuantity.text = tvQuantity.context.getString(R.string.format_quantity_product, quantity)
    }

}

@BindingAdapter("priceProductVN")
fun setPriceVietNamese(tv: TextView, price: Double) {
    tv.text = tv.context.getString(R.string.format_price_vietnam, price.toInt())
}

@BindingAdapter("rateAppointment")
fun setRateAppointment(rateBar: MaterialRatingBar, rate: Int?) {
    rateBar.rating = rate?.toFloat() ?: 0f
}

@BindingAdapter("imgUri")
fun setImageUri(imv: ImageView, uri: Uri?) {
    Glide.with(imv.context).load(uri).into(imv)

}

@BindingAdapter("filePathImg")
fun setImageUri(imv: ImageView, filePath: String) {
    Glide.with(imv.context).load(filePath).into(imv)

}

@BindingAdapter("salonItem")
fun setOpenCloseTime(tv: TextView, salonItem: Salon) {
    val openDisplay = DateTimeUtils.convertMinuteToSlotDisplayTime(salonItem.open_time)
    val closeDisplay = DateTimeUtils.convertMinuteToSlotDisplayTime(salonItem.close_time)
    tv.text = tv.context.getString(
        R.string.format_open_close_time, openDisplay, closeDisplay
    )
}
@BindingAdapter("starReview")
fun setRateSalon(rateBar: MaterialRatingBar, star: Int) {
        rateBar.rating = star.toFloat()
}