package linh.dev.salonapp.adapter

import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.util.forEach
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import kotlinx.android.synthetic.main.view_item_service.view.*
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemServiceBinding
import linh.dev.salonapp.model.local.Service

class ServiceAdapter : BaseRecycleViewAdapter<Service>() {

    private val indexSelected = SparseBooleanArray()




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemServiceBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_service,
            parent,
            false
        )
        return ViewHolder(binding)
    }


    fun getServiceSelected(): ArrayList<Service> {
        val results = ArrayList<Service>()
        indexSelected.forEach { index, selected ->
            if (selected && index in (0 until itemCount)) {
                results.add(getItem(index))
            }
        }
        return results
    }

    fun onAddServiceClick(position: Int) {
        if (indexSelected[position]) {
            indexSelected.put(position, false)
        } else {
            indexSelected.put(position, true)
        }
        notifyItemChanged(position)
    }

    inner class ViewHolder(val binding: ViewDataBinding) :
        BaseRecycleViewAdapter<Service>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            (binding as ViewItemServiceBinding).viewModel = getItem(position)
            if (indexSelected[position]) {
                binding.root.imvAddService.setImageResource(R.drawable.ic_added_service)
            } else {
                binding.root.imvAddService.setImageResource(R.drawable.ic_add_service)
            }
            binding.root.imvAddService.setOnClickListener {
                onViewClick(R.id.imvAddService)
            }
        }

    }
}