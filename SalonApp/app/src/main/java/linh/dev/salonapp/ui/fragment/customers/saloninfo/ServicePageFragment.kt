package linh.dev.salonapp.ui.fragment.customers.saloninfo

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import kotlinx.android.synthetic.main.fragment_service_page.*
import linh.dev.base.BaseFragment
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.AppAction
import linh.dev.salonapp.AppEvent
import linh.dev.salonapp.R
import linh.dev.salonapp.adapter.ServiceAdapter
import linh.dev.salonapp.databinding.FragmentServicePageBinding
import linh.dev.salonapp.viewmodel.SalonServiceViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ServicePageFragment : BaseFragment<FragmentServicePageBinding>() {
    var salonId: Int? = null
    private val mViewModel: SalonServiceViewModel by viewModel{ parametersOf(salonId)}
    private val adapter: ServiceAdapter by inject()

    companion object {
        fun newInstance(salonId: Int): ServicePageFragment {
            val fragment = ServicePageFragment()
            fragment.salonId = salonId
            return fragment
        }
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_service_page
    }

    override fun initView() {
        super.initView()

        rcvService.layoutManager = LinearLayoutManager(context)
        (rcvService.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        rcvService.adapter = adapter
        mViewModel.serviceData.observe(viewLifecycleOwner, Observer {
            adapter.setItems(it)
        })
        adapter.setOnViewClickListener(object : BaseRecycleViewAdapter.ViewClickListener {
            override fun onViewItemClock(resId: Int, position: Int) {
                if (resId == R.id.imvAddService) {
                    adapter.onAddServiceClick(position)
                    val dataTransfer = adapter.getServiceSelected()
                    bus.post(AppAction(AppEvent.EVENT_ADD_SERVICE.setData(dataTransfer)))
                }
            }

        })

    }
}