package linh.dev.salonapp.ext

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

fun mainThread(): CoroutineDispatcher {
    return Dispatchers.Main
}
fun io(): CoroutineDispatcher {
    return Dispatchers.IO
}
fun default(): CoroutineDispatcher {
    return Dispatchers.Default
}