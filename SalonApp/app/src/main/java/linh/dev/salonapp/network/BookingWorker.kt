package linh.dev.salonapp.network

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.withContext
import linh.dev.base.BaseConstant
import linh.dev.base.ext.retrofit.buildNetworkCoroutine
import linh.dev.salonapp.*
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.mainThread
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.model.local.Salon
import linh.dev.salonapp.notification.NotificationHelper
import linh.dev.salonapp.repository.SalonRepository
import linh.dev.salonapp.repository.UserRepository
import linh.dev.salonapp.ui.activitys.CustomerHomeActivity
import linh.dev.salonapp.util.DateTimeUtils

class BookingWorker(private val mContext: Context, workerParameters: WorkerParameters) :
    CoroutineWorker(mContext, workerParameters), BaseConstant {
    private val notificationId: Int = System.currentTimeMillis().toInt()
    private val notificationChannel: String = "demo"
    val appApi: AppApi by lazy {
        AppApi::class.java.buildNetworkCoroutine(
            url = "$BASE_URL/$API_VERSION/",
            enableLogging = true,
            connectTimeout = 10
        )
    }
    val repository: UserRepository by lazy {
        UserRepository(appApi)
    }
    private val notificationManager: NotificationManager by lazy {
        applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }


    override suspend fun doWork(): Result {
        val bookingId = inputData.getInt(EXTRA_ID_BOOKING, 0)
        val bookingTime = inputData.getString(EXTRA_BOOKING_TIME) ?: EMPTY
        var content = EMPTY
        if (bookingTime.isNotEmpty()) {
            content = mContext.getString(
                R.string.format_content_notification_msg_booking,
                DateTimeUtils.formatDateString(
                    bookingTime,
                    SERVER_TIME_FORMAT, APPOINTMENT_HOUR_START_FORMAT
                )
            )
        }

        if (bookingId != 0) {
            user()?.let {
                //                repository.getDetailAppointment(bookingId,it.getBearToken())
                val intent = Intent(mContext, CustomerHomeActivity::class.java).apply {
                    flags =
                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    putExtra(BUNDLE_HOME_TAB, 1)
                }
                val pendingIntent: PendingIntent =
                    PendingIntent.getActivity(mContext, 0, intent, 0)

                withContext(io()) {
                    try {
                        val response = appApi.getAppointmentDetail(bookingId, it.getBearToken())
                        if (response.success) {
                            val data = response.data
                            if (data.status == 0) {
                                content = mContext.getString(
                                    R.string.format_content_notification_msg_booking,
                                    DateTimeUtils.formatDateString(
                                        data.booking_time,
                                        SERVER_TIME_FORMAT, APPOINTMENT_HOUR_START_FORMAT
                                    )
                                )


                                NotificationHelper.showNotification(
                                    mContext,
                                    notificationManager,
                                    notificationChannel,
                                    pendingIntent,
                                    "BKSalon",
                                    content
                                )
                            }
                        }
                    } catch (ex: Exception) {
                        if (content.isNotEmpty()) {
                            NotificationHelper.showNotification(
                                mContext,
                                notificationManager,
                                notificationChannel,
                                pendingIntent,
                                "BKSalon",
                                content
                            )
                        }

                    }

                }

            }

        }
        return Result.success()
    }

    private fun displayNotification(data: Appointment) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                notificationChannel,
                notificationChannel,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.enableVibration(false)
            notificationManager.createNotificationChannel(channel)
        }
        val intent = Intent(mContext, CustomerHomeActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0)
        val content = mContext.getString(
            R.string.format_content_notification_msg_booking, DateTimeUtils.formatDateString(
                data.booking_time,
                SERVER_TIME_FORMAT, APPOINTMENT_HOUR_START_FORMAT
            )
        )
        val mBuilder = NotificationCompat.Builder(applicationContext, notificationChannel)
            .setSmallIcon(R.drawable.ic_logo)
            .setContentTitle("BKSalon")
            .setStyle(NotificationCompat.BigTextStyle().bigText(content))
            .setContentText(content)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setVibrate(longArrayOf(500, 1000))
            .setOnlyAlertOnce(false)
            .setColor(ContextCompat.getColor(mContext, R.color.colorAccent))
            .setAutoCancel(true)
        notificationManager.notify(notificationId, mBuilder.build())

    }
}