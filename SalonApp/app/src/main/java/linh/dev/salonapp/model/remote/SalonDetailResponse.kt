package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Salon
import linh.dev.salonapp.model.local.Service
import linh.dev.salonapp.model.local.User

data class SalonDetailResponse(val message: String, val data: SalonInfoResponse?) {
    data class SalonInfoResponse(val stylists: List<User>, val services: List<Service>)
}
