package linh.dev.salonapp.repository

import com.google.gson.JsonObject
import kotlinx.coroutines.withContext
import linh.dev.base.BaseRepository
import linh.dev.salonapp.ext.mainThread
import linh.dev.salonapp.model.local.Product
import linh.dev.salonapp.model.local.ProductCategory
import linh.dev.salonapp.model.remote.OrderParams
import linh.dev.salonapp.model.remote.SimpleResponse
import linh.dev.salonapp.network.AppApi
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SingleLiveEvent
import retrofit2.HttpException

class ProductRepository(val api: AppApi):BaseRepository() {
    val categoryProductResult : SingleLiveEvent<Result<SimpleResponse<List<ProductCategory>>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<List<ProductCategory>>>>()
    }
    val productResult : SingleLiveEvent<Result<SimpleResponse<Product>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Product>>>()
    }
    val addCartResult : SingleLiveEvent<Result<SimpleResponse<Any>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Any>>>()
    }
    val createOrderResult : SingleLiveEvent<Result<SimpleResponse<Any>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Any>>>()
    }

    suspend fun getProductSalonCategory(token: String, salonId: Int) {
        try {
            categoryProductResult.postValue(Result.loading())
            val result = api.getProductSalonCategory(token, salonId)
            categoryProductResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    categoryProductResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    categoryProductResult.value = Result.errorNetwork(null)
                }
            }

        }
    }
    suspend fun getProductSalonById(token: String, productId: Int) {
        try {
            productResult.postValue(Result.loading())
            val result = api.getProductById(token, productId)
            productResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    productResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    productResult.value = Result.errorNetwork(null)
                }
            }

        }
    }
    suspend fun addProductToCart(token: String, productId: Int) {
        try {
            addCartResult.postValue(Result.loading())
            val result = api.addProductToCart(token, productId)
            addCartResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    addCartResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    addCartResult.value = Result.errorNetwork(null)
                }
            }

        }
    }
    suspend fun createOrder(token: String, body: OrderParams) {
        try {
            createOrderResult.postValue(Result.loading())
            val result = api.createOrder(token, body)
            createOrderResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    createOrderResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    createOrderResult.value = Result.errorNetwork(null)
                }
            }

        }
    }
}