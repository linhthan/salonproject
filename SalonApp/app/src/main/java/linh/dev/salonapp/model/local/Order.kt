package linh.dev.salonapp.model.local

import com.google.gson.annotations.SerializedName

data class Order(
    val id: Int,
    @SerializedName("titles") val titleOrder:String,
    @SerializedName("phone_number") val phoneNumber:String,
    @SerializedName("cus_name") val cusName:String,
    val address:String,
    val salonName:String,
    @SerializedName("salon_id") val salonId: Int,
    @SerializedName("order_created_at") val orderCreateAt: String,
    var status: Int
)