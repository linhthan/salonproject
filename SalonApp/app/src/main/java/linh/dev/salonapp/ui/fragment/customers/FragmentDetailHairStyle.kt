package linh.dev.salonapp.ui.fragment.customers

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_detail_hairstyle.*
import kotlinx.android.synthetic.main.layout_no_data.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.currentText
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showMsg
import linh.dev.salonapp.*
import linh.dev.salonapp.databinding.FragmentDetailHairstyleBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.HairStyle
import linh.dev.salonapp.model.local.ProductCategory
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.DetailHairStyleViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.lang.Exception

class FragmentDetailHairStyle : BaseFragment<FragmentDetailHairstyleBinding>() {
    private val args: FragmentDetailHairStyleArgs by navArgs()
    private val viewModel: DetailHairStyleViewModel by viewModel { parametersOf(args.idHair) }
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_detail_hairstyle
    }

    override fun initView() {
        super.initView()
        btnDownload.setOnClickListener {
            val currentHairStyle = viewModel.hairStyleDetailResult.value?.data?.data
            if (currentHairStyle != null) {
                btnDownload.showProgress {
                    buttonText = "Đang tải xuống"
                    progressColor = Color.WHITE
                }
                viewModel.downloadListImage(requireContext(), arrayListOf(currentHairStyle))
            } else {
                showMsg("Chưa có thông tin mẫu tóc, vui lòng thử lại sau!")
            }
        }
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }
        btnTryAgain.setOnClickListener {
            viewModel.getHairStyleDetail()
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initObservable() {
        viewModel.hairStyleDetailResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoading.show()

                }
                Result.Status.SUCCESS -> {
                    progressLoading.hide()
                    if (it.data != null) {
                        if (it.data.success) {
                            mBinding.viewModel = it.data.data
                            layoutData.show()
                        } else {
                            layoutData.hide()
                            viewNoData.show()
                        }
                    }
                }
                Result.Status.ERROR -> {
                    progressLoading.hide()
                    layoutData.hide()
                    viewNoData.show()
                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
        viewModel.hairStyleLocalById?.observe(viewLifecycleOwner, Observer {

            if (it == null) {
                btnDownload.show()
            } else {
                btnDownload.hide()
            }

        })

    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_DOWNLOAD_IMAGE_SUCCESS) {
            val data =
                appAction.event.getData(linh.dev.salonapp.databases.model.HairStyle::class.java)
            data?.let {
                if (it.idHair == args.idHair) {
                    btnDownload.hide()
                    btnDownload.hideProgress(getString(R.string.download_common_lbl))
                    val countDownload = tvDownload.currentText
                    try {
                        val currentCount = countDownload.toInt()
                        tvDownload.text = (currentCount + 1).toString()
                    }catch (ex:Exception){

                    }
                    mBinding.viewModel?.let {
                        it.downloadCount += 1
                    }
                }
            }
        }
    }
}