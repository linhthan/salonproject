package linh.dev.salonapp.viewmodel

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import linh.dev.base.BaseViewModel
import linh.dev.base.StringUtil
import linh.dev.salonapp.CUSTOMER_TYPE
import linh.dev.salonapp.PREF_USER
import linh.dev.salonapp.SalonApplication
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.User
import linh.dev.salonapp.repository.UserRepository
import linh.dev.salonapp.util.PrefUtil

class SignUpViewModel(private val userRepository: UserRepository) : BaseViewModel() {
    var userTypeSelected: Int = CUSTOMER_TYPE
    val progressLoading = MutableLiveData<Boolean>()
    val errorMsg = MutableLiveData<String>()
    val user = MutableLiveData<User>()

    init {
        progressLoading.value = false
    }

    private val job = SupervisorJob()
    private val launchScope = CoroutineScope(Dispatchers.IO + job)

    fun signUp(email: String, name: String, phoneNumber: String, password: String) {
        launchScope.launch {
            try {
                withContext(Dispatchers.Main) {
                    progressLoading.value = true
                }
                val response =
                    userRepository.registerAsync(
                        email,
                        name,
                        phoneNumber,
                        password,
                        userTypeSelected
                    )
                withContext(Dispatchers.Main) {
                    progressLoading.value = false

                    val userInfo = response.data
                    if (userInfo != null) {
                        withContext(Dispatchers.Main) {
                            val userResponse = userInfo.user
                            userResponse.token = userInfo.token
                            userResponse.isNonePassUser = false
                            val userPref = StringUtil.convertObjectToJson<User>(userResponse)
                            val success = PrefUtil.savePreferenceWithResult(
                                SalonApplication.applicationContext(),
                                PREF_USER, userPref
                            )
                            if (success) {
                                user.value = userResponse
                            } else {
                                logE("Save user error")
                            }
                        }

                    } else {
                        errorMsg.value = response.message
                    }
                }

            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    progressLoading.value = false
                }
                e.printStackTrace()
            }

        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}