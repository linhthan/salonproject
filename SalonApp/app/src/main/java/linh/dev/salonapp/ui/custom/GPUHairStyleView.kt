package linh.dev.salonapp.ui.custom

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.drawable.Drawable
import android.media.MediaScannerConnection
import android.os.Environment
import android.util.AttributeSet
import com.sticker.DrawableSticker
import com.sticker.StickerView
import jp.co.cyberagent.android.gpuimage.GPUImageView
import kotlinx.coroutines.*
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.ext.mainThread
import java.io.File
import java.io.FileOutputStream

class GPUHairStyleView : GPUImageView {
    private val job = SupervisorJob()
    private val scope = CoroutineScope(job + Dispatchers.Default)
    private var listener: OnPictureSavedListener? = null


    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    var stickerView: StickerView = StickerView(context)

    init {
        stickerView.layoutParams =
            LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        addView(stickerView)
    }

    fun addSticker(drawable: Drawable) {
        if (drawable.intrinsicWidth == 0 || drawable.intrinsicWidth == 0) return
        val heightDrawable = (150 * drawable.intrinsicHeight * 1f / drawable.intrinsicWidth).toInt()
        val sticker = DrawableSticker(drawable, 150, heightDrawable)
        if (stickerView.stickerCount == 0) {
            stickerView.addSticker(sticker)
        } else {
            stickerView.replace(sticker)
        }

    }

    fun saveBitmapToFile() {
        scope.launch {
            val background = capture()
            val canvas = Canvas(background)
            canvas.drawBitmap(loadBitmapFromView(), Matrix(), null)
            val path = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            val file =
                File(path, "HairStyle" + "/" + System.currentTimeMillis().toString() + ".png")
            file.parentFile?.mkdir()
            background.compress(Bitmap.CompressFormat.JPEG, 80, FileOutputStream(file))
                MediaScannerConnection.scanFile(
                    context,
                    arrayOf(file.toString()), null
                ) { _, uri ->
                        handler.post{ listener?.onPictureSaved(uri) }
                }

        }
    }

    fun setSaveListener(listener: OnPictureSavedListener) {
        this.listener = listener
    }

    fun loadBitmapFromView(): Bitmap {
        val bitmap = Bitmap.createBitmap(
            stickerView.measuredWidth,
            stickerView.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        this.draw(canvas)
        return bitmap
    }

    fun release() {
        job.cancel()
//        stickerView.release()
    }
}