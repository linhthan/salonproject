package linh.dev.salonapp.ui.fragment.collector

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.paginate.Paginate
import com.paginate.recycler.LoadingListItemCreator
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_hair_style.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.HairStyleAllAdapter
import linh.dev.salonapp.adapter.HairStyleNewAdapter
import linh.dev.salonapp.databinding.ViewItemFooterBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.HairStyle
import linh.dev.salonapp.model.remote.HairStyleAllResponse
import linh.dev.salonapp.model.remote.SimpleResponse
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.ui.custom.SpaceItemGridDecoration
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.HairStyleCollectorViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class HairStyleFragment : BaseFragment<linh.dev.salonapp.databinding.FragmentHairStyleBinding>() {
    private val viewModel: HairStyleCollectorViewModel by viewModel()
    private val newHairStyleAdapter: HairStyleNewAdapter by inject()
    private val allHairStyleAdapter: HairStyleAllAdapter by inject()
    private var paginate: Paginate? = null

    companion object {
        fun newInstance(): HairStyleFragment {
            return HairStyleFragment()
        }
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_hair_style
    }


    override fun initView() {
        super.initView()
        initRecyclerView()
        btnAdd.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(R.id.action_fragmentCollectorHome_to_createHairStyleFragment, null)
        }
        btnRetryNew.setOnClickListener {
            viewModel.loadNewHairStyle()
        }
        swrLayout.setOnRefreshListener {
            newHairStyleAdapter.clearData()
            allHairStyleAdapter.clearData()
            viewModel.refresh()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initRecyclerView() {
        val newLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rcvNewHairStyle.layoutManager =
            newLayoutManager
        rcvNewHairStyle.adapter = newHairStyleAdapter
        rcvNewHairStyle.addItemDecoration(SpaceItemDecoration(10, 10, 10, 10, 10))
        rcvAllHairStyle.layoutManager = GridLayoutManager(context, 3)
        rcvAllHairStyle.addItemDecoration(SpaceItemGridDecoration(10, 10, 10, 10))
        rcvAllHairStyle.adapter = allHairStyleAdapter
        rcvNewHairStyle.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                swrLayout.isEnabled = newLayoutManager.findFirstCompletelyVisibleItemPosition() == 0
            }
        })
        (rcvAllHairStyle.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        paginate?.unbind()
        val callBacks = object : Paginate.Callbacks {
            override fun onLoadMore() {

                if (viewModel.currentPage != 1 && viewModel.loadingAllHairStyle.value == false) {
                    viewModel.loadingAllHairStyle.postValue(true)
                    viewModel.loadMoreHairStyle()
                }

            }

            @Synchronized
            override fun isLoading(): Boolean {
                return viewModel.loadingAllHairStyle.value ?: false
            }

            override fun hasLoadedAllItems(): Boolean {
                return viewModel.endPage
            }

        }
        paginate = Paginate.with(rcvAllHairStyle, callBacks)
            .setLoadingTriggerThreshold(0)
            .addLoadingListItem(true)
            .setLoadingListItemCreator(CustomLoadingListItemCreator())
            .setLoadingListItemSpanSizeLookup { 3 }
            .build()

    }

    private val observer = Observer<Result<SimpleResponse<HairStyleAllResponse>>> {
        when (it.status) {

            Result.Status.LOADING -> {
                progressAllLoading.show()
                viewModel.loadingAllHairStyle.value = true
            }
            Result.Status.SUCCESS -> {
                progressAllLoading.hide()
                if (swrLayout.isRefreshing) {
                    swrLayout.isRefreshing = false
                }
                viewModel.loadingAllHairStyle.value = false
                if (it.data != null && it.data.success) {
                    val dataResponse = it.data.data
                    if (dataResponse.data.isNotEmpty()) {
                        viewModel.currentPage++
                        allHairStyleAdapter.addItems(it.data.data.data)
                    } else {
                        viewModel.endPage = true
                        paginate?.setHasMoreDataToLoad(false)
                    }
                }

            }
            Result.Status.ERROR -> {
                progressAllLoading.hide()
                if (swrLayout.isRefreshing) {
                    swrLayout.isRefreshing = false
                }
                viewModel.loadingAllHairStyle.value = false
            }
            Result.Status.UNAUTHORIZED -> {
                progressAllLoading.hide()
                if (swrLayout.isRefreshing) {
                    swrLayout.isRefreshing = false
                }
                viewModel.loadingAllHairStyle.value = false
            }
        }
    }

    private fun initObservable() {
        viewModel.allHairStyleResult.observe(viewLifecycleOwner, observer)

        viewModel.moreHairStyleResult.observe(viewLifecycleOwner, Observer {
            logE("obser")
            when (it.status) {
                Result.Status.LOADING -> {
                    if (swrLayout.isRefreshing) {
                        swrLayout.isRefreshing = false
                    }
                    viewModel.loadingAllHairStyle.value = true
                }
                Result.Status.SUCCESS -> {
                    if (swrLayout.isRefreshing) {
                        swrLayout.isRefreshing = false
                    }
                    if (it.data != null && it.data.success) {
                        viewModel.loadingAllHairStyle.value = false
                        val dataResponse = it.data.data
                        if (dataResponse.data.isNotEmpty()) {
                            viewModel.currentPage++
                            allHairStyleAdapter.addItems(it.data.data.data)
                            if (dataResponse.data.size < PER_PAGE_HAIR_STYLE) {
                                viewModel.endPage = true
                                paginate?.setHasMoreDataToLoad(false)
                            }
                        } else {
                            viewModel.endPage = true
                            paginate?.setHasMoreDataToLoad(false)
                        }

                    }


                }
                Result.Status.ERROR -> {
                    if (swrLayout.isRefreshing) {
                        swrLayout.isRefreshing = false
                    }
                    viewModel.loadingAllHairStyle.value = false
                }
                Result.Status.UNAUTHORIZED -> {
                    if (swrLayout.isRefreshing) {
                        swrLayout.isRefreshing = false
                    }
                    viewModel.loadingAllHairStyle.value = false
                }
            }
        })

        viewModel.newHairStyleResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressNewLoading.show()
                }
                Result.Status.SUCCESS -> {
                    progressNewLoading.hide()
                    val item = it.data
                    item?.let { result ->
                        if (result.success) {
                            val hairStyleList = result.data
                            if (hairStyleList.isNotEmpty()) {
                                newHairStyleAdapter.setItems(hairStyleList)
                                layoutNewNoData.hide()
                            } else {
                                layoutNewNoData.show()
                            }
                        } else {

                        }

                    }

                }
                Result.Status.ERROR -> {
                    layoutNewNoData.show()
                    progressNewLoading.hide()

                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
    }

    class CustomLoadingListItemCreator :
        LoadingListItemCreator {
        override fun onCreateViewHolder(
            parent: ViewGroup?,
            viewType: Int
        ): RecyclerView.ViewHolder {
            val binding: ViewItemFooterBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent?.context),
                R.layout.view_item_footer, parent, false
            )
            return ListFooterViewHolder(binding)
        }

        override fun onBindViewHolder(p0: RecyclerView.ViewHolder?, p1: Int) {
            if (p0 is ListFooterViewHolder) {
                p0.onBindingData(p1)
            }
        }

        inner class ListFooterViewHolder(val binding: ViewItemFooterBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun onBindingData(position: Int) {
                binding.progressBar.show()

            }

        }


    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        logE("Bus")
        if (appAction.event == AppEvent.EVENT_SAVE_HAIR_STYLE_SUCCESS) {
            val hairStyle = appAction.event.getData(HairStyle::class.java)
            if (hairStyle == null) {
                logE("null")
            } else {
                logE("not null")
            }
            hairStyle?.let {
                logE("add item")
                newHairStyleAdapter.addItem(it)
                allHairStyleAdapter.addItem(it)
            }
        }
    }

}