package linh.dev.salonapp.ui.fragment.customers

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.core.permission.RxRequestPermission
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.dialog_take_photo.*
import kotlinx.android.synthetic.main.fragment_preview_hair_style.*
import kotlinx.android.synthetic.main.fragment_user_info.*
import kotlinx.android.synthetic.main.fragment_user_info.btnSave
import linh.dev.base.BaseActivity
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.currentText
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showToast
import linh.dev.base.permission.Permission
import linh.dev.salonapp.*
import linh.dev.salonapp.databinding.FragmentUserInfoBinding
import linh.dev.salonapp.ext.isValidEmail
import linh.dev.salonapp.ext.isValidPhoneNumber
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.EditProfileViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.io.IOException

class FragmentEditUser : BaseFragment<FragmentUserInfoBinding>() {
    private val viewModel: EditProfileViewModel by viewModel()
    var mode = 0
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_user_info
    }

    override fun initView() {
        super.initView()
        logE(mode)
        initEditTextFocus()
        user()?.let {
            mBinding.user = it
        }
        if (mode == 0) {
            btnSave.text = getString(R.string.edit_lbl_common)
            edtEmail.isEnabled = false
            edtName.isEnabled = false
            edtPhoneNumber.isEnabled = false
            imvCamera.hide()
        } else {
            btnSave.text = getString(R.string.save_lbl_common)
            edtEmail.isEnabled = true
            edtName.isEnabled = true
            edtPhoneNumber.isEnabled = true
            imvCamera.show()
        }

        btnSave.setOnClickListener {
            if (mode == 0) {
                mode = 1
                btnSave.text = getString(R.string.save_lbl_common)
                edtEmail.isEnabled = true
                edtName.isEnabled = true
                edtPhoneNumber.isEnabled = true
                imvCamera.show()
            } else {
                val cusName = edtName.currentText
                val phoneNumber = edtPhoneNumber.currentText
                val email = edtEmail.currentText
                validInfoAndSubmit(cusName, phoneNumber, email)
            }

        }
        imvCamera.setOnClickListener {
            requestPermission()
        }
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.updateResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressUpdate.show()
                }
                Result.Status.SUCCESS -> {
                    progressUpdate.hide()
                    if (it.data != null) {
                        if (it.data.success) {
                            val user = it.data.data
                            user.token = user()?.token
                            PrefUtil.savePreference(
                                SalonApplication.applicationContext(),
                                PREF_USER,
                                Gson().toJson(user)
                            )
                            btnSave.text = getString(R.string.edit_lbl_common)
                            edtEmail.isEnabled = false
                            edtName.isEnabled = false
                            edtPhoneNumber.isEnabled = false
                            imvCamera.hide()
                            viewModel.imageCrop.value = EMPTY
                            mode = 0
                            showToast(it.data.message)
                        } else {
                            showToast(it.data.message)
                        }
                    }
                }
                Result.Status.ERROR -> {
                    progressUpdate.hide()
                    it.message?.let { msg ->
                        showToast(msg)
                    }
                }

                Result.Status.UNAUTHORIZED -> {
                    progressUpdate.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
        viewModel.imageCrop.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                context?.let { ctx ->
                    Glide.with(ctx).load(it).thumbnail(0.5f).into(imvAvatar)
                }
            }
        })
    }

    private fun requestPermission() {
        viewModel.compositeDisposable.add(
            RxRequestPermission(activity as BaseActivity<*>).request(
                arrayListOf(
                    Permission(Manifest.permission.CAMERA, false),
                    Permission(Manifest.permission.READ_EXTERNAL_STORAGE, false),
                    Permission(Manifest.permission.WRITE_EXTERNAL_STORAGE, false)
                )
            ).subscribe { granted ->
                if (granted) {
                    showDialogAddPhoto()

                } else {
                    showToast(getString(R.string.msg_granted_all_permission))
                }

            }
        )
    }

    private fun showDialogAddPhoto() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_take_photo)
        dialog.layoutTakePhoto.setOnClickListener {
            dispatchTakePictureIntent()
            dialog.dismiss()
        }
        dialog.layoutPickPhoto.setOnClickListener {
            dispatchPickPictureIntent()
            dialog.dismiss()
        }
        dialog.setCanceledOnTouchOutside(true)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.CENTER)
        dialog.show()

    }

    private fun createImageFile(

    ): File? {
        val timeStamp: String = System.currentTimeMillis().toString()
        val storageDir: File? = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            viewModel.imagePath = absolutePath
            PrefUtil.savePreference(
                SalonApplication.applicationContext(),
                PREF_FILE_PATH,
                absolutePath
            )
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val mPhotoUri = FileProvider.getUriForFile(
                        requireContext(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri)
                    startActivityForResult(takePictureIntent, CAMERA_PIC_REQUEST)
                }
            }
        }
    }

    private fun dispatchPickPictureIntent() {
        Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        ).also { takePictureIntent ->
            takePictureIntent.type = "image/*"

            startActivityForResult(takePictureIntent, GALLERY_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_PIC_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                data?.let {
                    logE(it.data.toString())
                }
                if (viewModel.imagePath.isEmpty()) {

                    val pathSave = PrefUtil.getSharePreference(
                        SalonApplication.applicationContext(),
                        PREF_FILE_PATH,
                        EMPTY
                    )
                    pathSave?.let {
                        viewModel.imagePath = it
                    }
                }
                if (viewModel.imagePath.isNotEmpty()) {
                    val photo = File(viewModel.imagePath)
                    if (photo.exists()) {
                        val bundle = Bundle()
                        bundle.putString(BUNDLE_FILE_PATH, viewModel.imagePath)
                        bundle.putInt(BUNDLE_MODE, 1)
                        Navigation.findNavController(requireActivity(), R.id.nav_main)
                            .navigate(R.id.action_fragmentEditUser_to_cropFragment, bundle)
                    } else {
                        showToast(getString(R.string.image_file_does_not_exist))
                    }

                } else {
                    showToast(getString(R.string.save_image_error_msg))
                }

            }
        } else if (requestCode == GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            uri?.let {
                val proj = arrayOf(MediaStore.Images.Media.DATA)
                val cursor = requireContext().contentResolver.query(uri, proj, null, null, null);
                val columnIndex = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                cursor?.moveToFirst()
                columnIndex?.let {
                    val path = cursor.getString(it)
                    cursor.close()
                    val photo = File(path)
                    if (photo.exists()) {
                        val bundle = Bundle()
                        bundle.putString(BUNDLE_FILE_PATH, path)
                        bundle.putInt(BUNDLE_MODE, 1)
                        Navigation.findNavController(requireActivity(), R.id.nav_main)
                            .navigate(R.id.action_fragmentEditUser_to_cropFragment, bundle)

                    } else {
                        showToast(getString(R.string.image_file_does_not_exist))
                    }
                }

            }

        }
    }

    fun validInfoAndSubmit(cusName: String, phoneNumber: String, email: String) {
        var error = false
        if (cusName.isBlank()) {
            tilName.error = getString(R.string.name_can_not_empty)
            error = true
        } else {
            tilName.error = null
        }
        if (email.isBlank()) {
            tilEmail.error = getString(R.string.email_blank_error_msg)
            error = true
        } else {
            tilEmail.error = null
        }
        if (!email.isValidEmail()) {
            tilEmail.error = getString(R.string.email_invalid_error_msg)
            error = true
        } else {
            tilEmail.error = null
        }
        if (phoneNumber.isBlank()) {
            tilPhoneNumber.error = getString(R.string.phone_number_can_not_empty)
            error = true
        }
        if (!phoneNumber.isValidPhoneNumber()) {
            error = true
            tilPhoneNumber.error = getString(R.string.phone_number_invalid_error_msg)
        }
        if (!error) {
            val avatarPath = viewModel.imageCrop.value
            if (avatarPath.isNullOrEmpty()) {
                viewModel.updateUser(cusName, email, phoneNumber, null)
            } else {
                try {
                    val file = File(avatarPath)
                    if (file.exists()) {
                        viewModel.updateUser(cusName, email, phoneNumber, file)
                    } else {
                        viewModel.updateUser(cusName, email, phoneNumber, null)
                    }
                } catch (ex: Exception) {
                    viewModel.updateUser(cusName, email, phoneNumber, null)
                }
            }

        }

    }

    private fun initEditTextFocus() {
        edtName.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                tilName.error = null
            }
        }
        edtPhoneNumber.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                tilPhoneNumber.error = null
            }
        }
        edtEmail.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                tilEmail.error = null
            }
        }

    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_CROP_SUCCESS) {
            val filePath = appAction.event.getData(String::class.java)
            viewModel.imageCrop.value = filePath

        }
    }
}