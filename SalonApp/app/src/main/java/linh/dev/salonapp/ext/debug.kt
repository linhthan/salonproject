package linh.dev.salonapp.ext

import android.util.Log
import linh.dev.salonapp.DEBUG

fun Any.logE(message: Any?) {
    if (DEBUG) {
        Log.e(this.javaClass.simpleName, "message: $message")
    }

}

fun Any.logD(message: Any?) {
    if (DEBUG) {
        Log.d(this.javaClass.simpleName, "message: $message")
    }

}