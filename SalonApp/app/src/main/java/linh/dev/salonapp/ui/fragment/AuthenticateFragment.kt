package linh.dev.salonapp.ui.fragment

import android.content.Intent
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.android.synthetic.main.fragment_authenticate.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.CUSTOMER_TYPE
import linh.dev.salonapp.R
import linh.dev.salonapp.RC_SIGN_IN
import linh.dev.salonapp.SERVICE_EMPLOYEE
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.listenters.OnActionSignInCallback
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.activitys.CustomerHomeActivity
import linh.dev.salonapp.ui.activitys.HistoryAppointmentActivity
import linh.dev.salonapp.ui.dialog.BottomSheetLoginDialog
import linh.dev.salonapp.viewmodel.AuthViewModel
import org.koin.android.ext.android.inject
import java.util.*


class AuthenticateFragment :
    BaseFragment<linh.dev.salonapp.databinding.FragmentAuthenticateBinding>(),
    OnActionSignInCallback {

    private val viewModel: AuthViewModel by inject()
    private var mGoogleSignInClient: GoogleSignInClient? = null
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_authenticate
    }

    override fun initView() {
        super.initView()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso);
        layoutSignInGoogle.setOnClickListener {
            val signInIntent = mGoogleSignInClient?.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
        layoutSignInFacebook.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        }
        tvSignIn.setOnClickListener {
            val bottomSheetLoginDialog = BottomSheetLoginDialog.newInstance()
            bottomSheetLoginDialog.setOnActionSignInCallback(this)
            bottomSheetLoginDialog.show(childFragmentManager, BottomSheetLoginDialog.toString())
        }
        tvSkipSignIn.setOnClickListener {
            viewModel.loginAnonymous()
        }
        tvSignUp.setOnClickListener {
            findNavController().navigate(R.id.action_authenticateFragment_to_signUpFragment)
        }

        viewModel.userData.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoading.show()
                }
                Result.Status.SUCCESS -> {
                    progressLoading.hide()
                    logE("${it.data}")
                    logE("${it.data?.user_type}")
                    when (it.data?.user_type) {

                        CUSTOMER_TYPE -> {
                            startActivity(Intent(context, CustomerHomeActivity::class.java))
                            activity?.finish()
                        }
                        SERVICE_EMPLOYEE -> {
                            startActivity(Intent(context, HistoryAppointmentActivity::class.java))
                            activity?.finish()
                        }

                    }
                }
                Result.Status.ERROR -> {
                    progressLoading.hide()
                    it.message?.let { msg ->
                        showToast(msg)
                    }

                }
            }


        })
        viewModel.errorLogin.observe(viewLifecycleOwner, Observer {
            showToast(it)
        })

    }

    override fun login(email: String, password: String) {
        viewModel.login(email, password)
    }

    override fun signUpClick() {
        findNavController().navigate(R.id.action_authenticateFragment_to_signUpFragment)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data?.let {
            viewModel.onActivityResult(requestCode, resultCode, it)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}