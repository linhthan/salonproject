package linh.dev.salonapp.ui.fragment.customers

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.core.permission.RxRequestPermission
import com.squareup.otto.Subscribe
import jp.co.cyberagent.android.gpuimage.GPUImage
import jp.co.cyberagent.android.gpuimage.GPUImageView
import jp.co.cyberagent.android.gpuimage.LoadImageCallback
import kotlinx.android.synthetic.main.dialog_take_photo.*
import kotlinx.android.synthetic.main.fragment_preview_hair_style.*
import linh.dev.base.BaseActivity
import linh.dev.base.BaseFragment
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showMsg
import linh.dev.base.ext.view.showToast
import linh.dev.base.permission.Permission
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.HairStyleAdapter
import linh.dev.salonapp.databases.model.HairStyle
import linh.dev.salonapp.databinding.FragmentPreviewHairStyleBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.ItemDownload
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.HairStyleViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.io.File
import java.io.IOException


class HairStylePreviewTabFragment : BaseFragment<FragmentPreviewHairStyleBinding>() {
    private val args: HairStylePreviewTabFragmentArgs by navArgs()
    private val mViewModel: HairStyleViewModel by viewModel {
        parametersOf(
            args.salonId,
            args.listHairStyle
        )
    }
    private val hairStyleAdapter: HairStyleAdapter by inject()
    var imagePath: String = ""
    override fun getLayoutResourceId(): Int = R.layout.fragment_preview_hair_style

    companion object {
        fun newInstance(): HairStylePreviewTabFragment = HairStylePreviewTabFragment()
    }

    override fun initView() {
        super.initView()
        if (imagePath.isEmpty() && File(imagePath).exists()) {
            gpuHairStyleView.visibility = View.GONE
            emptyView.show()
        } else {
            gpuHairStyleView.visibility = View.VISIBLE
            gpuHairStyleView.setImage(File(imagePath))
            emptyView.hide()
        }

        gpuHairStyleView.setScaleType(GPUImage.ScaleType.CENTER_CROP)

        imvAddImage.setOnClickListener {
            requestPermission()
        }
        btnBack.setOnClickListener {
            activity?.onBackPressed()
        }
        gpuHairStyleView.getGPUImage().setCallback(object : LoadImageCallback {
            override fun onStartLoading() {
                gpuHairStyleView.setShowLoading(true)
            }

            override fun onLoadingSuccess() {
                logE("success loading")
                gpuHairStyleView.setShowLoading(false)
                val item = hairStyleAdapter.getItemSelected()
                item?.let {
                    val sticker = Drawable.createFromPath(item.filePath)
                    sticker?.let {
                        gpuHairStyleView.addSticker(it)
                    }
                }
            }

            override fun onLoadError() {
                logE("onLoadError")
                gpuHairStyleView.setShowLoading(false)
                gpuHairStyleView.visibility = View.GONE
                emptyView.visibility = View.VISIBLE
            }

        })
        rcvHairStyle.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rcvHairStyle.addItemDecoration(SpaceItemDecoration(10, 10, 10, 10))
        (rcvHairStyle.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        rcvHairStyle.adapter = hairStyleAdapter
        hairStyleAdapter.setOnItemClickListener(object : BaseRecycleViewAdapter.ItemClickListener {
            @SuppressLint("ResourceType")
            override fun onItemClick(view: View, position: Int) {
                logE("click")
                val item = hairStyleAdapter.getItem(position)
                logE(item)
                if (item is ItemDownload) {
                    context?.let {
                        if (item.hasDownloadItem()) {
                            mViewModel.downloadListImage(it, item.getDownloadList())
                        } else {
                            showMsg(getString(R.string.no_more_download_item_msg))
                        }
                    }
                } else if (item is HairStyle) {
                    hairStyleAdapter.onItemClick(position)
                    val imagePath = item.filePath
                    if (imagePath.isNotEmpty()) {
                        val sticker = Drawable.createFromPath(item.filePath)
                        sticker?.let {
                            gpuHairStyleView.addSticker(it)
                        }
                    }
                }

            }

        })
        imvChangeImage.setOnClickListener {
            requestPermission()
        }
        btnSave.setOnClickListener {
            gpuHairStyleView.saveBitmapToFile()
        }

        gpuHairStyleView.setSaveListener(GPUImageView.OnPictureSavedListener {
            showToast("Lưu ảnh thành công")
            val bundle = Bundle()
            bundle.putString(BUNDLE_FILE_PATH, it.toString())
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(R.id.action_hairStylePreviewTabFragment_to_previewResultFragment, bundle)
        })

        btnBack.setOnClickListener {
            activity?.onBackPressed()
        }

        mViewModel.hairStyleLocal?.observe(viewLifecycleOwner, Observer {
            val listHairRemote = mViewModel.getListHairStyleToDownload(it)
            val listObject = ArrayList<Any>()
            listObject.addAll(it)
            if (listHairRemote.isNotEmpty()) {
                listObject.add(ItemDownload(listHairRemote))
            }
            if (listObject.isEmpty()) {
                layoutNoData.show()
            } else {
                layoutNoData.hide()

            }
            hairStyleAdapter.setItems(listObject)

        })
    }


    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_DOWNLOAD_IMAGE_SUCCESS) {
            val data = appAction.event.getData(HairStyle::class.java)
            data?.let {
                hairStyleAdapter.addItem(it)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        gpuHairStyleView.release()
    }

    private fun requestPermission() {
        mViewModel.compositeDisposable.add(
            RxRequestPermission(activity as BaseActivity<*>).request(
                arrayListOf(
                    Permission(Manifest.permission.CAMERA, false),
                    Permission(Manifest.permission.READ_EXTERNAL_STORAGE, false),
                    Permission(Manifest.permission.WRITE_EXTERNAL_STORAGE, false)
                )
            ).subscribe { granted ->
                if (granted) {
                    showDialogAddPhoto()

                } else {
                    showToast(getString(R.string.msg_granted_all_permission))
                }

            }
        )
    }

    private fun showDialogAddPhoto() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_take_photo)
        dialog.layoutTakePhoto.setOnClickListener {
            dispatchTakePictureIntent()
            dialog.dismiss()
        }
        dialog.layoutPickPhoto.setOnClickListener {
            dispatchPickPictureIntent()
            dialog.dismiss()
        }
        dialog.setCanceledOnTouchOutside(true)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.CENTER)
        dialog.show()

    }

    private fun createImageFile(

    ): File? {
        val timeStamp: String = System.currentTimeMillis().toString()
        val storageDir: File? = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            logE("ImagePath $absolutePath")
            imagePath = absolutePath
            PrefUtil.savePreference(
                SalonApplication.applicationContext(),
                PREF_FILE_PATH,
                absolutePath
            )
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val mPhotoUri = FileProvider.getUriForFile(
                        requireContext(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri)
                    startActivityForResult(takePictureIntent, CAMERA_PIC_REQUEST)
                }
            }
        }
    }

    private fun dispatchPickPictureIntent() {
        Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        ).also { takePictureIntent ->
            takePictureIntent.type = "image/*"

            startActivityForResult(takePictureIntent, GALLERY_REQUEST_CODE)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_PIC_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                data?.let {
                    logE(it.data.toString())
                }
                logE(imagePath)

                if (imagePath.isEmpty()) {

                    val pathSave = PrefUtil.getSharePreference(
                        SalonApplication.applicationContext(),
                        PREF_FILE_PATH,
                        EMPTY
                    )
                    pathSave?.let {
                        imagePath = it
                    }
                }
                if (imagePath.isNotEmpty()) {
                    try {
                        val photo = File(imagePath)
                        if (photo.exists()) {

                            gpuHairStyleView.visibility = View.VISIBLE
                            emptyView.visibility = View.GONE
                            gpuHairStyleView.setImage(photo)
                        } else {
                            gpuHairStyleView.visibility = View.GONE
                            emptyView.visibility = View.VISIBLE
                            showToast(getString(R.string.image_file_does_not_exist))
                        }
                    } catch (ex: Exception) {
                        showToast(getString(R.string.image_file_does_not_exist))
                    }


                } else {
                    gpuHairStyleView.visibility = View.GONE
                    emptyView.visibility = View.VISIBLE
                    showToast(getString(R.string.save_image_error_msg))
                }
            }
        } else if (requestCode == GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            uri?.let {
                val proj = arrayOf(MediaStore.Images.Media.DATA)
                val cursor = requireContext().contentResolver.query(uri, proj, null, null, null);
                val columnIndex = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                cursor?.moveToFirst()
                columnIndex?.let {
                    val path = cursor.getString(it)
                    cursor.close()
                    try {
                        val photo = File(path)
                        if (photo.exists()) {
                            imagePath = path
                            gpuHairStyleView.visibility = View.VISIBLE
                            emptyView.visibility = View.GONE
                            gpuHairStyleView.setImage(photo)
                        } else {
                            gpuHairStyleView.visibility = View.GONE
                            emptyView.visibility = View.VISIBLE
                            showToast(getString(R.string.image_file_does_not_exist))
                        }
                    } catch (ex: Exception) {
                        showToast(getString(R.string.image_file_does_not_exist))
                    }

                }

            }

        }
    }
}