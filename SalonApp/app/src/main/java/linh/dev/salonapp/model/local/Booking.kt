package linh.dev.salonapp.model.local

data class Booking(
    val id: Int,
    val customer_id: Int,
    val id_salon: Int,
    val stylist_id: Int,
    val booking_time: String
)