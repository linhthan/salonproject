package linh.dev.salonapp.viewmodel

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ERROR_NETWORK_REQUEST
import linh.dev.salonapp.ERROR_UNKNOWN
import linh.dev.salonapp.RC_SIGN_IN
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.repository.UserRepository
import org.json.JSONException


class AuthViewModel(private val userRepository: UserRepository) : BaseViewModel() {

    var job = SupervisorJob()

    val progressLoading = MutableLiveData<Boolean>()
    val userData = userRepository.loginResult
    val callBackManager = CallbackManager.Factory.create()
    val errorLogin = MutableLiveData<String>()

    init {
        progressLoading.value = false
        initLoginFacebook()
    }


    fun login(email: String, password: String) {
        viewModelScope.launch {
            withContext(io()) {
                userRepository.loginAsync(email, password)
            }
        }
    }

    fun loginAnonymous(){
        viewModelScope.launch {
            withContext(io()) {
                userRepository.loginAnonymous()
            }
        }

    }

    private fun initLoginFacebook() {
        LoginManager.getInstance().registerCallback(callBackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                result?.let {
                    val accessToken = it.accessToken
                    val request = GraphRequest.newMeRequest(
                        accessToken
                    ) { resultObject, response ->
                        val json = response?.jsonObject
                        try {
                            if (json != null) {
                                val data = json.getJSONObject("picture").getJSONObject("data")
                                val name = json.getString("name")
                                val email = json.getString("email")
                                val picUrl = data.getString("url")
                                viewModelScope.launch {
                                    withContext(io()) {
                                        userRepository.loginWithFacebook(email, name, picUrl)
                                    }
                                }
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                            errorLogin.value = ERROR_NETWORK_REQUEST
                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "name,email,picture.type(large){url}")
                    request.parameters = parameters
                    request.executeAsync()
                }

            }

            override fun onCancel() {
                Log.e("onCancel", "done")
            }

            override fun onError(error: FacebookException?) {
                error?.printStackTrace()
                errorLogin.value = ERROR_NETWORK_REQUEST
            }

        })
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            account?.let {
                val email = it.email
                val name = it.displayName
                val avatar = it.photoUrl?.path
                if(email!=null && name!=null){
                    viewModelScope.launch {
                        withContext(io()) {
                            userRepository.loginWithGoogle(email, name, avatar)
                        }
                    }
                }else{
                    errorLogin.value = ERROR_UNKNOWN
                }

            }


        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            e.printStackTrace()
            errorLogin.value = ERROR_NETWORK_REQUEST
        }

    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        } else {
            callBackManager.onActivityResult(requestCode, resultCode, data)
        }

    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}