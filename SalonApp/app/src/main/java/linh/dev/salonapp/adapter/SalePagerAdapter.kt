package linh.dev.salonapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.asksira.loopingviewpager.LoopingPagerAdapter
import com.bumptech.glide.Glide
import linh.dev.salonapp.R
import linh.dev.salonapp.model.local.Sale

class SalePagerAdapter : LoopingPagerAdapter<Sale> {

    constructor(context: Context?, isInfinite: Boolean) : super(
        context,
        ArrayList(),
        isInfinite
    )

    override fun inflateView(viewType: Int, container: ViewGroup?, listPosition: Int): View {
        return LayoutInflater.from(context)
            .inflate(R.layout.sliding_images_layout, container, false)
    }

    override fun bindView(convertView: View?, listPosition: Int, viewType: Int) {
        convertView?.let {
            Glide.with(context).load(getItem(listPosition).banner_url).thumbnail(0.5f)
                .into(it.findViewById(R.id.imvSlide))
        }
    }
}