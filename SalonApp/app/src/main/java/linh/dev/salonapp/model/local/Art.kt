package linh.dev.salonapp.model.local

class Art(val id: Int, val url_art: String, val id_booking: Int) {
    override fun equals(other: Any?): Boolean {
        if (other is Art) {
            return other.id == this.id
        }
        return false
    }

    override fun hashCode(): Int {
        return id
    }
}