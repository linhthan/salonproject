package linh.dev.salonapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.*
import linh.dev.salonapp.data.OrderDataSource
import linh.dev.salonapp.data.OrderDataSourceFactory
import linh.dev.salonapp.model.local.Order
import linh.dev.salonapp.model.local.Product
import linh.dev.salonapp.repository.SalonRepository
import linh.dev.salonapp.util.PrefUtil
import java.lang.Exception

class OrderViewModel (val salonRepository: SalonRepository):BaseViewModel(){
    val itemCart = MutableLiveData<Int>()
    var orderList: LiveData<PagedList<Order>>
    private val orderDataSourceFactory: OrderDataSourceFactory
    init {
        orderDataSourceFactory = OrderDataSourceFactory(salonRepository.api, viewModelScope)
        val config = PagedList.Config.Builder()
            .setPageSize(PER_PAGE)
            .setInitialLoadSizeHint(PER_PAGE)
            .setEnablePlaceholders(false)
            .build()
        orderList = LivePagedListBuilder(orderDataSourceFactory, config).build()

        initCountCartItem()
    }
    private fun initCountCartItem(){
        val cartItemPref = PrefUtil.getSharePreference(SalonApplication.applicationContext(),
            PREF_CART_ITEM, EMPTY)
        if(cartItemPref.equals(EMPTY)){
            itemCart.value = 0
        }else{
            try {
                val cartItem = Gson().fromJson<List<Product>>(cartItemPref, object : TypeToken<List<Product>>() {}.type)
                itemCart.value = cartItem.size
            }catch (ex:Exception){
                itemCart.value = 0
            }

        }

    }

    fun getState(): LiveData<State> = Transformations.switchMap<OrderDataSource,
            State>(orderDataSourceFactory.orderDataSourceLiveData, OrderDataSource::state)

    fun retryFailed(){
        orderDataSourceFactory.orderDataSourceLiveData.value?.retryFailedQuery()
    }




    fun listIsEmpty(): Boolean {
        return orderList.value?.isEmpty() ?: true
    }

    fun refreshData() {
        orderDataSourceFactory.orderDataSourceLiveData.value?.refresh()
    }
}