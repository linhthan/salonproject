package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Order
import linh.dev.salonapp.model.local.Product

class ProductReponse (val data: List<Product>)