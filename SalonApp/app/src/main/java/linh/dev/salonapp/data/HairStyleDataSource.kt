package linh.dev.salonapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.*
import linh.dev.salonapp.State
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.HairStyle
import linh.dev.salonapp.network.AppApi

class HairStyleDataSource(
    private val salonId: Int?,
    private val appApi: AppApi,
    private val scope: CoroutineScope
) : PageKeyedDataSource<Int, HairStyle>() {


    var state: MutableLiveData<State> = MutableLiveData()
    private var retryQuery: (() -> Any)? = null
    private var supervisorJob = SupervisorJob()
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, HairStyle>
    ) {
        retryQuery = { loadInitial(params, callback) }
        user()?.let {
            scope.launch {
                try {
                    withContext(io()) {
                        updateState(State.LOADING)
                        val response = appApi.getAllHairStyle(
                            it.getBearToken(),
                            salonId,
                            1,
                            params.requestedLoadSize
                        )
                        retryQuery = null
                        updateState(State.DONE)
                        callback.onResult(response.data.data, null, 2)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    updateState(State.ERROR)

                }


            }

        }


    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, HairStyle>) {
        updateState(State.LOADING)
        retryQuery = { loadAfter(params, callback) }
        user()?.let {
            scope.launch(getJobErrorHandler() + supervisorJob) {
                withContext(io()) {
                    updateState(State.LOADING)
                    val response = appApi.getAllHairStyle(
                        it.getBearToken(),
                        salonId,
                        params.key,
                        params.requestedLoadSize
                    )
                    retryQuery = null
                    updateState(State.DONE)
                    callback.onResult(response.data.data, params.key + 1)
                }
            }

        }

    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, HairStyle>) {
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }


    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        updateState(State.ERROR)
    }

    override fun invalidate() {
        super.invalidate()
        supervisorJob.cancelChildren()
    }

    fun refresh() =
        this.invalidate()

    fun retryFailedQuery() {
        val prevQuery = retryQuery
        retryQuery = null
        prevQuery?.invoke()
    }
}