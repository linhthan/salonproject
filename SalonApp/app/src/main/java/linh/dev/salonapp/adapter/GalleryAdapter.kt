package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemGallerySalonBinding
import linh.dev.salonapp.model.local.Salon

class GalleryAdapter : BaseRecycleViewAdapter<Salon>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemGallerySalonBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_gallery_salon, parent, false
        )
        return ViewHolder(binding)
    }

    inner class ViewHolder(val binding: ViewItemGallerySalonBinding) :
        BaseRecycleViewAdapter<Salon>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            val item = getItem(position)
            binding.viewModel = item

        }

    }
}