package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_item_hair_style.view.*
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databases.model.HairStyle
import linh.dev.salonapp.databinding.ViewItemDownloadBinding
import linh.dev.salonapp.databinding.ViewItemHairStyleBinding
import linh.dev.salonapp.model.local.ItemDownload

class HairStyleAdapter : BaseRecycleViewAdapter<Any>() {
    private var positionSelected = 0
    val TYPE_ITEM = 0;
    val TYPE_DOWNLOAD = 1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        if (viewType == TYPE_ITEM) {
            val binding: ViewItemHairStyleBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_hair_style,
                parent,
                false
            )
            return ViewHolder(binding)
        } else {
            val binding: ViewItemDownloadBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_download,
                parent,
                false
            )
            return DownloadViewHolder(binding)
        }

    }

    override fun addItem(item: Any) {
        if(item is HairStyle){
            if(itemCount > 0){
                val download = getItem(itemCount-1)
                if(download is ItemDownload && download.removeDownloadedItem(item.id)){
                    if(!download.hasDownloadItem()){
                        dataSources.remove(download)
                    }
                    notifyItemChanged(itemCount -1)
                }
            }
            if(itemCount==0){
                dataSources.add(0,item)
                notifyItemChanged(0)
            }else{
                val indexAdd = itemCount-1
                dataSources.add(indexAdd,item)
                notifyItemChanged(indexAdd)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (getItem(position) is ItemDownload) {
            return TYPE_DOWNLOAD
        }
        return TYPE_ITEM
    }

    fun getItemSelected(): HairStyle? {
        return if (positionSelected in 0 until itemCount - 1) {
            val item = getItem(positionSelected)
            if (item is HairStyle) item else null
        } else {
            null
        }
    }


    fun onItemClick(position: Int) {
        val oldPosition = positionSelected
        positionSelected = position
        notifyItemChanged(oldPosition)
        notifyItemChanged(positionSelected)
    }

    inner class ViewHolder(val binding: ViewDataBinding) :
        BaseRecycleViewAdapter<Any>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            val item = getItem(position)
            if (item is HairStyle) {
                Glide.with(binding.root.context).load(item.filePath)
                    .into(binding.root.imvHairStyle)
                binding.root.layoutHairStyle.isSelected = positionSelected == position
            }
            binding.executePendingBindings()
        }

    }

    inner class DownloadViewHolder(val binding: linh.dev.salonapp.databinding.ViewItemDownloadBinding) :
        BaseRecycleViewAdapter<Any>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            val item = getItem(position)
            if (item is ItemDownload) {
                binding.viewModel = item
            }
            binding.executePendingBindings()
        }

    }
}