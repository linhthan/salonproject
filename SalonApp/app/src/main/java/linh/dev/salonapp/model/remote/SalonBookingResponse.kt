package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Salon

data class SalonBookingResponse(val message: String, val data:DataSalonBooking) {
}