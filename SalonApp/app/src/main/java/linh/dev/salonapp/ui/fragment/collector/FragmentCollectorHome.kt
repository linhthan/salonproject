package linh.dev.salonapp.ui.fragment.collector

import androidx.viewpager.widget.ViewPager
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_collector.*
import linh.dev.base.BaseFragment
import linh.dev.salonapp.AppAction
import linh.dev.salonapp.R
import linh.dev.salonapp.adapter.HomeCollectorPagerAdapter
import linh.dev.salonapp.ext.logE
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class FragmentCollectorHome : BaseFragment<linh.dev.salonapp.databinding.FragmentCollectorBinding>() {
    val homeCollectorPagerAdapter: HomeCollectorPagerAdapter by inject {
        parametersOf(
            childFragmentManager
        )
    }

    val indexToPage = mapOf(
        0 to R.id.historyTabFragment,
        1 to R.id.hairStyleTabFragment,
        2 to R.id.profileTabFragment
    )

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_collector
    }

    override fun initView() {
        super.initView()
        viewPagerCollector.adapter = homeCollectorPagerAdapter
        viewPagerCollector.offscreenPageLimit = 3
        viewPagerCollector.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                val itemId = indexToPage[position] ?: R.id.homeTabFragment
                if (bottomNavigationCollector.selectedItemId != itemId) {
                    bottomNavigationCollector.selectedItemId = itemId
                }
            }

        })
        bottomNavigationCollector.setOnNavigationItemSelectedListener {
            val position = indexToPage.values.indexOf(it.itemId)
            logE(position)
            if (viewPagerCollector.currentItem != position) {
                viewPagerCollector.currentItem = position
            }
            true
        }
    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {

    }
}