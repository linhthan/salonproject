package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemUsedAppointmentBinding
import linh.dev.salonapp.model.local.Appointment

class AppointmentUsedAdapter : LoadMoreRecycleViewAdapter<Appointment>() {
    private val typeItem = 0
    override fun getCustomItemViewType(position: Int): Int {
        return typeItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == typeItem) {
            val binding: ViewItemUsedAppointmentBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_used_appointment, parent, false
            )
            ViewHolder(binding)
        } else {
            super.onCreateViewHolder(parent, viewType)
        }
    }

    override fun getItemCount(): Int {
        if (dataSources.isEmpty()) return 0
        return dataSources.size + 1
    }

    inner class ViewHolder(val binding: ViewItemUsedAppointmentBinding) :
        BaseRecycleViewAdapter<Appointment>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.viewModel = getItem(position)
        }

    }
}