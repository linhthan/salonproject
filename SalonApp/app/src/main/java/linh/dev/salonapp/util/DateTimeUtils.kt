package linh.dev.salonapp.util

import linh.dev.salonapp.*
import linh.dev.salonapp.ext.logE
import java.text.SimpleDateFormat
import java.util.*

class DateTimeUtils {

    companion object {
        private val LOCAL_VI = Locale("vi", "VN")
        fun getDayOfWeek(): String {
            val calendar = Calendar.getInstance()
            val day = calendar.get(Calendar.DAY_OF_WEEK)

            when (day) {
                Calendar.SUNDAY -> {
                    return SUNDAY
                }
                Calendar.MONDAY -> {
                    return MONDAY
                }
                Calendar.TUESDAY -> {
                    return TUESDAY
                }
                Calendar.WEDNESDAY -> {
                    return WEDNESDAY
                }
                Calendar.THURSDAY -> {
                    return THURSDAY
                }
                Calendar.FRIDAY -> {
                    return FRIDAY

                }
                Calendar.SATURDAY -> {
                    return SATURDAY
                }

            }
            return MONDAY
        }


        fun getDayOfWeekSelected(position: Int): String {
            val list = arrayListOf(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY)

            val today = getDayOfWeek()
            val indexToday = list.indexOf(today)
            val sizeOfDay = list.size
            if (indexToday + position < sizeOfDay) {
                return list[indexToday + position]
            } else {
                return list[indexToday + position - sizeOfDay]
            }
        }

        fun getTabDataByPosition(position: Int): TabData {
            val list = arrayListOf(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY)
            val listString = arrayListOf(
                MONDAY_STRING,
                TUESDAY_STRING,
                WEDNESDAY_STRING,
                THURSDAY_STRING,
                FRIDAY_STRING,
                SATURDAY_STRING,
                SUNDAY_STRING
            )
            val today = getDayOfWeek()
            val indexToday = list.indexOf(today)
            val sizeOfDay = list.size
            val dayOfWeek: String
            if (position == 0) {
                dayOfWeek = "Hôm nay";
            } else {
                if (indexToday + position < sizeOfDay) {
                    dayOfWeek = listString[indexToday + position]
                } else {
                    dayOfWeek = listString[indexToday + position - sizeOfDay]
                }
            }

            val calNow = Calendar.getInstance(TimeZone.getTimeZone("GMT+7"))
            calNow.add(Calendar.DAY_OF_YEAR, position)
            val now = calNow.time
            val date = formatDateToString(now, PATTERN_DATE_TAB)
            return TabData(dayOfWeek, date)
        }

        fun getDateTodayForBooking(): String {
            val now = Calendar.getInstance(TimeZone.getTimeZone("GMT+7")).time
            return formatDateToString(now, PATTERN_DATE_BOOKING)
        }

        fun getDateSelectedString(addition: Int): String {
            val calNow = Calendar.getInstance(TimeZone.getTimeZone("GMT+7"))
            logE(calNow)
            calNow.add(Calendar.DAY_OF_YEAR, addition)
            logE(addition)
            logE(calNow)
            val now = calNow.time
            return formatDateToString(now, PATTERN_DATE_BOOKING)
        }

        fun getHourSelectedString(addition: Int): String {
            val calNow = Calendar.getInstance(TimeZone.getTimeZone("GMT+7"))
            calNow.add(Calendar.DAY_OF_YEAR, addition)
            val now = calNow.time
            return formatDateToString(now, PATTERN_HOUR_BOOKING)
        }

        fun getDateSelected(addition: Int): Calendar {
            val calNow = Calendar.getInstance(TimeZone.getTimeZone("GMT+7"))
            calNow.add(Calendar.DAY_OF_YEAR, addition)
            return calNow
        }


        fun formatDateToString(dateFormat: Date, format: String): String {
            val formatter = SimpleDateFormat(format, LOCAL_VI)
            formatter.timeZone = TimeZone.getTimeZone("GMT+7")
            val result = formatter.format(dateFormat)
            return result

        }


        fun getMinutesFromDate(source: String, format: String): Int {
            val date = convertStringToDate(source, format)
            val cal = Calendar.getInstance()
            date?.let {
                cal.time = it
                val hour = cal.get(Calendar.HOUR_OF_DAY)
                val minutes = cal.get(Calendar.MINUTE)
                return hour * 60 + minutes
            }
            return 0
        }

        fun convertStringToDate(source: String, format: String): Date? {
            return SimpleDateFormat(format, LOCAL_VI).parse(source)
        }

        fun convertMinuteToSlotDisplayTime(minute: Int): String {
            val hour = minute / 60
            val minutes = minute % 60
            var suffix = ""
            var hourString = "$hour"
            var minuteString = "$minutes"
            if (hour < 12) {
                suffix = "AM"
                if (hour < 10) hourString = "0$hourString"

            } else {
                suffix = "PM"

            }
            if (minutes < 10) {
                minuteString = "0$minuteString"
            }

            return "%s:%s %s".format(hourString, minuteString, suffix)
        }

        fun formatDateString(source: String, sourceFormat: String, desFormat: String): String? {
            val format1 = SimpleDateFormat(sourceFormat, LOCAL_VI)
            val format2 = SimpleDateFormat(desFormat, LOCAL_VI)
            val date = format1.parse(source)
            return if (date != null) {
                format2.format(date)
            } else {
                null
            }

        }

        fun convertStringToCalendar(source: String, formatSource: String): Calendar? {
            val cal = Calendar.getInstance()
            val sdf = SimpleDateFormat(formatSource, LOCAL_VI)
            val date = sdf.parse(source)
            return if (date != null) {
                cal.time = date
                cal

            } else {
                null
            }

        }
    }

    data class TabData(val dayOfWeek: String, val dayOfMonth: String)

}