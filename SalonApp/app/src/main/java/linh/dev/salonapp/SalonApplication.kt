package linh.dev.salonapp

import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import linh.dev.base.databases.DatabaseAssetOpenHelper
import linh.dev.salonapp.di.app_module
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger
import android.content.pm.PackageManager.GET_SIGNATURES
import android.util.Base64
import android.util.Log
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class SalonApplication : Application() {

    init {
        instance = this
    }

    companion object {
        private lateinit var instance: SalonApplication

        fun applicationContext(): Context {
            return instance.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(this, modules = arrayListOf(app_module), logger = AndroidLogger(true))
//        DatabaseAssetOpenHelper.copyDatabase(applicationContext,"hairstyle.sql")
        printHashKey(applicationContext)
        AppEventsLogger.activateApp(this);
    }

    fun printHashKey(pContext: Context) {
        try {
            val info = pContext.packageManager.getPackageInfo(pContext.packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.i("Key", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("KEY", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("KEY", "printHashKey()", e)
        }

    }
}