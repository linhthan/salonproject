package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import linh.dev.base.ext.view.hide
import linh.dev.salonapp.R
import linh.dev.salonapp.State
import linh.dev.salonapp.databinding.ViewItemFooterBinding
import linh.dev.salonapp.databinding.ViewItemSalonSearchBinding
import linh.dev.salonapp.model.local.Salon


class SalonSearchAdapter :
    PagedListAdapter<Salon, RecyclerView.ViewHolder>(SalonDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == DATA_VIEW_TYPE) {
            val binding: ViewItemSalonSearchBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_salon_search, parent, false
            )
            ViewHolder(binding)
        } else {
            val binding: ViewItemFooterBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_footer, parent, false
            )
            return ListFooterViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.onBindingData(position)
        } else if (holder is ListFooterViewHolder) {
            holder.bind(state)
        }
    }


    private var state = State.LOADING

    private var onItemClick: OnItemClick? = null

    interface OnItemClick {
        fun onItemClick(position: Int)
        fun onViewClick(resId: Int, position: Int)
    }


    fun setItemClick(onItemClick: OnItemClick) {
        this.onItemClick = onItemClick
    }

    fun clearData() {
        currentList?.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
//        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }

    fun getItemByPosition(position: Int): Salon? {
        return super.getItem(position)
    }


    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }


    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2

    companion object {
        val SalonDiffCallback = object : DiffUtil.ItemCallback<Salon>() {
            override fun areItemsTheSame(oldItem: Salon, newItem: Salon): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Salon, newItem: Salon): Boolean {
                return oldItem.id == newItem.id && oldItem.name == newItem.name

            }
        }
    }

    inner class ListFooterViewHolder(val binding: ViewItemFooterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(status: State?) {
            if (status == State.DONE) {
                binding.progressBar.hide()
                binding.txtError.hide()
            } else {
                binding.progressBar.visibility =
                    if (status == State.LOADING) View.VISIBLE else View.INVISIBLE
                binding.txtError.visibility =
                    if (status == State.ERROR) View.VISIBLE else View.INVISIBLE
            }

            binding.txtError.setOnClickListener {
                onItemClick?.onViewClick(
                    R.id.txt_error,
                    adapterPosition
                )
            }
        }


    }

    inner class ViewHolder(val binding: ViewItemSalonSearchBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBindingData(position: Int) {
            if (position in 0 until itemCount) {
                val item = getItem(position)
                binding.viewModel = item
                binding.root.setOnClickListener {
                    onItemClick?.onItemClick(position)
                }
                binding.buttonBook.setOnClickListener {
                    onItemClick?.onViewClick(R.id.buttonBook, position)
                }
            }

        }

    }
}