package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemProductOrderBinding
import linh.dev.salonapp.model.local.OrderItem

class OrderItemAdapter : BaseRecycleViewAdapter<OrderItem>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemProductOrderBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_product_order, parent, false
        )
        return ViewHolder(binding)
    }

    inner class ViewHolder(val binding: ViewItemProductOrderBinding) :
        BaseRecycleViewAdapter<OrderItem>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.viewModel = getItem(position)
        }

    }
}