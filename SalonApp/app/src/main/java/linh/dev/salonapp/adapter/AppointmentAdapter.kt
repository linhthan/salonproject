package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.chauthai.swipereveallayout.ViewBinderHelper
import kotlinx.android.synthetic.main.view_item_appointment.view.*
import linh.dev.base.BaseConstant
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.AppAction
import linh.dev.salonapp.AppEvent
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemAppointmentBinding
import linh.dev.salonapp.model.local.Appointment


class AppointmentAdapter : BaseRecycleViewAdapter<Appointment>(), BaseConstant {
    private val viewBinderHelper = ViewBinderHelper()
    private var lastSelectedPosition = -1


    override fun setItems(items: Collection<Appointment>) {
        if (items.isNullOrEmpty()) return
        dataSources.clear()
        dataSources.addAll(items)
        lastSelectedPosition = -1
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemAppointmentBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_appointment, parent, false
        )
        return ViewHolder(binding)
    }


    fun updateDataCancel() {
        if (lastSelectedPosition in (0 until itemCount)) {
            dataSources.get(lastSelectedPosition).status = 2
            notifyItemChanged(lastSelectedPosition)
        }
    }

    inner class ViewHolder(val binding: ViewDataBinding) :
        BaseRecycleViewAdapter<Appointment>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            val item = getItem(position)

            viewBinderHelper.bind(binding.root.swlAppointment, item.id.toString())
            (binding as ViewItemAppointmentBinding).viewModel = item
            binding.root.btnCancel.setOnClickListener {
                lastSelectedPosition = position
                bus.post(AppAction(AppEvent.EVENT_CLICK_CANCEL_BUTTON.setData(getItem(position))))
                binding.root.swlAppointment.close(true)

            }
            binding.root.layoutItem.setOnClickListener {
                onViewClick(R.id.layoutItem)
                bus.post(AppAction(AppEvent.EVENT_GO_TO_APPOINTMENT_DETAIL.setData(getItem(position))))
            }
            binding.executePendingBindings()
        }

    }
}