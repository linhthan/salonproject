package linh.dev.salonapp

import linh.dev.base.BaseActivity

class MainActivity : BaseActivity<linh.dev.salonapp.databinding.ActivityMainBinding>() {

    override fun getLayoutResourceId(): Int {
        return R.layout.activity_main
    }

}
