package linh.dev.salonapp.viewmodel

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.SalonRepository

class RateBookingViewModel(val idBooking: Int, val repository: SalonRepository) : BaseViewModel(

) {
    val rateBookingResult = repository.rateBookingResult

    fun rateBooking(content: String, rate: Int) {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    repository.rateBooking(it.getBearToken(),idBooking, content, rate)
                }
            }
        }
    }

}