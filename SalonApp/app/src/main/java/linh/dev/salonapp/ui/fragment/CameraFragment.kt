package linh.dev.salonapp.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Environment
import android.util.DisplayMetrics
import android.view.Surface
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.*
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_camera.*
import linh.dev.base.BaseFragment
import linh.dev.salonapp.BUNDLE_FILE_PATH
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.FragmentCameraBinding
import java.io.File
import java.lang.Math.*
import java.util.concurrent.Executors

class CameraFragment : BaseFragment<FragmentCameraBinding>() {
    private val executor = Executors.newSingleThreadExecutor()
    private var lensFacing = CameraX.LensFacing.BACK
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_camera
    }

    @SuppressLint("RestrictedApi")
    override fun initView() {
        super.initView()
        view_finder.post {
            startCamera()
        }
        btnReverse.setOnClickListener {
            lensFacing = if (CameraX.LensFacing.FRONT == lensFacing) {
                CameraX.LensFacing.BACK
            } else {
                CameraX.LensFacing.FRONT
            }
            try {
                // Only bind use cases if we can query a camera with this orientation
                CameraX.getCameraWithLensFacing(lensFacing)

                // Unbind all use cases and bind them again with the new lens facing configuration
                CameraX.unbindAll()
                startCamera()
            } catch (exc: Exception) {
                // Do nothing
            }
        }
    }

    private fun startCamera() {
        val metrics = DisplayMetrics().also { view_finder.display.getRealMetrics(it) }
        val screenAspectRatio = aspectRatio(metrics.widthPixels, metrics.heightPixels)
        val previewConfig = PreviewConfig.Builder().apply {
            setLensFacing(lensFacing)
            setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            setTargetRotation(view_finder.display.rotation)
        }.build()


        // Build the viewfinder use case
        val preview = Preview(previewConfig)

        // Every time the viewfinder is updated, recompute layout
        preview.setOnPreviewOutputUpdateListener {

            // To update the SurfaceTexture, we have to remove it and re-add it
            val parent = view_finder.parent as ViewGroup
            parent.removeView(view_finder)
            parent.addView(view_finder, 0)

            view_finder.surfaceTexture = it.surfaceTexture
            updateTransform()
        }

        val imageCaptureConfig = ImageCaptureConfig.Builder()
            .apply {
                setLensFacing(lensFacing)
                // We don't set a resolution for image capture; instead, we
                // select a capture mode which will infer the appropriate
                // resolution based on aspect ration and requested mode
                setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
                setTargetAspectRatio(screenAspectRatio)
                setTargetRotation(view_finder.display.rotation)
                // Set initial target rotation, we will have to call this again if rotation changes
                // during the lifecycle of this use case
//                setTargetRotation(view_finder.display.rotation)
            }.build()

        // Build the image capture use case and attach button click listener
        val imageCapture = ImageCapture(imageCaptureConfig)
        btnCapture.setOnClickListener {
            val metadata = ImageCapture.Metadata().apply {
                // Mirror image when using the front camera
                isReversedHorizontal = lensFacing == CameraX.LensFacing.FRONT
            }
            val timeStamp: String = System.currentTimeMillis().toString()
            val storageDir: File? =
                requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            val file = File.createTempFile(
                "JPEG_${timeStamp}_", /* prefix */
                ".png", /* suffix */
                storageDir /* directory */
            )


            imageCapture.takePicture(file,metadata, executor,
                object : ImageCapture.OnImageSavedListener {
                    override fun onError(
                        imageCaptureError: ImageCapture.ImageCaptureError,
                        message: String,
                        exc: Throwable?
                    ) {
                        val msg = "Photo capture failed: $message"
                        view_finder.post {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                        }

                    }

                    override fun onImageSaved(file: File) {
                        val msg = "Photo capture succeeded: ${file.absolutePath}"
                        view_finder.post {
//                            Glide.with(requireContext()).load(file).into(imvCamera)
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                            val bundle = Bundle()
                            bundle.putString(BUNDLE_FILE_PATH, file.absolutePath)
                            Navigation.findNavController(requireActivity(), R.id.nav_main)
                                .navigate(
                                    R.id.action_cameraFragment_to_cropFragment2,
                                    bundle
                                )
                        }
                    }
                })
        }

        // Bind use cases to lifecycle
        // If Android Studio complains about "this" being not a LifecycleOwner
        // try rebuilding the project or updating the appcompat dependency to
        // version 1.1.0 or higher.
//        CameraX.bindToLifecycle(this, preview)
        CameraX.bindToLifecycle(
            viewLifecycleOwner, preview, imageCapture)
    }
    private fun aspectRatio(width: Int, height: Int): AspectRatio {
        val previewRatio = max(width, height).toDouble() / min(width, height)

        if (abs(previewRatio - RATIO_4_3_VALUE) <= abs(previewRatio - RATIO_16_9_VALUE)) {
            return AspectRatio.RATIO_4_3
        }
        return AspectRatio.RATIO_16_9
    }
    private fun updateTransform() {
        // TODO: Implement camera viewfinder transformations
    }
    companion object {
        private const val RATIO_4_3_VALUE = 4.0 / 3.0
        private const val RATIO_16_9_VALUE = 16.0 / 9.0

    }
}