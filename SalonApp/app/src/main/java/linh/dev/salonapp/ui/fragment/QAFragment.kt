package linh.dev.salonapp.ui.fragment

import android.annotation.SuppressLint
import android.view.View
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.fragment_qa.*
import linh.dev.base.BaseFragment
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.FragmentQaBinding

class QAFragment : BaseFragment<FragmentQaBinding>() {
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_qa
    }

    override fun initView() {
        super.initView()
        settingWebView()
        webViewQA.loadUrl("file:///android_asset/qa.html")
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun settingWebView() {
        webViewQA.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        webViewQA.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webViewQA.isFocusableInTouchMode = false
        webViewQA.isFocusable = false
        webViewQA.webViewClient = WebViewClient()
        val settings = webViewQA.settings
        settings.builtInZoomControls = true
        settings.setSupportZoom(true)
        settings.javaScriptEnabled = true
        settings.defaultTextEncodingName = "utf-8"

    }

}