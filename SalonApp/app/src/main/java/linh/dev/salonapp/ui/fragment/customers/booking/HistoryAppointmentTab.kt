package linh.dev.salonapp.ui.fragment.customers.booking

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_history_appointment_tab.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.BookingHistoryAdapter
import linh.dev.salonapp.databinding.FragmentHistoryAppointmentTabBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.Art
import linh.dev.salonapp.model.local.Review
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.viewmodel.HistoryAppoinmentUserViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class HistoryAppointmentTab : BaseFragment<FragmentHistoryAppointmentTabBinding>() {
    private val viewModel: HistoryAppoinmentUserViewModel by viewModel()
    private val historyAdapter: BookingHistoryAdapter by inject()
    companion object {
        fun newInstance(): HistoryAppointmentTab {
            return HistoryAppointmentTab()
        }
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_history_appointment_tab
    }
    override fun initView() {
        super.initView()
        initRecyclerView()
        swrLayout.setOnRefreshListener {
            viewModel.refreshData()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservableData()
    }
    private fun initRecyclerView() {

        rcvHistory.layoutManager = LinearLayoutManager(context)
        rcvHistory.addItemDecoration(SpaceItemDecoration(10, 10, 10, 10, 10))
        rcvHistory.adapter = historyAdapter
        historyAdapter.setItemClick(object : BookingHistoryAdapter.OnItemClick {
            override fun onViewClick(resId: Int, position: Int) {
                if (resId == R.id.txt_error) {
                    viewModel.retryFailed()
                } else if (resId == R.id.btnRate) {
                    val item = historyAdapter.getItemByPosition(position)
                    item?.let {
                        val bundle = Bundle()
                        bundle.putInt(BUNDLE_ID_BOOKING, it.id)
                        Navigation.findNavController(requireActivity(), R.id.nav_main)
                            .navigate(
                                R.id.action_customerHomeFragment_to_rateBookingFragment,
                                bundle
                            )
                    }


                }
            }

            override fun onItemClick(position: Int) {
                val item = historyAdapter.getItemByPosition(position)
                item?.let {
                    val bundle = Bundle()
                    bundle.putInt(BUNDLE_ID_BOOKING, it.id)
                    Navigation.findNavController(requireActivity(), R.id.nav_main)
                        .navigate(
                            R.id.action_customerHomeFragment_to_appointmentDetailCSFragment3,
                            bundle
                        )
                }

            }

        })

    }

    private fun initObservableData() {

        btnTryNow.setOnClickListener {
            bus.post(AppAction(AppEvent.EVENT_GO_TO_HOME))
        }

        viewModel.getState().observe(viewLifecycleOwner, Observer { state ->
            if (swrLayout.isRefreshing && state == State.LOADING) {
                swrLayout.isRefreshing = false
            }
            if (viewModel.listIsEmpty() && state == State.LOADING) {
                progressLoading.show()
            } else {
                progressLoading.hide()
            }

            if (viewModel.listIsEmpty() && state == State.ERROR) {

                progressLoading.hide()
                emptyView.show()
            } else {
                emptyView.hide()
            }
            if(viewModel.listIsEmpty()&& historyAdapter.itemCount==1){
                emptyView.show()
            }else{
                emptyView.hide()
            }
            logE(viewModel.listIsEmpty())
            logE(historyAdapter.itemCount)
            historyAdapter.setState(state ?: State.DONE)

        })
        viewModel.appointmentUsedList.observe(viewLifecycleOwner, Observer {
            historyAdapter.submitList(it)
        })

    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        when {
            appAction.event == AppEvent.EVENT_COLLECT_IMAGE -> {
                val image = appAction.event.getData(Art::class.java)
                image?.let { art ->
                    viewModel.appointmentUsedList.value?.forEachIndexed { index, item ->
                        if (item.id == art.id_booking) {
                            item.images.add(art)
                            historyAdapter.notifyItemChanged(index)
                        }
                    }
                }

            }
            appAction.event == AppEvent.EVENT_REMOVE_ART -> {
                val image = appAction.event.getData(Art::class.java)

                image?.let { art ->
                    logE("Item Remove: idBooking= ${art.id_booking} và urlArt = ${art.url_art} ")
                    viewModel.appointmentUsedList.value?.forEachIndexed { index, item ->
                        if (item.id == art.id_booking) {
                            logE("Found")
                            item.images.remove(art)
                            historyAdapter.notifyItemChanged(index)
                        }
                    }
                }
            }
            appAction.event == AppEvent.EVENT_RATE_SUCCESS -> {
                val review = appAction.event.getData(Review::class.java)
                review?.let { rv ->
                    viewModel.appointmentUsedList.value?.forEachIndexed { index, item ->
                        if (item.id == rv.booking_id) {
                            item.star = rv.star
                            historyAdapter.notifyItemChanged(index)
                        }
                    }
                }
            }
        }
    }
}