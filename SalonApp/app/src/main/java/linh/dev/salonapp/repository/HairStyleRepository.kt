package linh.dev.salonapp.repository

import kotlinx.coroutines.withContext
import linh.dev.salonapp.SalonApplication
import linh.dev.salonapp.databases.AppDatabase
import linh.dev.salonapp.databases.HairStyleDao
import linh.dev.salonapp.ext.mainThread
import linh.dev.salonapp.model.local.HairStyle
import linh.dev.salonapp.model.remote.HairStyleCreateResponse
import linh.dev.salonapp.model.remote.HairStyleParams
import linh.dev.salonapp.model.remote.SalonResponse
import linh.dev.salonapp.model.remote.SimpleResponse
import linh.dev.salonapp.network.AppApi
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SingleLiveEvent
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.HttpException
import java.io.File

class HairStyleRepository(val appApi: AppApi) {
    val hairStyleResult: SingleLiveEvent<Result<HairStyleCreateResponse>> by lazy {
        SingleLiveEvent<Result<HairStyleCreateResponse>>()
    }
    val hairStyleDetailResult: SingleLiveEvent<Result<SimpleResponse<HairStyle>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<HairStyle>>>()
    }

    private val hairStyleDao: HairStyleDao by lazy {
        AppDatabase.getInstance(SalonApplication.applicationContext()).getHairStyleDao()
    }

    fun getHairStyleBySalonId(salonId: Int) = hairStyleDao.getAllHairStyleBySalonId(salonId)
    fun getHairStyleById(hairId: Int) = hairStyleDao.getAllHairStyleById(hairId)
    fun getHairStyleGallery() = hairStyleDao.getAllHairStyle()
    suspend fun predictHairStyle(file: File) {
        try {
            val image = RequestBody.create(
                MediaType.parse("multipart/form-data"), file
            )
            hairStyleResult.postValue(Result.loading())
            val body = MultipartBody.Part.createFormData("file", file.name, image)
            val result = appApi.predictHairStyle(body)
            hairStyleResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            ex.printStackTrace()
            withContext(mainThread()) {
                if (ex is HttpException) {
                    hairStyleResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    hairStyleResult.value = Result.errorNetwork(null)
                }
            }


        }
    }

    suspend fun updateDownloadHairStyle(authHeader: String, listId: List<Int>) {
        val params = HairStyleParams(listId)
        try {
            appApi.updateDownloadCount(authHeader, params)
        } catch (ex: Exception) {

        }

    }

    suspend fun updateViewHairStyle(authHeader: String, hairId: Int) {
        appApi.updateViewCount(authHeader, hairId)
    }

    suspend fun getHairStyleDetail(token: String, idHair: Int) {
        try {
            hairStyleDetailResult.postValue(Result.loading())
            val result = appApi.getHairStyleDetail(token, idHair)
            hairStyleDetailResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    hairStyleDetailResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    hairStyleDetailResult.value = Result.errorNetwork(null)
                }
            }

        }
    }
}