package linh.dev.salonapp.model.local

import linh.dev.salonapp.DEFAULT
import linh.dev.salonapp.EMPTY

class Stylist(
    val id: Int,
    val name: String,
    val email: String,
    val jobId: Int?,
    val sex: String,
    val avatar: String?,
    val user_type: Int,
    val phoneNumber: String,
    val start_time: Int,
    val end_time: Int
) {
    companion object {
        fun defaultStylist(): Stylist {
            return Stylist(Int.MIN_VALUE, DEFAULT, EMPTY, 0, EMPTY, EMPTY, 1, EMPTY, 0, 0)
        }

    }

    fun isDefaultStylist(): Boolean {
        return id == Int.MIN_VALUE
    }

}
