package linh.dev.salonapp.ui.fragment.customers

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.asksira.loopingviewpager.LoopingViewPager
import kotlinx.android.synthetic.main.fragment_customer_home_tab.*
import kotlinx.android.synthetic.main.layout_no_data.*
import linh.dev.base.BaseFragment
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.SalePagerAdapter
import linh.dev.salonapp.adapter.SalonAdapter
import linh.dev.salonapp.databinding.FragmentCustomerHomeTabBinding
import linh.dev.salonapp.ext.isValidPhoneNumber
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.SalonViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class HomeTabFragment : BaseFragment<FragmentCustomerHomeTabBinding>() {
    private val viewModel: SalonViewModel by viewModel()
    private val salonNewAdapter: SalonAdapter by inject()
    private val popularSalonAdapter: SalonAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_customer_home_tab
    }

    companion object {
        fun newInstance(): HomeTabFragment = HomeTabFragment()
    }

    override fun initView() {
        super.initView()
        btnTryAgain.hide()
        viewNoData.hide()
        initViewPager()
        initRecycleView()

    }

    private fun initRecycleView() {
        rcvSalonPopular.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val itemDecoration = SpaceItemDecoration(10, 10, 30, 10, 10)
        rcvSalonPopular.addItemDecoration(itemDecoration)
        rcvSalonPopular.adapter = popularSalonAdapter

        rcvSalonNew.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rcvSalonNew.addItemDecoration(itemDecoration)
        rcvSalonNew.adapter = salonNewAdapter
        imvSearch.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(R.id.action_customerHomeFragment_to_searchSalonFragment, null)
        }
        tvViewMoreSalon.setOnClickListener{
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(R.id.action_customerHomeFragment_to_searchSalonFragment, null)
        }
        tvViewMoreSalonPopular.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(R.id.action_customerHomeFragment_to_searchSalonFragment, null)
        }
        viewModel.updateResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    layoutLoadingUpdate.show()
                }
                Result.Status.SUCCESS -> {
                    layoutLoadingUpdate.hide()
                    it.data?.let { msg ->
                        showToast(msg)
                    }

                }
                Result.Status.ERROR -> {
                    layoutLoadingUpdate.hide()
                    it.message?.let { msg ->
                        showToast(msg)
                    }
                }
                Result.Status.UNAUTHORIZED -> {
                    layoutLoadingUpdate.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
        viewModel.errorUnAuthorized.observe(viewLifecycleOwner, Observer {
            activity?.let { context ->
                startActivity(Intent(context, MainActivity::class.java))
                context.finish()
            }
        })
        viewModel.progressSaleSalonLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressSaleLoading.show()
            } else {
                progressSaleLoading.hide()
            }

        })
        viewModel.progressNewSalonLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressNewLoading.show()
                layoutHeaderListSalon.visibility = View.GONE
            } else {
                progressNewLoading.hide()

            }

        })
        viewModel.progressPopularSalonLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressPopularLoading.show()
                layoutHeaderListSalonPopular.visibility = View.GONE
            } else {
                progressPopularLoading.hide()
            }

        })
        viewModel.salonPopularData.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                layoutHeaderListSalonPopular.show()
                if (it.size == 5) {
                    tvViewMoreSalonPopular.show()
                } else {
                    tvViewMoreSalonPopular.hide()
                }
            } else {
                layoutHeaderListSalonPopular.hide()
                tvViewMoreSalonPopular.hide()
            }
            popularSalonAdapter.setItems(it)

        })
        viewModel.salonNewData.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                layoutHeaderListSalon.show()
                if (it.size == 5) {
                    tvViewMoreSalon.show()
                } else {
                    tvViewMoreSalon.hide()
                }
            } else {
                layoutHeaderListSalon.hide()
                tvViewMoreSalon.hide()
            }
            salonNewAdapter.setItems(it)
        })

        salonNewAdapter.setOnViewClickListener(object : BaseRecycleViewAdapter.ViewClickListener {
            override fun onViewItemClock(resId: Int, position: Int) {
                if (resId == R.id.buttonBook) {
                    if (checkPhoneNumber()) {
                        val item = salonNewAdapter.getItem(position)
                        val bundle = Bundle()
                        bundle.putInt(BUNDLE_ID_SALON, item.id)
                        bundle.putString(BUNDLE_NAME_SALON, item.name)
                        bundle.putInt(BUNDLE_OPEN_TIME_SALON, item.open_time)
                        bundle.putInt(BUNDLE_CLOSE_TIME_SALON, item.close_time)
                        bundle.putInt(BUNDLE_AVG_TIME_SALON, item.avg_time_slot)
                        bundle.putString(BUNDLE_THUMBNAIL_SALON, item.thumb_url)
                        bundle.putString(BUNDLE_DETAIL_LOCATION_SALON, item.detail_location)
                        item.total_rate_service?.let {
                            bundle.putFloat(BUNDLE_TOTAL_RATE_SALON, it)
                        }
                        Navigation.findNavController(requireActivity(), R.id.nav_main)
                            .navigate(
                                R.id.action_customerHomeFragment_to_salonDetailFragment,
                                bundle
                            )
                    } else {
                        showDialogUpdatePhone()
                    }

                }
            }
        })
        popularSalonAdapter.setOnViewClickListener(object :
            BaseRecycleViewAdapter.ViewClickListener {
            override fun onViewItemClock(resId: Int, position: Int) {
                if (resId == R.id.buttonBook) {
                    if (checkPhoneNumber()) {
                        val item = popularSalonAdapter.getItem(position)
                        val bundle = Bundle()
                        bundle.putInt(BUNDLE_ID_SALON, item.id)
                        bundle.putString(BUNDLE_NAME_SALON, item.name)
                        bundle.putInt(BUNDLE_OPEN_TIME_SALON, item.open_time)
                        bundle.putInt(BUNDLE_CLOSE_TIME_SALON, item.close_time)
                        bundle.putInt(BUNDLE_AVG_TIME_SALON, item.avg_time_slot)
                        bundle.putString(BUNDLE_THUMBNAIL_SALON, item.thumb_url)
                        bundle.putString(BUNDLE_DETAIL_LOCATION_SALON, item.detail_location)
                        item.total_rate_service?.let {
                            bundle.putFloat(BUNDLE_TOTAL_RATE_SALON, it.toFloat())
                        }
                        Navigation.findNavController(requireActivity(), R.id.nav_main)
                            .navigate(
                                R.id.action_customerHomeFragment_to_salonDetailFragment,
                                bundle
                            )
                    } else {
                        showDialogUpdatePhone()
                    }

                }
            }
        })
    }

    private fun checkPhoneNumber(): Boolean {
        val user = user()
        user?.let {
            return it.phoneNumber != null

        }
        return false
    }


    private fun showDialogUpdatePhone() {
        context?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(getString(R.string.title_dialog_update_phone_number))
            builder.setMessage(getString(R.string.msg_dialog_update_phone_number))

            val input = EditText(it)
            input.hint = getString(R.string.phone_number_hint)
            input.inputType = InputType.TYPE_CLASS_PHONE
            builder.setView(input)

            builder.setPositiveButton(getString(R.string.lbl_update_common)) { p0, p1 ->
                val phone = input.text.toString()
                if (!phone.isValidPhoneNumber()) {
                    showToast(getString(R.string.phone_number_invalid_error_msg))
                } else {
                    viewModel.updatePhoneNumber(phone)
                    p0.dismiss()
                }
            }
            builder.setNegativeButton(
                getString(R.string.cancel_dialog_common)
            ) { p0, p1 -> p0.dismiss() }

            builder.show()
        }

    }

    private fun initViewPager() {
        val salePageAdapter = SalePagerAdapter(context, true)
        salePager.adapter = salePageAdapter
        salePager.offscreenPageLimit = 3
        tabDots.count = salePager.indicatorCount
        salePager.setIndicatorPageChangeListener(object :
            LoopingViewPager.IndicatorPageChangeListener {
            override fun onIndicatorProgress(selectingPosition: Int, progress: Float) {
                tabDots?.setProgress(selectingPosition, progress)

            }

            override fun onIndicatorPageChange(newIndicatorPosition: Int) {
            }

        })
        viewModel.saleData.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) {
                viewNoData.show()
            } else {
                viewNoData.hide()
            }
            salePageAdapter.setItemList(it)
            tabDots.count = salePager.indicatorCount
            salePager.reset()
        })
    }

    override fun onResume() {
        super.onResume()
        salePager.resumeAutoScroll()
    }

    override fun onPause() {
        super.onPause()
        salePager.pauseAutoScroll()
    }
}