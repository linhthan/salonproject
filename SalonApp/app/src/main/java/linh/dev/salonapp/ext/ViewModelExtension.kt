package linh.dev.salonapp.ext

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.PREF_USER
import linh.dev.salonapp.SalonApplication
import linh.dev.salonapp.model.local.User

fun user():User?{
    val userPref = SalonApplication.applicationContext().getSharedPreferences(
        SalonApplication.applicationContext().packageName,
        Context.MODE_PRIVATE
    ).getString(PREF_USER, EMPTY)
    return if (!userPref.isNullOrEmpty()) {
        GsonBuilder().create().fromJson(userPref, object : TypeToken<User>() {}.type)
    }else{
        null
    }
}