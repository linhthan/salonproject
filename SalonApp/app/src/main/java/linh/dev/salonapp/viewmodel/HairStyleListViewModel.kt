package linh.dev.salonapp.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.work.*
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.databases.model.HairStyle
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.Gallery
import linh.dev.salonapp.network.ImageDownloadWorker
import linh.dev.salonapp.repository.HairSalonRepository

class HairStyleListViewModel(
    val salonId: Int,
    val hairStyleJson: String,
    val repository: HairSalonRepository
) :
    BaseViewModel() {
    val listOfImages: List<linh.dev.salonapp.model.local.HairStyle> by lazy {
        val gSon = Gson()
        return@lazy gSon.fromJson<List<linh.dev.salonapp.model.local.HairStyle>>(
            hairStyleJson,
            object : TypeToken<List<linh.dev.salonapp.model.local.HairStyle>>() {}.type
        )
    }
    val hairStyleLocal: LiveData<List<HairStyle>> by lazy {
        repository.getHairStyleBySalonId(salonId)
    }
    val gallery: MutableLiveData<Gallery> by lazy {
        MutableLiveData<Gallery>()
    }

    fun processGallery(hairStyleLocal: List<HairStyle>) {
        val itemDownload = getListHairStyleToDownload(hairStyleLocal)
        logE("Item download ${itemDownload.size}")
        gallery.value = Gallery(salonId, itemDownload)
    }

    private fun getListHairStyleToDownload(hairStyleLocal: List<HairStyle>): ArrayList<linh.dev.salonapp.model.local.HairStyle> {
        val result = ArrayList<linh.dev.salonapp.model.local.HairStyle>()
        val mapRemoteHairStyle = HashMap<Int, linh.dev.salonapp.model.local.HairStyle>()
        listOfImages.forEach {
            mapRemoteHairStyle[it.id] = it
        }
        logE("List images ${listOfImages.size}")

        hairStyleLocal.forEach {
            if (mapRemoteHairStyle.containsKey(it.idHair)) {
                mapRemoteHairStyle.remove(it.idHair)
            }
        }
        result.addAll(mapRemoteHairStyle.values)
        return result
    }

    fun downloadListImage(context: Context, images: List<linh.dev.salonapp.model.local.HairStyle>) {
        val jsonImages = Gson().toJson(images)
        val data = Data.Builder()
            .putString("images", jsonImages)
            .build()

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)

        val oneTimeRequest = OneTimeWorkRequest.Builder(ImageDownloadWorker::class.java)
            .setInputData(data)
            .setConstraints(constraints.build())
            .addTag("download")
            .build()



        WorkManager.getInstance(context).enqueueUniqueWork("SalonAppDownload", ExistingWorkPolicy.APPEND, oneTimeRequest)


    }

}