package linh.dev.salonapp.viewmodel

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Service
import linh.dev.salonapp.model.local.User
import linh.dev.salonapp.repository.SalonRepository
import java.lang.Exception

class SalonDetailViewModel(private val salonId: Int, private val salonRepository: SalonRepository) :
    BaseViewModel() {
    private val job = SupervisorJob()
    private val launchScope = CoroutineScope(Dispatchers.IO + job)
    val stylistData = MutableLiveData<List<User>>()
    val serviceData = MutableLiveData<List<Service>>()
    val progressLoading = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()

    val serviceSelected = MutableLiveData<List<Service>>()

    init {
        progressLoading.value = false
        errorMessage.value = EMPTY
        serviceSelected.value = ArrayList()
    }


    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}