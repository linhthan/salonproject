package linh.dev.salonapp.model.local

class Slot(val time: Int, var slotCount: Int) {
    fun isAvailable(): Boolean {
        return slotCount > 0
    }
}