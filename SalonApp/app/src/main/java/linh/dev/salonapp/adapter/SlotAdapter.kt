package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import kotlinx.android.synthetic.main.view_item_slot.view.*
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemSlotBinding
import linh.dev.salonapp.model.local.Slot

class SlotAdapter : BaseRecycleViewAdapter<Slot>() {
    private var selectedPosition = -1

    override fun getItem(position: Int): Slot = dataSources[position]

    override fun setItems(items: Collection<Slot>) {
        selectedPosition = -1
        super.setItems(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemSlotBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_slot,
            parent,
            false
        )
        return ViewHolder(binding)
    }


    fun getSelectedPosition(): Int {
        return selectedPosition
    }

    fun getItemSelected(): Slot? {
        if (selectedPosition in (0 until itemCount)) return getItem(selectedPosition)
        return null
    }

    fun onSlotSelected(position: Int) {
        if (selectedPosition == position) {
            selectedPosition = -1
            notifyItemChanged(position)
        } else {
            val oldPosition = selectedPosition
            selectedPosition = position
            notifyItemChanged(oldPosition)
            notifyItemChanged(selectedPosition)
        }
    }

    inner class ViewHolder(val binding: ViewDataBinding) :
        BaseRecycleViewAdapter<Slot>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            (binding as ViewItemSlotBinding).viewModel = getItem(position)
            if (selectedPosition != 1 && position == selectedPosition) {
                binding.root.layoutSlot.isSelected = true
                binding.root.layoutSlot.background = ContextCompat.getDrawable(
                    binding.root.context,
                    R.drawable.bg_tv_slot_rounder_corner_border_gray
                )
                binding.root.tvSlotTime.setTextColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.white
                    )
                )
            } else {
                if (getItem(position).isAvailable()) {
                    binding.root.layoutSlot.background = ContextCompat.getDrawable(
                        binding.root.context,
                        R.drawable.bg_tv_slot_rounder_corner_border_gray
                    )
                    binding.root.tvSlotTime.setTextColor(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.black
                        )
                    )
                } else {
                    binding.root.layoutSlot.background = ContextCompat.getDrawable(
                        binding.root.context,
                        R.color.white
                    )
                    binding.root.tvSlotTime.setTextColor(
                        ContextCompat.getColor(
                            binding.root.context,
                            R.color.slot_unavailable_text_color
                        )
                    )
                }

                binding.root.layoutSlot.isSelected = false

            }
            binding.root.layoutSlot.setOnClickListener {
                onViewClick(R.id.layoutSlot)
            }
            binding.executePendingBindings()

        }

    }

}