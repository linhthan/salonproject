package linh.dev.salonapp.model.local

class Salon(
    val id: Int,
    val name: String,
    val thumb_url: String,
    val open_time: Int,
    val close_time: Int,
    val avg_time_slot: Int,
    val detail_location: String,
    val salon_create_at: String,
    val total_rate: Float?,
    val totalStaff:Int,
    val adminName:String,
    val total_rate_service: Float?,
    val styles:List<HairStyle>
) {
}