package linh.dev.salonapp.ui.fragment.customers

import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.view.View
import androidx.core.content.FileProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.fragment_crop.*
import linh.dev.base.BaseFragment
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.base.FileUtil
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.RatioAdapter
import linh.dev.salonapp.databinding.FragmentCropBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.Ratio
import linh.dev.salonapp.viewmodel.SalonViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import java.io.File

class CropFragment : BaseFragment<FragmentCropBinding>(),
    CropImageView.OnCropImageCompleteListener {


    val args: CropFragmentArgs by navArgs()
    val adapter: RatioAdapter by inject()

    var cropPath = EMPTY
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_crop
    }


    override fun initView() {
        super.initView()
        cropView.setOnCropImageCompleteListener(this)
        cropView.setAspectRatio(1, 1)
        cropView.setFixedAspectRatio(true)
        imvClose.setOnClickListener {
            activity!!.onBackPressed()
        }
        imvApply.setOnClickListener {
            context?.let { mContext ->
                val photoFile =
                    createImageFile()
                if (photoFile != null && context != null) {
                    val uriFile = FileProvider.getUriForFile(
                        requireContext(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile
                    )
                    if(args.mode==0){
                        cropView.saveCroppedImageAsync(uriFile,Bitmap.CompressFormat.PNG,100,CROP_IMAGE_SIZE,CROP_IMAGE_SIZE)
                    }else{
                        cropView.saveCroppedImageAsync(uriFile,Bitmap.CompressFormat.PNG,100,CROP_AVATAR_SIZE,CROP_AVATAR_SIZE)
                    }

                }
            }
        }
        cropView.setImageUriAsync(Uri.fromFile(File(args.filePath)))
    }
    private fun createImageFile(

    ): File? {
        val timeStamp: String = System.currentTimeMillis().toString()
        val storageDir: File? = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            cropPath = absolutePath
        }
    }
    override fun onCropImageComplete(view: CropImageView?, result: CropImageView.CropResult?) {
        if (result != null && result.isSuccessful && cropPath.isNotEmpty()) {
//            salonViewModel.imagePathHairStylePreview.value = cropPath
//            Navigation.findNavController(requireActivity(), R.id.nav_main).popBackStack()
            activity?.onBackPressed()
            bus.post(AppAction(AppEvent.EVENT_CROP_SUCCESS.setData(cropPath)))
        } else {
            showToast(result?.error.toString())
        }

    }
}