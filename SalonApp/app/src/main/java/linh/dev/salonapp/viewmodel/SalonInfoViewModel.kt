package linh.dev.salonapp.viewmodel

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.SalonRepository

class SalonInfoViewModel(salonId: Int?,val  repository: SalonRepository) : BaseViewModel() {
    val salonInfoResult = repository.salonInfoResult
    init {
        salonId?.let {
            getSalonInfo(it)
        }

    }
    fun getSalonInfo(salonId:Int){
        user()?.let {
            viewModelScope.launch {
                withContext(io()){
                    repository.getSalonInfo(it.getBearToken(),salonId)
                }
            }
        }

    }
}