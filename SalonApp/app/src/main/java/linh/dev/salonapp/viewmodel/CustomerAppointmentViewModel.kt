package linh.dev.salonapp.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.*
import linh.dev.salonapp.data.AppointmentDataSource
import linh.dev.salonapp.data.AppointmentDataSourceFactory
import linh.dev.salonapp.data.AppointmentHistoryDataSource
import linh.dev.salonapp.data.AppointmentHistoryDataSourceFactory
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.ext.mainThread
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.model.local.User
import linh.dev.salonapp.repository.UserRepository
import linh.dev.salonapp.ui.custom.SingleLiveEvent

class CustomerAppointmentViewModel(val userRepository: UserRepository) : BaseViewModel() {

    val listAppointment = userRepository.listAppointment
    val cancelResult = userRepository.cancelAppointmentResult


    init {
        loadAllAppointment()
    }

    fun loadAllAppointment() {
        viewModelScope.launch {
            user()?.let {
                withContext(io()) {
                    userRepository.getPendingAppointmentByUser(it.getBearToken())
                }
            }
        }

    }

    fun cancelAppointment(appointment: Appointment) {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    userRepository.cancelAppointment(appointment.id, it.getBearToken())
                }
            }
        }
    }

}