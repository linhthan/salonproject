package linh.dev.salonapp.ui.fragment

import android.content.Intent
import android.util.Log
import androidx.navigation.Navigation
import linh.dev.base.BaseFragment
import linh.dev.salonapp.*
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.ui.activitys.CustomerHomeActivity
import linh.dev.salonapp.ui.activitys.HistoryAppointmentActivity

class MainFragment : BaseFragment<linh.dev.salonapp.databinding.FragmentMainBinding>() {
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_main
    }


    override fun initView() {
        Log.e("init", "intiView")
        super.initView()
//        val user = PrefUtil.getSharePreference(SalonApplication.applicationContext(), PREF_USER, EMPTY)
        context?.let {
            val user = user()
            if (user != null) {
                when (user.user_type) {
                    CUSTOMER_TYPE -> {
                        startActivity(Intent(context, CustomerHomeActivity::class.java))
                        activity?.finish()
                    }
                    SERVICE_EMPLOYEE -> {
                        startActivity(Intent(context, HistoryAppointmentActivity::class.java))
                        activity?.finish()
                    }

                    else -> Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
                        .navigate(R.id.action_mainFragment_to_authenticateFragment)
                }

            } else {
                Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
                    .navigate(R.id.action_mainFragment_to_authenticateFragment)
            }

        }

//        if (user.isNullOrEmpty()) {
//            Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
//                .navigate(R.id.action_mainFragment_to_authenticateFragment)
//        } else {
//            startActivity(Intent(context, CustomerHomeActivity::class.java))
//            activity?.finish()
//        }


    }


}
