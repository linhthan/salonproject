package linh.dev.salonapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import kotlinx.coroutines.CoroutineScope
import linh.dev.salonapp.data.AppointmentDataSource
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.model.local.Order
import linh.dev.salonapp.network.AppApi

class OrderDataSourceFactory(
    private val appApi: AppApi,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, Order>() {

    val orderDataSourceLiveData = MutableLiveData<OrderDataSource>()

    override fun create(): DataSource<Int, Order> {
        val newsDataSource = OrderDataSource(appApi, scope)
        orderDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}