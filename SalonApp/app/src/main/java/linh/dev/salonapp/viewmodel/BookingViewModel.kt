package linh.dev.salonapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Service
import linh.dev.salonapp.model.local.Slot
import linh.dev.salonapp.model.local.Stylist
import linh.dev.salonapp.repository.SalonRepository
import linh.dev.salonapp.util.DateTimeUtils

class BookingViewModel(
    salonId: Int,
    duration: Int,
    private val salonRepository: SalonRepository
) : BaseViewModel() {
    private val bookingJob = SupervisorJob()
    private val stylistJob = SupervisorJob()
    val dataBookingSlot = MutableLiveData<List<Slot>>()
    val slotSelected = MutableLiveData<Int>()
    val confirmBookingStatus = MutableLiveData<Boolean>()
    val confirmBookingResult = salonRepository.confirmBookingResult
    val stylistResult = salonRepository.stylistResult
    val salonSlotResult = salonRepository.salonSlotResult

    init {

        slotSelected.value = -1
        loadAllBooking(
            salonId, duration,
            DateTimeUtils.getDayOfWeek(),
            DateTimeUtils.getDateTodayForBooking()
        )
    }

    fun loadAllBooking(salonId: Int, duration: Int, dayOfWeek: String, dateSelected: String) {
        user()?.let {
            viewModelScope.launch {
                salonRepository.getSalonSlotAvailable(
                    it.getBearToken(),
                    salonId,
                    dateSelected, duration
                )
                salonRepository.getStylistAvailable(
                    it.getBearToken(),
                    salonId,
                    dayOfWeek
                )
            }
        }
    }

    fun loadStylistAvailable(salonId: Int, dayOfWeek: String) {
        user()?.let {
            viewModelScope.launch {
                salonRepository.getStylistAvailable(
                    it.getBearToken(),
                    salonId,
                    dayOfWeek
                )
            }
        }

    }

    fun loadBookingByStylist(
        salonId: Int,
        stylistId: Int,
        duration: Int,
        dateSelected: String
    ) {
        user()?.let {
            viewModelScope.launch {
                if (stylistId != Int.MIN_VALUE)
                    salonRepository.getSalonSlotAvailableByStylist(
                        it.getBearToken(),
                        salonId, stylistId,
                        dateSelected, duration
                    ) else salonRepository.getSalonSlotAvailable(
                    it.getBearToken(),
                    salonId,
                    dateSelected, duration
                )
            }
        }


    }

    fun applyBooking(
        salonId: Int,
        services: List<Service.ServiceJson>,
        time_booking: Int,
        date_booking: String,
        duration: Int
    ) {
        user()?.let {
            val customerId = it.id
            val phoneNumber = it.phoneNumber
            phoneNumber?.let { phoneNo ->
                viewModelScope.launch {
                    withContext(io()){
                        salonRepository.confirmBooking(
                            it.getBearToken(),
                            salonId,
                            services,
                            time_booking,
                            date_booking,
                            duration,
                            customerId, phoneNo, it.name
                        )

                    }
                }

//                applyBookingJobLaunchScope.launch {
//                    try {
//                        withContext(Dispatchers.Main) {
//                            progressConfirmBooking.value = true
//                        }
//                        val response = salonRepository.confirmBooking(
//                            it.getBearToken(),
//                            salonId,
//                            services,
//                            time_booking,
//                            date_booking,
//                            duration,
//                            customerId, phoneNo, it.name
//                        )
//
//                        withContext(Dispatchers.Main) {
//                            progressConfirmBooking.value = false
//                            messageComfirm.value = response.message
//                            confirmBookingStatus.value = response.success
//                        }
//
//                    } catch (ex: Exception) {
//                        withContext(Dispatchers.Main) {
//                            progressConfirmBooking.value = false
//                            confirmBookingStatus.value = false
//                            messageComfirm.value = ex.message
//                            logE(ex.javaClass)
//                            logE(ex.message)
//                        }
//
//                    }
//
//                }
            }

        }


    }

    override fun onCleared() {
        super.onCleared()
        bookingJob.cancel()
        stylistJob.cancel()
    }
}