package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ItemRatioBinding
import linh.dev.salonapp.model.local.Ratio

class RatioAdapter(private val items: ArrayList<Ratio>) : BaseRecycleViewAdapter<Ratio>() {

    override fun setItems(items: Collection<Ratio>) {
        if (this.items.isNullOrEmpty()) return
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): BaseViewHolder {
        val binding: ItemRatioBinding = DataBindingUtil.inflate(
            LayoutInflater.from(p0.context),
            R.layout.item_ratio, p0, false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size
    override fun getItem(position: Int): Ratio {
        return items[position]
    }

    inner class ViewHolder(private val binding: ItemRatioBinding) : BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.ratio = getItem(position)
            binding.executePendingBindings()
        }

    }

    fun updateSelected(position: Int) {
        if (position < 0 || position >= items.size) return
        items.withIndex().first {
            it.value.isSelected
        }.apply {
            this.value.isSelected = false
            notifyItemChanged(this.index)
        }
        items[position].isSelected = true

        notifyItemChanged(position)
    }
}