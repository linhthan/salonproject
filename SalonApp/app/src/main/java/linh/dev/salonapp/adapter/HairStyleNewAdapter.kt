package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemNewHairStyleBinding
import linh.dev.salonapp.model.local.HairStyle

class HairStyleNewAdapter : BaseRecycleViewAdapter<HairStyle>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemNewHairStyleBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_new_hair_style,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun addItem(item: HairStyle) {
        if (dataSources.isNotEmpty()) {
            val positionRemove = dataSources.size - 1
            dataSources.removeAt(dataSources.size - 1)
            notifyItemRemoved(positionRemove)
        }
        dataSources.add(0, item)
        notifyItemInserted(0)
    }

    inner class ViewHolder(val binding: ViewItemNewHairStyleBinding) :
        BaseRecycleViewAdapter<HairStyle>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.viewModel = getItem(position)
        }

    }
}