package linh.dev.salonapp.viewmodel

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.repository.UserRepository

class AppointmentUserViewModel (val userRepository: UserRepository) : BaseViewModel()  {
    val listAppointment = userRepository.listAppointment
    val cancelResult = userRepository.cancelAppointmentResult


    init {
        loadAllAppointment()
    }

    fun loadAllAppointment() {
        viewModelScope.launch {
            user()?.let {
                withContext(io()) {
                    userRepository.getPendingAppointmentByUser(it.getBearToken())
                }
            }
        }

    }

    fun cancelAppointment(appointment: Appointment) {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    userRepository.cancelAppointment(appointment.id, it.getBearToken())
                }
            }
        }
    }

}