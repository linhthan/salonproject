package linh.dev.salonapp.ui.fragment.customers

import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_preview_result.*
import linh.dev.base.BaseFragment
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.FragmentPreviewResultBinding

class PreviewResultFragment : BaseFragment<FragmentPreviewResultBinding>() {
    private val args: PreviewResultFragmentArgs by navArgs()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_preview_result
    }

    override fun initView() {
        super.initView()
        mBinding.filePath = args.filePath
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }
    }
}