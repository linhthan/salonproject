package linh.dev.salonapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.PREF_USER
import linh.dev.salonapp.SalonApplication
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Sale
import linh.dev.salonapp.model.local.Salon
import linh.dev.salonapp.repository.SalonRepository
import linh.dev.salonapp.repository.UserRepository
import linh.dev.salonapp.util.PrefUtil
import retrofit2.HttpException

class SalonViewModel(private val salonRepository: SalonRepository, val userRepository: UserRepository) :
    BaseViewModel() {
    private val job = SupervisorJob()
    private val launchScope = CoroutineScope(Dispatchers.IO + job)
    val progressLoading = MutableLiveData<Boolean>()
    val progressNewSalonLoading = MutableLiveData<Boolean>()
    val progressPopularSalonLoading = MutableLiveData<Boolean>()
    val progressSaleSalonLoading = MutableLiveData<Boolean>()
    val saleData = MutableLiveData<List<Sale>>()
    val salonPopularData = MutableLiveData<List<Salon>>()
    val salonNewData = MutableLiveData<List<Salon>>()
    val errorUnAuthorized = MutableLiveData<Boolean>()
    val updateResult = userRepository.updateResult
    var  currentTab = 0


    var user = user()

    init {
        progressLoading.value = false
        progressNewSalonLoading.value = false
        progressPopularSalonLoading.value = false
        progressSaleSalonLoading.value = false
        saleData.value = ArrayList()
        salonPopularData.value = ArrayList()
        salonNewData.value = ArrayList()

        loadAllSalon()
    }


     fun loadAllSalon() {
        user?.let {
            launchScope.launch {
                withContext(Dispatchers.Main) {
                    progressLoading.value = true
                    progressSaleSalonLoading.value = true
                    progressNewSalonLoading.value = true
                    progressPopularSalonLoading.value = true
                }
                loadSale(it.getBearToken())
                loadNewSalon(it.getBearToken())
                loadPopularSalon(it.getBearToken())

            }
        }

    }

    private suspend fun loadSale(token: String) {
        try {
            val saleResponse = salonRepository.getAllSale(token)
            withContext(Dispatchers.Main) {
                progressSaleSalonLoading.value = false

                saleResponse.let {
                    val listSales = it.data
                    if (listSales.isNullOrEmpty()) {

                        saleData.value = ArrayList()
                    } else {
                        //TODO update UI empty sale
                        saleData.value = listSales
                    }

                }
            }
        } catch (ex: Exception) {
            withContext(Dispatchers.Main) {
                progressSaleSalonLoading.value = false
                handleUnAuthorized(ex)
            }


        }
    }

    private suspend fun loadNewSalon(token: String) {
        try {
            val salonNewResponse = salonRepository.getNewSalon(token)
            withContext(Dispatchers.Main) {
                progressNewSalonLoading.value = false

                salonNewResponse.let {
                    val listSales = it.data
                    if (listSales.isNotEmpty()) {
                        salonNewData.value = listSales
                    } else {
                        //TODO update UI empty sale
                        salonNewData.value = ArrayList()
                    }

                }
            }
        } catch (ex: Exception) {
            withContext(Dispatchers.Main) {
                progressNewSalonLoading.value = false
                handleUnAuthorized(ex)
            }

        }
    }

    private suspend fun loadPopularSalon(token: String) {
        try {
            val salonPopularResponse = salonRepository.getPopularSalon(token)
            withContext(Dispatchers.Main) {
                progressPopularSalonLoading.value = false

                salonPopularResponse.let {
                    val listSales = it.data
                    if (listSales.isNotEmpty()) {
                        salonPopularData.value = listSales
                    } else {
                        //TODO update UI empty sale
                        salonPopularData.value = ArrayList()
                    }

                }
            }
        } catch (ex: Exception) {
            withContext(Dispatchers.Main) {
                progressPopularSalonLoading.value = false
                handleUnAuthorized(ex)
            }


        }
    }

    fun updatePhoneNumber(phoneNumber: String) {
        user?.let {
            viewModelScope.launch {
                userRepository.updatePhoneNumber(it.getBearToken(), phoneNumber)
            }
        }

    }

    private fun handleUnAuthorized(ex: Exception) {
        ex.printStackTrace()
        if (ex is HttpException && ex.code() == 401 && user() != null) {
            processUnAuthorizedExp()
        }
    }

    fun processUnAuthorizedExp() {
        if (PrefUtil.removeSharePreference(
                SalonApplication.applicationContext(),
                PREF_USER
            )
        ) {
            errorUnAuthorized.value = true
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}