package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Appointment

data class AppointmentResponse(val message:String,val data:List<Appointment>)