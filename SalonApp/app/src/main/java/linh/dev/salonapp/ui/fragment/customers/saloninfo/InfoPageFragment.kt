package linh.dev.salonapp.ui.fragment.customers.saloninfo

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_info_salon_page.*
import kotlinx.android.synthetic.main.layout_no_data.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.salonapp.*
import linh.dev.salonapp.databinding.FragmentInfoSalonPageBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.DateTimeUtils
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.SalonInfoViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class InfoPageFragment : BaseFragment<FragmentInfoSalonPageBinding>() {
    var salonId: Int? = null
    private val viewModel: SalonInfoViewModel by viewModel { parametersOf(salonId) }
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_info_salon_page
    }

    companion object {
        fun newInstance(salonId: Int): InfoPageFragment {
            val fragment = InfoPageFragment()
            fragment.salonId = salonId
            return fragment
        }
    }

    override fun initView() {
        super.initView()

        btnTryAgain.setOnClickListener {
            salonId?.let {
                viewModel.getSalonInfo(it)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.salonInfoResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressSalonLoading.show()
                }

                Result.Status.SUCCESS -> {
                    progressSalonLoading.hide()
                    if (it.data != null) {
                        if (it.data.success) {
                            viewNoData.hide()
                            val salon = it.data.data
                            tvOpenCloseTime.text = getString(
                                R.string.format_open_close_time,
                                DateTimeUtils.convertMinuteToSlotDisplayTime(salon.open_time),
                                DateTimeUtils.convertMinuteToSlotDisplayTime(salon.close_time)
                            )
                            tvAdminName.text = salon.adminName
                            tvContentLocation.text = salon.detail_location
                            tvCreated.text = DateTimeUtils.formatDateString(
                                salon.salon_create_at, SERVER_TIME_FORMAT,
                                CREATE_AT_TIME_FORMAT
                            )
                            tvNumberStaff.text = salon.totalStaff.toString()
                            layoutItem.show()
                        } else {
                            viewNoData.show()
                        }
                    } else {
                        viewNoData.show()
                    }
                }
                Result.Status.ERROR -> {
                    progressSalonLoading.hide()
                    viewNoData.show()
                }
                Result.Status.UNAUTHORIZED -> {
                    progressSalonLoading.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
    }
}