package linh.dev.salonapp.repository

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import linh.dev.base.BaseRepository
import linh.dev.salonapp.PER_PAGE_HAIR_STYLE
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.ext.mainThread
import linh.dev.salonapp.model.local.*
import linh.dev.salonapp.model.remote.*
import linh.dev.salonapp.network.AppApi
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SingleLiveEvent
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.HttpException
import java.io.File


class SalonRepository(val api: AppApi) : BaseRepository() {
    val collectPhotoResult: SingleLiveEvent<Result<CollectPhotoResponse>> by lazy {
        logE("Init collect")
        SingleLiveEvent<Result<CollectPhotoResponse>>()
    }
    val saveHairStyleResult: SingleLiveEvent<Result<SimpleResponse<HairStyle>>> by lazy {
        logE("Init saveHairStyleResult")
        SingleLiveEvent<Result<SimpleResponse<HairStyle>>>()
    }
    val newHairStyleResult: SingleLiveEvent<Result<HairStyleSalonResponse>> by lazy {
        logE("Init newHairStyleResult")
        SingleLiveEvent<Result<HairStyleSalonResponse>>()
    }
    val allHairStyleResult: SingleLiveEvent<Result<SimpleResponse<HairStyleAllResponse>>> by lazy {
        logE("Init allHairStyleResult")
        SingleLiveEvent<Result<SimpleResponse<HairStyleAllResponse>>>()
    }
    val moreHairStyleResult: SingleLiveEvent<Result<SimpleResponse<HairStyleAllResponse>>> by lazy {
        logE("Init moreHairStyleResult")
        SingleLiveEvent<Result<SimpleResponse<HairStyleAllResponse>>>()
    }
    val topArtOfSalonResult: SingleLiveEvent<Result<SimpleResponse<List<Art>>>> by lazy {
        logE("Init topArtOfSalonResult")
        SingleLiveEvent<Result<SimpleResponse<List<Art>>>>()
    }
    val orderItemResult: SingleLiveEvent<Result<SimpleResponse<List<OrderItem>>>> by lazy {
        logE("Init orderItemResult")
        SingleLiveEvent<Result<SimpleResponse<List<OrderItem>>>>()
    }
    val cancelOrderResult: SingleLiveEvent<Result<SimpleResponse<Any>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Any>>>()
    }

    val confirmBookingResult: SingleLiveEvent<Result<SimpleResponse<Appointment>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Appointment>>>()
    }
    val removeArtResult: SingleLiveEvent<Result<SimpleResponse<Int>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Int>>>()
    }
    val rateBookingResult: SingleLiveEvent<Result<SimpleResponse<Review>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Review>>>()
    }

    val salonSlotResult: SingleLiveEvent<Result<SlotAvailableResponse>> by lazy {
        SingleLiveEvent<Result<SlotAvailableResponse>>()
    }
    val stylistResult: SingleLiveEvent<Result<StylistAvailableResponse>> by lazy {
        SingleLiveEvent<Result<StylistAvailableResponse>>()
    }
    val salonInfoResult: SingleLiveEvent<Result<SimpleResponse<Salon>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<Salon>>>()
    }
   val reviewSalonResult: SingleLiveEvent<Result<SimpleResponse<List<Review>>>> by lazy {
        SingleLiveEvent<Result<SimpleResponse<List<Review>>>>()
    }

    suspend fun saveHairStyle(token: String, salonId: Int, title: String, url: String) {
        try {
            saveHairStyleResult.postValue(Result.loading())
            val result = api.saveHairStyle(token, salonId, title, url)
            saveHairStyleResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    saveHairStyleResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    saveHairStyleResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun getNewHairStyle(token: String, salonId: Int) {
        try {
            newHairStyleResult.postValue(Result.loading())
            val result = api.getNewHairStyle(token, salonId)
            newHairStyleResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    newHairStyleResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    newHairStyleResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun cancelOrder(token: String, orderId: Int) {
        try {
            cancelOrderResult.postValue(Result.loading())
            val result = api.cancelOrder(token, orderId)
            cancelOrderResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            ex.printStackTrace()
            withContext(mainThread()) {
                if (ex is HttpException) {

                    cancelOrderResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    cancelOrderResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun getOrderDetail(token: String, orderId: Int) {
        try {
            orderItemResult.postValue(Result.loading())
            val result = api.getDetailOrder(token, orderId)
            orderItemResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    orderItemResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    orderItemResult.value = Result.errorNetwork(null)
                }
            }

        }
    }


    suspend fun getAllHairStyle(token: String, salonId: Int) {
        try {
            allHairStyleResult.postValue(Result.loading())
            val result = api.getAllHairStyle(token, salonId, 1, PER_PAGE_HAIR_STYLE)
            allHairStyleResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    allHairStyleResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    allHairStyleResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun loadMoreHairStyle(token: String, salonId: Int, page: Int) {
        try {
            moreHairStyleResult.postValue(Result.loading())
            val result = api.getAllHairStyle(token, salonId, page, PER_PAGE_HAIR_STYLE)
            moreHairStyleResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    moreHairStyleResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    moreHairStyleResult.value = Result.errorNetwork(null)
                }
            }

        }
    }


    suspend fun collectPhoto(token: String, bookingId: Int, file: File) {
        try {
            withContext(mainThread()) {
                collectPhotoResult.value = Result.loading()
            }
            val id = RequestBody.create(
                MediaType.parse("text/plain"), bookingId.toString()
            )
            val image = RequestBody.create(
                MediaType.parse("multipart/form-data"), file
            )
            val body = MultipartBody.Part.createFormData("image", file.name, image)
            val result = api.collectPhoto(token, id, body)
            withContext(mainThread()) {

                collectPhotoResult.value = Result.success(result)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            withContext(mainThread()) {
                if (ex is HttpException) {
                    collectPhotoResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    collectPhotoResult.value = Result.errorNetwork(null)
                }
            }


        }
    }

    suspend fun getPopularSalon(authHeader: String): SalonResponse {
        return api.getPopularSalon(authHeader)
    }

    suspend fun getAllSale(authHeader: String): SaleResponse {
        return api.getAllSale(authHeader)
    }

    suspend fun getNewSalon(authHeader: String): SalonResponse {
        return api.getNewSalon(authHeader)
    }

    suspend fun getSalonSlotAvailable(
        authHeader: String,
        salonId: Int,
        date: String,
        duration: Int
    ) {
        try {
            salonSlotResult.postValue(Result.loading())
            val result = api.getSalonSlotAvailable(authHeader, salonId, date, duration)
            salonSlotResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    salonSlotResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    salonSlotResult.value = Result.errorNetwork(null)
                }
            }

        }

    }

    suspend fun getSalonSlotAvailableByStylist(
        authHeader: String,
        salonId: Int,
        stylistId: Int,
        date: String,
        duration: Int
    ) {

        try {
            salonSlotResult.postValue(Result.loading())
            val result =   api.getSalonSlotAvailableByStylist(authHeader, salonId, stylistId, date, duration)
            salonSlotResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    salonSlotResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    salonSlotResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun getStylistAvailable(
        authHeader: String,
        salonId: Int,
        dayOfWeek: String
    ){
        try {
            stylistResult.postValue(Result.loading())
            val result = api.getStylistAvailable(authHeader, salonId, dayOfWeek)
            stylistResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    stylistResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    stylistResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun getSalonInfo(authHeader: String, salonId: Int) {
        try {
            salonInfoResult.postValue(Result.loading())
            val result = api.getSalonInfo(authHeader, salonId)
            salonInfoResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    salonInfoResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    salonInfoResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun getSalonService(authHeader: String, salonId: Int): ServiceResponse {
        return api.getSalonService(authHeader, salonId)
    }

    suspend fun getSalonStylists(authHeader: String, salonId: Int): StylistResponse {
        return api.getSalonStylists(authHeader, salonId)
    }

    suspend fun confirmBooking(
        authHeader: String,
        salonId: Int,
        services: List<Service.ServiceJson>,
        time_booking: Int,
        date_booking: String,
        duration: Int,
        customer_id: Int, phoneNumber: String, cusName: String
    ) {

        val params = BookingParams(
            salonId,
            time_booking,
            date_booking,
            duration,
            phoneNumber,
            cusName,
            customer_id,
            services
        )

        try {
            confirmBookingResult.postValue(Result.loading())
            val result = api.confirmBooking(authHeader, params)
            confirmBookingResult.postValue(Result.success(result))
        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    confirmBookingResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    confirmBookingResult.value = Result.errorNetwork(null)
                }
            }

        }

    }

    suspend fun getTopArtOfSalon(token: String, salonId: Int) {
        try {
            topArtOfSalonResult.postValue(Result.loading())
            val result = api.getTopArtOfSalon(token, salonId)
            topArtOfSalonResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    topArtOfSalonResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    topArtOfSalonResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun removeArt(token: String, artId: Int) {
        try {
            removeArtResult.postValue(Result.loading())
            val result = api.removeArt(token, artId)
            removeArtResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    removeArtResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    removeArtResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

    suspend fun rateBooking(token: String, bookingId: Int, content: String, rate: Int) {
        try {
            rateBookingResult.postValue(Result.loading())
            val result = api.rateBooking(token, bookingId, content, rate)
            rateBookingResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    rateBookingResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    rateBookingResult.value = Result.errorNetwork(null)
                }
            }

        }
    }
    suspend fun getAllReviewSalon(token:String, salonId:Int){
        try {
            reviewSalonResult.postValue(Result.loading())
            val result = api.getAllSalonReview(token, salonId)
            reviewSalonResult.postValue(Result.success(result))

        } catch (ex: Exception) {
            withContext(mainThread()) {
                if (ex is HttpException) {

                    reviewSalonResult.value =
                        Result.error(
                            "Có lỗi xảy ra, vui lòng thử lại",
                            null,
                            ex.code()
                        )
                } else {
                    reviewSalonResult.value = Result.errorNetwork(null)
                }
            }

        }
    }

}