package linh.dev.salonapp.model.local

import linh.dev.salonapp.CATEGORY_ALL
import linh.dev.salonapp.CATEGORY_ALL_ID

class ProductCategory(val id: Int, val title: String) {

    companion object {
        fun defaultCategory(): ProductCategory {
            return ProductCategory(CATEGORY_ALL_ID, CATEGORY_ALL)
        }

    }

    fun isDefaultStylist(): Boolean {
        return id == CATEGORY_ALL_ID
    }

    override fun toString(): String {
        return title
    }
}