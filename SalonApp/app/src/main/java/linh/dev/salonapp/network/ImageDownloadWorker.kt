package linh.dev.salonapp.network

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import linh.dev.base.BaseConstant
import linh.dev.base.ext.retrofit.buildNetworkCoroutine
import linh.dev.salonapp.*
import linh.dev.salonapp.databases.AppDatabase
import linh.dev.salonapp.databases.model.HairStyle
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.HairStyleRepository
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class ImageDownloadWorker(private val mContext: Context, workerParameters: WorkerParameters) :
    CoroutineWorker(mContext, workerParameters), BaseConstant {
    val appApi = AppApi::class.java.buildNetworkCoroutine(
        url = "$BASE_URL/$API_VERSION/",
        enableLogging = true,
        connectTimeout = 10
    )
    val listIds = ArrayList<Int>()

    private val hairStyleRepository = HairStyleRepository(appApi)
    private val notificationManager =
        applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    override suspend fun doWork(): Result {
        listIds.clear()
        val imagesJson = inputData.getString("images")
        val gSon = Gson()
        val listOfImages = gSon.fromJson<List<linh.dev.salonapp.model.local.HairStyle>>(
            imagesJson,
            object : TypeToken<List<linh.dev.salonapp.model.local.HairStyle>>() {}.type
        )
        val progressUpdate = ProgressUpdateEvent("Vui lòng chờ...", listOfImages.size, 0, 0, 0)
        displayNotification(progressUpdate)
        listOfImages.forEachIndexed { index, item ->
            delay(1000)
            downloadImage(item, index, listOfImages.size, progressUpdate)
        }
        progressUpdate.message = "Tải xuống mẫu tóc"
        displayFinishDownloadImage(progressUpdate)
        withContext(io()) {
            user()?.let {
                if (listIds.isNotEmpty()) {
                    hairStyleRepository.updateDownloadHairStyle(it.getBearToken(), listIds)
                }
            }

        }
//        notificationManager.cancel(notificationId)
        return Result.success()
    }

    private val notificationId: Int = 500
    private val notificationChannel: String = "demo"

    private suspend fun downloadImage(
        image: linh.dev.salonapp.model.local.HairStyle,
        index: Int,
        totalItem: Int,
        progressUpdate: ProgressUpdateEvent
    ) {
        withContext(Dispatchers.IO) {
            val client = OkHttpClient()
            val request = Request.Builder()
                .url(image.url)
                .build()
            try {
                val response = client.newCall(request).execute()
                val bitmap = BitmapFactory.decodeStream(response.body()?.byteStream())
                withContext(Dispatchers.Default) {
                    val stream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream)
                    val mediaByteArray = stream.toByteArray()

                    try {

                        val photoDirectory =
                            File(
                                mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                                "PhotoApp"
                            )
                        if (!photoDirectory.exists() && !photoDirectory.mkdirs()) {
                            logE("folder exits")
                        }
                        val photo =
                            File(photoDirectory, System.currentTimeMillis().toString() + ".png")
                        logE(photo.path)
                        val fos = FileOutputStream(photo)
                        fos.write(mediaByteArray)
                        fos.flush()
                        fos.close()
                        // insert image to database
                        val hairStyle = HairStyle(image.id, image.title,image.id, photo.path, image.salonId)
                        AppDatabase.getInstance(mContext).getHairStyleDao()
                            .insertHairStyle(hairStyle)

                        withContext(Dispatchers.Main) {
                            progressUpdate.success += 1
                            progressUpdate.message = image.title

                            displayNotification(
                                progressUpdate
                            )
                            bus.post(
                                AppAction(
                                    AppEvent.EVENT_DOWNLOAD_IMAGE_SUCCESS.setData(
                                        hairStyle
                                    )
                                )
                            )
                            listIds.add(hairStyle.id)
                        }
                    } catch (exception: IOException) {
                        exception.printStackTrace()

                    }
                }


            } catch (e: Exception) {
                e.printStackTrace()
                progressUpdate.fail += 1
                displayFailNotification(progressUpdate)
            }
        }


    }

    private fun displayNotification(prog: ProgressUpdateEvent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                notificationChannel,
                notificationChannel,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.enableVibration(false)
            notificationManager.createNotificationChannel(channel)
        }

        val notificationBuilder =
            NotificationCompat.Builder(applicationContext, notificationChannel)

        val remoteView =
            RemoteViews(applicationContext.packageName, R.layout.view_download_image_notification)
        remoteView.setImageViewResource(R.id.iv_notif, R.drawable.ic_logo)
        remoteView.setTextViewText(
            R.id.tv_notif_progress,
            "${prog.message} (${prog.progress}/${prog.total} thành công)"
        )
        remoteView.setTextViewText(R.id.tv_notif_title, "Tải mẫu tóc")
        remoteView.setProgressBar(R.id.pb_notif, prog.total, prog.progress, false)

        notificationBuilder
            .setContent(remoteView)
            .setSmallIcon(R.drawable.ic_logo)
            .setColor(ContextCompat.getColor(mContext, R.color.tab_color_selected))

        notificationManager.notify(notificationId, notificationBuilder.build())
    }

    private fun displayFailNotification(prog: ProgressUpdateEvent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                notificationChannel,
                notificationChannel,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.enableVibration(false)
            notificationManager.createNotificationChannel(channel)
        }

        val notificationBuilder =
            NotificationCompat.Builder(applicationContext, notificationChannel)

        val remoteView =
            RemoteViews(applicationContext.packageName, R.layout.view_download_image_notification)
        remoteView.setImageViewResource(R.id.iv_notif, R.drawable.ic_logo)
        remoteView.setTextViewText(
            R.id.tv_notif_progress,
            "${prog.message} (${prog.progress}/${prog.total} thất bại)"
        )
        remoteView.setTextViewText(R.id.tv_notif_title, "Tải mẫu tóc")
        remoteView.setProgressBar(R.id.pb_notif, prog.total, prog.progress, false)

        notificationBuilder
            .setContent(remoteView)
            .setSmallIcon(R.drawable.ic_logo)

        notificationManager.notify(notificationId, notificationBuilder.build())
    }

    private fun displayFinishDownloadImage(prog: ProgressUpdateEvent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                notificationChannel,
                notificationChannel,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.enableVibration(false)
            notificationManager.createNotificationChannel(channel)
        }

        val notificationBuilder =
            NotificationCompat.Builder(applicationContext, notificationChannel)

        val remoteView =
            RemoteViews(applicationContext.packageName, R.layout.view_download_image_notification)
        remoteView.setImageViewResource(R.id.iv_notif, R.drawable.ic_logo)
        remoteView.setTextViewText(
            R.id.tv_notif_progress,
            "${prog.message} với ${prog.success} thành công và ${prog.fail} thất bại"
        )
        remoteView.setTextViewText(R.id.tv_notif_title, "Tải mẫu tóc")
        remoteView.setProgressBar(R.id.pb_notif, prog.total, prog.progress, false)

        notificationBuilder
            .setContent(remoteView)
            .setSmallIcon(R.drawable.ic_logo)

        notificationManager.notify(notificationId, notificationBuilder.build())
    }


    data class ProgressUpdateEvent(
        var message: String,
        var total: Int,
        var progress: Int,
        var success: Int,
        var fail: Int
    )


}