package linh.dev.salonapp.model.local

class Review(
    val id: Int,
    val customer_id: Int,
    val avatar: String?,
    val cusName: String,
    val stylist_id: Int?,
    val salon_id: Int,
    val booking_id: Int?,
    val content: String,
    val star: Int,
    val review_at: String
) {
}