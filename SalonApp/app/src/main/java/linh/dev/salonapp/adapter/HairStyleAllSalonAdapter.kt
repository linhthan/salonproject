package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.AsyncPagedListDiffer
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.State
import linh.dev.salonapp.databinding.ViewItemAllHairStyleBinding
import linh.dev.salonapp.databinding.ViewItemFooterBinding
import linh.dev.salonapp.model.local.HairStyle


class HairStyleAllSalonAdapter : BaseRecycleViewAdapter<HairStyle>() {
    private val mDiffer = AsyncPagedListDiffer(this, HairStyleDiffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == DATA_VIEW_TYPE) {
            val binding: ViewItemAllHairStyleBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_all_hair_style,
                parent,
                false
            )
            ViewHolder(binding)
        } else {
            val binding: ViewItemFooterBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_footer, parent, false
            )
            return ListFooterViewHolder(binding)
        }
    }


    inner class ListFooterViewHolder(val binding: ViewItemFooterBinding) :
        BaseRecycleViewAdapter<HairStyle>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.progressBar.visibility =
                if (state == State.LOADING) View.VISIBLE else View.INVISIBLE
            binding.txtError.visibility =
                if (state == State.ERROR) View.VISIBLE else View.INVISIBLE
            binding.txtError.setOnClickListener {
                onItemClick?.onViewClick(
                    R.id.txt_error,
                    adapterPosition
                )
            }
        }

    }

    fun getArrayListByPosition(position: Int){

    }

    inner class ViewHolder(val binding: ViewItemAllHairStyleBinding) :
        BaseRecycleViewAdapter<HairStyle>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.viewModel = mDiffer.getItem(position)
            binding.root.setOnClickListener {
                onItemClick?.onItemClick(position)
            }
        }

    }


    private var state = State.LOADING

    private var onItemClick: OnItemClick? = null

    interface OnItemClick {
        fun onItemClick(position: Int)
        fun onViewClick(resId: Int, position: Int)
    }

    fun setItemClick(onItemClick: OnItemClick) {
        this.onItemClick = onItemClick
    }

    override fun getItemCount(): Int {
        return  mDiffer.itemCount + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }

    fun submitList(pagedList: PagedList<HairStyle>) {
        mDiffer.submitList(pagedList)
    }

    fun getItemByPosition(position: Int): HairStyle? {
        return mDiffer.getItem(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < mDiffer.itemCount) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }
    fun getCurrentData(): List<HairStyle> {
        val listHairStyle = mDiffer.currentList?.toList()
        if(listHairStyle.isNullOrEmpty()){
            return ArrayList()
        }else{
            return listHairStyle
        }
    }

    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2

    companion object {
        val HairStyleDiffCallback = object : DiffUtil.ItemCallback<HairStyle>() {
            override fun areItemsTheSame(oldItem: HairStyle, newItem: HairStyle): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: HairStyle, newItem: HairStyle): Boolean {
                return oldItem == newItem
            }
        }
    }


}