package linh.dev.salonapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import linh.dev.salonapp.data.AppointmentDataSource
import linh.dev.salonapp.data.AppointmentDataSourceFactory
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.PER_PAGE
import linh.dev.salonapp.State
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.repository.SalonRepository
import linh.dev.salonapp.ui.custom.SingleLiveEvent

class HistoryAppointmentViewModel(private val salonRepository: SalonRepository) : BaseViewModel() {
    val loadmore = MutableLiveData<Boolean>()
    val keyword: SingleLiveEvent<String> by lazy {
        SingleLiveEvent<String>()
    }
    var appointmentUsedList: LiveData<PagedList<Appointment>>
    private val appointmentDataSourceFactory: AppointmentDataSourceFactory
    init {
        keyword.value = EMPTY
        loadmore.value = false
        appointmentDataSourceFactory = AppointmentDataSourceFactory(EMPTY,salonRepository.api, viewModelScope)
        val config = PagedList.Config.Builder()
            .setPageSize(PER_PAGE)
            .setInitialLoadSizeHint(PER_PAGE)
            .setEnablePlaceholders(false)
            .build()
        appointmentUsedList = LivePagedListBuilder(appointmentDataSourceFactory, config).build()
    }


    fun replaceSubscription() {
        appointmentDataSourceFactory.keyword = keyword.value?: EMPTY
        refreshData()
    }
    fun getState(): LiveData<State> = Transformations.switchMap<AppointmentDataSource,
            State>(appointmentDataSourceFactory.appointmentDataSourceLiveData, AppointmentDataSource::state)

    fun retryFailed(){
        appointmentDataSourceFactory.appointmentDataSourceLiveData.value?.retryFailedQuery()
    }




    fun listIsEmpty(): Boolean {
        return appointmentUsedList.value?.isEmpty() ?: true
    }

    fun refreshData() {
        appointmentDataSourceFactory.appointmentDataSourceLiveData.value?.refresh()
    }


}