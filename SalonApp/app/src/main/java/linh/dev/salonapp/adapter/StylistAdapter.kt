package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import kotlinx.android.synthetic.main.view_item_stylist_select.view.*
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemStylistSelectBinding
import linh.dev.salonapp.model.local.Stylist

class StylistAdapter : BaseRecycleViewAdapter<Stylist>() {

    private var selectedPosition = 0

    override fun setItems(items: Collection<Stylist>) {
        if (items.isNullOrEmpty()) return
        dataSources.clear()
        dataSources.add(Stylist.defaultStylist())
        dataSources.addAll(items)
        notifyDataSetChanged()
    }

    fun getItemSelected(): Stylist? {
        if (selectedPosition in (0 until itemCount))
            return getItem(selectedPosition)
        return null
    }
    fun resetSelected(){
        selectedPosition =0;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemStylistSelectBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_stylist_select,
            parent,
            false
        )
        return ViewHolder(binding)
    }


    inner class ViewHolder(val binding: ViewDataBinding) :
        BaseRecycleViewAdapter<Stylist>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            (binding as ViewItemStylistSelectBinding).viewModel = getItem(position)


            binding.root.imvStylist.setOnClickListener {
                val oldPosition = selectedPosition
                notifyItemChanged(oldPosition)
                selectedPosition = adapterPosition
                notifyItemChanged(selectedPosition, null)
                onViewClick(R.id.imvStylist)
            }
            if (selectedPosition == position) {
                binding.imvStylist.borderColor =
                    ContextCompat.getColor(binding.imvStylist.context, R.color.tab_color_selected)
            } else {
                binding.imvStylist.borderColor =
                    ContextCompat.getColor(binding.imvStylist.context, android.R.color.white)
            }
            binding.executePendingBindings()
        }

    }
}