package linh.dev.salonapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.UserRepository
import linh.dev.salonapp.ui.custom.SingleLiveEvent
import java.io.File

class EditProfileViewModel(val repository: UserRepository) : BaseViewModel() {
    var imagePath: String = EMPTY
    val imageCrop: SingleLiveEvent<String> by lazy {
        SingleLiveEvent<String>()
    }
    val mode: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }
    val compositeDisposable = CompositeDisposable()
    val updateResult = repository.userProfileResult


    fun updateUser(name: String, email: String, phoneNumber: String, avatar: File?) {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    repository.updateProfile(it.getBearToken(), name, email, phoneNumber, avatar)
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}