package linh.dev.salonapp.ui.fragment

import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_create_salon.*
import linh.dev.base.BaseFragment
import linh.dev.salonapp.R

class CreateSalonFragment : BaseFragment<linh.dev.salonapp.databinding.FragmentCreateSalonBinding>() {
    val navs: CreateSalonFragmentArgs by navArgs()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_create_salon
    }

    override fun initView() {
        super.initView()
        tvWho.text = navs.email
    }
}