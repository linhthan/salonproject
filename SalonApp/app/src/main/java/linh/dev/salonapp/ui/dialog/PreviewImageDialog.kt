package linh.dev.salonapp.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.asksira.loopingviewpager.LoopingViewPager
import kotlinx.android.synthetic.main.dialog_preview_image.*
import linh.dev.salonapp.R
import linh.dev.salonapp.adapter.HairStylePagerAdapter
import linh.dev.salonapp.model.local.HairStyle

class PreviewImageDialog : DialogFragment() {
    private var images: List<HairStyle>? = null

    companion object {
        fun newInstance(images: List<HairStyle>): PreviewImageDialog {
            val dialog = PreviewImageDialog()
            dialog.images = images
            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Preview_Dialog)
    }

    override fun onStart() {
        super.onStart()
        val d = dialog
        if (d != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            d.window?.setLayout(width, height)
            d.window?.setWindowAnimations(R.style.Preview_Dialog)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.dialog_preview_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val salePageAdapter = HairStylePagerAdapter(context, true)
        images?.let {
            salePageAdapter.setItemList(it)
        }
        pagerHairStyle.adapter = salePageAdapter
        pagerHairStyle.offscreenPageLimit = 3
        tabDots.count = pagerHairStyle.indicatorCount
        pagerHairStyle.setIndicatorPageChangeListener(object :
            LoopingViewPager.IndicatorPageChangeListener {
            override fun onIndicatorProgress(selectingPosition: Int, progress: Float) {
                tabDots?.setProgress(selectingPosition, progress)

            }

            override fun onIndicatorPageChange(newIndicatorPosition: Int) {
            }

        })
    }

}