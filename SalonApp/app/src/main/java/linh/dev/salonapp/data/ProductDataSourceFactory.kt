package linh.dev.salonapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import kotlinx.coroutines.CoroutineScope
import linh.dev.salonapp.data.AppointmentDataSource
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.model.local.Product
import linh.dev.salonapp.network.AppApi

class ProductDataSourceFactory(
     var catId:Int,
     var keyword:String,
    private val salonId:Int,
    private val appApi: AppApi,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, Product>() {

    val productDataSourceLiveData = MutableLiveData<ProductDataSource>()

    override fun create(): DataSource<Int, Product> {
        val newsDataSource = ProductDataSource(catId,keyword,salonId,appApi, scope)
        productDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }

}