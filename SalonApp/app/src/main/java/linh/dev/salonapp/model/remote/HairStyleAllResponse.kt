package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.HairStyle

data class HairStyleAllResponse(val data: List<HairStyle>,
                                val current_page: Int,
                                val last_page: Int) {
}