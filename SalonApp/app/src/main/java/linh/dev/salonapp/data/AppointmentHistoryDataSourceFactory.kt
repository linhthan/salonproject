package linh.dev.salonapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import kotlinx.coroutines.CoroutineScope
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.network.AppApi

class AppointmentHistoryDataSourceFactory(
    private val appApi: AppApi,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, Appointment>() {

    val appointmentDataSourceLiveData = MutableLiveData<AppointmentHistoryDataSource>()

    override fun create(): DataSource<Int, Appointment> {
        val newsDataSource = AppointmentHistoryDataSource(appApi, scope)
        appointmentDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}