package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.AsyncPagedListDiffer
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.State
import linh.dev.salonapp.databinding.ViewItemAllHairStyleBinding
import linh.dev.salonapp.databinding.ViewItemFooterBinding
import linh.dev.salonapp.model.local.HairStyle


class HairStyleAllAdapter : BaseRecycleViewAdapter<HairStyle>() {
//    private val mDiffer = AsyncPagedListDiffer(this, HairStyleDiffCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {

        val binding: ViewItemAllHairStyleBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_all_hair_style,
            parent,
            false
        )
        return ViewHolder(binding)
    }


    inner class ViewHolder(val binding: ViewItemAllHairStyleBinding) :
        BaseRecycleViewAdapter<HairStyle>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.viewModel = getItem(position)
            binding.root.setOnClickListener {
                onItemClick?.onItemClick(position)
            }
        }

    }


    private var onItemClick: OnItemClick? = null

    interface OnItemClick {
        fun onItemClick(position: Int)
        fun onViewClick(resId: Int, position: Int)
    }

    fun setItemClick(onItemClick: OnItemClick) {
        this.onItemClick = onItemClick
    }

    override fun getItemCount(): Int {
        return dataSources.size
    }


    fun submitList(pagedList: PagedList<HairStyle>) {
        super.setItems(pagedList.toList())
//        mDiffer.submitList(pagedList)

    }

    fun getItemByPosition(position: Int): HairStyle? {
        return super.getItem(position)
    }


    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2

    companion object {
        val HairStyleDiffCallback = object : DiffUtil.ItemCallback<HairStyle>() {
            override fun areItemsTheSame(oldItem: HairStyle, newItem: HairStyle): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: HairStyle, newItem: HairStyle): Boolean {
                return oldItem == newItem
            }
        }
    }


}