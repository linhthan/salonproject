package linh.dev.salonapp.ui.fragment.customers.order

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import cn.refactor.lib.colordialog.PromptDialog
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_create_order.*
import linh.dev.base.BaseFragment
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.base.ext.view.currentText
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.ProductItemOrderAdapter
import linh.dev.salonapp.databinding.FragmentCreateOrderBinding
import linh.dev.salonapp.ext.isValidPhoneNumber
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.CreateOrderViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CreateOrderFragment : BaseFragment<FragmentCreateOrderBinding>() {
    private val args: CreateOrderFragmentArgs by navArgs()
    private var colorDialog: PromptDialog? = null
    private var errorDialog: PromptDialog? = null
    private val viewModel: CreateOrderViewModel by viewModel {
        parametersOf(
            args.listProduct,
            args.salonId
        )
    }
    private val adapter: ProductItemOrderAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_create_order
    }

    override fun initView() {
        super.initView()
        user()?.let {
            mBinding.user = it
        }
        initRecyclerView()
        initEditTextFocus()
        initDialog()

    }

    private fun initEditTextFocus() {
        editName.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                tilName.error = null
            }
        }
        edtPhoneNumberOrder.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                tilPhoneNumber.error = null
            }
        }
        edtAddress.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                tilAddress.error = null
            }
        }

    }

    private fun initRecyclerView() {
        rcvProduct.layoutManager = object : LinearLayoutManager(context) {
            override fun canScrollHorizontally(): Boolean {
                return false
            }

            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        (rcvProduct.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        rcvProduct.adapter = adapter
        adapter.setItems(viewModel.listProductSelected)
        tvTotalPriceOrder.text = resources.getString(
            R.string.format_price_vietnam,
            adapter.getTotalPrice().toInt()
        )
        adapter.setOnViewClickListener(object : BaseRecycleViewAdapter.ViewClickListener {
            override fun onViewItemClock(resId: Int, position: Int) {
                val item = adapter.getItem(position)
                when (resId) {
                    R.id.imvIncrease -> {
                        if (item.selectedQuantity == item.quantity) {
                            showToast("Số lượng tối đa cho sản phẩm là ${item.quantity}")
                        } else {
                            item.selectedQuantity += 1
                            adapter.notifyItemChanged(position)
                            tvTotalPriceOrder.text = resources.getString(
                                R.string.format_price_vietnam,
                                adapter.getTotalPrice().toInt()
                            )
                        }

                    }
                    R.id.imvDecrease -> {
                        if (item.selectedQuantity > 1) {
                            item.selectedQuantity -= 1
                            adapter.notifyItemChanged(position)
                            tvTotalPriceOrder.text = resources.getString(
                                R.string.format_price_vietnam,
                                adapter.getTotalPrice().toInt()
                            )
                        }

                    }
                }

            }

        })

        btnCreateOrder.setOnClickListener {
            val cusName = editName.currentText
            val phoneNumber = edtPhoneNumberOrder.currentText
            val address = edtAddress.currentText
            validInfo(cusName, phoneNumber, address)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initDialog() {
        context?.let {
            if (colorDialog == null) {
                colorDialog = PromptDialog(it)
                colorDialog?.dialogType = PromptDialog.DIALOG_TYPE_SUCCESS
                colorDialog?.setAnimationEnable(true)
                colorDialog?.contentTextView?.gravity = Gravity.CENTER
                colorDialog?.setTitleText(getString(R.string.success_dialog_title))
                colorDialog?.setPositiveListener(getString(R.string.dialog_ok)) {
                    it.dismiss()
                }



            }
            if (errorDialog == null) {
                errorDialog = PromptDialog(it)
                errorDialog?.dialogType = PromptDialog.DIALOG_TYPE_WRONG
                errorDialog?.contentTextView?.gravity = Gravity.CENTER
                errorDialog?.setAnimationEnable(true)
                errorDialog?.setTitleText(getString(R.string.error_dialog_title))
                errorDialog?.setPositiveListener(getString(R.string.dialog_ok)) {
                    it.dismiss()
                }

            }

        }

    }


    private fun initObservable() {
        viewModel.createOrderResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
//                    progressCreateOrder.show()
                    btnCreateOrder.showProgress {
                        buttonText = getString(R.string.title_dialog_create_order)
                        progressColor = Color.WHITE
                    }
//                    loadingDialog?.setTitleText(R.string.title_dialog_create_order)
//                    loadingDialog?.changeAlertType(SweetAlertDialog.PROGRESS_TYPE)
//                    loadingDialog?.setCancelable(true)
//                    loadingDialog?.setCanceledOnTouchOutside(true)
//                    loadingDialog?.setOnCancelListener { dialog ->
//                        dialog.dismiss()
//                    }
//                    loadingDialog?.show()
                }
                Result.Status.SUCCESS -> {
                    btnCreateOrder.hideProgress(getString(R.string.create_order_lbl))

                    if (it.data != null) {
                        if(it.data.success){
                            colorDialog?.setContentText(it.data.message)
                            colorDialog?.show()
                            colorDialog?.setOnDismissListener {
                                logE("Dismiss")

                                val bundle = Bundle()
                                bundle.putInt(BUNDLE_HOME_TAB, 2)
                                Navigation.findNavController(requireActivity(), R.id.nav_main)
                                    .navigate(R.id.action_createOrderFragment_to_customerHomeFragment, bundle)
                            }

                        }else{
                            errorDialog?.setContentText(it.data.message)
                            errorDialog?.show()
                        }

                    }
                }
                Result.Status.ERROR -> {
                    errorDialog?.setContentText(it.message)
                    errorDialog?.show()
                    btnCreateOrder.hideProgress(getString(R.string.create_order_lbl))
//                    progressCreateOrder.hide()
//                    showMsg(it.message)
                }

                Result.Status.UNAUTHORIZED -> {
                    btnCreateOrder.hideProgress(getString(R.string.create_order_lbl))
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
    }

    fun validInfo(cusName: String, phoneNumber: String, address: String) {
        var error = false
        if (cusName.isBlank()) {
            tilName.error = getString(R.string.name_can_not_empty)
            error = true
        } else {
            tilName.error = null
        }
        if (address.isBlank()) {
            tilAddress.error = getString(R.string.address_can_not_empty)
            error = true
        } else {
            tilAddress.error = null
        }
        if (phoneNumber.isBlank()) {
            tilPhoneNumber.error = getString(R.string.phone_number_can_not_empty)
            error = true
        }
        if (!phoneNumber.isValidPhoneNumber()) {
            error = true
            tilPhoneNumber.error = getString(R.string.phone_number_invalid_error_msg)
        }
        if (!error) {
            viewModel.createOrder(
                cusName,
                phoneNumber,
                address,
                edtNote.currentText,
                adapter.getCurrentList()
            )
        }


    }
}