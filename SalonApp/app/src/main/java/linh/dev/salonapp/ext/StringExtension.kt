package linh.dev.salonapp.ext

import android.util.Patterns
import java.util.regex.Matcher
import java.util.regex.Pattern

fun String.isValidEmail(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isValidPhoneNumber(): Boolean {
    val regex = "^(07[09768]|08[1234568]|03[23456789]|05[689]|09[01234678])[0-9]{7}"
    val phoneChecker  = Pattern.compile(regex)
    return phoneChecker.matcher(this).matches()
}

fun String.isContainSpecialCharacter(): Boolean {
    val specialCharacter = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!-]")
    return specialCharacter.matcher(this).find()
}