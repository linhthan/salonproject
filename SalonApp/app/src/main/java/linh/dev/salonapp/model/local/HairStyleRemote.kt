package linh.dev.salonapp.model.local

import com.google.gson.annotations.SerializedName

class HairStyleRemote(
    val id: Int,
    val title: String,
    val url: String,
    @SerializedName("id_salon") val idSalon: Int
) {
}