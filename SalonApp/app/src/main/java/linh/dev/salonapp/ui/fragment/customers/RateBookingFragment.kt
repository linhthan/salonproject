package linh.dev.salonapp.ui.fragment.customers

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import cn.refactor.lib.colordialog.PromptDialog
import kotlinx.android.synthetic.main.fragment_rate_booking.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.currentText
import linh.dev.salonapp.*
import linh.dev.salonapp.databinding.FragmentRateBookingBinding
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.RateBookingViewModel
import me.zhanghai.android.materialratingbar.MaterialRatingBar
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class RateBookingFragment : BaseFragment<FragmentRateBookingBinding>() {
    private val args: RateBookingFragmentArgs by navArgs()
    private val viewModel: RateBookingViewModel by viewModel { parametersOf(args.idBooking) }
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_rate_booking
    }

    override fun initView() {
        super.initView()
        edtContentRate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val currentTextLength = edtContentRate.currentText.length
                if (currentTextLength == 0) {
                    tvSaveRate.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))
                } else {
                    if (rateBar.rating != 0f) {
                        tvSaveRate.setTextColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.white
                            )
                        )
                    } else {
                        tvSaveRate.setTextColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.gray
                            )
                        )
                    }
                }
                tvCountContent.text =
                    getString(R.string.format_length_rate_content, currentTextLength)
            }

        })

        rateBar.onRatingChangeListener = object : MaterialRatingBar.OnRatingChangeListener {
            override fun onRatingChanged(ratingBar: MaterialRatingBar?, rating: Float) {
                if (rating == 0f) {
                    tvSaveRate.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.gray
                        )
                    )
                } else {
                    if (edtContentRate.currentText.isBlank()) {
                        tvSaveRate.setTextColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.gray
                            )
                        )
                    } else {
                        tvSaveRate.setTextColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.white
                            )
                        )
                    }

                }
            }

        }
        tvSaveRate.setOnClickListener {
            val contentRate = edtContentRate.currentText
            val rateValue = rateBar.rating
            if (!contentRate.isBlank() && rateValue.toInt() != 0) {
                viewModel.rateBooking(contentRate, rateValue.toInt())
            }
        }
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initObservable() {
        viewModel.rateBookingResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    loadingRate.show()
                }
                Result.Status.SUCCESS -> {
                    loadingRate.hide()
                    if (it.data != null) {
                        if (it.data.success) {
                            bus.post(AppAction(AppEvent.EVENT_RATE_SUCCESS.setData(it.data.data)))
                            showSuccessDialog(it.data.message)
                        } else {
                            showFailedDialog(it.data.message)
                        }
                    }

                }
                Result.Status.ERROR -> {
                    loadingRate.hide()
                    it.message?.let { msg ->
                        showFailedDialog(msg)
                    }
                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
    }

    private fun showSuccessDialog(msg: String) {
        context?.let {
            val dialog = PromptDialog(it)
            dialog.dialogType = PromptDialog.DIALOG_TYPE_SUCCESS
            dialog.setAnimationEnable(true)
            dialog.setContentText(msg)
            dialog.setTitleText(getString(R.string.success_dialog_title))
            dialog.setPositiveListener(getString(R.string.dialog_ok)) {
                it.dismiss()
            }
            dialog.setOnDismissListener {
                activity?.onBackPressed()
            }
            dialog.show()
        }

    }

    private fun showFailedDialog(msg: String) {
        context?.let {
            val dialog = PromptDialog(it)
            dialog.dialogType = PromptDialog.DIALOG_TYPE_WRONG
            dialog.setAnimationEnable(true)
            dialog.setContentText(msg)
            dialog.setTitleText(getString(R.string.error_dialog_title))
            dialog.setPositiveListener(getString(R.string.dialog_ok)) {
                it.dismiss()
            }
            dialog.show()
        }

    }
}