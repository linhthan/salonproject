package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Salon

data class SalonResponse(val success:Boolean,val message: String, val data:  List<Salon>)