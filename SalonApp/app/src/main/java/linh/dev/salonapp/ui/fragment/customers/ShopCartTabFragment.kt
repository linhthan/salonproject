package linh.dev.salonapp.ui.fragment.customers

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_customer_cart_tab.*
import kotlinx.android.synthetic.main.layout_no_data.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.OrderAdapter
import linh.dev.salonapp.databinding.FragmentCustomerCartTabBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.Order
import linh.dev.salonapp.viewmodel.OrderViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class ShopCartTabFragment : BaseFragment<FragmentCustomerCartTabBinding>() {
    val viewModel: OrderViewModel by viewModel()
    private val adapter: OrderAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_customer_cart_tab
    }

    companion object {
        fun newInstance(): ShopCartTabFragment = ShopCartTabFragment()
    }

    override fun initView() {
        super.initView()
        initRecyclerView()
        initRefreshData()
    }

    private fun initRecyclerView() {
        rcvOrder.layoutManager = LinearLayoutManager(context)
        rcvOrder.adapter = adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initRefreshData() {
        swLayout.setOnRefreshListener {
            viewModel.refreshData()
        }
        btnTryAgain.setOnClickListener {
            viewModel.refreshData()
        }
    }

    private fun initObservable() {

        viewModel.getState().observe(viewLifecycleOwner, Observer { state ->
            if (swLayout.isRefreshing && state == State.LOADING) {
                swLayout.isRefreshing = false
            }
            if (viewModel.listIsEmpty() && state == State.LOADING) {
                progressLoading.show()
            } else {
                progressLoading.hide()
            }
            if (viewModel.listIsEmpty() && state == State.ERROR) {
                viewNoData.show()
            } else {
                viewNoData.hide()
            }
            if (viewModel.listIsEmpty() && adapter.itemCount==1){
                viewNoData.show()
            }else{
                viewNoData.hide()
            }
            if (!viewModel.listIsEmpty()) {
                adapter.setState(state ?: State.DONE)
            }
        })
        viewModel.orderList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
        adapter.setItemClick(object : OrderAdapter.OnItemClick {
            override fun onItemClick(position: Int) {
                val item = adapter.getItemByPosition(position)
                item?.let {
                    val bundle = Bundle()
                    bundle.putString(BUNDLE_ORDER, Gson().toJson(it))
                    Navigation.findNavController(requireActivity(), R.id.nav_main)
                        .navigate(
                            R.id.action_customerHomeFragment_to_orderDetailFragment,
                            bundle
                        )
                }
            }

            override fun onViewClick(resId: Int, position: Int) {

            }

        })
    }
    @Subscribe
    fun onAppAction(appAction: AppAction){
        when(appAction.event){
            AppEvent.EVENT_CANCEL_ORDER_SUCCESS ->{
                val orderId = appAction.event.getData(Order::class.java)
                logE(appAction.event)
                logE("AppAction $orderId")
                orderId?.let {
                    viewModel.orderList.value?.forEachIndexed { index, item ->
                        if (item.id == it.id) {
                            logE("AppAction found $orderId")
                            item.status = 3
                            adapter.notifyItemChanged(index)
                        }
                    }
                }
            }
        }
    }
}