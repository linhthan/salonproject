package linh.dev.salonapp.ui.fragment.customers.saloninfo

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_rate_salon.*
import kotlinx.android.synthetic.main.layout_no_data.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.salonapp.R
import linh.dev.salonapp.adapter.RateSalonAdapter
import linh.dev.salonapp.databinding.FragmentRateSalonBinding
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.viewmodel.RateSalonViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class FragmentRateSalon : BaseFragment<FragmentRateSalonBinding>() {
    private val args: FragmentRateSalonArgs by navArgs()
    private val viewModel: RateSalonViewModel by viewModel { parametersOf(args.salonId) }
    private val adapter: RateSalonAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_rate_salon
    }

    override fun initView() {
        super.initView()
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }
        btnTryAgain.setOnClickListener {
            viewModel.getAllReview()
        }
        rcvRate.layoutManager = LinearLayoutManager(context)
        rcvRate.adapter = adapter

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.reviewSalonResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressRateLoading.show()
                }
                Result.Status.SUCCESS -> {
                    progressRateLoading.hide()
                    if (it.data != null) {
                        if (it.data.success) {
                            viewNoData.hide()
                            adapter.setItems(it.data.data)
                        } else {
                            viewNoData.show()
                        }
                    } else {
                        viewNoData.show()
                    }

                }
                Result.Status.ERROR -> {
                    progressRateLoading.hide()
                    viewNoData.show()
                }

                Result.Status.UNAUTHORIZED -> {

                }
            }
        })
    }
}