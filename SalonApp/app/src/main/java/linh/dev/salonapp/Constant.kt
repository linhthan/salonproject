package linh.dev.salonapp


const val BASE_URL = "http://192.168.0.102/api"
const val BASE_URL_FLASK = "http://192.168.0.102:5000"
//const val BASE_URL = "http://192.168.43.125/api"
//const val BASE_URL_FLASK = "http://192.168.43.125:5000"
//192.168.43.208

const val API_VERSION = "v1"
const val DEBUG = true

const val PREF_USER = "pref_user"
const val PREF_CART_ITEM ="pref_cart_item"
const val PREF_FILE_PATH ="pref_file_path"

const val CUSTOMER_TYPE = 0
const val STYLIST_TYPE = 1
const val ADMIN_TYPE = 2
const val WAITER_TYPE = 3
const val SERVICE_EMPLOYEE = 4

const val BUNDLE_ID_SALON = "salonId"
const val BUNDLE_DURATION_SALON = "duration"
const val BUNDLE_NAME_SALON = "salonName"
const val BUNDLE_OPEN_TIME_SALON = "salonOpenTime"
const val BUNDLE_CLOSE_TIME_SALON = "salonCloseTime"
const val BUNDLE_AVG_TIME_SALON = "salonTimeSlot"
const val BUNDLE_THUMBNAIL_SALON = "thumbnail"
const val BUNDLE_TOTAL_RATE_SALON = "totalRate"
const val BUNDLE_DETAIL_LOCATION_SALON = "detailLocation"
const val BUNDLE_FILE_PATH = "filePath"
const val BUNDLE_LIST_SERVICE = "service_list"
const val BUNDLE_LIST_HAIR_STYLE = "listHairStyle"
const val BUNDLE_MODE = "mode"

const val BUNDLE_START_TIME  = "startTime"
const val BUNDLE_END_TIME  = "endTime"
const val BUNDLE_DATE  = "date"
const val BUNDLE_ID_BOOKING = "idBooking"
const val BUNDLE_STATUS= "status"
const val BUNDLE_CHECK_IN_TIME ="checkIn"
const val BUNDLE_ORDER ="order"
const val BUNDLE_PRODUCT_ID ="productId"
const val BUNDLE_PRODUCT_SELECTED ="listProduct"
const val BUNDLE_HOME_TAB  = "currentTab"
const val BUNDLE_ID_HAIR  = "idHair"
const val BUNDLE_LIST_HAIRSTYLE  = "listHair"

const val MONDAY = "2"
const val TUESDAY = "3"
const val WEDNESDAY = "4"
const val THURSDAY = "5"
const val FRIDAY = "6"
const val SATURDAY = "7"
const val SUNDAY = "8"

const val MONDAY_STRING = "Thứ hai"
const val TUESDAY_STRING = "Thứ ba"
const val WEDNESDAY_STRING = "Thứ tư"
const val THURSDAY_STRING = "Thứ năm"
const val FRIDAY_STRING = "Thứ sáu"
const val SATURDAY_STRING = "Thứ bảy"
const val SUNDAY_STRING = "Chủ nhật"
const val PATTERN_DATE_BOOKING = "dd-MM-yyyy"
const val PATTERN_HOUR_BOOKING = "hh:mm"
const val PATTERN_DATE_TAB = "dd"
const val PATTERN_DATE_TIME_BOOKING = "Y-M-D H:i:s"
const val EMPTY = ""
const val DEFAULT = "Ngẫu nhiên"

const val CAMERA_PIC_REQUEST = 90
const val GALLERY_REQUEST_CODE = 91
const val RC_SIGN_IN = 93
const val PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.8f
const val PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f
const val ALPHA_ANIMATIONS_DURATION = 200L


const val APPOINTMENT_STATUS_PENDING = 0
const val APPOINTMENT_STATUS_DONE = 1
const val APPOINTMENT_STATUS_CANCEL = 2
const val APPOINTMENT_STATUS_PAID = 3

const val KEY_DOWNLOAD_URL = "downloadUrl"
const val DATABASE_NAME = "hairstyle.db"

const val SERVER_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
const val APPOINTMENT_USED_FORMAT = "hh:mm a dd-MM-yyyy"
const val APPOINTMENT_HOUR_START_FORMAT = "hh:mm a"
const val APPOINTMENT_DATE_FORMAT = "dd-MM-yyyy"
const val CREATE_AT_TIME_FORMAT = "hh:mm a dd-MM-yyyy"
const val ORDER_DATE_FORMAT= "HH:mm:ss dd-MM-yyyy"

const val CROP_IMAGE_SIZE = 512
const val CROP_AVATAR_SIZE = 200

const val ERROR_NETWORK_REQUEST = "Có lỗi xảy ra, vui lòng kiểm tra kết nối mạng và thử lại!"
const val ERROR_UNKNOWN = "Có lỗi trong quá trình thực thi, vui lòng thử lại sau!"

const val PER_PAGE = 6
const val PER_PAGE_HAIR_STYLE = 9

const val ORDER_STATUS_PENDING = 0
const val ORDER_STATUS_TRANS= 1
const val ORDER_STATUS_RECEIVED = 2
const val ORDER_STATUS_CANCEL =3
const val ORDER_STATUS_PAID =4

const val CATEGORY_ALL ="Tất cả"
const val CATEGORY_ALL_ID = -1

const val EXTRA_ID_BOOKING = "id"
const val EXTRA_BOOKING_TIME= "booking_time"

const val REQUEST_CODE_ALARM = 441

enum class AppEvent {

    EVENT_ADD_SERVICE, EVENT_BOOKING_SUCCESS,EVENT_GO_TO_HOME,EVENT_CLICK_CANCEL_BUTTON,EVENT_GO_TO_APPOINTMENT_DETAIL,EVENT_DOWNLOAD_IMAGE_SUCCESS
    ,EVENT_COLLECT_IMAGE,EVENT_CROP_SUCCESS,EVENT_SAVE_HAIR_STYLE_SUCCESS,EVENT_CANCEL_ORDER_SUCCESS,EVENT_ADD_PRODUCT_TO_CART,EVENT_REMOVE_ART,EVENT_RATE_SUCCESS;

    private var extraData: Any? = null

    fun setData(extraData: Any): AppEvent {
        this.extraData = extraData
        return this
    }

    fun <T> getData(target: Class<T>): T? {
        return try {
            target.cast(extraData)
        } catch (e: Exception) {
            null
        }

    }

}



