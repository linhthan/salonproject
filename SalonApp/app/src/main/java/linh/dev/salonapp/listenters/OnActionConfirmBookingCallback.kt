package linh.dev.salonapp.listenters

import linh.dev.salonapp.model.local.Service

interface OnActionConfirmBookingCallback {
    fun onConfirmClick(startTime: Int, services: List<Service.ServiceJson>)
}