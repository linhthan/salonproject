package linh.dev.salonapp.ui.fragment.customers

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_gallery_hairstyle.*
import kotlinx.android.synthetic.main.layout_no_data.*
import linh.dev.base.BaseFragment
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showMsg
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.GalleryAdapter
import linh.dev.salonapp.databinding.FragmentGalleryHairstyleBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.GalleryViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class GalleryHairFragment : BaseFragment<FragmentGalleryHairstyleBinding>() {
    private val viewModel: GalleryViewModel by viewModel()
    private val adapter: GalleryAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_gallery_hairstyle
    }

    override fun initView() {
        super.initView()
        rcvSalonGallery.layoutManager = LinearLayoutManager(context)
        rcvSalonGallery.adapter = adapter
        adapter.setOnItemClickListener(object : BaseRecycleViewAdapter.ItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val item = adapter.getItem(position)
                val bundle = Bundle()
                bundle.putInt(BUNDLE_ID_SALON, item.id)
                bundle.putString(BUNDLE_NAME_SALON, item.name)
                bundle.putString(BUNDLE_LIST_HAIRSTYLE, Gson().toJson(item.styles))
                Navigation.findNavController(requireActivity(), R.id.nav_main)
                    .navigate(R.id.action_galleryHairFragment_to_fragmentHairStyleItem, bundle)
            }

        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.salonIdList.observe(viewLifecycleOwner, Observer {
            it.forEach {
                logE("salonId: $it")
            }
            if (it.isNullOrEmpty()) {
                viewNoData.show()
            } else {
                viewNoData.hide()
                viewModel.getSalonWithHairStyleResult(it)
            }
        })
        viewModel.salonWithHairStyleResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoading.show()
                }
                Result.Status.SUCCESS -> {
                    progressLoading.hide()
                    if (it.data != null) {
                        if (it.data.success) {
                            viewNoData.hide()
                            adapter.setItems(it.data.data)
                        } else {
                            viewNoData.show()
                        }
                    }

                }
                Result.Status.ERROR -> {
                    progressLoading.hide()
                    viewNoData.show()
                    showMsg(it.message)
                }
                Result.Status.UNAUTHORIZED -> {
                    progressLoading.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
    }

}