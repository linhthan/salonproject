package linh.dev.salonapp.adapter

import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.util.forEach
import androidx.core.util.set
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_item_product.view.*
import kotlinx.android.synthetic.main.view_item_service.view.*
import linh.dev.base.ext.view.hide
import linh.dev.salonapp.R
import linh.dev.salonapp.State
import linh.dev.salonapp.databinding.ViewItemFooterBinding
import linh.dev.salonapp.databinding.ViewItemProductBinding
import linh.dev.salonapp.model.local.Product


class ProductSalonAdapter :
    PagedListAdapter<Product, RecyclerView.ViewHolder>(ProductDiffCallback) {
    val isShowItem = SparseBooleanArray()
    private val indexSelected = SparseBooleanArray()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == DATA_VIEW_TYPE) {
            val binding: ViewItemProductBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_product, parent, false
            )
            ViewHolder(binding)
        } else {
            val binding: ViewItemFooterBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_footer, parent, false
            )
            return ListFooterViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.onBindingData(position)
        } else if (holder is ListFooterViewHolder) {
            holder.bind(state)
        }
    }


    private var state = State.LOADING

    private var onItemClick: OnItemClick? = null

    interface OnItemClick {
        fun onItemClick(position: Int)
        fun onViewClick(resId: Int, position: Int)
    }

    fun onAddProductClick(position: Int) {
        if (indexSelected[position]) {
            indexSelected.put(position, false)
        } else {
            indexSelected.put(position, true)
        }
        notifyItemChanged(position)
    }
    fun clearSelected(){
        indexSelected.clear()
    }
    override fun submitList(pagedList: PagedList<Product>?) {
        pagedList?.forEach {
            isShowItem[it.id] = true
        }
        super.submitList(pagedList)

    }
    fun getProductSelected(): ArrayList<Product> {
        val results = ArrayList<Product>()
        indexSelected.forEach { index, selected ->
            if (selected && index in (0 until itemCount)) {
                val item = getItem(index)
                item?.let {
                    results.add(it)
                }

            }
        }
        return results
    }
    fun setItemClick(onItemClick: OnItemClick) {
        this.onItemClick = onItemClick
    }

    fun clearData() {
        currentList?.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
//        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }

    fun getItemByPosition(position: Int): Product? {
        return super.getItem(position)
    }


    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }


    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2

    companion object {
        val ProductDiffCallback = object : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
                return oldItem.id == newItem.id && oldItem.title == newItem.title
                        && oldItem.catId == newItem.catId && oldItem.price == newItem.price &&
                        oldItem.thumbnail == newItem.thumbnail && oldItem.description == newItem.description
                        && oldItem.salonId == newItem.salonId
            }
        }
    }

    inner class ListFooterViewHolder(val binding: ViewItemFooterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(status: State?) {
            if(status==State.DONE){
                binding.progressBar.hide()
                binding.txtError.hide()
            }else{
                binding.progressBar.visibility =
                    if (status == State.LOADING) View.VISIBLE else View.INVISIBLE
                binding.txtError.visibility =
                    if (status == State.ERROR) View.VISIBLE else View.INVISIBLE
            }

            binding.txtError.setOnClickListener {
                onItemClick?.onViewClick(
                    R.id.txt_error,
                    adapterPosition
                )
            }
        }


    }

    inner class ViewHolder(val binding: ViewItemProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBindingData(position: Int) {
            if(position in 0 until itemCount){
                val item = getItem(position)
                binding.viewModel = item
                binding.showHide = if (item == null) false else isShowItem[item.id]
                if (indexSelected[position]) {
                    binding.root.imvAddProduct.setImageResource(R.drawable.ic_added_service)
                } else {
                    binding.root.imvAddProduct.setImageResource(R.drawable.ic_add_service)
                }
                binding.root.setOnClickListener {
                    onItemClick?.onItemClick(position)
                }
                binding.imvAddProduct.setOnClickListener {
                    onItemClick?.onViewClick(R.id.imvAddProduct,position)
                }
            }

        }

    }
}