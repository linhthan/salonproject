package linh.dev.salonapp.model.remote

import com.google.gson.annotations.SerializedName
import linh.dev.salonapp.model.local.Product
import linh.dev.salonapp.model.local.Service

data class OrderParams(
    @SerializedName("salon_id") val salonId: Int,
    @SerializedName("custom_id") val userId: Int,
    @SerializedName("cus_name") val cusName: String,
    @SerializedName("phone_number") val phoneNumber: String,
    @SerializedName("address") val address: String,
    @SerializedName("note") val note: String?,
    val listProduct: List<Product>
)