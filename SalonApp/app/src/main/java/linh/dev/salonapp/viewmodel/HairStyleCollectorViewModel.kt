package linh.dev.salonapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.PER_PAGE_HAIR_STYLE
import linh.dev.salonapp.State
import linh.dev.salonapp.data.HairStyleDataSource
import linh.dev.salonapp.data.HairStyleDataSourceFactory
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.HairStyle
import linh.dev.salonapp.repository.HairStyleRepository
import linh.dev.salonapp.repository.SalonRepository

class HairStyleCollectorViewModel(
    val hairStyleRepository: HairStyleRepository,
    val salonRepository: SalonRepository
) : BaseViewModel() {
    val hairStyleList: LiveData<PagedList<HairStyle>>
    val compositeDisposable = CompositeDisposable()
    val saveHairStyleResult = salonRepository.saveHairStyleResult
    val newHairStyleResult = salonRepository.newHairStyleResult

    val allHairStyleResult = salonRepository.allHairStyleResult
    val moreHairStyleResult = salonRepository.moreHairStyleResult

    private val hairStyleDataSourceFactory: HairStyleDataSourceFactory =
        HairStyleDataSourceFactory(user()?.salonId, salonRepository.api, viewModelScope)

    val loadingAllHairStyle = MutableLiveData<Boolean>()
    var endPage: Boolean = false
    var currentPage = 1

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(PER_PAGE_HAIR_STYLE)
            .setInitialLoadSizeHint(PER_PAGE_HAIR_STYLE)
            .setEnablePlaceholders(false)
            .build()
        hairStyleList = LivePagedListBuilder(hairStyleDataSourceFactory, config).build()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    init {
        loadNewHairStyle()
        loadAllHairStyle()
    }
    fun refresh(){
        endPage = false
        currentPage = 1
        loadNewHairStyle()
        loadAllHairStyle()
    }

    fun loadNewHairStyle() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    salonRepository.getNewHairStyle(it.getBearToken(), it.salonId)
                }
            }
        }
    }

    private fun loadAllHairStyle() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    salonRepository.getAllHairStyle(it.getBearToken(), it.salonId)
                }
            }
        }
    }

    fun loadMoreHairStyle() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    salonRepository.loadMoreHairStyle(it.getBearToken(), it.salonId, currentPage)
                }
            }
        }
    }

    fun getState(): LiveData<State> = Transformations.switchMap<HairStyleDataSource,
            State>(
        hairStyleDataSourceFactory.hairStyleDataSourceLiveData,
        HairStyleDataSource::state
    )

    fun retryFailed() {
        hairStyleDataSourceFactory.hairStyleDataSourceLiveData.value?.retryFailedQuery()
    }


    fun listIsEmpty(): Boolean {
        return hairStyleList.value?.isEmpty() ?: true
    }

    fun refreshData() {
        hairStyleDataSourceFactory.hairStyleDataSourceLiveData.value?.refresh()
    }

}