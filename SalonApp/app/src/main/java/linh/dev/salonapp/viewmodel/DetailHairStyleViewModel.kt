package linh.dev.salonapp.viewmodel

import android.content.Context
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.google.gson.Gson
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.HairStyle
import linh.dev.salonapp.network.ImageDownloadWorker
import linh.dev.salonapp.repository.HairSalonRepository
import linh.dev.salonapp.repository.HairStyleRepository

class DetailHairStyleViewModel(val styleId: Int, val repository: HairSalonRepository) :
    BaseViewModel() {
    val hairStyleLocalById = repository.getHairStyleById(styleId)
    val hairStyleDetailResult = repository.hairStyleDetailResult

    init {
        updateViewHair()
        getHairStyleDetail()
    }

    private fun updateViewHair() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    repository.updateViewHairStyle(it.getBearToken(), styleId)
                }
            }
        }
    }

    fun getHairStyleDetail() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    repository.getHairStyleDetail(it.getBearToken(), styleId)
                }
            }
        }
    }

    fun downloadListImage(context: Context, images: List<HairStyle>) {
        val jsonImages = Gson().toJson(images)
        val data = Data.Builder()
            .putString("images", jsonImages)
            .build()

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)

        val oneTimeRequest = OneTimeWorkRequest.Builder(ImageDownloadWorker::class.java)
            .setInputData(data)
            .setConstraints(constraints.build())
            .addTag("demo")
            .build()



        WorkManager.getInstance(context)
            .enqueueUniqueWork("SalonApp", ExistingWorkPolicy.APPEND, oneTimeRequest)

    }


}