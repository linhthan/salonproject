package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.User

data class StylistResponse(val message: String, val data: List<User>)