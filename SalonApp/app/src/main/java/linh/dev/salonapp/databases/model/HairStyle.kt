package linh.dev.salonapp.databases.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "hairstyle")
class HairStyle(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "id_hair") var idHair: Int,
    @ColumnInfo(name = "file_path") var filePath: String,
    @ColumnInfo(name = "salon_id") var salonId: Int
)