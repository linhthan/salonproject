package linh.dev.salonapp.ui.fragment.customers

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.animation.AlphaAnimation
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.GsonBuilder
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_salon_detail.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.PagerSalonInfoAdapter
import linh.dev.salonapp.databinding.FragmentSalonDetailBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.Service
import linh.dev.salonapp.viewmodel.SalonDetailViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import kotlin.math.abs

class SalonDetailFragment : BaseFragment<FragmentSalonDetailBinding>() {
    private val args: SalonDetailFragmentArgs by navArgs()
    private val mViewModel: SalonDetailViewModel by viewModel { parametersOf(args.salonId) }


    private var mIsTheTitleVisible = false
    private var mIsTheTitleContainerVisible = true

    override fun getLayoutResourceId(): Int = R.layout.fragment_salon_detail

    val pagerSalonInfoAdapter: PagerSalonInfoAdapter by inject {
        parametersOf(
            args.salonId,
            childFragmentManager
        )
    }

    @SuppressLint("StringFormatMatches")
    override fun initView() {
        super.initView()
        mBinding.thumbnail = args.thumbnail
        mBinding.salonName = args.salonName
        mBinding.salonLocation = args.detailLocation
        mBinding.totalRate = args.totalRate
        mViewModel.progressLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                avLoading.show()
            } else {
                avLoading.hide()
            }

        })
        viewPagerSalonInfo.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if(position==0){
                    if(mViewModel.serviceSelected.value.isNullOrEmpty()){
                        layoutCart.hide()
                    }else{
                        layoutCart.show()
                    }
                }else{
                    layoutCart.hide()
                }
            }

        })
        mViewModel.serviceSelected.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) {
                layoutCart.visibility = View.GONE
            } else {
                layoutCart.visibility = View.VISIBLE
                tvCountService.text = it.size.toString()
                var totalPrice = 0
                it.forEach { service ->
                    totalPrice += service.price.toInt()
                }
                tvTotalPrice.text = resources.getString(R.string.price_service, (totalPrice / 1000))
            }
        })
        startAlphaAnimation(toolbar, 0, View.GONE)

//
//        imvBack.setOnClickListener {
//            logE("Click imvBack")
//            Navigation.findNavController(requireActivity(), R.id.nav_main).popBackStack()
//        }
        imvShopCart.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(BUNDLE_ID_SALON, args.salonId)
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(R.id.action_salonDetailFragment_to_productSalonFragment, bundle)
        }
        imvBack.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.nav_main).popBackStack()
        }
        imvThumbSalon.setOnClickListener {
            logE("Click thumb")
        }
//        imvInfo.setOnClickListener {
//            logE("Click imvInfo")
//        }
        imvViewRate.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(BUNDLE_ID_SALON, args.salonId)
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(R.id.action_salonDetailFragment_to_fragmentRateSalon, bundle)
        }
        appBar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            appBarLayout?.let {
                val maxScroll = it.totalScrollRange
                if (maxScroll != 0) {
                    val percentage = abs(verticalOffset).toFloat() / maxScroll.toFloat()
                    handleAlphaOnTitle(percentage)
                    handleToolbarTitleVisibility(percentage)
                } else {
                    handleAlphaOnTitle(0F)
                    handleToolbarTitleVisibility(0F)
                }
            }
        })


        btnBooking.setOnClickListener {
            val bundle = Bundle()
            val dataSelected = mViewModel.serviceSelected.value
            if (dataSelected.isNullOrEmpty()) {
                showToast("Bạn chưa chọn dịch vụ!")
            } else {
                var duration = 0
                val service_list = GsonBuilder().create().toJson(dataSelected)
                for (service in dataSelected) {
                    duration += service.duration
                }
                bundle.putInt(BUNDLE_ID_SALON, args.salonId)
                bundle.putString(BUNDLE_NAME_SALON, args.salonName)
                bundle.putInt(BUNDLE_DURATION_SALON, duration)
                bundle.putString(BUNDLE_DETAIL_LOCATION_SALON, args.detailLocation)
                bundle.putString(BUNDLE_LIST_SERVICE, service_list)
                Navigation.findNavController(requireActivity(), R.id.nav_main)
                    .navigate(R.id.action_salonDetailFragment_to_bookingFragment, bundle)
            }

        }
        viewPagerSalonInfo.adapter = pagerSalonInfoAdapter
        viewPagerSalonInfo.offscreenPageLimit = 3
        tabSalonInfo.setupWithViewPager(viewPagerSalonInfo)

    }

    private fun handleToolbarTitleVisibility(percentage: Float) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(toolbar, ALPHA_ANIMATIONS_DURATION, View.VISIBLE)
                mIsTheTitleVisible = true
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(toolbar, ALPHA_ANIMATIONS_DURATION, View.GONE)
                mIsTheTitleVisible = false
            }
        }
    }

    private fun handleAlphaOnTitle(percentage: Float) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
//                startAlphaAnimation(layoutThumb, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE)
                mIsTheTitleContainerVisible = false
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
//                startAlphaAnimation(layoutThumb, ALPHA_ANIMATIONS_DURATION, View.VISIBLE)
                mIsTheTitleContainerVisible = true
            }
        }
    }

    private fun startAlphaAnimation(v: View, duration: Long, visibility: Int) {
        val alphaAnimation = if (visibility == View.VISIBLE)
            AlphaAnimation(0f, 1f)
        else
            AlphaAnimation(1f, 0f)

        alphaAnimation.duration = duration
        alphaAnimation.fillAfter = true
        v.startAnimation(alphaAnimation)
    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_ADD_SERVICE) {
            val data = appAction.event.getData(List::class.java)
            data?.filterIsInstance<Service>()?.forEach { item ->
                logE(item.content)
            }
            mViewModel.serviceSelected.value = data as List<Service>
        }
    }
}