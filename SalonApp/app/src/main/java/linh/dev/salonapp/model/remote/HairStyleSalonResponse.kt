package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.HairStyle

data class HairStyleSalonResponse(val success: Boolean, val message: String, val data: List<HairStyle>)