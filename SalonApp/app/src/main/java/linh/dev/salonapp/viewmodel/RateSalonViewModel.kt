package linh.dev.salonapp.viewmodel

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.SalonRepository

class RateSalonViewModel(val salonId: Int, val repository: SalonRepository) : BaseViewModel() {
    val reviewSalonResult = repository.reviewSalonResult

    init {
        getAllReview()
    }

     fun getAllReview() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    repository.getAllReviewSalon(it.getBearToken(), salonId)
                }
            }
        }
    }
}