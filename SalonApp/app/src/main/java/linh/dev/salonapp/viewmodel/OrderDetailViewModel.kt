package linh.dev.salonapp.viewmodel

import androidx.lifecycle.viewModelScope
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Order
import linh.dev.salonapp.repository.SalonRepository

class OrderDetailViewModel(val orderJson:String,val salonRepository: SalonRepository):BaseViewModel() {
    val order = Gson().fromJson<Order>(orderJson, object : TypeToken<Order>() {}.type)

    val orderItemResult =  salonRepository.orderItemResult
    val cancelOrderResult =  salonRepository.cancelOrderResult

    init {
       loadOrderDetail(order.id)
    }
    private fun loadOrderDetail(orderId:Int){
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    salonRepository.getOrderDetail(it.getBearToken(), orderId)
                }
            }
        }

    }
    fun cancelOrder(){
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    salonRepository.cancelOrder(it.getBearToken(), order.id)
                }
            }
        }
    }
}