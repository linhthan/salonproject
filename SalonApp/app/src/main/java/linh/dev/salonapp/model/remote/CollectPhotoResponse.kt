package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Art

data class CollectPhotoResponse(val success: Boolean, val message: String, val data: Art?)