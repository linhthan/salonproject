package linh.dev.salonapp.ui.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_history_appointment.*
import kotlinx.android.synthetic.main.layout_no_data.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.currentText
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.AppointmentDoneAdapter
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.Art
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.viewmodel.HistoryAppointmentViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService




class HistoryAppointmentFragment :
    BaseFragment<linh.dev.salonapp.databinding.FragmentHistoryAppointmentBinding>() {
    private val viewModel: HistoryAppointmentViewModel by sharedViewModel()
    private val adapter: AppointmentDoneAdapter by inject()
    private var firstTime = true

    companion object {
        fun newInstance(): HistoryAppointmentFragment {
            return HistoryAppointmentFragment()
        }
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_history_appointment
    }

    override fun initView() {
        super.initView()
        edtSearch.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == IME_ACTION_SEARCH
                    || event?.action == KeyEvent.KEYCODE_SEARCH
                ) {
                    Log.e("search",edtSearch.currentText)
                    val keySearch = edtSearch.currentText
                    if (viewModel.keyword.value != null && !viewModel.keyword.value.equals(keySearch)) {
                        Log.e("equal",edtSearch.currentText)
                        viewModel.keyword.value = keySearch
                    }


                    return true

                }
                return false
            }

        })
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        val imm = context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.hideSoftInputFromWindow(edtSearch.windowToken, 0)

        initRecyclerView()
        initRefreshData()

    }


    private fun initRefreshData() {
        swrLayout.setOnRefreshListener {
            viewModel.refreshData()
        }
        btnTryAgain.setOnClickListener {
            viewModel.refreshData()
        }
    }

    private fun initRecyclerView() {
        rcvListAppointment.layoutManager = LinearLayoutManager(context)
        rcvListAppointment.addItemDecoration(SpaceItemDecoration(10, 10, 10, 10, 10))
        rcvListAppointment.adapter = adapter
        adapter.setItemClick(object : AppointmentDoneAdapter.OnItemClick {
            override fun onViewClick(resId: Int, position: Int) {
                if (resId == R.id.txt_error) {
                    viewModel.retryFailed()
                }
            }

            override fun onItemClick(position: Int) {
                val item = adapter.getItemByPosition(position)
                item?.let {
                    val bundle = Bundle()
                    bundle.putInt(BUNDLE_ID_BOOKING, it.id)
                    Navigation.findNavController(requireActivity(), R.id.nav_main)
                        .navigate(
                            R.id.action_fragmentCollectorHome_to_appointmentDetailCSFragment2,
                            bundle
                        )
                }

            }

        })
        viewModel.appointmentUsedList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initObservable() {
        viewModel.keyword.observe(viewLifecycleOwner, Observer {
            logE("search $it")
            if(it.equals(EMPTY)&& firstTime){
                firstTime = false
            }else{
                viewModel.replaceSubscription()
            }

        })
        viewModel.getState().observe(viewLifecycleOwner, Observer { state ->
            if (swrLayout.isRefreshing && state == State.LOADING) {
                swrLayout.isRefreshing = false
            }
            if (viewModel.listIsEmpty() && state == State.LOADING) {
                disableTouchView.show()
            } else {
                disableTouchView.hide()
            }
            if (viewModel.listIsEmpty() && state == State.ERROR) {
                disableTouchView.hide()
                viewNoData.show()
            } else {
                viewNoData.hide()
            }
            if (viewModel.listIsEmpty() && adapter.itemCount == 1) {
                viewNoData.show()
            } else {
                viewNoData.hide()
            }
            adapter.setState(state ?: State.DONE)

        })
    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_COLLECT_IMAGE) {
            val image = appAction.event.getData(Art::class.java)
            image?.let { art ->
                viewModel.appointmentUsedList.value?.forEachIndexed { index, item ->
                    if (item.id == art.id_booking) {
                        item.images.add(art)
                        adapter.notifyItemChanged(index)
                    }
                }
            }

        } else if (appAction.event == AppEvent.EVENT_REMOVE_ART) {
            val image = appAction.event.getData(Art::class.java)
            image?.let { art ->
                viewModel.appointmentUsedList.value?.forEachIndexed { index, item ->
                    if (item.id == art.id_booking) {
                        item.images.remove(art)
                        adapter.notifyItemChanged(index)
                    }
                }
            }
        }

    }
}