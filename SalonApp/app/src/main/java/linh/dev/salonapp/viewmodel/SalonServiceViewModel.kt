package linh.dev.salonapp.viewmodel

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Service
import linh.dev.salonapp.repository.SalonRepository
import java.lang.Exception

class SalonServiceViewModel(salonId:Int?, private val salonRepository: SalonRepository) : BaseViewModel() {
    private val job = SupervisorJob()
    private val launchScope = CoroutineScope(Dispatchers.IO + job)
    val serviceData = MutableLiveData<List<Service>>()
    val progressLoading = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()

    init {
        progressLoading.value = false
        errorMessage.value = EMPTY
        salonId?.let {
            loadSalonService(it)
        }

    }

    fun loadSalonService(salonId: Int) {
        user()?.let {
            launchScope.launch {
                withContext(Dispatchers.Main) {
                    progressLoading.value = true

                }
                try {
                    val salonDetailResponse = salonRepository.getSalonService(it.getBearToken(),salonId)
                    withContext(Dispatchers.Main) {
                        progressLoading.value = false
                        val data = salonDetailResponse.data
                        if (data.isNotEmpty()) {
                            serviceData.value = data
                        } else {
                            errorMessage.value = "Salon hiện tại không có dịch vụ nào!"
                        }
                    }

                } catch (ex: Exception) {
                    withContext(Dispatchers.Main) {
                        progressLoading.value = false
                        errorMessage.value = ex.message
                    }
                    ex.printStackTrace()
                }

            }
        }

    }
}