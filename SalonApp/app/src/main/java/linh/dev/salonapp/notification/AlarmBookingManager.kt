package linh.dev.salonapp.notification

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.AlarmManagerCompat
import linh.dev.salonapp.model.local.Appointment
import androidx.core.content.ContextCompat.getSystemService
import linh.dev.salonapp.EXTRA_BOOKING_TIME
import linh.dev.salonapp.EXTRA_ID_BOOKING
import linh.dev.salonapp.REQUEST_CODE_ALARM
import linh.dev.salonapp.SERVER_TIME_FORMAT
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.util.DateTimeUtils
import java.util.*
import java.util.concurrent.TimeUnit


class AlarmBookingManager {
    companion object {
        fun setAlarmBooking(context: Context, appointment: Appointment) {
            val alarmMgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            val cal = DateTimeUtils.convertStringToCalendar(
                appointment.booking_time,
                SERVER_TIME_FORMAT
            )
            cal?.let {
                val now = Calendar.getInstance()
                logE(cal <= now)
                if (cal <= now) {
                    return
                }
                val minutesBetween =
                    TimeUnit.MILLISECONDS.toMinutes(cal.timeInMillis - now.timeInMillis)
                if (minutesBetween <= 30) {
                    cal.add(Calendar.MINUTE, 0 - minutesBetween.toInt() + 1)
                } else {
                    cal.add(Calendar.MINUTE, -30)
                }
                val intent = Intent(context, AlarmReceiver::class.java)
                intent.putExtra(EXTRA_ID_BOOKING, appointment.id)
                intent.putExtra(EXTRA_BOOKING_TIME, appointment.booking_time)
                val pendingIntent = PendingIntent.getBroadcast(
                    context,
                    REQUEST_CODE_ALARM,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
                alarmMgr.set(AlarmManager.RTC_WAKEUP, cal.timeInMillis, pendingIntent)
            }
        }
    }
}