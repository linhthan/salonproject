package linh.dev.salonapp.ui.fragment

import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_sign_up.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.currentText
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.ADMIN_TYPE
import linh.dev.salonapp.CUSTOMER_TYPE
import linh.dev.salonapp.R
import linh.dev.salonapp.STYLIST_TYPE
import linh.dev.salonapp.ext.isContainSpecialCharacter
import linh.dev.salonapp.ext.isValidEmail
import linh.dev.salonapp.ext.isValidPhoneNumber
import linh.dev.salonapp.ui.activitys.CustomerHomeActivity
import linh.dev.salonapp.viewmodel.SignUpViewModel
import org.koin.android.ext.android.inject

class SignUpFragment : BaseFragment<linh.dev.salonapp.databinding.FragmentSignUpBinding>() {
    private val viewModel: SignUpViewModel by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_sign_up
    }

    override fun initView() {
        super.initView()

        viewModel.progressLoading.observe(this, Observer {
            if (it) {
                progressLoading.visibility = View.VISIBLE
            } else {
                progressLoading.visibility = View.GONE
            }
        })
        viewModel.errorMsg.observe(this, Observer {
            showToast(it)
        })
        viewModel.user.observe(this, Observer {
            if (it.user_type == ADMIN_TYPE) {
                val email = it.email
                val action =
                    SignUpFragmentDirections.actionSignUpFragmentToCreateSalonFragment2(email = email)
                findNavController().navigate(action)
            } else if (it.user_type == CUSTOMER_TYPE) {
                startActivity(Intent(context, CustomerHomeActivity::class.java))
                activity?.finish()
            }

        })

        tvBackToLogin.setOnClickListener {
            findNavController().popBackStack()
        }
        imvBack.setOnClickListener {
            findNavController().popBackStack()
        }
        btnSignUp.setOnClickListener {
            val email = edtEmail.currentText
            val name = edtName.currentText
            val phoneNumber = edtPhoneNumber.currentText
            val password = edtPassword.currentText
            if (email.isBlank() || name.isBlank() || phoneNumber.isBlank() || password.isBlank()) {
                showToast(getString(R.string.sign_up_fill_field_error_msg))
            } else if (!email.isValidEmail()) {
                showToast(getString(R.string.email_invalid_error_msg))
            } else if (!phoneNumber.isValidPhoneNumber()) {
                showToast(getString(R.string.phone_number_invalid_error_msg))
            } else if (password.length < 6) {
                showToast(getString(R.string.password_too_short_error_msg))
            } else if (password.isContainSpecialCharacter()) {
                showToast(getString(R.string.password_contain_special_character_error_msg))
            } else {
                viewModel.signUp(email, name, phoneNumber, password)
            }
        }
    }

}