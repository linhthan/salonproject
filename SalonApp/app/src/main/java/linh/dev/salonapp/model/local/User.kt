package linh.dev.salonapp.model.local

import com.google.gson.annotations.SerializedName

open class User(
    var id: Int,
    var name: String,
    var email: String, @SerializedName("job_id") var jobId: Int?,
    var sex: String,
    var avatar: String?,
    var user_type: Int, @SerializedName("phone_number") var phoneNumber: String?,
    var token: String?,
    var salonId:Int,
    var isNonePassUser:Boolean
) {
    fun getBearToken(): String {
        return " Bearer $token"
    }
}