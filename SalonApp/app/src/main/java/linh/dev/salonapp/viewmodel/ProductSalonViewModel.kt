package linh.dev.salonapp.viewmodel

import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.CATEGORY_ALL_ID
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.PER_PAGE
import linh.dev.salonapp.State
import linh.dev.salonapp.data.ProductDataSource
import linh.dev.salonapp.data.ProductDataSourceFactory
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Product
import linh.dev.salonapp.model.local.Service
import linh.dev.salonapp.repository.ProductRepository
import linh.dev.salonapp.ui.custom.SingleLiveEvent

class ProductSalonViewModel(val salonId: Int, val repository: ProductRepository) : BaseViewModel() {
    var productList: LiveData<PagedList<Product>>
    private var productDataSourceFactory: ProductDataSourceFactory
    var firstTime = true
    var catId = SingleLiveEvent<Int>()
    var keyword = SingleLiveEvent<String>()
    val currentListProduct = MutableLiveData<ArrayList<Int>>()
    val categoryProductResult = repository.categoryProductResult
    val productSelected = MutableLiveData<List<Product>>()
    init {
        currentListProduct.value = ArrayList()
        productSelected.value = ArrayList()
        keyword.value = EMPTY
        catId.value = CATEGORY_ALL_ID
        productDataSourceFactory =
            ProductDataSourceFactory(CATEGORY_ALL_ID, EMPTY, salonId, repository.api, viewModelScope)
        val config = PagedList.Config.Builder()
            .setPageSize(PER_PAGE)
            .setInitialLoadSizeHint(PER_PAGE)
            .setEnablePlaceholders(false)
            .build()
        productList = LivePagedListBuilder(productDataSourceFactory, config).build()
        getProductCategory()
    }

    private fun createProductData(catId: Int, keyword: String): LiveData<PagedList<Product>> {
        productDataSourceFactory =
            ProductDataSourceFactory(catId, keyword, salonId, repository.api, viewModelScope)
        val config = PagedList.Config.Builder()
            .setPageSize(PER_PAGE)
            .setInitialLoadSizeHint(PER_PAGE)
            .setEnablePlaceholders(false)
            .build()
        return LivePagedListBuilder(productDataSourceFactory, config).build()
    }


    private fun getProductCategory() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    repository.getProductSalonCategory(it.getBearToken(), salonId)
                }
            }
        }
    }

    fun searchProduct(lifecycleObserver: LifecycleOwner,catId: Int, keyword: String) {
        productList.removeObservers(lifecycleObserver)
        productList = createProductData(catId, keyword)
    }

    fun replaceSubscription( catId: Int,keyword: String) {
//        productList.removeObservers(lifecycleObserver)
//        productList = createProductData(catId, keyword)
        productDataSourceFactory.catId = catId
        productDataSourceFactory.keyword = keyword
        refreshData()
    }


    fun getState(): LiveData<State> = Transformations.switchMap<ProductDataSource,
            State>(
        productDataSourceFactory.productDataSourceLiveData,
        ProductDataSource::state
    )

    fun retryFailed() {
        productDataSourceFactory.productDataSourceLiveData.value?.retryFailedQuery()
    }


    fun listIsEmpty(): Boolean {
        return productList.value?.isEmpty() ?: true
    }

    fun refreshData() {
        productDataSourceFactory.productDataSourceLiveData.value?.refresh()
    }
}