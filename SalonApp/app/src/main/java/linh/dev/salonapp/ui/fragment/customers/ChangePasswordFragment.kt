package linh.dev.salonapp.ui.fragment.customers

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.lifecycle.Observer
import cn.refactor.lib.colordialog.PromptDialog
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_change_password.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.currentText
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.salonapp.MainActivity
import linh.dev.salonapp.PREF_USER
import linh.dev.salonapp.R
import linh.dev.salonapp.SalonApplication
import linh.dev.salonapp.databinding.FragmentChangePasswordBinding
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.ChangePasswordViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ChangePasswordFragment : BaseFragment<FragmentChangePasswordBinding>() {
    private val viewModel: ChangePasswordViewModel by viewModel()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_change_password
    }

    override fun initView() {
        super.initView()
        user()?.let {
            if (it.isNonePassUser) {
                tilOldPass.hide()
                edtOldPass.hide()
            } else {
                tilOldPass.show()
                edtOldPass.show()
            }
        }
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }
        btnChangePassword.setOnClickListener {
            validInputAndSubmit()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.updatePassResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressUpdate.show()
                    btnChangePassword.showProgress {
                        buttonText = getString(R.string.uploading_update_password)
                        progressColor = Color.WHITE
                    }
                }
                Result.Status.SUCCESS -> {
                    progressUpdate.hide()
                    btnChangePassword.hideProgress(getString(R.string.common_change_password))
                    if (it.data != null) {
                        if (it.data.success) {
                            user()?.let { user ->
                                user.isNonePassUser = false
                                PrefUtil.savePreference(
                                    SalonApplication.applicationContext(),
                                    PREF_USER, Gson().toJson(user)
                                )
                            }
                            showDialog(it.data.message, PromptDialog.DIALOG_TYPE_SUCCESS)
                        } else {
                            showDialog(it.data.message, PromptDialog.DIALOG_TYPE_WRONG)
                        }
                    }
                }
                Result.Status.ERROR -> {
                    progressUpdate.hide()
                    btnChangePassword.hideProgress(getString(R.string.common_change_password))
                    it.message?.let { msg ->
                        showDialog(msg, PromptDialog.DIALOG_TYPE_WRONG)
                    }
                }
                Result.Status.UNAUTHORIZED -> {
                    progressUpdate.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
    }

    private fun showDialog(message: String, type: Int) {
        val dialog = PromptDialog(context)
        dialog.dialogType = type
        dialog.setAnimationEnable(true)
        dialog.setTitleText(
            if (type == PromptDialog.DIALOG_TYPE_SUCCESS) getString(R.string.success_dialog_title) else getString(
                R.string.error_dialog_title
            )
        )
        dialog.setContentText(message)
        dialog.setPositiveListener(getString(R.string.dialog_ok)) {
            it.dismiss()
        }
        dialog.setOnDismissListener {
            if (type == PromptDialog.DIALOG_TYPE_SUCCESS) {
                activity?.onBackPressed()
            }
        }
        dialog.show()
    }

    private fun validInputAndSubmit() {
        val oldPassword = edtOldPass.currentText
        val newPassword = edtNewPass.currentText
        val retypePassword = edtRetypePass.currentText
        var error = false
        user()?.let {
            if (!it.isNonePassUser) {
                if (oldPassword.isEmpty()) {
                    error = true
                    tilOldPass.error = getString(R.string.old_password_blank_error_msg)
                } else {
                    tilOldPass.error = null
                }
                if (oldPassword.length < 6) {
                    error = true
                    tilOldPass.error = getString(R.string.password_too_short_error_msg)
                }
            }
        }

        if (newPassword.isEmpty()) {
            error = true
            tilNewPass.error = getString(R.string.new_password_blank_error_msg)
        } else {
            tilNewPass.error = null
        }
        if (retypePassword.isEmpty()) {
            error = true
            tilRetypePass.error = getString(R.string.retype_password_blank_error_msg)
        } else {
            tilRetypePass.error = null
        }

        if (newPassword.length < 6) {
            error = true
            tilNewPass.error = getString(R.string.password_too_short_error_msg)
        }
        if (retypePassword.length < 6) {
            error = true
            tilRetypePass.error = getString(R.string.password_too_short_error_msg)
        }
        if (newPassword != retypePassword) {
            error = true
            tilNewPass.error = getString(R.string.same_password_error_msg)
            tilRetypePass.error = getString(R.string.same_password_error_msg)
        }
        if (!error) {
            viewModel.updatePassword(oldPassword, newPassword)
        }
    }
}