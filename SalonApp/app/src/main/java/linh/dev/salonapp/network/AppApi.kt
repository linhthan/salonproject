package linh.dev.salonapp.network

import com.google.gson.JsonObject
import linh.dev.salonapp.model.local.*
import linh.dev.salonapp.model.remote.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface AppApi {

    @POST("loginEmail")
    @FormUrlEncoded
    suspend fun loginWithEmail(@Field("email") email: String, @Field("password") password: String): LoginResponse

    @POST("loginFacebook")
    @FormUrlEncoded
    suspend fun loginWithFacebook(
        @Field("email") email: String, @Field("name") name: String, @Field(
            "avatar"
        ) avatar: String
    ): LoginResponse

    @POST("loginGoogle")
    @FormUrlEncoded
    suspend fun loginWithGoogle(@Field("email") email: String, @Field("name") name: String, @Field("avatar") avatar: String?): LoginResponse

    @POST("loginAnonymous")
    suspend fun loginAnonymous(): LoginResponse


    @POST("register")
    @FormUrlEncoded
    suspend fun signUp(
        @Field("name") name: String, @Field("email") email: String, @Field("phone_number") phoneNumber: String,
        @Field("password") password: String, @Field("user_type") user_type: Int
    ): LoginResponse

    @GET("salon/getPopularSalon")
    suspend fun getPopularSalon(@Header("Authorization") authHeader: String): SalonResponse

    @GET("salon/getNewSalon")
    suspend fun getNewSalon(@Header("Authorization") authHeader: String): SalonResponse

    @GET("salon/getAllSale")
    suspend fun getAllSale(@Header("Authorization") authHeader: String): SaleResponse


    @GET("salon/getStylistAvailable")
    suspend fun getStylistAvailable(
        @Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int, @Query(
            "day_of_week"
        ) day_of_week: String
    ): StylistAvailableResponse

    @GET("salon/getSalonInfo")
    suspend fun getSalonInfo(@Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int): SimpleResponse<Salon>

    @GET("salon/getSalonService")
    suspend fun getSalonService(@Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int): ServiceResponse

    @GET("salon/getSalonStylists")
    suspend fun getSalonStylists(@Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int): StylistResponse

    @GET("salon/getSalonSlotAvailable")
    suspend fun getSalonSlotAvailable(
        @Header("Authorization") authHeader: String,
        @Query("salon_id") salonId: Int, @Query("date") date: String, @Query("duration") duration: Int
    ): SlotAvailableResponse

    @GET("salon/getSalonSlotByStylistAvailable")
    suspend fun getSalonSlotAvailableByStylist(
        @Header("Authorization") authHeader: String,
        @Query("salon_id") salonId: Int, @Query("stylist_id") stylistId: Int, @Query("date") date: String, @Query(
            "duration"
        ) duration: Int
    ): SlotAvailableResponse


    @POST("salon/applyBooking")
    suspend fun confirmBooking(@Header("Authorization") authHeader: String, @Body params: BookingParams): SimpleResponse<Appointment>

    @GET("user/getAppointmentByUser")
    suspend fun getAppointmentByUser(@Header("Authorization") authHeader: String): AppointmentResponse


    @PATCH("user/cancelAppointment")
    @FormUrlEncoded
    suspend fun cancelAppointment(@Field("booking_id") booking_id: Int, @Header("Authorization") authHeader: String): SimpleResponse<Any>


    @GET("salon/getAppointmentDetail")
    suspend fun getAppointmentDetail(@Query("booking_id") appointmentId: Int,  @Header("Authorization") authHeader: String): AppointmentDetailResponse

    @POST("user/logout")
    suspend fun logout(@Header("Authorization") authHeader: String): Response<Unit>

    @PATCH("user/updatePhoneNumber")
    @FormUrlEncoded
    suspend fun updatePhoneNumber(@Header("Authorization") authHeader: String, @Field("phone_number") phoneNumber: String): SimpleResponse<Any>

    @GET("salon/getDoneAppointment")
    suspend fun getDoneAppointment(
        @Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int, @Query("keyword") keyword: String, @Query(
            "page"
        ) page: Int, @Query("pageSize") pageSize: Int
    ): AppointmentUsedResponse

    @POST("salon/collectPhoto")
    @Multipart
    suspend fun collectPhoto(@Header("Authorization") authHeader: String, @Part("booking_id") appointmentId: RequestBody, @Part image: MultipartBody.Part): CollectPhotoResponse

    @POST("predict")
    @Multipart
    suspend fun predictHairStyle(@Part file: MultipartBody.Part): HairStyleCreateResponse

    @POST("salon/saveHairStyle")
    @FormUrlEncoded
    suspend fun saveHairStyle(
        @Header("Authorization") authHeader: String, @Field("salon_id") salonId: Int, @Field("title") title: String, @Field(
            "url"
        ) url: String
    ): SimpleResponse<HairStyle>

    @GET("salon/getNewHairStyle")
    suspend fun getNewHairStyle(@Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int): HairStyleSalonResponse

    @GET("salon/getAllHairStyle")
    suspend fun getAllHairStyle(
        @Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int?, @Query(
            "page"
        ) page: Int, @Query("pageSize") pageSize: Int
    ): SimpleResponse<HairStyleAllResponse>

    @GET("salon/getTopArtOfSalon")
    suspend fun getTopArtOfSalon(
        @Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int
    ): SimpleResponse<List<Art>>

    @GET("user/getOrderByUser")
    suspend fun getOrderByUser(
        @Header("Authorization") authHeader: String, @Query("page") page: Int
    ): SimpleResponse<OrderReponse>

    @GET("user/getDetailOrder")
    suspend fun getDetailOrder(
        @Header("Authorization") authHeader: String, @Query("order_id") orderId: Int
    ): SimpleResponse<List<OrderItem>>

    @PATCH("user/cancelOrder")
    @FormUrlEncoded
    suspend fun cancelOrder(
        @Header("Authorization") authHeader: String, @Field("order_id") orderId: Int
    ): SimpleResponse<Any>

    @GET("salon/getAllProductCategorySalon")
    suspend fun getProductSalonCategory(
        @Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int
    ): SimpleResponse<List<ProductCategory>>

    @GET("salon/getAllProductSalon")
    suspend fun getProductSalonByCategory(
        @Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int, @Query("cat_id") catId: Int, @Query(
            "keyword"
        ) keyword: String, @Query("page") page: Int
    ): SimpleResponse<ProductReponse>

    @GET("salon/getProductById")
    suspend fun getProductById(@Header("Authorization") authHeader: String, @Query("id") productId: Int): SimpleResponse<Product>

    @PATCH("salon/getProductById")
    @FormUrlEncoded
    suspend fun addProductToCart(@Header("Authorization") authHeader: String, @Field("product_id") productId: Int): SimpleResponse<Any>

    @POST("salon/createOrderUser")
    suspend fun createOrder(@Header("Authorization") authHeader: String, @Body body: OrderParams): SimpleResponse<Any>

    @GET("user/getBookingHistoryByUser")
    suspend fun getBookingHistoryByUser(
        @Header("Authorization") authHeader: String, @Query(
            "page"
        ) page: Int, @Query("pageSize") pageSize: Int
    ): AppointmentUsedResponse

    @POST("salon/removeArt")
    @FormUrlEncoded
    suspend fun removeArt(@Header("Authorization") authHeader: String, @Field("artId") artId: Int): SimpleResponse<Int>

    @POST("salon/rateBooking")
    @FormUrlEncoded
    suspend fun rateBooking(
        @Header("Authorization") authHeader: String, @Field("booking_id") idBooking: Int, @Field("content") content: String, @Field(
            "rate"
        ) rate: Int
    ): SimpleResponse<Review>

    @POST("salon/updateDownloadHairStyle")
    suspend fun updateDownloadCount(@Header("Authorization") authHeader: String, @Body params: HairStyleParams): SimpleResponse<Any>

    @POST("salon/updateViewHairStyle")
    @FormUrlEncoded
    suspend fun updateViewCount(@Header("Authorization") authHeader: String, @Field("id_hair") hairId: Int): SimpleResponse<Any>

    @GET("salon/getHairStyleDetail")
    suspend fun getHairStyleDetail(@Header("Authorization") authHeader: String, @Query("id_hair") idHair: Int): SimpleResponse<HairStyle>


    @GET("salon/getSalonByKeyword")
    suspend fun getSalonByKeyword(
        @Header("Authorization") authHeader: String, @Query("keyword") keyword: String, @Query("page") page: Int
    ): SimpleResponse<SalonResponse>

    @POST("user/updateProfile")
    @Multipart
    suspend fun updateProfile(
        @Header("Authorization") authHeader: String, @Part("name") name: RequestBody, @Part(
            "email"
        ) email: RequestBody, @Part("phone_number") phoneNumber: RequestBody, @Part image: MultipartBody.Part? = null
    ): SimpleResponse<User>

    @POST("user/updatePassword")
    @FormUrlEncoded
    suspend fun updatePassword(
        @Header("Authorization") authHeader: String, @Field("old_pass") oldPassword: String, @Field(
            "new_pass"
        ) new_pass: String
    ): SimpleResponse<Any>

    @GET("salon/getSalonWithGallery")
    suspend fun getSalonWithGallery(@Header("Authorization") authHeader: String, @Query("idSalon[]") listSalonId: List<Int>): SalonResponse

    @GET("salon/getAllSalonReview")
    suspend fun getAllSalonReview(@Header("Authorization") authHeader: String, @Query("salon_id") salonId: Int): SimpleResponse<List<Review>>
}