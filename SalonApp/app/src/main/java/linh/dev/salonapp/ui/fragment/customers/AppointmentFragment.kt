package linh.dev.salonapp.ui.fragment.customers

import android.widget.RadioGroup
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.fragment_customer_appointment_tab.*
import linh.dev.base.BaseFragment
import linh.dev.salonapp.R
import linh.dev.salonapp.adapter.HomeAppointmentPagerAdapter
import linh.dev.salonapp.databinding.FragmentCustomerAppointmentTabBinding
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class AppointmentFragment : BaseFragment<FragmentCustomerAppointmentTabBinding>() {

    val homeAppointmentPagerAdapter: HomeAppointmentPagerAdapter by inject {
        parametersOf(
            childFragmentManager
        )
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_customer_appointment_tab
    }


    companion object {
        fun newInstance(): AppointmentFragment = AppointmentFragment()
    }

    override fun initView() {
        super.initView()
        initTabView()

    }

    private fun initTabView() {
        btnAppointment.isChecked = true
        tabBar.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                if (checkedId == R.id.btnAppointment) {
                    pagerAppointment.currentItem = 0
                } else if (checkedId == R.id.btnHistory) {
                    pagerAppointment.currentItem = 1
                }
            }

        })
        pagerAppointment.offscreenPageLimit = 2
        pagerAppointment.adapter = homeAppointmentPagerAdapter
        pagerAppointment.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    btnAppointment.isSelected = true
                } else {
                    btnHistory.isSelected = true
                }

            }

        })
    }



}