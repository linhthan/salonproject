package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemServiceAppointmentBinding
import linh.dev.salonapp.model.local.Service

class ServiceAppointmentAdapter : BaseRecycleViewAdapter<Service>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemServiceAppointmentBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_service_appointment,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    inner class ViewHolder(val binding: ViewItemServiceAppointmentBinding) :
        BaseRecycleViewAdapter<Service>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.viewModel = getItem(position)
            binding.position = position + 1
        }

    }
}