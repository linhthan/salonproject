package linh.dev.salonapp.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import linh.dev.salonapp.R
import linh.dev.salonapp.ui.activitys.CustomerHomeActivity

class NotificationHelper {
    companion object{
        fun showNotification(context :Context,notificationManager:NotificationManager, notificationChannel:String, pendingIntent: PendingIntent,title:String,content:String){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    notificationChannel,
                    notificationChannel,
                    NotificationManager.IMPORTANCE_DEFAULT
                )
                channel.enableVibration(false)
                notificationManager.createNotificationChannel(channel)
            }


            val mBuilder = NotificationCompat.Builder(context, notificationChannel)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setStyle(NotificationCompat.BigTextStyle().bigText(content))
                .setContentText(content)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setVibrate(longArrayOf(500, 1000))
                .setOnlyAlertOnce(false)
                .setColor(ContextCompat.getColor(context, R.color.active_blue))
                .setAutoCancel(true)
            with(NotificationManagerCompat.from(context)) {
                notify(System.currentTimeMillis().toInt(), mBuilder.build())
            }
        }
    }
}