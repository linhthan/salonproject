package linh.dev.salonapp.viewmodel

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ERROR_UNKNOWN
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.repository.UserRepository

class ProfileViewModel(private val userRepository: UserRepository) : BaseViewModel() {
    val logoutResult = userRepository.logoutResult
    fun logout() {
        val user = user()
        if (user != null) {
            viewModelScope.launch {
                withContext(io()) {
                    userRepository.doLogout(user)
                }
            }
        } else {
            logoutResult.value = Result.error(ERROR_UNKNOWN)
        }

    }
}