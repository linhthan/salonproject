package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Service

data class ServiceResponse(val success:Boolean,val message: String, val data: List<Service>)