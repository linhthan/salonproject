package linh.dev.salonapp.util

import linh.dev.salonapp.R
import linh.dev.salonapp.model.local.HairStyleItem
import linh.dev.salonapp.model.local.Ratio

object DataUtil {

    fun getHairStyleItem():ArrayList<HairStyleItem>{
        return arrayListOf(
            HairStyleItem(R.raw.img1,R.raw.img_1),
            HairStyleItem(R.raw.img2,R.raw.img_2),
            HairStyleItem(R.raw.img3,R.raw.img_3),
            HairStyleItem(R.raw.img4,R.raw.img_4),
            HairStyleItem(R.raw.img5,R.raw.img_5),
            HairStyleItem(R.raw.img6,R.raw.img_6),
            HairStyleItem(R.raw.img7,R.raw.img_7),
            HairStyleItem(R.raw.img8,R.raw.img_8),
            HairStyleItem(R.raw.img9,R.raw.img_9),
            HairStyleItem(R.raw.img10,R.raw.img_10),
            HairStyleItem(R.raw.img11,R.raw.img_11),
            HairStyleItem(R.raw.img12,R.raw.img_12),
            HairStyleItem(R.raw.img13,R.raw.img_13),
            HairStyleItem(R.raw.img14,R.raw.img_14),
            HairStyleItem(R.raw.img15,R.raw.img_15),
            HairStyleItem(R.raw.img16,R.raw.img_16),
            HairStyleItem(R.raw.img17,R.raw.img_17),
            HairStyleItem(R.raw.img18,R.raw.img_18),
            HairStyleItem(R.raw.img19,R.raw.img_19),
            HairStyleItem(R.raw.img20,R.raw.img_20),
            HairStyleItem(R.raw.img21,R.raw.img_21),
            HairStyleItem(R.raw.img22,R.raw.img_22),
            HairStyleItem(R.raw.img23,R.raw.img_23),
            HairStyleItem(R.raw.img24,R.raw.img_24),
            HairStyleItem(R.raw.img25,R.raw.img_25),
            HairStyleItem(R.raw.img26,R.raw.img_26),
            HairStyleItem(R.raw.img27,R.raw.img_27),
            HairStyleItem(R.raw.img28,R.raw.img_28),
            HairStyleItem(R.raw.img29,R.raw.img_29),
            HairStyleItem(R.raw.img30,R.raw.img_30),
            HairStyleItem(R.raw.img31,R.raw.img_31),
            HairStyleItem(R.raw.img32,R.raw.img_32),
            HairStyleItem(R.raw.img33,R.raw.img_33),
            HairStyleItem(R.raw.img34,R.raw.img_34),
            HairStyleItem(R.raw.img35,R.raw.img_35)
        )
    }
     fun initCropRatio() :ArrayList<Ratio>{
        val items = ArrayList<Ratio>()
        items.add(Ratio(R.string.ratio_free, R.drawable.ratio_1_1,R.drawable.ratio_1_1_click,Ratio.RatioType.FREE))
        items.add(Ratio(R.string.ratio_11, R.drawable.ratio_1_1,R.drawable.ratio_1_1_click,Ratio.RatioType.RATIO1_1))
        items.add(Ratio(R.string.ratio_12, R.drawable.ratio_1_2,R.drawable.ratio_1_2_click,Ratio.RatioType.RATIO1_2))
        items.add(Ratio(R.string.ratio_23, R.drawable.ratio_2_3,R.drawable.ratio_2_3_click,Ratio.RatioType.RATIO2_3))
        items.add(Ratio(R.string.ratio_32, R.drawable.ratio_3_2,R.drawable.ratio_3_2_click,Ratio.RatioType.RATIO3_2))
        items.add(Ratio(R.string.ratio_34, R.drawable.ratio_3_4,R.drawable.ratio_3_4_click,Ratio.RatioType.RATIO3_4))
        items.add(Ratio(R.string.ratio_43, R.drawable.ratio_4_3,R.drawable.ratio_4_3_click,Ratio.RatioType.RATIO4_3))
        items.add(Ratio(R.string.ratio_45, R.drawable.ratio_4_5,R.drawable.ratio_4_5_click,Ratio.RatioType.RATIO4_5))
        items.add(Ratio(R.string.ratio_54, R.drawable.ratio_5_4,R.drawable.ratio_5_4_click,Ratio.RatioType.RATIO5_4))
        items.add(Ratio(R.string.ratio_916, R.drawable.ratio_9_16,R.drawable.ratio_9_16_click,Ratio.RatioType.RATIO9_16))
        items.add(Ratio(R.string.ratio_169, R.drawable.ratio_16_9,R.drawable.ratio_16_9_click,Ratio.RatioType.RATIO16_9))
        items[0].isSelected = true

        return items
    }
}