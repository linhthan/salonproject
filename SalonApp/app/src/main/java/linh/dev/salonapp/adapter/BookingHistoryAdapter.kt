package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import linh.dev.base.ext.view.hide
import linh.dev.salonapp.R
import linh.dev.salonapp.State
import linh.dev.salonapp.databinding.ViewItemBookingHistoryBinding
import linh.dev.salonapp.databinding.ViewItemFooterBinding
import linh.dev.salonapp.model.local.Appointment


class BookingHistoryAdapter :
    PagedListAdapter<Appointment, RecyclerView.ViewHolder>(BookingDiffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == DATA_VIEW_TYPE) {
            val binding: ViewItemBookingHistoryBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_booking_history, parent, false
            )
            ViewHolder(binding)
        } else {
            val binding: ViewItemFooterBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_footer, parent, false
            )
            return ListFooterViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.onBindingData(position)
        } else if (holder is ListFooterViewHolder) {
            holder.bind(state)
        }
    }


    private var state = State.LOADING

    private var onItemClick: OnItemClick? = null

    interface OnItemClick {
        fun onItemClick(position: Int)
        fun onViewClick(resId: Int, position: Int)
    }




    fun setItemClick(onItemClick: OnItemClick) {
        this.onItemClick = onItemClick
    }

    fun clearData() {
        currentList?.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
//        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }

    fun getItemByPosition(position: Int): Appointment? {
        return super.getItem(position)
    }


    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }


    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2

    companion object {
        val BookingDiffCallback = object : DiffUtil.ItemCallback<Appointment>() {
            override fun areItemsTheSame(oldItem: Appointment, newItem: Appointment): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Appointment, newItem: Appointment): Boolean {
                return oldItem == newItem
            }
        }
    }

    inner class ListFooterViewHolder(val binding: ViewItemFooterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(status: State?) {
            if (status == State.DONE) {
                binding.progressBar.hide()
                binding.txtError.hide()
            } else {
                binding.progressBar.visibility =
                    if (status == State.LOADING) View.VISIBLE else View.INVISIBLE
                binding.txtError.visibility =
                    if (status == State.ERROR) View.VISIBLE else View.INVISIBLE
            }

            binding.txtError.setOnClickListener {
                onItemClick?.onViewClick(
                    R.id.txt_error,
                    adapterPosition
                )
            }
        }


    }

    inner class ViewHolder(val binding: ViewItemBookingHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBindingData(position: Int) {
            if (position in 0 until itemCount) {
                val item = getItem(position)
                binding.viewModel = item

                binding.root.setOnClickListener {
                    onItemClick?.onItemClick(position)
                }
                binding.btnRate.setOnClickListener {
                    onItemClick?.onViewClick(R.id.btnRate,position)
                }
            }

        }

    }
}