package linh.dev.salonapp.ui.custom

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import com.sticker.Sticker
import com.sticker.StickerView

class HairStyleEditView : StickerView {
    var currentHairStyle: HairStyle? = null

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun addSticker(sticker: Sticker): StickerView {
        removeAllStickers()
        currentHairStyle = sticker as HairStyle
        return super.addSticker(sticker)
    }

    fun addSticker(drawable: Drawable) {
        val sticker = HairStyle(drawable)
        addSticker(sticker)
    }

    fun release() {
        currentHairStyle?.release()
    }
}