package linh.dev.salonapp.model.local

data class Gallery(
    val salonId: Int,
    val itemHairStyle: ArrayList<HairStyle>
)