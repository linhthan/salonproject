package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemAddArtBinding
import linh.dev.salonapp.databinding.ViewItemArtBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.Art

class ArtAdapter : BaseRecycleViewAdapter<Art>() {
     val TYPE_ADD = 12

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        if (viewType == TYPE_ADD) {
            val binding: ViewItemAddArtBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_add_art, parent, false
            )
            return AddArtViewHolder(binding)
        } else {
            val binding: ViewItemArtBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.view_item_art, parent, false
            )
            return ViewHolder(binding)
        }

    }

    fun removeItem(artId:Int):Art?{
        var itemRemove :Art? = null
        dataSources.forEachIndexed{index,item->
            if(item.id == artId){
                itemRemove = item
                return@forEachIndexed
            }
        }
        itemRemove?.let {
            val indexRemove = dataSources.indexOf(it)
            dataSources.removeAt(indexRemove)
            notifyItemRemoved(indexRemove)
        }
        logE("Item Remove: idBooking= ${itemRemove?.id_booking} và urlArt = ${itemRemove?.url_art} ")
        return itemRemove
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
    }

    override fun getItemViewType(position: Int): Int {
        if (position == itemCount - 1) {
            return TYPE_ADD
        }
        return super.getItemViewType(position)
    }

    inner class ViewHolder(val binding: ViewItemArtBinding) :
        BaseRecycleViewAdapter<Art>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.viewModel = getItem(position)
            binding.itemArt.setOnClickListener {
                onViewClick(R.id.itemArt)
            }
            binding.imvDelete.setOnClickListener {
                onViewClick(R.id.imvDelete)
            }
        }

    }

    inner class AddArtViewHolder(val binding: ViewItemAddArtBinding) :
        BaseRecycleViewAdapter<Art>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.btnAddArt.setOnClickListener {
                onViewClick(R.id.btnAddArt)
            }
        }

    }

    interface ItemClick {
        fun onItemClick(position: Int)
    }
}