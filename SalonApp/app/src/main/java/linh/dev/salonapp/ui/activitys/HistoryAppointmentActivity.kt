package linh.dev.salonapp.ui.activitys

import android.content.Intent
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_history_appointment.*
import kotlinx.android.synthetic.main.layout_no_data.*
import linh.dev.base.BaseActivity
import linh.dev.base.EndlessRecyclerViewScrollListener
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.salonapp.MainActivity
import linh.dev.salonapp.PREF_USER
import linh.dev.salonapp.R
import linh.dev.salonapp.adapter.AppointmentUsedAdapter
import linh.dev.salonapp.databinding.ActivityHistoryAppointmentBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.HistoryAppointmentViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


class HistoryAppointmentActivity : BaseActivity<ActivityHistoryAppointmentBinding>() {

    override fun getLayoutResourceId(): Int {
        return R.layout.activity_history_appointment
    }

    override fun initView() {
        super.initView()
        supportActionBar?.hide()

    }
}