package linh.dev.salonapp.model.local

data class Sale(
    val id: Int,
    val title: String,
    val banner_url: String,
    val sale_url: String,
    val salon_id: Int,
    val sale_create_at: String
)