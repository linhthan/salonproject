package linh.dev.salonapp.model.local

import com.google.gson.annotations.SerializedName
import linh.dev.salonapp.APPOINTMENT_USED_FORMAT
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.SERVER_TIME_FORMAT
import linh.dev.salonapp.util.DateTimeUtils
import java.lang.StringBuilder

data class Appointment(
    val id: Int,
    val salonName: String,
    var status: Int,
    val customer_id: Int,
    val phone_number: String,
    val cus_name: String,
    val booking_time: String,
    val booking_end_time: String,
    val checkin_at: String?,
    val paidAt: String?,
    val images: ArrayList<Art>,
    var star: Int?,
    @SerializedName("detail_location") val detailLocation: String,
    val services: List<Service>
) {

    fun getDoneTime(): String? {
        return DateTimeUtils.formatDateString(
            booking_end_time, SERVER_TIME_FORMAT,
            APPOINTMENT_USED_FORMAT
        )
    }

    fun getServiceTitle(): String {
        val result = StringBuilder(EMPTY)
        services.forEachIndexed { index, item ->
            if (index == services.size - 1) {
                result.append(item.title)
            } else {
                result.append("${item.title},")
            }

        }
        return result.toString()
    }
}