package linh.dev.salonapp.ui.fragment.customers

import android.content.Intent
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_customer_profile_tab.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.MainActivity
import linh.dev.salonapp.PREF_USER
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.FragmentCustomerProfileTabBinding
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.AppUtil
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.ProfileViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ProfileTabFragment : BaseFragment<FragmentCustomerProfileTabBinding>() {
    private val viewModel: ProfileViewModel by viewModel()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_customer_profile_tab
    }

    companion object {
        fun newInstance(): ProfileTabFragment = ProfileTabFragment()
    }

    override fun initView() {
        super.initView()
        initActionClick()
        mBinding.user = user()
        viewModel.logoutResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoading.show()
                }
                Result.Status.SUCCESS -> {
                    progressLoading.hide()
                    context?.let {
                        PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                        activity?.let { context ->
                            startActivity(Intent(context, MainActivity::class.java))
                            context.finish()
                        }
                    }

                }
                Result.Status.ERROR -> {
                    progressLoading.hide()
                    if (it.statusCode == 401) {
                        PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                        activity?.let { context ->
                            startActivity(Intent(context, MainActivity::class.java))
                            context.finish()
                        }

                    } else {
                        it.message?.let { msg ->
                            showToast(msg)
                        }

                    }
                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })

        btnViewInfo.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(
                    R.id.action_customerHomeFragment_to_fragmentEditUser,
                    null
                )
        }
        imvAvatar.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(
                    R.id.action_customerHomeFragment_to_fragmentEditUser,
                    null
                )
        }
        layoutChangePass.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(
                    R.id.action_customerHomeFragment_to_changePasswordFragment,
                    null
                )
        }
        layoutHairStyle.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(
                    R.id.action_customerHomeFragment_to_galleryHairFragment,
                    null
                )

        }
        layoutFAQ.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.nav_main)
                .navigate(
                    R.id.action_customerHomeFragment_to_QAFragment,
                    null
                )
        }
        layoutShare.setOnClickListener {
            AppUtil.shareApplication(
                requireContext(),
                "Ứng dụng đặt lịch làm tóc online BKSalon"
            )
        }
        layoutRate.setOnClickListener { AppUtil.rateApp(requireContext()) }
        layoutChangePass.setOnClickListener {

        }
    }

    private fun initActionClick() {
        layoutLogout.setOnClickListener {
            viewModel.logout()
        }
    }
}