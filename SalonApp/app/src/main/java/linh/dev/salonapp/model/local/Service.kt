package linh.dev.salonapp.model.local

class Service(
    val id: Int,
    val title: String,
    val thumbnail: String?,
    val content: String,
    val price: Double,
    val duration: Int
) {
    class ServiceJson(val id:Int,val price: Double)
}