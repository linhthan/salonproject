package linh.dev.salonapp.viewmodel

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.UserRepository

class ChangePasswordViewModel(val userRepository: UserRepository) : BaseViewModel() {
    val updatePassResult = userRepository.updatePassResult
    fun updatePassword(oldPass: String, newPass: String) {
        user()?.let {
            viewModelScope.launch {
                userRepository.updatePassword(it.getBearToken(), oldPass, newPass)
            }
        }
    }
}