package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Appointment

data class AppointmentDetailResponse(val success:Boolean, val message:String, val data:Appointment)