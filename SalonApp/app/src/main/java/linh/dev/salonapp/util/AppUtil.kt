package linh.dev.salonapp.util

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import java.net.InetAddress
import androidx.core.content.ContextCompat.getSystemService



class AppUtil {
    companion object {
        fun rateApp(context: Context) {
            val uri = Uri.parse("market://details?id=linh.dev.salonapp")
            val goToMarket = Intent(Intent.ACTION_VIEW, uri)
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags((Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK))
            try {
                context.startActivity(goToMarket)
            } catch (e: ActivityNotFoundException) {
                context.startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())))
            }

        }

        fun sendFeedBack(context: Context) {
            val emailIntent = Intent(Intent.ACTION_VIEW)
            emailIntent.data = Uri.parse("mailto:")
            // emailIntent.setType("text/email");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("englishflashcardtoefl@gmail.com"))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Dear," + "")


            try {
                context.startActivity(emailIntent)
            } catch (e: Exception) {

                e.printStackTrace()
            }

        }

        fun shareApplication(context: Context, content: String) {
            val url = "https://play.google.com/store/apps/details?id=linh.dev.salonapp"
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Chia sẻ với bạn bè")
            shareIntent.putExtra(Intent.EXTRA_TEXT, "$content $url")
            context.startActivity(Intent.createChooser(shareIntent, "chia sẻ qua"))
        }
        fun isInternetAvailable(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

            return cm!!.getActiveNetworkInfo() != null && cm!!.activeNetworkInfo.isConnected()

        }
    }
}