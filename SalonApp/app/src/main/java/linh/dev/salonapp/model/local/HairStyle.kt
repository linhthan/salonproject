package linh.dev.salonapp.model.local

import com.google.gson.annotations.SerializedName
import linh.dev.salonapp.APPOINTMENT_USED_FORMAT
import linh.dev.salonapp.SERVER_TIME_FORMAT
import linh.dev.salonapp.util.DateTimeUtils

data class HairStyle(
    val id: Int,
    @SerializedName("salon_id") val salonId: Int,
    val salonName: String,
    val title: String,
    val url: String,
    @SerializedName("create_at") val createAt: String,
    @SerializedName("view_count")var viewCount: Int,
    @SerializedName("download_count")var downloadCount: Int
){
    fun getCreateAtDisplay():String?{
        return DateTimeUtils.formatDateString(createAt, SERVER_TIME_FORMAT, APPOINTMENT_USED_FORMAT)
    }
}
