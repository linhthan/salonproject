package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemArtBinding
import linh.dev.salonapp.databinding.ViewItemRateBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Art
import linh.dev.salonapp.model.local.Review

class RateSalonAdapter : BaseRecycleViewAdapter<Review>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemRateBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_rate, parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = getItem(position)
        logE(item.customer_id)
        logE(user()?.id)
        logE(holder is ViewHolder)
        if(holder is ViewHolder){
            if (item.customer_id == user()?.id ) {
                holder.binding.tvCusName.text = "Bạn"
            }else{
                holder.binding.tvCusName.text = item.cusName
            }
        }

    }

    inner class ViewHolder(val binding: ViewItemRateBinding) :
        BaseRecycleViewAdapter<Review>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {

            binding.viewModel = getItem(position)

        }
    }

}




