package linh.dev.salonapp.viewmodel

import android.content.Context
import android.os.Environment
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.work.*
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.databases.model.HairStyle
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.network.ImageDownloadWorker
import linh.dev.salonapp.repository.HairStyleRepository
import java.io.File

class HairStyleViewModel(val salonId: Int, listHairStyleJson: String, val repository: HairStyleRepository) :
    BaseViewModel() {
    val compositeDisposable = CompositeDisposable()
    val hairStyleLocal = repository.getHairStyleBySalonId(salonId)
    var imagePath: String = ""
    val imagePathHairStylePreview = MutableLiveData<String>()
    val listOfImages: List<linh.dev.salonapp.model.local.HairStyle> by lazy {
        val gSon = Gson()
        return@lazy gSon.fromJson<List<linh.dev.salonapp.model.local.HairStyle>>(
            listHairStyleJson,
            object : TypeToken<List<linh.dev.salonapp.model.local.HairStyle>>() {}.type
        )
    }

    init {
        imagePathHairStylePreview.value = EMPTY
    }

    fun getListHairStyleToDownload(hairStyleLocal: List<HairStyle>): ArrayList<linh.dev.salonapp.model.local.HairStyle> {
        val result = ArrayList<linh.dev.salonapp.model.local.HairStyle>()
        val mapRemoteHairStyle = HashMap<Int, linh.dev.salonapp.model.local.HairStyle>()
        listOfImages.forEach {
            logE("id salon ${it.salonId}")
            mapRemoteHairStyle[it.id] = it
        }
        hairStyleLocal.forEach {
            if (mapRemoteHairStyle.containsKey(it.idHair)) {
                mapRemoteHairStyle.remove(it.idHair)
            }
        }
        result.addAll(mapRemoteHairStyle.values)
        return result
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun downloadListImage(context: Context, images: List<linh.dev.salonapp.model.local.HairStyle>) {
        val jsonImages = Gson().toJson(images)
        val data = Data.Builder()
            .putString("images", jsonImages)
            .build()

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)

        val oneTimeRequest = OneTimeWorkRequest.Builder(ImageDownloadWorker::class.java)
            .setInputData(data)
            .setConstraints(constraints.build())
            .addTag("demo")
            .build()



        WorkManager.getInstance(context).enqueueUniqueWork("SalonApp", ExistingWorkPolicy.APPEND, oneTimeRequest)


    }

    private fun downloadFileFromUrl(context: Context, url: String, fileName: String) {
        try {
            val mediaStorageDir =
                File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "hairstyle")
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("MyCameraApp", "failed to create directory")
                }
            }

            val mediaFile = File(mediaStorageDir.absolutePath + File.separator + fileName)
            if (mediaFile.exists()) {
                mediaFile.delete()
            }


        } catch (ex: Exception) {
            ex.printStackTrace()

        }
    }
}