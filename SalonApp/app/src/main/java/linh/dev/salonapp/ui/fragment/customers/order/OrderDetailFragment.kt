package linh.dev.salonapp.ui.fragment.customers.order

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_order_detail.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.showLongMsg
import linh.dev.base.ext.view.showMsg
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.OrderItemAdapter
import linh.dev.salonapp.databinding.FragmentOrderDetailBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.OrderDetailViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class OrderDetailFragment : BaseFragment<FragmentOrderDetailBinding>() {

    private val args: OrderDetailFragmentArgs by navArgs()
    private val viewModel: OrderDetailViewModel by viewModel { parametersOf(args.order) }
    private val adapter: OrderItemAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_order_detail
    }

    override fun initView() {
        super.initView()
        mBinding.order = viewModel.order
        initRecycleView()
        btnCancelOrder.setOnClickListener {
            viewModel.cancelOrder()
        }
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }

    }

    private fun initRecycleView() {
        rcvProduct.layoutManager = object : LinearLayoutManager(context) {
            override fun canScrollVertically(): Boolean {
                return false
            }

            override fun canScrollHorizontally(): Boolean {
                return false
            }
        }
        rcvProduct.adapter = adapter

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initObservable() {
        viewModel.orderItemResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoadingProduct.show()
                }
                Result.Status.SUCCESS -> {
                    progressLoadingProduct.hide()
                    if (it.data != null && it.data.success) {
                        val items = it.data.data
                        var totalPrice = 0
                        items.forEach { item ->
                            totalPrice += item.price
                        }
                        tvTotalPriceOrder.text =
                            resources.getString(R.string.format_total_price, totalPrice)
                        adapter.setItems(items)

                    }
                }
                Result.Status.ERROR -> {
                    progressLoadingProduct.hide()
                    showMsg(it.message)
                }
                Result.Status.UNAUTHORIZED -> {
                    progressLoadingProduct.hide()
                    if (PrefUtil.removeSharePreference(
                            SalonApplication.applicationContext(),
                            PREF_USER
                        )
                    ) {
                        activity?.let { context ->
                            startActivity(Intent(context, MainActivity::class.java))
                            context.finish()
                        }
                    }
                }
            }
        })

        viewModel.cancelOrderResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressCancelOrder.show()
                }
                Result.Status.SUCCESS -> {
                    progressCancelOrder.hide()
                    if (it.data != null && it.data.success) {
                        if (it.data.success) {
                            showMsg(it.data.message)
                            tvStatusText.text = resources.getString(
                                R.string.order_status_display,
                                resources.getString(R.string.order_cancel_status)
                            )
                            bus.post(AppAction(AppEvent.EVENT_CANCEL_ORDER_SUCCESS.setData(viewModel.order)))
                        }
                    }
                }
                Result.Status.ERROR -> {
                    progressCancelOrder.hide()
                    showMsg(it.message)
                }
                Result.Status.UNAUTHORIZED -> {
                    progressCancelOrder.hide()
                    if (PrefUtil.removeSharePreference(
                            SalonApplication.applicationContext(),
                            PREF_USER
                        )
                    ) {
                        activity?.let { context ->
                            startActivity(Intent(context, MainActivity::class.java))
                            context.finish()
                        }
                    }
                }
            }
        })
    }
}