package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Booking
import linh.dev.salonapp.model.local.Stylist
import linh.dev.salonapp.model.local.TimeSlot

data class DataSalonBooking(
    val time_slot: TimeSlot,
    val booking: List<Booking>,
    val stylists: List<Stylist>
)