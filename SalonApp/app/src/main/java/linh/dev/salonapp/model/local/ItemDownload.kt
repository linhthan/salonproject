package linh.dev.salonapp.model.local

class ItemDownload(private val listRemote: ArrayList<HairStyle>) {

    fun hasDownloadItem(): Boolean {
        return !listRemote.isNullOrEmpty()
    }

    fun addItem(remote: HairStyle) {
        listRemote.add(remote)
    }

    fun removeDownloadedItem(hairId: Int):Boolean {
        listRemote.forEach {
            if (it.id == hairId) {
                listRemote.remove(it)
                return true
            }
        }
        return false
    }

    fun getDownloadList() = listRemote

    fun countDownload() = listRemote.size
}