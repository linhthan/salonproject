package linh.dev.salonapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import kotlinx.coroutines.CoroutineScope
import linh.dev.salonapp.model.local.HairStyle
import linh.dev.salonapp.network.AppApi

class HairStyleDataSourceFactory(
    private val salonId:Int?,
    private val appApi: AppApi,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, HairStyle>() {

    val hairStyleDataSourceLiveData = MutableLiveData<HairStyleDataSource>()

    override fun create(): DataSource<Int, HairStyle> {
        val newsDataSource = HairStyleDataSource(salonId,appApi, scope)
        hairStyleDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}