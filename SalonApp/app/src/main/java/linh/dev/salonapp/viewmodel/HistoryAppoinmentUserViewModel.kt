package linh.dev.salonapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.PER_PAGE
import linh.dev.salonapp.State
import linh.dev.salonapp.data.AppointmentHistoryDataSource
import linh.dev.salonapp.data.AppointmentHistoryDataSourceFactory
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.repository.UserRepository

class HistoryAppoinmentUserViewModel(val userRepository: UserRepository) : BaseViewModel() {

    val listAppointment = userRepository.listAppointment

    private val appointmentDataSourceFactory: AppointmentHistoryDataSourceFactory
    var appointmentUsedList: LiveData<PagedList<Appointment>>


    init {
        appointmentDataSourceFactory = AppointmentHistoryDataSourceFactory(userRepository.api, viewModelScope)
        val config = PagedList.Config.Builder()
            .setPageSize(PER_PAGE)
            .setInitialLoadSizeHint(PER_PAGE)
            .setEnablePlaceholders(false)
            .build()
        appointmentUsedList = LivePagedListBuilder(appointmentDataSourceFactory, config).build()
    }
    fun getState(): LiveData<State> = Transformations.switchMap<AppointmentHistoryDataSource,
            State>(appointmentDataSourceFactory.appointmentDataSourceLiveData, AppointmentHistoryDataSource::state)

    fun retryFailed(){
        appointmentDataSourceFactory.appointmentDataSourceLiveData.value?.retryFailedQuery()
    }




    fun listIsEmpty(): Boolean {
        return appointmentUsedList.value?.isEmpty() ?: true
    }

    fun refreshData() {
        appointmentDataSourceFactory.appointmentDataSourceLiveData.value?.refresh()
    }
}