package linh.dev.salonapp.util

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import linh.dev.salonapp.model.local.Service

object ServiceUtil {

    fun convertStringToListService(source: String): List<Service> {
        return Gson().fromJson(source, object : TypeToken<List<Service>>() {}.type)
    }

    fun getDurationFromJsonService(source: String): Int {
        var duration = 0
        val listService = convertStringToListService(source)
        listService.forEach {
            duration += it.duration
        }
        return duration

    }

    fun getListServiceIdFromJsonService(source: String): List<Int> {
        val array: ArrayList<Int> = ArrayList()
        val listService = convertStringToListService(source)
        listService.forEach {
            array.add(it.id)
        }
        return array

    }
}