package linh.dev.salonapp.ui.fragment.customers.order

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_product_detail.*
import kotlinx.android.synthetic.main.layout_no_data.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showMsg
import linh.dev.salonapp.*
import linh.dev.salonapp.databinding.FragmentProductDetailBinding
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.ProductDetailViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ProductDetailFragment : BaseFragment<FragmentProductDetailBinding>() {
    private val args: ProductDetailFragmentArgs by navArgs()
    private val viewModel: ProductDetailViewModel by viewModel { parametersOf(args.productId) }
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_product_detail
    }

    override fun initView() {
        super.initView()
        btnTryAgain.setOnClickListener {
            viewModel.getProductById()
        }
        btnAddCart.setOnClickListener {
            bus.post(AppAction(AppEvent.EVENT_ADD_PRODUCT_TO_CART.setData(args.productId.toString())))
        }
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }


    private fun initObservable() {
        viewModel.productResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoadingData.show()
                    viewNoData.hide()
                    scrollView.hide()
                }
                Result.Status.SUCCESS -> {
                    progressLoadingData.hide()

                    if (it.data !== null && it.data.success) {
                        mBinding.product = it.data.data
                        scrollView.show()
                    } else {
                        viewNoData.show()
                        scrollView.hide()
                    }

                }
                Result.Status.ERROR -> {
                    viewNoData.show()
                    showMsg(it.message)
                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })

        viewModel.addCartResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoadingData.show()
                }
                Result.Status.SUCCESS -> {
                    progressLoadingData.hide()
                    if (it.data !== null && it.data.success) {
                        bus.post(AppAction(AppEvent.EVENT_ADD_PRODUCT_TO_CART.setData(args.productId)))
                    } else {
                        showMsg(it.message)
                    }

                }
                Result.Status.ERROR -> {
                    showMsg(it.message)
                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })


    }
}