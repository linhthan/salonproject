package linh.dev.salonapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import kotlinx.coroutines.CoroutineScope
import linh.dev.salonapp.model.local.Salon
import linh.dev.salonapp.network.AppApi

class SalonDataSourceFactory(
    var keyword: String,
    private val appApi: AppApi,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, Salon>() {

    val salonDataSourceLiveData = MutableLiveData<SalonDataSource>()

    override fun create(): DataSource<Int, Salon> {
        val newsDataSource = SalonDataSource(keyword, appApi, scope)
        salonDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
    fun  resetKeyword(keyword: String){
        salonDataSourceLiveData.postValue(SalonDataSource(keyword, appApi, scope))

    }
}