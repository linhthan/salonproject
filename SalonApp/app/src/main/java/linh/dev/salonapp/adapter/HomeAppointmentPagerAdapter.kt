package linh.dev.salonapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import linh.dev.base.BaseFragment
import linh.dev.salonapp.ui.fragment.customers.booking.AppointmentUserTabFragment
import linh.dev.salonapp.ui.fragment.customers.booking.HistoryAppointmentTab

class HomeAppointmentPagerAdapter(fm: FragmentManager, behavior: Int) :
    FragmentStatePagerAdapter(fm, behavior) {
    private val items = arrayListOf<BaseFragment<*>>(
        AppointmentUserTabFragment.newInstance(),
        HistoryAppointmentTab.newInstance()
    )

    override fun getItem(position: Int): Fragment {
        return items[position]
    }

    override fun getCount(): Int = items.size

}