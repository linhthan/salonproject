package linh.dev.salonapp.di

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

import linh.dev.base.ext.retrofit.buildNetworkCoroutine
import linh.dev.salonapp.API_VERSION
import linh.dev.salonapp.BASE_URL
import linh.dev.salonapp.BASE_URL_FLASK
import linh.dev.salonapp.adapter.*
import linh.dev.salonapp.network.AppApi
import linh.dev.salonapp.repository.*
import linh.dev.salonapp.util.DataUtil
import linh.dev.salonapp.viewmodel.*
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val app_module = module {


    single(name = "salonapi") {
        AppApi::class.java.buildNetworkCoroutine(
            url = "$BASE_URL/$API_VERSION/",
            enableLogging = true,
            connectTimeout = 10
        )
    }
    single(name = "flaskapi") {
        AppApi::class.java.buildNetworkCoroutine(
            url = "$BASE_URL_FLASK/",
            enableLogging = true,
            connectTimeout = 30
        )
    }

    factory { DataUtil.initCropRatio() }
    factory { UserRepository(get("salonapi")) }
    factory { SalonRepository(get("salonapi")) }
    factory { ProductRepository(get("salonapi")) }
    factory { HairStyleRepository(get("flaskapi")) }
    factory { HairSalonRepository(get("salonapi")) }



    factory { SalonAdapter() }
    factory { StylistAdapter() }
    factory { SlotAdapter() }
    factory { ServiceAdapter() }
    factory { HairStyleAdapter() }
    factory { RatioAdapter(get()) }
    factory { ServiceBillAdapter() }
    factory { AppointmentAdapter() }
    factory { ArtAdapter() }
    factory { AppointmentDoneAdapter() }
    factory { AppointmentUsedAdapter() }
    factory { ServiceAppointmentAdapter() }
    factory { HairStyleNewAdapter() }
    factory { HairStyleAllAdapter() }
    factory { ArtSalonAdapter() }
    factory { HairStyleAllSalonAdapter() }
    factory { OrderAdapter() }
    factory { OrderItemAdapter() }
    factory { ProductSalonAdapter() }
    factory { ProductItemOrderAdapter() }
    factory { BookingHistoryAdapter() }
    factory { PhotoCollectAdapter() }
    factory { SalonSearchAdapter() }
    factory { GalleryAdapter() }
    factory { HairStyleItemAdapter() }
    factory { RateSalonAdapter() }
    factory { (salonId: Int, fragmentManager: FragmentManager) ->
        PagerSalonInfoAdapter(
            salonId,
            fragmentManager,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
    }
    factory { (fragmentManager: FragmentManager) ->
        HomeAppointmentPagerAdapter(
            fragmentManager,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
    }
    factory { (fragmentManager: FragmentManager) ->
        HomeCustomerPagerAdapter(
            fragmentManager,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
    }
    factory { (fragmentManager: FragmentManager) ->
        HomeCollectorPagerAdapter(
            fragmentManager,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
    }

    viewModel { AuthViewModel(get()) }
    viewModel { CustomerHomeViewModel() }
    viewModel { (salonId: Int, listHairStyleJson: String) -> HairStyleViewModel(salonId, listHairStyleJson, get()) }
    viewModel { SignUpViewModel(get()) }
    viewModel { SalonViewModel(get(), get()) }
    viewModel { CustomerAppointmentViewModel(get()) }
    viewModel { (salonId: Int) -> SalonDetailViewModel(salonId, get()) }
    viewModel { (bookingId: Int) -> AppointmentDetailViewModel(bookingId, get(), get()) }
    viewModel {(salonId: Int?) -> SalonServiceViewModel(salonId,get()) }
    viewModel { (salonId: Int, duration: Int) -> BookingViewModel(salonId, duration, get()) }
    viewModel { ProfileViewModel(get()) }
    viewModel { HistoryAppointmentViewModel(get()) }
    viewModel { OrderViewModel(get()) }
    viewModel { HairStyleCollectorViewModel(get(), get()) }
    viewModel { CreateHairStyleViewModel(get(), get()) }
    viewModel { (salonId: Int?) -> SalonArtViewModel(salonId, get()) }
    viewModel { (orderJson: String) -> OrderDetailViewModel(orderJson, get()) }
    viewModel { (salonId: Int) -> ProductSalonViewModel(salonId, get()) }
    viewModel { (salonId: Int?) -> SalonInfoViewModel(salonId, get()) }
    viewModel { (salonId: Int) -> RateSalonViewModel(salonId, get()) }
    viewModel { (productId: Int) -> ProductDetailViewModel(productId, get()) }
    viewModel { (productCartJson: String,salonId: Int) -> CreateOrderViewModel(productCartJson,salonId, get()) }
    viewModel { (bookingId: Int) -> RateBookingViewModel(bookingId, get()) }
    viewModel { (idHair: Int) -> DetailHairStyleViewModel(idHair, get()) }
    viewModel { HistoryAppoinmentUserViewModel(get()) }
    viewModel { AppointmentUserViewModel(get()) }
    viewModel { SearchSalonViewModel(get(),get()) }
    viewModel { EditProfileViewModel(get()) }
    viewModel { ChangePasswordViewModel(get()) }
    viewModel { GalleryViewModel(get()) }
    viewModel { (salonId: Int,hairStyleJson: String) ->HairStyleListViewModel(salonId,hairStyleJson,get()) }
}