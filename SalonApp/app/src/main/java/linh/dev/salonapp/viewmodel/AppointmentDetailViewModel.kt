package linh.dev.salonapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import id.zelory.compressor.Compressor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.base.ext.rx.AndroidSchedulerProvider
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.SalonApplication
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.SalonRepository
import linh.dev.salonapp.repository.UserRepository
import java.io.File

class AppointmentDetailViewModel(
    private val bookingId: Int,
    private val userRepository: UserRepository,
    private val salonRepository: SalonRepository
) :
    BaseViewModel() {
    val dataAppointment = userRepository.appointment
    val compositeDisposable = CompositeDisposable()
    val imageCollect = MutableLiveData<String>()
    val collectResult = salonRepository.collectPhotoResult
    val removeArtResult = salonRepository.removeArtResult

    init {
        imageCollect.value = EMPTY
        loadData()
    }

    private fun loadData() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {

                    userRepository.getDetailAppointment(bookingId, it.getBearToken())
                }
            }
        }

    }

    fun removeArt(artId: Int) {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    salonRepository.removeArt(it.getBearToken(), artId)
                }
            }
        }

    }

    fun compressAndUpload(imageFile: File) {
        compositeDisposable.add(
            Compressor(SalonApplication.applicationContext()).compressToFileAsFlowable(
                imageFile
            )
                .subscribeOn(AndroidSchedulerProvider().computation())
                .observeOn(AndroidSchedulerProvider().io())
                .subscribe({
                    uploadCollectImage(it)
                }, {
                    it.printStackTrace()
                })
        )
    }

    private fun uploadCollectImage(imageFile: File) {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    salonRepository.collectPhoto(it.getBearToken(), bookingId, imageFile)
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}