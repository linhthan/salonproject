package linh.dev.salonapp.ui.fragment.customers.saloninfo

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_order_detail.rcvProduct
import kotlinx.android.synthetic.main.fragment_product_salon.*
import kotlinx.android.synthetic.main.layout_no_data.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showMsg
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.ProductSalonAdapter
import linh.dev.salonapp.databinding.FragmentProductSalonBinding
import linh.dev.salonapp.model.local.ProductCategory
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.ProductSalonViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ProductSalonFragment : BaseFragment<FragmentProductSalonBinding>() {
    private val args: ProductSalonFragmentArgs by navArgs()
    private val viewModel: ProductSalonViewModel by viewModel { parametersOf(args.salonId) }
    private val adapter: ProductSalonAdapter by inject()
    var spinnerAdapter: ArrayAdapter<ProductCategory>? = null
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_product_salon
    }

    override fun initView() {
        super.initView()
        if (spinnerAdapter == null) {
            context?.let {
                spinnerAdapter = ArrayAdapter(it, android.R.layout.simple_expandable_list_item_1)
            }

        }
        initRecyclerView()
    }

    private fun initRecyclerView() {
        rcvProduct.layoutManager = object : GridLayoutManager(context, 1) {
            override fun supportsPredictiveItemAnimations(): Boolean {
                return false
            }
        }
        rcvProduct.adapter = adapter
        (rcvProduct.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        rcvProduct.itemAnimator = null
        adapter.setItemClick(object : ProductSalonAdapter.OnItemClick {
            override fun onItemClick(position: Int) {
                val item = adapter.getItemByPosition(position)
                item?.let {
                    val bundle = Bundle()
                    bundle.putInt(BUNDLE_PRODUCT_ID, item.id)
                    Navigation.findNavController(requireActivity(), R.id.nav_main)
                        .navigate(R.id.action_productSalonFragment_to_productDetailFragment, bundle)
                }
            }

            override fun onViewClick(resId: Int, position: Int) {

                if (resId == R.id.imvAddProduct) {
                    adapter.onAddProductClick(position)
                    val dataTransfer = adapter.getProductSelected()
                    viewModel.productSelected.value = dataTransfer

                }
            }

        })
        spinnerAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCategory.adapter = spinnerAdapter
        spinnerCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                showToast("Nothing")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (viewModel.firstTime) {
                    viewModel.firstTime = false
                } else {
                    val category = spinnerAdapter?.getItem(position)
                    category?.let {

                        if (viewModel.catId.value == null || viewModel.catId.value == it.id) {
                            return
                        } else {
                            adapter.clearSelected()
                            viewModel.productSelected.value = ArrayList()
                            viewModel.catId.value = it.id
                        }
                    }
                }

            }

        }
        searchView.setOnQueryTextListener(DebouncingQueryTextListener(viewModel) { keyword ->

            val keySearch = keyword ?: EMPTY
            if (viewModel.keyword.value != null && !viewModel.keyword.value.equals(keySearch)) {
                adapter.clearSelected()
                viewModel.productSelected.value = ArrayList()
                viewModel.keyword.value = keySearch
            }


        })
        btnCreateOrder.setOnClickListener {
            val listProductSelected = viewModel.productSelected.value
            if (!listProductSelected.isNullOrEmpty()) {
                val bundle = Bundle()
                val jsonPass = Gson().toJson(listProductSelected)
                bundle.putString(BUNDLE_PRODUCT_SELECTED, jsonPass)
                bundle.putInt(BUNDLE_ID_SALON, args.salonId)
                Navigation.findNavController(requireActivity(), R.id.nav_main)
                    .navigate(R.id.action_productSalonFragment_to_createOrderFragment, bundle)

            }

        }
        btnTryAgain.setOnClickListener {
            viewModel.retryFailed()
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun startObservable() {
        viewModel.productList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
    }

    private fun initObservable() {
        viewModel.catId.observe(viewLifecycleOwner, Observer {
            val keyword = viewModel.keyword.value ?: EMPTY
            viewModel.replaceSubscription( it, keyword)
            startObservable()

        })
        viewModel.keyword.observe(viewLifecycleOwner, Observer {
            val catId = viewModel.catId.value ?: CATEGORY_ALL_ID
            viewModel.replaceSubscription( catId, it)
            startObservable()

        })
        viewModel.productSelected.observe(viewLifecycleOwner, Observer {
            if (it.isNullOrEmpty()) {
                layoutCart.visibility = View.GONE
            } else {
                layoutCart.visibility = View.VISIBLE
                tvCountService.text = it.size.toString()
                var totalPrice = 0
                it.forEach { product ->
                    totalPrice += product.price.toInt()
                }
                tvTotalPrice.text =
                    resources.getString(R.string.format_price_product, (totalPrice / 1000))
            }
        })
        viewModel.categoryProductResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {

                }
                Result.Status.SUCCESS -> {
                    spinnerAdapter?.clear()
                    spinnerAdapter?.add(ProductCategory.defaultCategory())
                    if (it.data != null) {
                        spinnerAdapter?.addAll(it.data.data)
                    }

                }
                Result.Status.ERROR -> {
                    spinnerAdapter?.clear()
                    spinnerAdapter?.add(ProductCategory.defaultCategory())
                    showMsg(it.message)
                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })



        viewModel.getState().observe(viewLifecycleOwner, Observer { state ->
            if (viewModel.listIsEmpty() && state == State.LOADING) {
                progressLoadingData.show()
            } else {
                progressLoadingData.hide()
            }
            if (viewModel.listIsEmpty() && state == State.ERROR) {
                progressLoadingData.hide()
                viewNoData.show()
            } else {
                viewNoData.hide()
            }
            if(viewModel.listIsEmpty() &&adapter.itemCount ==1){
                viewNoData.show()
            }else{
                viewNoData.hide()
            }
            adapter.setState(state ?: State.DONE)

        })
    }

    internal class DebouncingQueryTextListener(
        viewModel: ProductSalonViewModel,
        private val onDebouncingQueryTextChange: (String?) -> Unit
    ) : SearchView.OnQueryTextListener {
        var debouncePeriod: Long = 500

        private val coroutineScope = viewModel.viewModelScope

        private var searchJob: Job? = null

        override fun onQueryTextSubmit(query: String?): Boolean {
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            searchJob?.cancel()
            searchJob = coroutineScope.launch {
                newText?.let {
                    delay(debouncePeriod)
                    onDebouncingQueryTextChange(newText)
                }
            }
            return false
        }
    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_ADD_PRODUCT_TO_CART) {
            val productId = appAction.event.getData(String::class.java)
            val currentList = viewModel.currentListProduct.value
            if (productId == null) return
            currentList?.add(productId.toInt())
        }
    }
}