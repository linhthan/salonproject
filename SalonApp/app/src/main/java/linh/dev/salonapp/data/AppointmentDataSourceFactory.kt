package linh.dev.salonapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import kotlinx.coroutines.CoroutineScope
import linh.dev.salonapp.data.AppointmentDataSource
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.network.AppApi

class AppointmentDataSourceFactory(
    var keyword:String,
    private val appApi: AppApi,
    private val scope: CoroutineScope
) : DataSource.Factory<Int, Appointment>() {

    val appointmentDataSourceLiveData = MutableLiveData<AppointmentDataSource>()

    override fun create(): DataSource<Int, Appointment> {
        val newsDataSource = AppointmentDataSource(keyword,appApi, scope)
        appointmentDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}