package linh.dev.salonapp.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_booking_detail.*
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.R
import linh.dev.salonapp.adapter.ServiceBillAdapter
import linh.dev.salonapp.listenters.OnActionConfirmBookingCallback
import linh.dev.salonapp.model.local.Service
import linh.dev.salonapp.util.DateTimeUtils
import org.koin.android.ext.android.inject

class BottomSheetBooking : BottomSheetDialogFragment() {
    private var services: List<Service> = emptyList()
    private var salonName: String = EMPTY
    private var salonLocation = EMPTY
    private var bookingTime = 0
    private var bookingDate = EMPTY
    private var onActionConfirmBookingCallback: OnActionConfirmBookingCallback? = null
    private val adapter: ServiceBillAdapter by inject()

    companion object {
        fun newInstance(
            services: List<Service>,
            salonName: String,
            salonLocation: String,
            bookingTime: Int,
            bookingDate: String
        ): BottomSheetBooking {
            val dialog = BottomSheetBooking()
            dialog.services = services
            dialog.salonName = salonName
            dialog.salonLocation = salonLocation
            dialog.bookingTime = bookingTime
            dialog.bookingDate = bookingDate
            return dialog

        }
    }

    fun setTimeBooking( bookingTime: Int, bookingDate: String){
        this.bookingTime = bookingTime
        this.bookingDate = bookingDate
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetLogin)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.bottom_sheet_booking_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initAction()

    }

    private fun initAction() {
        btnConfirm.setOnClickListener {
            val listServices = ArrayList<Service.ServiceJson>()
            services.forEach {
                listServices.add(Service.ServiceJson(it.id, it.price))
            }
            onActionConfirmBookingCallback?.onConfirmClick(bookingTime, listServices)
        }
    }

    private fun initRecyclerView() {
        tvDateBooking.text = getString(R.string.format_lbl_date_common,bookingDate)
        tvTimeBooking.text = DateTimeUtils.convertMinuteToSlotDisplayTime(bookingTime)
        tvSalonName.text = salonName
        tvLocation.text = salonLocation
        var totalPrice =0
        services.forEach {
            totalPrice+=it.price.toInt()
        }
        tvTotalPrice.text = "${totalPrice/1000} K"
        rcvService.layoutManager = LinearLayoutManager(context)
        rcvService.adapter = adapter
        adapter.setItems(services)
    }

    fun setOnActionConfirmBooking(onActionConfirmBookingCallback: OnActionConfirmBookingCallback) {
        this.onActionConfirmBookingCallback = onActionConfirmBookingCallback
    }
}