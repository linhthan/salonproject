package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemCreateOrderBinding
import linh.dev.salonapp.databinding.ViewItemProductOrderBinding
import linh.dev.salonapp.model.local.OrderItem
import linh.dev.salonapp.model.local.Product

class ProductItemOrderAdapter : BaseRecycleViewAdapter<Product>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemCreateOrderBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_create_order, parent, false
        )
        return ViewHolder(binding)
    }

    inner class ViewHolder(val binding: ViewItemCreateOrderBinding) :
        BaseRecycleViewAdapter<Product>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.viewModel = getItem(position)
            binding.imvDecrease.setOnClickListener {
                onViewClick(R.id.imvDecrease)
            }
            binding.imvIncrease.setOnClickListener {
                onViewClick(R.id.imvIncrease)
            }
            binding.tvQuantity.setOnClickListener {
                onViewClick(R.id.tvQuantity)
            }
        }

    }
    fun getCurrentList():ArrayList<Product>{
        return dataSources
    }
    fun getTotalPrice(): Double {
        var result = 0.0
        dataSources.forEach {
            result += it.price * it.selectedQuantity
        }
        return result
    }
}