package linh.dev.salonapp.model.local

import com.google.gson.annotations.SerializedName

class Product(
    val id: Int,
    val title: String,
    val salonId: Int,
    val catId: Int,
    val quantity: Int,
    val catTitle: String,
    val description: String,
    val thumbnail: String,
    var selectedQuantity: Int = 1,
    @SerializedName("current_price") val price: Double
) {
}