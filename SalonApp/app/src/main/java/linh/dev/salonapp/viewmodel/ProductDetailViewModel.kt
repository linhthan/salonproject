package linh.dev.salonapp.viewmodel

import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.ProductRepository

class ProductDetailViewModel(val productId: Int, val repository: ProductRepository) :
    BaseViewModel() {
    val productResult = repository.productResult
    val addCartResult = repository.addCartResult

    init {
        getProductById()
    }

     fun getProductById() {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    repository.getProductSalonById(it.getBearToken(), productId)
                }
            }
        }
    }
    fun addProductToCart(){
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    repository.addProductToCart(it.getBearToken(), productId)
                }
            }
        }
    }
}