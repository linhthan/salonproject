package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.base.STATUS
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemFooterBinding


abstract class LoadMoreRecycleViewAdapter<T> : BaseRecycleViewAdapter<T>() {
    private val TYPE_PROGRESS = 0xFFFF

    private var mOnLoadMoreFailed: Boolean = false
    private var mIsReachEnd: Boolean = false
    private var status = STATUS.SUCCESS
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        when (viewType) {
            TYPE_PROGRESS -> {
                val binding: ViewItemFooterBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.view_item_footer, parent, false
                )
                return BottomViewHolder(binding)
            }
        }
        throw RuntimeException("LoadMoreRecyclerViewAdapter: ViewHolder = null")
    }

    inner class BottomViewHolder(val binding: ViewItemFooterBinding) :
        BaseRecycleViewAdapter<T>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.progressBar.show()
            binding.txtError.hide()
            when (status) {
                STATUS.LOADING -> {
                    binding.progressBar.show()
                    binding.txtError.hide()
                }
                STATUS.SUCCESS -> {
                    binding.progressBar.hide()
                    binding.txtError.hide()
                }
            }
            binding.txtError.setOnClickListener {
                onViewClick(R.id.txt_error)
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        if (position == bottomItemPosition()) {
            return TYPE_PROGRESS
        }
        return getCustomItemViewType(position)
    }
    protected abstract fun getCustomItemViewType(position: Int): Int

    private fun bottomItemPosition(): Int {
        return itemCount-1
    }

    fun startLoadMore() {
        status = STATUS.LOADING
        notifyItemChanged(bottomItemPosition())
    }

    fun onLoadMoreSuccess() {
        status = STATUS.SUCCESS
        notifyItemChanged(bottomItemPosition())
    }

    /**
     * It help visible layout retry when load more failed
     */
    fun onLoadMoreFailed() {
        status = STATUS.FAILED
        notifyItemChanged(bottomItemPosition())
    }

    fun onReachEnd() {
        mIsReachEnd = true
        notifyItemChanged(bottomItemPosition())
    }
}