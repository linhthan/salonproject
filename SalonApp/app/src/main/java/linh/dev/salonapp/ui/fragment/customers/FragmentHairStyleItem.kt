package linh.dev.salonapp.ui.fragment.customers

import android.os.Bundle
import android.util.DisplayMetrics
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_hairstyle_list.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.AppAction
import linh.dev.salonapp.AppEvent
import linh.dev.salonapp.R
import linh.dev.salonapp.adapter.HairStyleItemAdapter
import linh.dev.salonapp.databases.model.HairStyle
import linh.dev.salonapp.databinding.FragmentHairstyleListBinding
import linh.dev.salonapp.ui.custom.SpaceItemGridDecoration
import linh.dev.salonapp.viewmodel.HairStyleListViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class FragmentHairStyleItem : BaseFragment<FragmentHairstyleListBinding>() {
    private val args: FragmentHairStyleItemArgs by navArgs()
    private val viewModel: HairStyleListViewModel by viewModel {
        parametersOf(
            args.salonId,
            args.listHair
        )
    }
    private val adapter: HairStyleItemAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_hairstyle_list
    }

    override fun initView() {
        super.initView()
        mBinding.viewModel = viewModel
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val scale = displayMetrics.density;
        val pixel90dp = (90 * scale + 0.5f)
        if (pixel90dp == 0f) {
            rcvHairStyleList.layoutManager = GridLayoutManager(context, 3, GridLayoutManager.VERTICAL,false)
        } else {
            val itemCount = ((width - 8 * 10) / pixel90dp)

            rcvHairStyleList.layoutManager =
                GridLayoutManager(context, if (itemCount.toInt() != 0) itemCount.toInt() else 3, GridLayoutManager.VERTICAL,false)
        }
        rcvHairStyleList.addItemDecoration(SpaceItemGridDecoration(10, 10, 10, 10))
        rcvHairStyleList.adapter = adapter
        imvDownload.setOnClickListener {
            if (viewModel.gallery.value?.itemHairStyle.isNullOrEmpty()) {

            }
            val gallery = viewModel.gallery.value
            gallery?.let {
                val itemDownload = it.itemHairStyle
                if (itemDownload.isNullOrEmpty()) {
                    showToast("Không có mẫu tóc mới nào!")
                } else {
                    context?.let {
                        viewModel.downloadListImage(it, itemDownload)
                    }

                }
            }
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.hairStyleLocal.observe(viewLifecycleOwner, Observer {
            viewModel.processGallery(it)
            adapter.setItems(it)
        })
    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_DOWNLOAD_IMAGE_SUCCESS) {
            val data = appAction.event.getData(HairStyle::class.java)
            data?.let {
                var itemRemove: linh.dev.salonapp.model.local.HairStyle? = null
                viewModel.gallery.value?.itemHairStyle?.forEach {
                    if (it.id == data.idHair) {
                        itemRemove = it
                        return@forEach
                    }
                }
                viewModel.gallery.value?.itemHairStyle?.remove(itemRemove)
                adapter.addItem(it)
            }
        }
    }
}