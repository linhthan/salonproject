package linh.dev.salonapp.ui.custom

import android.graphics.drawable.Drawable
import com.sticker.DrawableSticker

data class HairStyle(val drawableRes: Drawable?) : DrawableSticker(drawableRes) {

}