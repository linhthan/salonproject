package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Slot

data class SlotAvailableResponse(val message:String, val data:List<Slot>) {
}