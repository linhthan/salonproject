package linh.dev.salonapp.notification

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import androidx.work.*
import linh.dev.salonapp.*
import linh.dev.salonapp.R
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.network.BookingWorker
import linh.dev.salonapp.ui.activitys.CustomerHomeActivity
import linh.dev.salonapp.util.DateTimeUtils


class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        intent?.let {
            val bookingId = it.extras?.getInt(EXTRA_ID_BOOKING)
            val bookingTime = it.extras?.getString(EXTRA_BOOKING_TIME, EMPTY) ?: EMPTY

            bookingId?.let { id ->
                val data = Data.Builder()
                    .putInt(EXTRA_ID_BOOKING, id)
                    .putString(EXTRA_BOOKING_TIME, bookingTime)
                    .build()
                try {
                    val connectivityManager =
                        context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val builder = NetworkRequest.Builder()

                    connectivityManager.registerNetworkCallback(
                        builder.build(),
                        object : ConnectivityManager.NetworkCallback() {
                            override fun onAvailable(network: Network) {
                                val constraints = Constraints.Builder()
                                    .setRequiredNetworkType(NetworkType.CONNECTED)

                                val oneTimeRequest =
                                    OneTimeWorkRequest.Builder(BookingWorker::class.java)
                                        .setInputData(data)
                                        .setConstraints(constraints.build())
                                        .addTag("bookingNotification")
                                        .build()
                                WorkManager.getInstance(context).enqueueUniqueWork(
                                    "Booking Salon App",
                                    ExistingWorkPolicy.APPEND,
                                    oneTimeRequest
                                )
                            }


                            override fun onUnavailable() {
                                val notificationManager =
                                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                                val notificationChannel = "Booking Notification"
                                if (bookingTime.isNotEmpty()) {
                                    val intentActivity =
                                        Intent(context, CustomerHomeActivity::class.java).apply {
                                            flags =
                                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            putExtra(BUNDLE_HOME_TAB, 1)
                                        }
                                    val pendingIntent: PendingIntent =
                                        PendingIntent.getActivity(context, 0, intentActivity, 0)
                                    val content = context?.getString(
                                        R.string.format_content_notification_msg_booking,
                                        DateTimeUtils.formatDateString(
                                            bookingTime,
                                            SERVER_TIME_FORMAT, APPOINTMENT_HOUR_START_FORMAT
                                        )
                                    )
                                    NotificationHelper.showNotification(
                                        context,
                                        notificationManager,
                                        notificationChannel,
                                        pendingIntent,
                                        "BKSalon",
                                        content
                                    )
                                } else {
                                    logE("No content")
                                }
                            }
                        }

                    )
                } catch (e: Exception) {
                    val notificationManager =
                        context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val notificationChannel = "Booking Notification"
                    if (bookingTime.isNotEmpty()) {
                        val intentActivity =
                            Intent(context, CustomerHomeActivity::class.java).apply {
                                flags =
                                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                putExtra(BUNDLE_HOME_TAB, 1)
                            }
                        val pendingIntent: PendingIntent =
                            PendingIntent.getActivity(context, 0, intentActivity, 0)
                        val content = context.getString(
                            R.string.format_content_notification_msg_booking,
                            DateTimeUtils.formatDateString(
                                bookingTime,
                                SERVER_TIME_FORMAT, APPOINTMENT_HOUR_START_FORMAT
                            )
                        )
                        NotificationHelper.showNotification(
                            context,
                            notificationManager,
                            notificationChannel,
                            pendingIntent,
                            "BKSalon",
                            content
                        )
                    } else {
                        logE("No content")
                    }
                }


            }
        }
    }
}