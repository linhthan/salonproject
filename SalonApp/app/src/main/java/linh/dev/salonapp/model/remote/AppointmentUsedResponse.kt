package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Appointment

data class AppointmentUsedResponse(
    val data: List<Appointment>,
    val current_page: Int,
    val last_page: Int
)