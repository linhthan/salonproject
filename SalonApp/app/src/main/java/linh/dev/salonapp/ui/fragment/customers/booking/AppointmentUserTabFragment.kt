package linh.dev.salonapp.ui.fragment.customers.booking

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_appointment_user_tab.*
import kotlinx.android.synthetic.main.fragment_customer_appointment_tab.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.AppointmentAdapter
import linh.dev.salonapp.databinding.FragmentAppointmentUserTabBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.model.local.Appointment
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.AppointmentUserViewModel
import linh.dev.salonapp.viewmodel.CustomerAppointmentViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class AppointmentUserTabFragment : BaseFragment<FragmentAppointmentUserTabBinding>() {
    private val viewModel: AppointmentUserViewModel by viewModel()
    private val adapter: AppointmentAdapter by inject()

    companion object {
        fun newInstance(): AppointmentUserTabFragment {
            return AppointmentUserTabFragment()
        }
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_appointment_user_tab
    }

    override fun initView() {
        super.initView()
        initRecyclerView()
    }

    private fun initRecyclerView() {
        rcvListAppointment.layoutManager = LinearLayoutManager(context)
        rcvListAppointment.addItemDecoration(SpaceItemDecoration(10, 10, 10, 10, 10))
        rcvListAppointment.adapter = adapter
        swrLayout.setOnRefreshListener {
            adapter.clearData()
            viewModel.loadAllAppointment()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservableData()
    }

    private fun initObservableData() {
        viewModel.listAppointment.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    if (!swrLayout.isRefreshing) {
                        progressLoading.show()
                    }
                }
                Result.Status.SUCCESS -> {
                    if (swrLayout.isRefreshing) {
                        swrLayout.isRefreshing = false
                    }
                    progressLoading.hide()
                    val data = it.data
                    if (data.isNullOrEmpty()) {
                        emptyView.show()

                    } else {
                        emptyView.hide()
                        adapter.setItems(data)
                    }
                }
                Result.Status.ERROR -> {
                    if (swrLayout.isRefreshing) {
                        swrLayout.isRefreshing = false
                    }
                    progressLoading.hide()
                    emptyView.show()
                    it.message?.let { msg ->
                        showToast(msg)
                    }

                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
        viewModel.cancelResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoading.show()
                }
                Result.Status.SUCCESS -> {
                    progressLoading.hide()
                    val data = it.data
                    if (data != null) {
                        if (data.success) {
                            showToast(data.message)
                            adapter.updateDataCancel()
                        } else {
                            showToast(data.message)
                        }
                    }
                }
                Result.Status.ERROR -> {
                    progressLoading.hide()
                    it.message?.let { msg ->
                        showToast(msg)
                    }

                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })



        btnTryNow.setOnClickListener {
            bus.post(AppAction(AppEvent.EVENT_GO_TO_HOME))
        }


    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        when {
            appAction.event == AppEvent.EVENT_CLICK_CANCEL_BUTTON -> {
                val appointment = appAction.event.getData(Appointment::class.java)
                appointment?.let {
                    if (appointment.status == APPOINTMENT_STATUS_PENDING) {
                        viewModel.cancelAppointment(appointment)
                    } else {
                        showToast("Bạn không thể hủy lịch ở trạng thái này!")
                    }

                }
            }
            appAction.event == AppEvent.EVENT_GO_TO_APPOINTMENT_DETAIL -> {
                val appointment = appAction.event.getData(Appointment::class.java)
                appointment?.let { item ->
                    val bundle = Bundle()
                    bundle.putInt(BUNDLE_ID_BOOKING, item.id)
                    Navigation.findNavController(requireActivity(), R.id.nav_main)
                        .navigate(
                            R.id.action_customerHomeFragment_to_appointmentDetailFragment,
                            bundle
                        )
                }

            }

        }
    }

}