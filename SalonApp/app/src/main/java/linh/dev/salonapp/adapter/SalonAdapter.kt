package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import kotlinx.android.synthetic.main.view_item_salon.view.*
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemSalonBinding
import linh.dev.salonapp.model.local.Salon

class SalonAdapter : BaseRecycleViewAdapter<Salon>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemSalonBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_salon,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    inner class ViewHolder(val binding: ViewDataBinding) :
        BaseRecycleViewAdapter<Salon>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            (binding as ViewItemSalonBinding).viewModel = getItem(position)
            binding.root.buttonBook.setOnClickListener {
                onViewClick(R.id.buttonBook)
            }
        }

    }
}