package linh.dev.salonapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import linh.dev.base.BaseFragment
import linh.dev.salonapp.ui.fragment.customers.saloninfo.ArtSalonFragment
import linh.dev.salonapp.ui.fragment.customers.saloninfo.InfoPageFragment
import linh.dev.salonapp.ui.fragment.customers.saloninfo.ServicePageFragment

class PagerSalonInfoAdapter(val salonId: Int, fm: FragmentManager, behavior: Int) :
    FragmentStatePagerAdapter(fm, behavior) {
    private val items = arrayListOf<BaseFragment<*>>(
        ServicePageFragment.newInstance(salonId),
        ArtSalonFragment.newInstance(salonId),
        InfoPageFragment.newInstance(salonId)
    )
    private val pageTitles = arrayListOf("Dịch vụ", "Thư viện ảnh", "Thông tin")

    override fun getItem(position: Int): Fragment {
        return items[position]
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return pageTitles[position]
    }


}