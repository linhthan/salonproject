package linh.dev.salonapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.repository.HairStyleRepository
import linh.dev.salonapp.repository.SalonRepository
import java.io.File

class CreateHairStyleViewModel(
    val hairStyleRepository: HairStyleRepository,
    val salonRepository: SalonRepository
) : BaseViewModel() {
    val compositeDisposable = CompositeDisposable()
    val imagePathSelected = MutableLiveData<String>()
    val imagePathCrop = MutableLiveData<String>()
    val hairStyleResult = hairStyleRepository.hairStyleResult
    val saveHairStyleResult = salonRepository.saveHairStyleResult

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun createHairStyleImage(imageFile: File) {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    hairStyleRepository.predictHairStyle(imageFile)
                }
            }
        }
    }

    fun saveHairStyle(title: String, url: String) {
        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    salonRepository.saveHairStyle(it.getBearToken(), it.salonId, title, url)
                }
            }
        }
    }
}