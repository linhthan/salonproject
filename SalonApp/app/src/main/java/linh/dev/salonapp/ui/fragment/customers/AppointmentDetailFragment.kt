package linh.dev.salonapp.ui.fragment.customers

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.core.permission.RxRequestPermission
import kotlinx.android.synthetic.main.dialog_take_photo.*
import kotlinx.android.synthetic.main.fragment_appointment_detail.*
import linh.dev.base.BaseActivity
import linh.dev.base.BaseFragment
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.base.ext.view.hide
import linh.dev.base.ext.view.show
import linh.dev.base.ext.view.showToast
import linh.dev.base.permission.Permission
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.ArtAdapter
import linh.dev.salonapp.adapter.ServiceAppointmentAdapter
import linh.dev.salonapp.databinding.FragmentAppointmentDetailBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.AppointmentDetailViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.io.File
import java.io.IOException

class AppointmentDetailFragment : BaseFragment<FragmentAppointmentDetailBinding>() {


    private val artAdapter: ArtAdapter by inject()
    private val serviceAdapter: ServiceAppointmentAdapter by inject()
    private val args: AppointmentDetailFragmentArgs by navArgs()
    private val viewModel: AppointmentDetailViewModel by viewModel { parametersOf(args.idBooking) }
    override fun getLayoutResourceId(): Int = R.layout.fragment_appointment_detail

    override fun initView() {
        super.initView()
        mBinding.viewModel = viewModel
        initRecycleView()
        initObservable()
        imvBack.setOnClickListener {
            activity?.onBackPressed()
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initRecycleView() {
        rcvArt.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rcvArt.addItemDecoration(SpaceItemDecoration(10, 10, 10, 10))
        rcvArt.adapter = artAdapter
        rcvService.layoutManager = LinearLayoutManager(context)
        rcvService.adapter = serviceAdapter
        rcvService.addItemDecoration(SpaceItemDecoration(10, 10, 10, 10))


        artAdapter.setOnViewClickListener(object : BaseRecycleViewAdapter.ViewClickListener {
            override fun onViewItemClock(resId: Int, position: Int) {
                if (resId == R.id.btnAddArt) {
                    requestPermission()
                } else if (resId == R.id.itemArt) {
                    showToast("Click art")
                } else if (resId == R.id.imvDelete) {
                    val item = artAdapter.getItem(position)
                    showConfirmDelete(item.id)
                }
            }

        })


    }

    private fun showConfirmDelete(artId: Int) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Bạn có chắc muốn xóa ảnh này?")
        builder.setPositiveButton(getString(R.string.dialog_ok)) { dialog, _ ->
            dialog.dismiss()
            viewModel.removeArt(artId)
        }
        builder.setNegativeButton(getString(R.string.cancel_dialog_common)) { dialog, _ ->
            dialog.dismiss()

        }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    private fun requestPermission() {
        viewModel.compositeDisposable.add(
            RxRequestPermission(activity as BaseActivity<*>).request(
                arrayListOf(
                    Permission(Manifest.permission.CAMERA, false),
                    Permission(Manifest.permission.READ_EXTERNAL_STORAGE, false),
                    Permission(Manifest.permission.WRITE_EXTERNAL_STORAGE, false)
                )
            ).subscribe { granted ->
                if (granted) {
                    //TODO open camera
                    showDialogAddPhoto()

                } else {
                    showToast("Bạn đã từ chối quyền truy cập máy ảnh")
                }

            }
        )
    }

    private fun showDialogAddPhoto() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_take_photo)
        dialog.layoutTakePhoto.setOnClickListener {
            dispatchTakePictureIntent()
            dialog.dismiss()
        }
        dialog.layoutPickPhoto.setOnClickListener {
            dispatchPickPictureIntent()
            dialog.dismiss()
        }
        dialog.setCanceledOnTouchOutside(true)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.CENTER)
        dialog.show()

    }


    private fun createImageFile(

    ): File? {
        val timeStamp: String = System.currentTimeMillis().toString()
        val storageDir: File? = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            viewModel.imageCollect.value = absolutePath
        }
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val mPhotoUri = FileProvider.getUriForFile(
                        requireContext(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri)
                    startActivityForResult(takePictureIntent, CAMERA_PIC_REQUEST)
                }
            }
        }
    }

    private fun dispatchPickPictureIntent() {
        Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        ).also { takePictureIntent ->
            takePictureIntent.type = "image/*"

            startActivityForResult(takePictureIntent, GALLERY_REQUEST_CODE)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_PIC_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                val imagePath = viewModel.imageCollect.value
                if (!imagePath.isNullOrEmpty()) {
                    val photo = File(imagePath)
                    if (photo.exists()) {
                        viewModel.compressAndUpload(photo)
                    } else {
                        showToast("Ảnh không tồn tại hoặc đã bị xóa, vui lòng thử lại")
                    }
                } else {
                    showToast("Có lỗi xảy ra trong quá trình lưu ảnh")
                }
            }
        } else if (requestCode == GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            uri?.let {
                val proj = arrayOf(MediaStore.Images.Media.DATA)
                val cursor = requireContext().contentResolver.query(uri, proj, null, null, null);
                val columnIndex = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                cursor?.moveToFirst()
                columnIndex?.let {
                    val path = cursor.getString(it)
                    cursor.close()
                    val photo = File(path)
                    if (photo.exists()) {
                        viewModel.compressAndUpload(photo)
                    } else {
                        showToast("Ảnh không tồn tại hoặc đã bị xóa, vui lòng thử lại")
                    }
                }

            }

        }
    }


    private fun initObservable() {

        viewModel.collectResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressBar.show()
                }
                Result.Status.SUCCESS -> {
                    progressBar.hide()
                    it.data?.let { data ->
                        if (data.success) {
                            val art = data.data
                            art?.let { image ->
                                artAdapter.addItem(image)
                                bus.post(AppAction(AppEvent.EVENT_COLLECT_IMAGE.setData(image)))
                            }

                        } else {
                            showToast(data.message)
                        }
                    }

                }
                Result.Status.ERROR -> {
                    progressBar.hide()
                    it.message?.let { msg ->
                        showToast(msg)
                    }
                }
                Result.Status.UNAUTHORIZED -> {
                    progressBar.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })

        viewModel.removeArtResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressBar.show()
                }
                Result.Status.SUCCESS -> {
                    progressBar.hide()
                    it.data?.let { data ->
                        if (data.success) {
                            showToast(data.message)
                            val artId = data.data
                            val artRemove = artAdapter.removeItem(artId)

                            artRemove?.let {
                                logE(artRemove.id)
                                logE(artRemove.id_booking)
                                bus.post(AppAction(AppEvent.EVENT_REMOVE_ART.setData(it)))
                            }

                        } else {
                            showToast(data.message)
                        }
                    }

                }
                Result.Status.ERROR -> {
                    progressBar.hide()
                    it.message?.let { msg ->
                        showToast(msg)
                    }

                }
                Result.Status.UNAUTHORIZED -> {
                    progressBar.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })

        viewModel.dataAppointment.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressBar.show()
                    layoutNoData.visibility = View.GONE
                    layoutData.visibility = View.GONE
                    logE("loading $it.data")
                }
                Result.Status.SUCCESS -> {
                    layoutNoData.visibility = View.GONE
                    layoutData.visibility = View.VISIBLE

                    progressBar.hide()
                    val data = it.data
                    data?.let { ap ->
                        if(ap.status== APPOINTMENT_STATUS_PAID){
                            layoutArt.show()
                            tvArt.show()
                            val arts = ap.images
                            artAdapter.setItems(arts)

                        }else{
                            layoutArt.hide()
                            tvArt.hide()
                        }
                        val services = ap.services
                        serviceAdapter.setItems(services)
                    }

                }
                Result.Status.ERROR -> {
                    progressBar.hide()
                    layoutNoData.visibility = View.VISIBLE
                    layoutData.visibility = View.GONE
                }
                Result.Status.UNAUTHORIZED -> {
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
    }
}