package linh.dev.salonapp.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.launch
import linh.dev.base.BaseViewModel
import linh.dev.salonapp.EMPTY
import linh.dev.salonapp.PER_PAGE
import linh.dev.salonapp.State
import linh.dev.salonapp.data.SalonDataSource
import linh.dev.salonapp.data.SalonDataSourceFactory
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Salon
import linh.dev.salonapp.repository.SalonRepository
import linh.dev.salonapp.repository.UserRepository
import linh.dev.salonapp.ui.custom.SingleLiveEvent

class SearchSalonViewModel(val repository: SalonRepository,val userRepository: UserRepository) : BaseViewModel() {
    var salonList: LiveData<PagedList<Salon>>
    val updateResult = userRepository.updateResult

    val keyword: SingleLiveEvent<String> by lazy {
        SingleLiveEvent<String>()
    }
    private var salonDataSourceFactory: SalonDataSourceFactory


    init {
        keyword.value = EMPTY
        salonDataSourceFactory =
            SalonDataSourceFactory(EMPTY, repository.api, viewModelScope)
        val config = PagedList.Config.Builder()
            .setPageSize(PER_PAGE)
            .setInitialLoadSizeHint(PER_PAGE)
            .setEnablePlaceholders(false)
            .build()
        salonList = LivePagedListBuilder(salonDataSourceFactory, config).build()
    }

    fun updatePhoneNumber(phoneNumber: String) {
        user()?.let {
            viewModelScope.launch {
                userRepository.updatePhoneNumber(it.getBearToken(), phoneNumber)
            }
        }

    }


    fun replaceSubscription() {
        salonDataSourceFactory.keyword = keyword.value?: EMPTY
        refreshData()
    }

    fun getState(): LiveData<State> = Transformations.switchMap<SalonDataSource,
            State>(
        salonDataSourceFactory.salonDataSourceLiveData,
        SalonDataSource::state
    )

    fun retryFailed() {
        salonDataSourceFactory.salonDataSourceLiveData.value?.retryFailedQuery()
    }


    fun listIsEmpty(): Boolean {
        return salonList.value?.isEmpty() ?: true
    }

    fun refreshData() {
        salonDataSourceFactory.salonDataSourceLiveData.value?.refresh()
    }
}