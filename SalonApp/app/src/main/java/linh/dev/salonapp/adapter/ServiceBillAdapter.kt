package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemServiceBookingBinding
import linh.dev.salonapp.model.local.Service

class ServiceBillAdapter : BaseRecycleViewAdapter<Service>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding:ViewItemServiceBookingBinding = DataBindingUtil.inflate( LayoutInflater.from(parent.context),
            R.layout.view_item_service_booking,
            parent,
            false)
        return ViewHolder(binding)
    }


    inner class ViewHolder(val binding: ViewDataBinding) :
        BaseRecycleViewAdapter<Service>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            (binding as ViewItemServiceBookingBinding).viewModel = getItem(position)
        }

    }
}