package linh.dev.salonapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.ViewItemArtBinding
import linh.dev.salonapp.model.local.Art

class ArtSalonAdapter : BaseRecycleViewAdapter<Art>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewItemArtBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.view_item_art, parent, false
        )
        return ViewHolder(binding)
    }

    inner class ViewHolder(val binding: ViewItemArtBinding) :
        BaseRecycleViewAdapter<Art>.BaseViewHolder(binding) {
        override fun onBindingData(position: Int) {
            binding.viewModel = getItem(position)
            binding.itemArt.setOnClickListener {
                onViewClick(R.id.itemArt)
            }
        }
    }

}




