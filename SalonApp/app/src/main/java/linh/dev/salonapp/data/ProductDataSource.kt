package linh.dev.salonapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.*
import linh.dev.salonapp.State
import linh.dev.salonapp.ext.io
import linh.dev.salonapp.ext.user
import linh.dev.salonapp.model.local.Product
import linh.dev.salonapp.network.AppApi

class ProductDataSource(
    private var catId:Int,
    private var keyword:String,
    private val salonId:Int,
    private val appApi: AppApi,
    private val scope: CoroutineScope
) : PageKeyedDataSource<Int, Product>() {

    var state: MutableLiveData<State> = MutableLiveData()
    private var retryQuery: (() -> Any)? = null
    private var supervisorJob = SupervisorJob()
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Product>
    ) {
        retryQuery = { loadInitial(params, callback) }
        user()?.let {
            scope.launch {
                try {
                    withContext(io()) {
                        updateState(State.LOADING)
                        val response = appApi.getProductSalonByCategory(
                            it.getBearToken(),
                            salonId,
                            catId,
                            keyword,
                            1
                        )
                        retryQuery = null
                        updateState(State.DONE)
                        callback.onResult(response.data.data, null, 2)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    updateState(State.ERROR)

                }


            }

        }


    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
        updateState(State.LOADING)
        retryQuery = { loadAfter(params, callback) }
        user()?.let {
            scope.launch(getJobErrorHandler() + supervisorJob) {
                withContext(io()) {
                    updateState(State.LOADING)
                    val response =   appApi.getProductSalonByCategory(
                        it.getBearToken(),
                        salonId,
                        catId,
                        keyword,
                        params.key
                    )
                    retryQuery = null
                    updateState(State.DONE)
                    callback.onResult(response.data.data, params.key + 1)
                }
            }

        }

    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Product>) {
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }


    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        updateState(State.ERROR)
    }

    override fun invalidate() {
        super.invalidate()
        supervisorJob.cancelChildren()
    }

    fun refresh() =
        this.invalidate()

    fun retryFailedQuery() {
        val prevQuery = retryQuery
        retryQuery = null
        prevQuery?.invoke()
    }
}