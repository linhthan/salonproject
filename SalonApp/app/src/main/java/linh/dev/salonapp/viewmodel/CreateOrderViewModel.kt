package linh.dev.salonapp.viewmodel

import androidx.lifecycle.viewModelScope
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import linh.dev.base.BaseViewModel
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.R
import linh.dev.salonapp.ext.*
import linh.dev.salonapp.model.local.Product
import linh.dev.salonapp.model.remote.OrderParams
import linh.dev.salonapp.repository.ProductRepository
import kotlin.math.min

class CreateOrderViewModel(
    productCartJson: String,
    val salonId: Int,
    val repository: ProductRepository
) :
    BaseViewModel() {
    val listProductSelected =
        Gson().fromJson<List<Product>>(productCartJson, object : TypeToken<List<Product>>() {}.type)
    val createOrderResult = repository.createOrderResult

    init {
        processProductSelected()
    }

    fun createOrder(
        customerName: String,
        phoneNumber: String,
        address: String,
        note: String?,
        listProduct: ArrayList<Product>
    ) {


        user()?.let {
            viewModelScope.launch {
                withContext(io()) {
                    val jsonObjects =
                        OrderParams(
                            salonId,
                            it.id,
                            customerName,
                            phoneNumber,
                            address,
                            note,
                            listProduct
                        )
                    repository.createOrder(it.getBearToken(), jsonObjects)
                }
            }
        }
    }

    private fun processProductSelected() {
        listProductSelected.forEach {
            it.selectedQuantity = min(1, it.quantity)
        }
    }


}