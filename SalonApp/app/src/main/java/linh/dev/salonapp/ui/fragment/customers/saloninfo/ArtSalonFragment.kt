package linh.dev.salonapp.ui.fragment.customers.saloninfo

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_art_salon.*
import linh.dev.base.BaseFragment
import linh.dev.base.ext.view.showMsg
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.HairStyleAllSalonAdapter
import linh.dev.salonapp.adapter.PhotoCollectAdapter
import linh.dev.salonapp.databases.model.HairStyle
import linh.dev.salonapp.databinding.FragmentArtSalonBinding
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.ui.custom.SpaceItemGridDecoration
import linh.dev.salonapp.ui.dialog.PreviewImageDialog
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.viewmodel.SalonArtViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class ArtSalonFragment : BaseFragment<FragmentArtSalonBinding>() {
    var salonId: Int? = null
    val viewModel: SalonArtViewModel by viewModel { parametersOf(salonId) }
    private val artSalonAdapter: PhotoCollectAdapter by inject()
    private val hairStyleAdapter: HairStyleAllSalonAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_art_salon
    }

    companion object {
        fun newInstance(salonId: Int): ArtSalonFragment {
            val fragment = ArtSalonFragment()
            fragment.salonId = salonId
            return fragment
        }
    }

    override fun initView() {
        super.initView()
        initRecyclerView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initObservable()
    }

    private fun initRecyclerView() {
        rcvArt.layoutManager =
            object : LinearLayoutManager(context, HORIZONTAL, false) {
                override fun canScrollVertically(): Boolean {
                    return false
                }
            }
        rcvArt.addItemDecoration(SpaceItemDecoration(10, 10, 10, 10, 0))
        rcvHairStyle.layoutManager = GridLayoutManager(context, 3)
        rcvHairStyle.addItemDecoration(SpaceItemGridDecoration(10, 10, 10, 10))
        rcvArt.adapter = artSalonAdapter
        rcvHairStyle.adapter = hairStyleAdapter
        hairStyleAdapter.setItemClick(object : HairStyleAllSalonAdapter.OnItemClick {
            override fun onItemClick(position: Int) {
                val item = hairStyleAdapter.getItemByPosition(position)
                item?.let {
                    item.viewCount +=1
                    hairStyleAdapter.notifyItemChanged(position)
                    val bundle = Bundle()
                    bundle.putInt(BUNDLE_ID_HAIR, it.id)
                    Navigation.findNavController(requireActivity(), R.id.nav_main)
                        .navigate(
                            R.id.action_salonDetailFragment_to_fragmentDetailHairStyle,
                            bundle
                        )
                }

            }

            override fun onViewClick(resId: Int, position: Int) {
            }

        })
    }

    private fun initObservable() {

        viewModel.hairStyleList.observe(this, Observer {
            hairStyleAdapter.submitList(it)

        })
        tvPreviewHairStyle.setOnClickListener {
            salonId?.let {
                val bundle = Bundle()
                val hairStyle = hairStyleAdapter.getCurrentData()
                val dataPass = Gson().toJson(hairStyle)
                bundle.putInt(BUNDLE_ID_SALON, it)
                bundle.putString(BUNDLE_LIST_HAIR_STYLE, dataPass)
                Navigation.findNavController(requireActivity(), R.id.nav_main)
                    .navigate(
                        R.id.action_salonDetailFragment_to_hairStylePreviewTabFragment,
                        bundle
                    )
            }


        }
        viewModel.topArtOfSalonResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressNewLoading.show()
                }
                Result.Status.SUCCESS -> {
                    progressNewLoading.hide()
                    if (it.data != null && it.data.success) {

                        artSalonAdapter.setItems(it.data.data)

                    }
                }
                Result.Status.ERROR -> {
                    progressNewLoading.hide()
                    showMsg(it.message)
                }
                Result.Status.UNAUTHORIZED -> {
                    progressNewLoading.hide()
                    if (PrefUtil.removeSharePreference(
                            SalonApplication.applicationContext(),
                            PREF_USER
                        )
                    ) {
                        activity?.let { context ->
                            startActivity(Intent(context, MainActivity::class.java))
                            context.finish()
                        }
                    }
                }
            }
        })
        viewModel.getState().observe(viewLifecycleOwner, Observer { state ->
            if (state == State.LOADING) {
            }
            if (viewModel.listIsEmpty() && state == State.LOADING) {
                progressHairStyle.show()
            } else {
                progressHairStyle.hide()
            }
            if (viewModel.listIsEmpty() && state == State.ERROR) {
                progressHairStyle.show()
            } else {
                progressHairStyle.hide()
            }
            if (!viewModel.listIsEmpty()) {
                hairStyleAdapter.setState(state ?: State.DONE)
            }
        })

    }

    @Subscribe
    fun onAppAction(appAction: AppAction) {
        if (appAction.event == AppEvent.EVENT_DOWNLOAD_IMAGE_SUCCESS) {
            val data = appAction.event.getData(HairStyle::class.java)
            data?.let {
                viewModel.hairStyleList.value?.forEachIndexed { index, item ->
                    if (item.id == it.idHair) {
                        item.downloadCount += 1
                        hairStyleAdapter.notifyItemChanged(index)
                    }
                }
            }
        }
    }


}