package linh.dev.salonapp.ui.fragment.customers.booking

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import cn.refactor.lib.colordialog.PromptDialog
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_book_appointment.*
import linh.dev.base.BaseFragment
import linh.dev.base.BaseRecycleViewAdapter
import linh.dev.base.ext.view.showMsg
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.*
import linh.dev.salonapp.adapter.SlotAdapter
import linh.dev.salonapp.adapter.StylistAdapter
import linh.dev.salonapp.databinding.FragmentBookAppointmentBinding
import linh.dev.salonapp.ext.logE
import linh.dev.salonapp.listenters.OnActionConfirmBookingCallback
import linh.dev.salonapp.model.local.ProductCategory
import linh.dev.salonapp.model.local.Service
import linh.dev.salonapp.network.Result
import linh.dev.salonapp.notification.AlarmBookingManager
import linh.dev.salonapp.ui.custom.SpaceItemDecoration
import linh.dev.salonapp.ui.dialog.BottomSheetBooking
import linh.dev.salonapp.util.DateTimeUtils
import linh.dev.salonapp.util.PrefUtil
import linh.dev.salonapp.util.ServiceUtil
import linh.dev.salonapp.viewmodel.BookingViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.*

class BookingFragment : BaseFragment<FragmentBookAppointmentBinding>() {

    private val args: BookingFragmentArgs by navArgs()
    private val viewModel: BookingViewModel by viewModel {
        parametersOf(
            args.salonId,
            ServiceUtil.getDurationFromJsonService(args.serviceList)
        )
    }
    private var colorDialog: PromptDialog? = null
    private var errorDialog: PromptDialog? = null
    private val stylistAdapter: StylistAdapter by inject()
    private val slotAdapter: SlotAdapter by inject()
    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_book_appointment
    }

    override fun initView() {
        super.initView()
        initTabLayout()
        initRecycleViewStylist()
        initActionView()
        initDialog()
    }

    private fun initActionView() {
        btnNext.setOnClickListener {
            val position = tabDayBooking.selectedTabPosition
            val itemSelected = slotAdapter.getItemSelected()
            itemSelected?.let {
                val dialog = BottomSheetBooking.newInstance(
                    ServiceUtil.convertStringToListService(args.serviceList),
                    args.salonName,
                    args.detailLocation,
                    itemSelected.time,
                    DateTimeUtils.getDateSelectedString(position)
                )
                dialog.setOnActionConfirmBooking(object : OnActionConfirmBookingCallback {
                    override fun onConfirmClick(
                        startTime: Int,
                        services: List<Service.ServiceJson>
                    ) {
                        dialog.dismiss()
                        viewModel.applyBooking(
                            args.salonId,
                            services,
                            startTime,
                            DateTimeUtils.getDateSelectedString(position),
                            args.duration
                        )
                    }

                })
                dialog.show(childFragmentManager, dialog.tag)
            }

        }
    }

    private fun initDialog() {
        context?.let {
            if (colorDialog == null) {
                colorDialog = PromptDialog(it)
                colorDialog?.dialogType = PromptDialog.DIALOG_TYPE_SUCCESS
                colorDialog?.setAnimationEnable(true)
                colorDialog?.contentTextView?.gravity = Gravity.CENTER
                colorDialog?.setTitleText(getString(R.string.success_dialog_title))
                colorDialog?.setPositiveListener(getString(R.string.dialog_ok)) {
                    it.dismiss()
                }
                colorDialog?.setOnDismissListener {
                    val bundle = Bundle()
                    bundle.putInt(BUNDLE_HOME_TAB, 1)
                    Navigation.findNavController(requireActivity(), R.id.nav_main)
                        .navigate(R.id.action_bookingFragment_to_customerHomeFragment, bundle)
                }


            }
            if (errorDialog == null) {
                errorDialog = PromptDialog(it)
                errorDialog?.dialogType = PromptDialog.DIALOG_TYPE_WRONG
                errorDialog?.contentTextView?.gravity = Gravity.CENTER
                errorDialog?.setAnimationEnable(true)
                errorDialog?.setTitleText(getString(R.string.error_dialog_title))
                errorDialog?.setPositiveListener(getString(R.string.dialog_ok)) {
                    it.dismiss()
                }

            }

        }

    }

    private fun initTabLayout() {
        tabDayBooking.removeAllTabs()
        for (i in (0..6)) {
            val tabData = DateTimeUtils.getTabDataByPosition(i)
            val tab = tabDayBooking.newTab().setCustomView(R.layout.view_item_day_booking)
            tab.customView?.findViewById<TextView>(R.id.tvDayOfMonth)?.text = tabData.dayOfMonth
            tab.customView?.findViewById<TextView>(R.id.tvDayOfWeek)?.text = tabData.dayOfWeek
            tabDayBooking.addTab(tab)
        }

        tabDayBooking.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab?.customView?.findViewById<TextView>(R.id.tvDayOfMonth)
                    ?.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
                tab?.customView?.findViewById<TextView>(R.id.tvDayOfWeek)
                    ?.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                logE("Selected tab")
                tab?.customView?.findViewById<TextView>(R.id.tvDayOfMonth)
                    ?.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                tab?.customView?.findViewById<TextView>(R.id.tvDayOfWeek)
                    ?.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                val position = tabDayBooking.selectedTabPosition
                val dayOfWeek = DateTimeUtils.getDayOfWeekSelected(position)
                val stylistSelected = stylistAdapter.getItemSelected()
                stylistAdapter.resetSelected()
                viewModel.loadAllBooking(
                    args.salonId,
                    ServiceUtil.getDurationFromJsonService(args.serviceList),
                    dayOfWeek,
                    DateTimeUtils.getDateSelectedString(position)
                )
//                stylistSelected?.let {
//                    if (it.isDefaultStylist()) {
//                        viewModel.loadAllBooking(
//                            args.salonId,
//                            ServiceUtil.getDurationFromJsonService(args.serviceList),
//                            dayOfWeek,
//                            DateTimeUtils.getDateSelectedString(position)
//                        )
//                    } else {
//                        logE("Load by stylist id ")
//                        viewModel.loadStylistAvailable(args.salonId, dayOfWeek)
//                        viewModel.loadBookingByStylist(
//                            args.salonId,
//                            it.id,
//                            ServiceUtil.getDurationFromJsonService(args.serviceList),
//                            DateTimeUtils.getDateSelectedString(position)
//                        )
//                    }



            }

        })
        val tab = tabDayBooking.getTabAt(0)
        tab?.let {
            it.select()
            it.customView?.findViewById<TextView>(R.id.tvDayOfMonth)
                ?.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            it.customView?.findViewById<TextView>(R.id.tvDayOfWeek)
                ?.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        }

    }

    private fun initRecycleViewStylist() {
        rcvStylist.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rcvStylist.addItemDecoration(SpaceItemDecoration(10, 10, 20, 10))
        rcvStylist.adapter = stylistAdapter
        (rcvStylist.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        rcvSlot.layoutManager = GridLayoutManager(context, 4, GridLayoutManager.HORIZONTAL, false)
        rcvSlot.adapter = slotAdapter
        rcvSlot.addItemDecoration(SpaceItemDecoration(0, 10, 20, 10))
        (rcvSlot.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

        viewModel.confirmBookingStatus.observe(viewLifecycleOwner, Observer {
            if (it) {
                //TODO show appointment tab
                val bundle = Bundle()
                bundle.putInt(BUNDLE_HOME_TAB, 1)
                Navigation.findNavController(requireActivity(), R.id.nav_main)
                    .navigate(R.id.action_bookingFragment_to_customerHomeFragment, bundle)
                bus.post(AppAction(AppEvent.EVENT_BOOKING_SUCCESS))
            }
        })


        viewModel.dataBookingSlot.observe(viewLifecycleOwner, Observer {
            slotAdapter.setItems(it)
            viewModel.slotSelected.value = slotAdapter.getSelectedPosition()
        })
        stylistAdapter.setOnViewClickListener(object : BaseRecycleViewAdapter.ViewClickListener {
            override fun onViewItemClock(resId: Int, position: Int) {
                if (resId == R.id.imvStylist) {
                    val itemClick = stylistAdapter.getItem(position)
                    val positionTabSelected = tabDayBooking.selectedTabPosition
                    viewModel.loadBookingByStylist(
                        args.salonId,
                        itemClick.id,
                        ServiceUtil.getDurationFromJsonService(args.serviceList),
                        DateTimeUtils.getDateSelectedString(positionTabSelected)
                    )
                }
            }

        })

        slotAdapter.setOnViewClickListener(object : BaseRecycleViewAdapter.ViewClickListener {
            override fun onViewItemClock(resId: Int, position: Int) {
                if (resId == R.id.layoutSlot) {
                    val item = slotAdapter.getItem(position)
                    if (item.isAvailable()) {
                        slotAdapter.onSlotSelected(position)
                        viewModel.slotSelected.value = slotAdapter.getSelectedPosition()
                    } else {
                        showToast("Thời gian này đã hết chỗ, vui lòng chọn thời gian khác!")
                    }
                }
            }

        })
        viewModel.confirmBookingResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    loadingConfirmBooking.show()
                }
                Result.Status.SUCCESS -> {
                    loadingConfirmBooking.hide()
                    if (it.data != null) {
                        if (it.data.success) {
                            colorDialog?.setContentText(it.data.message)
                            colorDialog?.show()
                            val data = it.data.data
                            AlarmBookingManager.setAlarmBooking(SalonApplication.applicationContext(),data)


                        } else {
                            errorDialog?.setContentText(it.data.message)
                            errorDialog?.show()
                        }

                    }
                }
                Result.Status.ERROR -> {
                    loadingConfirmBooking.hide()
                    errorDialog?.setContentText(it.message)
                    errorDialog?.show()
                }
                Result.Status.UNAUTHORIZED -> {
                    loadingConfirmBooking.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })

        viewModel.salonSlotResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    loadingSlot.show()
                }
                Result.Status.SUCCESS -> {
                    loadingSlot.hide()
                    if (it.data != null) {
                        slotAdapter.setItems(it.data.data)
                    }
                }
                Result.Status.ERROR -> {
                    loadingSlot.hide()
                }
                Result.Status.UNAUTHORIZED -> {
                    loadingSlot.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })

        viewModel.stylistResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Result.Status.LOADING -> {
                    progressLoadStylist.show()
                }
                Result.Status.SUCCESS -> {
                    progressLoadStylist.hide()
                    if (it.data != null) {
                        stylistAdapter.setItems(it.data.data)
                    }
                }
                Result.Status.ERROR -> {
                    progressLoadStylist.hide()
                }
                Result.Status.UNAUTHORIZED -> {
                    progressLoadStylist.hide()
                    PrefUtil.removeSharePreference(requireContext(), PREF_USER)
                    activity?.let { context ->
                        startActivity(Intent(context, MainActivity::class.java))
                        context.finish()
                    }
                }
            }
        })
//        viewModel.progressLoadStylist.observe(viewLifecycleOwner, Observer {
//            if (it) {
//                progressLoadStylist.visibility = View.VISIBLE
//            } else {
//                progressLoadStylist.visibility = View.GONE
//            }
//        })
//        viewModel.progressLoadSlot.observe(viewLifecycleOwner, Observer {
//            if (it) {
//                loadingSlot.visibility = View.VISIBLE
//            } else {
//                loadingSlot.visibility = View.GONE
//            }
//        })
        viewModel.slotSelected.observe(viewLifecycleOwner, Observer {
            if (it == -1) {
                btnNext.visibility = View.GONE
            } else {
                btnNext.visibility = View.VISIBLE
            }
        })
    }
}