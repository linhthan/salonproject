package linh.dev.salonapp.model.remote

data class SimpleResponse<T>(val success: Boolean, val message: String, val data: T)