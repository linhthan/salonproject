package linh.dev.salonapp.ui.fragment

import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_sale_slide.*
import linh.dev.base.BaseFragment
import linh.dev.salonapp.R
import linh.dev.salonapp.databinding.FragmentSaleSlideBinding
import linh.dev.salonapp.model.local.Sale
import linh.dev.salonapp.model.remote.SaleResponse

class SaleSlideFragment : BaseFragment<FragmentSaleSlideBinding>() {
    private var sale: Sale? = null

    companion object {
        fun newInstance(sale: Sale): SaleSlideFragment {
            val saleSlideFragment = SaleSlideFragment()
            saleSlideFragment.sale = sale
            return saleSlideFragment
        }
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.fragment_sale_slide
    }

    override fun initView() {
        super.initView()
        context?.let {
            Glide.with(it).load(sale?.banner_url).into(imvSlide)
        }

    }
}