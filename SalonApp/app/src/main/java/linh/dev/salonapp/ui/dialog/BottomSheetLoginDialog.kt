package linh.dev.salonapp.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.util.Patterns.EMAIL_ADDRESS
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_sign_in.*
import linh.dev.base.ext.view.showToast
import linh.dev.salonapp.R
import linh.dev.salonapp.ext.isContainSpecialCharacter
import linh.dev.salonapp.listenters.OnActionSignInCallback
import java.util.regex.Pattern


class BottomSheetLoginDialog : BottomSheetDialogFragment() {
    private var specialCharacterRegex = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!-]")

    private var onActionSignInCallback: OnActionSignInCallback? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.bottom_sheet_sign_in, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnLogin.setOnClickListener {
            val email = edtEmail.text.toString()
            val password = edtPassword.text.toString()
            if (isValidEmail(email) && isValidPassword(password)) {
                onActionSignInCallback?.login(email, password)
                dialog?.dismiss()
            }
        }
        tvSignUp.setOnClickListener {
            dialog?.dismiss()
            onActionSignInCallback?.signUpClick()

        }

    }

    fun setOnActionSignInCallback(onActionSignInCallback: OnActionSignInCallback) {
        this.onActionSignInCallback = onActionSignInCallback
    }

    private fun isValidEmail(email: String): Boolean {
        if (email.isBlank()) {
            showToast(getString(R.string.email_blank_error_msg))
            return false
        } else if (!EMAIL_ADDRESS.matcher(email).matches()) {
            showToast(getString(R.string.email_invalid_error_msg))
            return false
        }
        return true
    }

    private fun isValidPassword(password: String): Boolean {
        if (password.isBlank()) {
            showToast(getString(R.string.password_blank_error_msg))
            return false
        } else if (password.length < 6) {
            showToast(getString(R.string.password_too_short_error_msg))
            return false
        } else if (password.isContainSpecialCharacter()) {
            showToast(getString(R.string.password_contain_special_character_error_msg))
            return false
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetLogin)
    }


    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    companion object {
        fun newInstance() = BottomSheetLoginDialog()
    }

}