package linh.dev.salonapp.databases

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import linh.dev.salonapp.databases.model.HairStyle

@Dao
interface HairStyleDao {
    @Insert(onConflict = REPLACE)
    fun insertHairStyle(hairStyle: HairStyle)

    @Query("SELECT * FROM hairstyle WHERE salon_id = :salonId")
    fun getAllHairStyleBySalonId(salonId: Int): LiveData<List<HairStyle>>

    @Query("SELECT * FROM hairstyle WHERE id = :hairId")
    fun getAllHairStyleById(hairId: Int): LiveData<HairStyle>

    @Query("SELECT * FROM hairstyle order by salon_id asc")
    fun getAllHairStyle(): LiveData<List<HairStyle>>
    @Query("SELECT DISTINCT salon_id FROM hairstyle")
    fun getAllSalonId(): LiveData<List<Int>>

}