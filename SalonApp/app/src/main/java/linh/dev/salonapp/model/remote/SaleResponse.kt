package linh.dev.salonapp.model.remote

import linh.dev.salonapp.model.local.Sale

data class SaleResponse(val message: String, val data: List<Sale>)